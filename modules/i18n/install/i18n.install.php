<?php

$modules_array = registry::get('modules_array');
foreach ($modules_array AS $module_dir=>$module_title) {
	if (is_file(MODULES_PATH . $module_dir . '/i18n_items_list.php')) {
		require(MODULES_PATH . $module_dir . '/i18n_items_list.php');
	}
}
registry::set('i18n_fields', $i18n_fields);

$system_settings = registry::get('system_settings');

$CI = &get_instance();
$CI->load->model('languages', 'i18n');
$CI->languages->saveLanguage(
	array(
			'name'=>'English',
			'code'=>'en',
			'db_code'=>'en',
			'flag'=>'flag_usa.png',
			'custom_theme'=>$system_settings['design_theme'],
			'decimals_separator'=>'.',
			'thousands_separator'=>'',
			'date_format'=>'%m/%d/%y',
			'time_format'=>'%H:%M:%S'
	)
);
?>