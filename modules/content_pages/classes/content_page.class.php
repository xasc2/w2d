<?php

class content_page
{
	public $id;
	public $url;
	public $title;
	public $meta_title;
	public $meta_description;
	public $in_top_menu = 1;
	public $in_bottom_menu = 1;
	public $order_num;
	public $creation_date;
	public $input_params;
	public $php_code;
	public $output_params;
	public $template_file;
	
	public $content_fields;
	
	public function __construct($node_id = null)
	{
		if (empty($node_id)) {
			$this->id = 'new';
		}
		$this->content_fields = new contentFields(CONTENT_PAGES_GROUP_CUSTOM_NAME, 0, $node_id);
	}
	
	public function setPageFromArray($node_array)
    {
        $this->id = $node_array['id'];
		$this->url = $node_array['url'];
		$this->title = $node_array['title'];
		$this->meta_title = $node_array['meta_title'];
		$this->meta_description = $node_array['meta_description'];
		$this->in_top_menu = $node_array['in_top_menu'];
		$this->in_bottom_menu = $node_array['in_bottom_menu'];
		if (isset($node_array['order_num']))
        	$this->order_num = $node_array['order_num'];
		$this->creation_date = $node_array['creation_date'];
		$this->input_params = $node_array['input_params'];
		$this->php_code = $node_array['php_code'];
		$this->output_params = $node_array['output_params'];
		$this->template_file = $node_array['template_file'];
		
		$this->content_fields->select();
    }
    
    public function inputMode()
    {
    	return $this->content_fields->inputMode();
    }
    
    public function outputMode()
    {
    	// func_get_arg() does not appear to be allowed to be used as a function argument itself
    	$args = func_get_args();
    	return call_user_func_array(array($this->content_fields, 'outputMode'), $args);
    }
    
    public function validateFields($form)
    {
    	$this->content_fields->validate($form);
    }
    
    public function getPageFromForm($form)
	{
		if (isset($form['id']))
			$this->id = $form['id'];
		$this->url = $form['url'];
		$this->title = $form['title'];
		$this->meta_title = $form['meta_title'];
		$this->meta_description = $form['meta_description'];
		$this->in_top_menu = $form['in_top_menu'];
		$this->in_bottom_menu = $form['in_bottom_menu'];
		$this->input_params = $form['input_params'];
		$this->php_code = $form['php_code'];
		$this->output_params = $form['output_params'];
		$this->template_file = $form['template_file'];
		
		$this->content_fields->getValuesFromForm($form);
	}
	
	public function saveFields($node_id, $form)
	{
		$this->content_fields->setObjectId($node_id);
		return $this->content_fields->save($form);
	}
	
	public function updateFields($form)
	{
		$this->content_fields->select();
		return $this->content_fields->update($form);
	}
	
	public function deleteFields()
	{
		$this->content_fields->select();
		return $this->content_fields->delete();
	}
	
	public function execute()
	{
		if ($this->php_code || $this->template_file) {
			$CI = &get_instance();
			
			$execution_code = "\$CI = &get_instance();\n";
			
			if ($this->input_params) {
				$input_params = explode('
', $this->input_params);
				$args = func_get_args();
				foreach ($args AS $key=>$arg) {
					$execution_code .= "\$" . trim($input_params[$key], "\$") . " = '" . $CI->input->_clean_input_data($arg) . "';\n";
				}
			}

			if ($this->php_code)
				$execution_code .= $this->php_code . "\n\n";
			
			if ($this->template_file) {
				$execution_code .= "\$view = \$CI->load->view();\n";
				if ($this->output_params) {
					$output_params = explode('
', $this->output_params);
					foreach ($output_params AS $param) {
						$execution_code .= "\$view->assign('" . trim($param, "\$") . "', \$" . trim($param, "\$") . ");\n";						
					}
				}
				$execution_code .= "\$view->display('frontend/" . $this->template_file . "');";
			}

			eval($execution_code);
		}
	}
}
?>