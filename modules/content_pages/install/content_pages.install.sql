CREATE TABLE IF NOT EXISTS `content_pages` (
  `id` int(11) NOT NULL auto_increment,
  `order_num` int(11) NOT NULL,
  `url` varchar(255) default NULL,
  `title` varchar(255) default NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `in_top_menu` tinyint(1) NOT NULL default '1',
  `in_bottom_menu` tinyint(1) NOT NULL default '1',
  `creation_date` datetime default NULL,
  `input_params` text NOT NULL,
  `php_code` text NOT NULL,
  `output_params` text NOT NULL,
  `template_file` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `url` (`url`),
  KEY `order_num` (`order_num`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `content_pages` (`order_num`, `url`, `title`, `meta_title`, `meta_description`, `in_top_menu`, `in_bottom_menu`, `creation_date`, `input_params`, `php_code`, `output_params`, `template_file`) VALUES
(1, 'node/about', 'About us', '', '', 1, 0, '2009-07-10 00:58:28', '', '', '', ''),
(2, 'node/terms_conditions', 'Terms & Conditions', '', '', 0, 0, '2012-10-05 13:07:19', '', '', '', '');