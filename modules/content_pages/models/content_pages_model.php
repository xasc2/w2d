<?php
include_once(MODULES_PATH . 'content_pages/classes/content_page.class.php');

class content_pagesModel extends model
{
	private $_node_id;
	
	public function setNodeId($node_id)
	{
		$this->_node_id = $node_id;
	}
	
	public function getNodeIdByUrl($url)
	{
		$this->db->select('id');
		$this->db->from('content_pages');
		$this->db->where('url', $url);
		$this->db->or_where('url', 'node/' . $url);
		$query = $this->db->get();
		if ($query->num_rows()) {
			$row = $query->row_array();
			return $row['id'];
		} else 
			return false;
	}
	
	public function getNodeById()
	{
		$this->db->select();
		$this->db->from('content_pages');
		$this->db->where('id', $this->_node_id);
		$row = $this->db->get()->row_array();

		$node = new content_page($row['id']);
    	$node->setPageFromArray($row);
    	return $node;
	}
	
	/**
     * is there content page with such url in the DB? or in reserved url parts?
     *
     * @param string $node_url
     */
    public function is_unique_node($node_url)
    {
    	$this->db->select();
		$this->db->from('content_pages');
		$this->db->where('url', $node_url);
		if (!is_null($this->_node_id)) {
			$this->db->where('id !=', $this->_node_id);
		}
		$query = $this->db->get();

		if ($query->num_rows())
			return FALSE;
		else {
			$node_url_parts = explode('/', $node_url);
			$reserved_url_parts = explode('|', RESERVED_URL_PARTS);
			
			if (in_array($node_url_parts[0], $reserved_url_parts))
				return FALSE;
			else
				return TRUE;
		}
    }
	
	public function selectAllNodes()
    {
	    $this->db->select();
	    $this->db->from('content_pages');
	    $this->db->order_by('order_num');
    	$result = $this->db->get()->result_array();
    	
    	$nodes = array();
    	foreach ($result AS $row) {
    		$node = new content_page($row['id']);
    		$node->setPageFromArray($row);
    		$nodes[] = $node;
    	}
    	return $nodes;
    }
    
	public function selectAllFrontendNodes()
    {
	    $this->db->select();
	    $this->db->from('content_pages');
	    $this->db->order_by('order_num');
	    $this->db->or_where('in_top_menu', 1);
		$this->db->or_where('in_bottom_menu', 1);
    	$result = $this->db->get()->result_array();
    	
    	$nodes = array();
    	foreach ($result AS $row) {
    		$node = new content_page($row['id']);
    		$node->setPageFromArray($row);
    		$nodes[] = $node;
    	}
    	return $nodes;
    }
    
    /**
     * saves the order of pages by its weight
     *
     * @param string $serialized_order
     */
    public function setPagesOrder($serialized_order)
    {
    	$a = explode("=", $serialized_order);
    	$start = 1;
    	foreach ($a AS $row) {
    		$b = explode("&", $row);
    		foreach ($b AS $id) {
    			$id = trim($id, "_id");
    			if (is_numeric($id)) {
    				$this->db->set('order_num', $start++);
    				$this->db->where('id', $id);
    				$this->db->update('content_pages');
    			}
    		}
    	}
    }
    
    public function savePage($form)
    {
    	$this->db->select_max('order_num');
    	$query = $this->db->get('content_pages');
    	if ($row = $query->row())
    		$order_num = $row->order_num + 1;
    	else 
    		$order_num = 1;

		$this->db->set('url', $form['url']);
		$this->db->set('title', $form['title']);
		$this->db->set('meta_title', $form['meta_title']);
		$this->db->set('meta_description', $form['meta_description']);
		$this->db->set('in_top_menu', $form['in_top_menu']);
		$this->db->set('in_bottom_menu', $form['in_bottom_menu']);
		$this->db->set('creation_date', date("Y-m-d H:i:s"));
		$this->db->set('input_params', $form['input_params']);
		$this->db->set('php_code', $form['php_code']);
		$this->db->set('output_params', $form['output_params']);
		$this->db->set('template_file', $form['template_file']);
		$this->db->set('order_num', $order_num);
        
		if ($this->db->insert('content_pages')) {
    		$content_page_id = $this->db->insert_id();
    		
    		$system_settings = registry::get('system_settings');
        	if (isset($system_settings['language_areas_enabled']) && $system_settings['language_areas_enabled']) {
        		translations::saveTranslations(array('content_pages', 'title', $content_page_id));
        		translations::saveTranslations(array('content_pages', 'meta_title', $content_page_id));
        		translations::saveTranslations(array('content_pages', 'meta_description', $content_page_id));
        	}
        	return $content_page_id;
		}
		return false;
    }
    
    public function savePageById($form)
    {
		$this->db->set('url', $form['url']);
		$this->db->set('title', $form['title']);
		$this->db->set('meta_title', $form['meta_title']);
		$this->db->set('meta_description', $form['meta_description']);
		$this->db->set('in_top_menu', $form['in_top_menu']);
		$this->db->set('in_bottom_menu', $form['in_bottom_menu']);
		$this->db->set('input_params', $form['input_params']);
		$this->db->set('php_code', $form['php_code']);
		$this->db->set('output_params', $form['output_params']);
		$this->db->set('template_file', $form['template_file']);
		$this->db->where('id', $this->_node_id);
		return $this->db->update('content_pages');
    }
    
    public function deletePageById()
    {
		return $this->db->delete('content_pages', array('id' => $this->_node_id));
    }
    
    /**
	 * select content pages with in_menu flag
	 *
	 * @return array
	 */
	public function getContentPagesForFront($bottom = false)
	{
		$this->db->select('url');
		$this->db->select('title');
		$this->db->from('content_pages');
		if (!$bottom)
			$this->db->where('in_top_menu', 1);
		else 
			$this->db->where('in_bottom_menu', 1);
		$this->db->order_by('order_num');
		$query = $this->db->get();
		
		return $query->result_array();
	}
}
?>