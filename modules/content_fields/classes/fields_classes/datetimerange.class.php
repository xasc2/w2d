<?php
class datetimerangeClass extends processFieldValue
{
	public $name = 'Date Time range';
	public $configuration = true;
	public $search_configuration = false;
	public $order_option = true;
	
	public static $instance_count = 0;
	
	public function __construct($field_array = null)
	{
		parent::__construct($field_array);
		self::$instance_count++;

		$themes_roller = registry::get('themes_roller');
		if ($themes_roller->theme() != 'mobile' && self::$instance_count == 1) {
			$view = new view;
			$view->addJsFile('ui/jquery-ui-timepicker-addon.min.js');
			$view->addJsFile('ui/fullcalendar.min.js');
			$view->addCssFile('ui/fullcalendar.css');
		}
	}

	/**
     * Overload of processFieldValue::select($custom_group_name, $object_id)
     *
     */
	public function select($custom_group_name, $object_id)
    {
    	$this->field->object_id = $object_id;

    	$CI = &get_instance();
    	$CI->load->model('content_fields', 'content_fields');
    	
    	$this->field->value = array();
    	
    	$CI->content_fields->db->select();
    	$CI->content_fields->db->from('content_fields_type_' . $this->field->type . '_data');
    	$CI->content_fields->db->where('field_id', $this->field->id);
    	$CI->content_fields->db->where('custom_name', $custom_group_name);
    	$CI->content_fields->db->where('object_id', $object_id);
    	$query = $CI->content_fields->db->get();
    	if ($query->num_rows()) {
    		$this->field->field_value_id = 1;
    		$result = $query->result_array();

    		$dates_array = array();
    		foreach ($result AS $row) {
    			$dates_array[] = array(
    				'id' => $row['id'],
    				'title' => $row['title'],
    				'description' => $row['description'],
    				'start_date' => strtotime($row['from_field_value']),
    				'start' => strtotime($row['from_field_value']),
    				'end_date' => strtotime($row['to_field_value']),
    				'end' => strtotime($row['to_field_value']),
    				'cycle_days_monday' => $row['cycle_days_monday'],
    				'cycle_days_tuesday' => $row['cycle_days_tuesday'],
    				'cycle_days_wednesday' => $row['cycle_days_wednesday'],
    				'cycle_days_thursday' => $row['cycle_days_thursday'],
    				'cycle_days_friday' => $row['cycle_days_friday'],
    				'cycle_days_saturday' => $row['cycle_days_saturday'],
    				'cycle_days_sunday' => $row['cycle_days_sunday']
    			);
    		}
    		$this->field->value = $dates_array;
    	}
    }
    
    /**
     * Overload of processFieldValue::save($custom_group_name, $object_id, $form_result)
     *
     */
    public function save($custom_group_name, $object_id, $form_result)
    {
    	$CI = &get_instance();
    	$CI->load->model('content_fields', 'content_fields');
    	
    	$this->field->value = array();
    	
    	$seo_name = $this->field->seo_name;
    	
    	if (!empty($form_result['serialized_events_' . $seo_name])) {
    		if ($events_array = json_decode($form_result['serialized_events_' . $seo_name]))
	    		foreach ($events_array AS $event) {
	    			if ($event->start_date && $event->end_date) {
			    		$CI->content_fields->db->set('field_id', $this->field->id);
		    			$CI->content_fields->db->set('custom_name', $custom_group_name);
		    			$CI->content_fields->db->set('object_id', $object_id);
		    			$CI->content_fields->db->set('title', $event->title);
		    			$CI->content_fields->db->set('description', $event->description);
		    			$CI->content_fields->db->set('from_field_value', date('Y-m-d H:i:s', $event->start_date));
		    			$CI->content_fields->db->set('to_field_value', date('Y-m-d H:i:s', $event->end_date));
		    			$CI->content_fields->db->set('cycle_days_monday', $event->cycle_days_monday);
		    			$CI->content_fields->db->set('cycle_days_tuesday', $event->cycle_days_tuesday);
		    			$CI->content_fields->db->set('cycle_days_wednesday', $event->cycle_days_wednesday);
		    			$CI->content_fields->db->set('cycle_days_thursday', $event->cycle_days_thursday);
		    			$CI->content_fields->db->set('cycle_days_friday', $event->cycle_days_friday);
		    			$CI->content_fields->db->set('cycle_days_saturday', $event->cycle_days_saturday);
		    			$CI->content_fields->db->set('cycle_days_sunday', $event->cycle_days_sunday);
		    			$CI->content_fields->db->insert('content_fields_type_' . $this->field->type . '_data');
	    			}
	    		}
    	}
    	return true;
    }
    
    /**
     * Overload of processFieldValue::update($custom_group_name, $object_id, $form)
     *
     */
    public function update($custom_group_name, $object_id, $form_result)
    {
    	$CI = &get_instance();
    	$CI->load->model('content_fields', 'content_fields');

    	if ($this->field->field_value_id == 'new') {
    		return $this->save($custom_group_name, $object_id, $form_result);
    	} else {
    		$CI->content_fields->db->delete('content_fields_type_' . $this->field->type . '_data', array('field_id' => $this->field->id, 'custom_name' => $custom_group_name, 'object_id' => $object_id));

    		$seo_name = $this->field->seo_name;

    		if (!empty($form_result['serialized_events_' . $seo_name])) {
    			if ($events_array = json_decode($form_result['serialized_events_' . $seo_name]))
	    			foreach ($events_array AS $event) {
	    				if ($event->start_date && $event->end_date) {
		    				$CI->content_fields->db->set('field_id', $this->field->id);
		    				$CI->content_fields->db->set('custom_name', $custom_group_name);
		    				$CI->content_fields->db->set('object_id', $object_id);
		    				$CI->content_fields->db->set('title', $event->title);
		    				$CI->content_fields->db->set('description', $event->description);
		    				$CI->content_fields->db->set('from_field_value', date('Y-m-d H:i:s', $event->start_date));
		    				$CI->content_fields->db->set('to_field_value', date('Y-m-d H:i:s', $event->end_date));
		    				$CI->content_fields->db->set('cycle_days_monday', $event->cycle_days_monday);
		    				$CI->content_fields->db->set('cycle_days_tuesday', $event->cycle_days_tuesday);
		    				$CI->content_fields->db->set('cycle_days_wednesday', $event->cycle_days_wednesday);
		    				$CI->content_fields->db->set('cycle_days_thursday', $event->cycle_days_thursday);
		    				$CI->content_fields->db->set('cycle_days_friday', $event->cycle_days_friday);
		    				$CI->content_fields->db->set('cycle_days_saturday', $event->cycle_days_saturday);
		    				$CI->content_fields->db->set('cycle_days_sunday', $event->cycle_days_sunday);
		    				$CI->content_fields->db->insert('content_fields_type_' . $this->field->type . '_data');
	    				}
	    			}
    		}
    		return true;
    	}
    }
    
    /**
     * Overload of processFieldValue::getValueFromForm($form)
     * 
     * Return field's value after the form was not valid
     *
     * @param object $form
     */
    public function getValueFromForm($form_result)
    {
    	$CI = &get_instance();
    	$CI->load->model('content_fields', 'content_fields');
    	
    	$this->field->value = array();

    	$seo_name = $this->field->seo_name;
    	
    	if (!empty($form_result['serialized_events_' . $seo_name])) {
    		$dates_array = array();
    		$events_array = json_decode(str_replace("'", '"', $form_result['serialized_events_' . $seo_name]));

    		foreach ($events_array AS $object) {
    			$dates_array[] = array(
    				'id' => $object->id,
    				'title' => $object->title,
    				'description' => $object->description,
    				'start_date' => $object->start_date,
    				'end_date' => $object->end_date,
    				'cycle_days_monday' => $object->cycle_days_monday,
    				'cycle_days_tuesday' => $object->cycle_days_tuesday,
    				'cycle_days_wednesday' => $object->cycle_days_wednesday,
    				'cycle_days_thursday' => $object->cycle_days_thursday,
    				'cycle_days_friday' => $object->cycle_days_friday,
    				'cycle_days_saturday' => $object->cycle_days_saturday,
    				'cycle_days_sunday' => $object->cycle_days_sunday
    			);
    		}

    		$this->field->value = $dates_array;
    	}
    }

    public function renderDataEntry()
    {
    	$CI = &get_instance();
    	$CI->load->model('content_fields', 'content_fields');

        $view = $CI->load->view();
        if (!$this->field->value)
        	$this->field->value = array();
        $view->assign('field', $this->field);

        $enable_time = $this->field->options[0]['enable_time'];
        $view->assign('enable_time', $enable_time);

        if ($view->template_exists('content_fields/fields/' . $this->field->type . '/field_input-' . $this->field->seo_name . '.tpl')) {
        	$template = 'content_fields/fields/' . $this->field->type . '/field_input-' . $this->field->seo_name . '.tpl';
        } else {
        	$template = 'content_fields/fields/' . $this->field->type . '/field_input.tpl';
        }
        return $view->fetch($template);
    }
    
    public function renderDataOutput($view_type)
    {
    	$CI = &get_instance();
    	$CI->load->model('content_fields', 'content_fields');
    	
    	$view = $CI->load->view();
        if (!$this->field->value)
        	$this->field->value = array();
        $view->assign('field', $this->field);

        $enable_time = $this->field->options[0]['enable_time'];
        $view->assign('enable_time', $enable_time);

        // Find the most relevant template, according to view type and field seo name
        if ($view->template_exists('content_fields/fields/' . $this->field->type . '/field_output-' . $view_type . '-' . $this->field->seo_name . '.tpl')) {
        	$template = 'content_fields/fields/' . $this->field->type . '/field_output-' . $view_type . '-' . $this->field->seo_name . '.tpl';
        } elseif ($view->template_exists('content_fields/fields/' . $this->field->type . '/field_output-' . $this->field->seo_name . '.tpl')) {
        	$template = 'content_fields/fields/' . $this->field->type . '/field_output-' . $this->field->seo_name . '.tpl';
        } elseif ($view->template_exists('content_fields/fields/' . $this->field->type . '/field_output-' . $view_type . '.tpl')) {
        	$template = 'content_fields/fields/' . $this->field->type . '/field_output-' . $view_type . '.tpl';
        } else {
        	$template = 'content_fields/fields/' . $this->field->type . '/field_output.tpl';
        }

        return $view->fetch($template);
    }
    
    public function getValueAsString($glue = ' - ')
    {
    	$dates_array = $this->getSomeNearestDates();
    	
    	$enable_time = $this->field->options[0]['enable_time'];
    	$format = "%D";
    	if ($enable_time)
    		$format .= " %T";

    	$CI = &get_instance();
    	$view = new view();
    	$view->_get_plugin_filepath('modifier', 'date_format');
    	
    	$output = '';
    	foreach ($dates_array AS $date)
    		$output .= smarty_modifier_date_format($date, $format) . $glue;

    	return $output;
    }

    public function validate($form_validation)
    {
    	$CI = &get_instance();
    	$CI->load->model('content_fields', 'content_fields');
    	
    	if ($this->field->required)
    		$required = '|required';
    	else
    		$required = '';
    	
    	$form_validation->set_rules('serialized_events_' . $this->field->seo_name, LANG_SELECTED_EXACT_DATES . " " . $this->field->name, $required);
    }

    public function configurationPage()
    {
    	$CI = &get_instance();
    	$CI->load->model('content_fields', 'content_fields');

        if ($CI->input->post('submit')) {
            $CI->form_validation->set_rules('id', LANG_FIELD_ID, 'sanitize_string|required');
            $CI->form_validation->set_rules('enable_time', LANG_ENABLE_TIME, 'is_checked');

            if ($CI->form_validation->run() !== FALSE) {
				if ($this->saveOptionsByFieldId($CI->form_validation->result_array())) {
					// Clean cache
					$CI->cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('content_fields'));

					$CI->setSuccess(LANG_FIELD_CONFIG_SAVE_SUCCESS);
					redirect('admin/fields/');
				}
			} else {
				$options[0]['enable_time'] = $CI->form_validation->set_value('enable_time');
			}
        } else {
        	$options = $this->field->options;
        }

        $view  = new view;
        $view->assign('field', $this->field);
        $view->assign('enable_time', $options[0]['enable_time']);
        $view->assign('last_page', $CI->session->userdata('last_page'));
        $view->display('content_fields/fields/' . $this->field->type . '/field_configure.tpl');
	}

	public function saveOptionsByFieldId($result)
	{
		$CI = &get_instance();
		$CI->load->model('content_fields', 'content_fields');

		$CI->content_fields->db->set('field_id', $this->field->id);
		$CI->content_fields->db->set('enable_time', $result['enable_time']);
		return $CI->content_fields->db->on_duplicate_insert('content_fields_type_datetimerange');
	}
	
	public function getDefaultOptions()
	{
		$options[0]['enable_time'] = 0;
		return $options;
	}
	
	
	// --------------------------------------------------------------------------------------------
	// Search methods
	// --------------------------------------------------------------------------------------------
	public function renderSearch($args)
	{
		$CI = &get_instance();
		$CI->load->model('content_fields', 'content_fields');

		$view = $CI->load->view();
		$view->assign('field', $this->field);
		$view->assign('field_index', "search_" . $this->field->seo_name);
		$view->assign('args', $args);
		$mode = 'single';

		if (isset($args["search_" . $this->field->seo_name])) {
			$tmstmp = null;
			// Receives date in unix timestamp and also in 'Y-m-d' formats
			if (is_numeric($args["search_" . $this->field->seo_name]) && (strtotime(date("Y-m-d", $args["search_" . $this->field->seo_name]))) !== FALSE) {
	    		// Timestamp
	    		$tmstmp = $args["search_" . $this->field->seo_name];
	    	} elseif ((strtotime($args["search_" . $this->field->seo_name])) !== FALSE) {
	    		// 'Y-m-d' format
	    		$tmstmp = strtotime($args["search_" . $this->field->seo_name]);
	    	}

	    	if ($tmstmp) {
	    		$view->assign('date', date("Y-m-d", $tmstmp));
	    		$view->assign('date_tmstmp', $tmstmp);
	    	}
		}
		if (isset($args["from_tmstmp_" . $this->field->seo_name])) {
			$tmstmp = null;
			// Receives date in unix timestamp and also in 'Y-m-d' formats
			if (is_numeric($args["from_tmstmp_" . $this->field->seo_name]) && (strtotime(date("Y-m-d", $args["from_tmstmp_" . $this->field->seo_name]))) !== FALSE) {
	    		// Timestamp
	    		$tmstmp = $args["from_tmstmp_" . $this->field->seo_name];
	    	} elseif ((strtotime($args["from_tmstmp_" . $this->field->seo_name])) !== FALSE) {
	    		// 'Y-m-d' format
	    		$tmstmp = strtotime($args["from_tmstmp_" . $this->field->seo_name]);
	    	}
	    	
	    	if ($tmstmp) {
	    		$view->assign('from_date', date("Y-m-d", $tmstmp));
	    		$view->assign('from_date_tmstmp', $tmstmp);
	    	}
			
			$mode = 'range';
		}
		if (isset($args["to_tmstmp_" . $this->field->seo_name])) {
			$tmstmp = null;
			// Receives date in unix timestamp and also in 'Y-m-d' formats
			if (is_numeric($args["to_tmstmp_" . $this->field->seo_name]) && (strtotime(date("Y-m-d", $args["to_tmstmp_" . $this->field->seo_name]))) !== FALSE) {
	    		// Timestamp
	    		$tmstmp = $args["to_tmstmp_" . $this->field->seo_name];
	    	} elseif ((strtotime($args["to_tmstmp_" . $this->field->seo_name])) !== FALSE) {
	    		// 'Y-m-d' format
	    		$tmstmp = strtotime($args["to_tmstmp_" . $this->field->seo_name]);
	    	}
	    	
	    	if ($tmstmp) {
	    		$view->assign('to_date', date("Y-m-d", $tmstmp));
	    		$view->assign('to_date_tmstmp', $tmstmp);
	    	}
			
			$mode = 'range';
		}
		$view->assign('mode', $mode);

		if ($view->template_exists('content_fields/search/' . $this->field->type . '/search_input-' . $this->field->seo_name . '.tpl')) {
			$template = 'content_fields/search/' . $this->field->type . '/search_input-' . $this->field->seo_name . '.tpl';
		} else {
			$template = 'content_fields/search/' . $this->field->type . '/search_input.tpl';
		}
		return $view->fetch($template);
	}
	
	public function validateSearch($fields_group_name, $args, &$search_url_rebuild)
	{
		$CI = &get_instance();
		$CI->load->model('content_fields', 'content_fields');

		if (isset($args["search_" . $this->field->seo_name])) {
			// Receives date in unix timestamp and also in 'Y-m-d' formats
			if (is_numeric($args["search_" . $this->field->seo_name]) && (strtotime(date("Y-m-d", $args["search_" . $this->field->seo_name]))) !== FALSE)
	    		$tmstmp = $args["search_" . $this->field->seo_name];
	    	elseif (($tmstmp = strtotime($args["search_" . $this->field->seo_name])) !== FALSE && strtotime($args["search_" . $this->field->seo_name]) != -1)
	    		$tmstmp = strtotime($args["search_" . $this->field->seo_name]);

	    	$search_url_rebuild .= "search_" . $this->field->seo_name . '/' . $args["search_" . $this->field->seo_name] . '/';
	    	$search_date = date("Y-m-d", $tmstmp);
	    	$search_datetime = date("Y-m-d H:i:s", $tmstmp);

	    	/* $sql = '
		    		SELECT
		    			object_id
		    		FROM
		    			content_fields_type_datetimerange_data
		    		WHERE
				    		(
						    		(
										(WEEKDAY("' . $search_date . '") = 0 AND cycle_days_monday = 1) OR
							  			(WEEKDAY("' . $search_date . '") = 1 AND cycle_days_tuesday = 1) OR
							   			(WEEKDAY("' . $search_date . '") = 2 AND cycle_days_wednesday = 1) OR
							   			(WEEKDAY("' . $search_date . '") = 3 AND cycle_days_thursday = 1) OR
							   			(WEEKDAY("' . $search_date . '") = 4 AND cycle_days_friday = 1) OR
							   			(WEEKDAY("' . $search_date . '") = 5 AND cycle_days_saturday = 1) OR
							   			(WEEKDAY("' . $search_date . '") = 6 AND cycle_days_sunday = 1) OR
							   			(cycle_days_monday=0 AND cycle_days_tuesday=0 AND cycle_days_wednesday=0 AND cycle_days_thursday=0 AND cycle_days_friday=0 AND cycle_days_saturday=0 AND cycle_days_sunday=0)
									)
						    	AND
							   		(TO_DAYS(from_field_value) <= TO_DAYS("' . $search_date . '") AND
							   		TO_DAYS(to_field_value) >= TO_DAYS("' . $search_date . '"))
				    		)
		    			AND 
			    			field_id = ' . $this->field->id . ' AND
			    			custom_name = "' . $fields_group_name . '"
	    	'; */
	    	
	    	$sql = '
		    		SELECT
		    			object_id
		    		FROM
		    			content_fields_type_datetimerange_data
		    		WHERE
				    		(
						    		(
										(WEEKDAY("' . $search_date . '") = 0 AND cycle_days_monday = 1) OR
							  			(WEEKDAY("' . $search_date . '") = 1 AND cycle_days_tuesday = 1) OR
							   			(WEEKDAY("' . $search_date . '") = 2 AND cycle_days_wednesday = 1) OR
							   			(WEEKDAY("' . $search_date . '") = 3 AND cycle_days_thursday = 1) OR
							   			(WEEKDAY("' . $search_date . '") = 4 AND cycle_days_friday = 1) OR
							   			(WEEKDAY("' . $search_date . '") = 5 AND cycle_days_saturday = 1) OR
							   			(WEEKDAY("' . $search_date . '") = 6 AND cycle_days_sunday = 1) OR
							   			(cycle_days_monday=0 AND cycle_days_tuesday=0 AND cycle_days_wednesday=0 AND cycle_days_thursday=0 AND cycle_days_friday=0 AND cycle_days_saturday=0 AND cycle_days_sunday=0)
									)
						    	AND
						    		(
						    				(from_field_value <= "'.$search_datetime.'" AND to_field_value > "'.$search_datetime.'")
						    			OR
						    				(TO_DAYS("'.$search_date.'") = TO_DAYS(from_field_value) AND from_field_value = to_field_value)
						    		)
				    		)
		    			AND 
			    			field_id = ' . $this->field->id . ' AND
			    			custom_name = "' . $fields_group_name . '"
	    	';
	    	
	    	return array('AND' => str_replace("\n"," ",$sql));
		}
		
		
/* 		(TO_DAYS("'.$search_date.'") = TO_DAYS(`from_field_value`) AND `from_field_value` = `to_field_value`)
		OR
		('.$tmstmp.' BETWEEN UNIX_TIMESTAMP(STR_TO_DATE(`from_field_value`, "%Y-%m-%d %H:%i:%s")) AND UNIX_TIMESTAMP(STR_TO_DATE(`to_field_value`, "%Y-%m-%d %H:%i:%s"))) */

		if (isset($args["from_tmstmp_" . $this->field->seo_name]) || isset($args["to_tmstmp_" . $this->field->seo_name])) {
			if (isset($args["from_tmstmp_" . $this->field->seo_name])) {
				// Receives date in unix timestamp and also in 'Y-m-d' formats
		    	if (is_numeric($args["from_tmstmp_" . $this->field->seo_name]) && (strtotime(date("Y-m-d", $args["from_tmstmp_" . $this->field->seo_name]))) !== FALSE)
		    		$from_tmstmp = $args["from_tmstmp_" . $this->field->seo_name];
		    	elseif ((strtotime($args["from_tmstmp_" . $this->field->seo_name])) !== FALSE && strtotime($args["from_tmstmp_" . $this->field->seo_name]) != -1)
					$from_tmstmp = strtotime($args["from_tmstmp_" . $this->field->seo_name]);
			} else {
				$from_tmstmp = false;
			}
			if (isset($args["to_tmstmp_" . $this->field->seo_name])) {
				// Receives date in unix timestamp and also in 'Y-m-d' formats
				if (is_numeric($args["to_tmstmp_" . $this->field->seo_name]) && (strtotime(date("Y-m-d", $args["to_tmstmp_" . $this->field->seo_name]))) !== FALSE)
		    		$to_tmstmp = $args["to_tmstmp_" . $this->field->seo_name];
		    	elseif ((strtotime($args["to_tmstmp_" . $this->field->seo_name])) !== FALSE && strtotime($args["to_tmstmp_" . $this->field->seo_name]) != -1)
					$to_tmstmp = strtotime($args["to_tmstmp_" . $this->field->seo_name]);
			} else {
				$to_tmstmp = false;
			}

			$days_of_week_search_sql = array();
			if ($from_tmstmp && $to_tmstmp) {
				$sStartDate = gmdate("Y-m-d", $from_tmstmp);  
				$sEndDate = gmdate("Y-m-d", $to_tmstmp);  
				$aDays[] = date('w', $from_tmstmp);   
				$sCurrentDate = $sStartDate;  
				while($sCurrentDate < $sEndDate){  
					$sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
					$aDays[] = date('w', strtotime($sCurrentDate));  
				}
				$aDays = array_unique($aDays);
			} else {
				$aDays = array(0, 1, 2, 3, 4, 5, 6);
			}
			foreach ($aDays AS $day) {
				if ($day == 1) $days_of_week_search_sql[] = 'cycle_days_monday = 1';
				if ($day == 2) $days_of_week_search_sql[] = 'cycle_days_tuesday = 1';
				if ($day == 3) $days_of_week_search_sql[] = 'cycle_days_wednesday = 1';
				if ($day == 4) $days_of_week_search_sql[] = 'cycle_days_thursday = 1';
				if ($day == 5) $days_of_week_search_sql[] = 'cycle_days_friday = 1';
				if ($day == 6) $days_of_week_search_sql[] = 'cycle_days_saturday = 1';
				if ($day == 0) $days_of_week_search_sql[] = 'cycle_days_sunday = 1';
			}
			
			$sql = '
					SELECT
		    			object_id
		    		FROM
		    			content_fields_type_datetimerange_data 
		    		WHERE
		    				(
					    		' . ($days_of_week_search_sql ? '(' . implode(' OR ', $days_of_week_search_sql) . ') OR ' : '') . '
					    		
								(cycle_days_monday=0 AND cycle_days_tuesday=0 AND cycle_days_wednesday=0 AND cycle_days_thursday=0 AND cycle_days_friday=0 AND cycle_days_saturday=0 AND cycle_days_sunday=0)
							)
						AND
							(
								' . (($from_tmstmp && $to_tmstmp) ? '
									((from_field_value <= "'.date("Y-m-d H:i:s", $to_tmstmp).'" AND
									to_field_value > "'.date("Y-m-d H:i:s", $from_tmstmp).'"))

									' . (($from_tmstmp == $to_tmstmp) ? '
									OR
									(from_field_value = to_field_value AND
									TO_DAYS(from_field_value) = TO_DAYS("' . date("Y-m-d H:i:s", $from_tmstmp) . '"))' : '') . '

								' : '') . '

								' . (($from_tmstmp && !$to_tmstmp) ? '(to_field_value > "'.date("Y-m-d H:i:s", $from_tmstmp).'")' : '') . '

								' . (($to_tmstmp && !$from_tmstmp) ? '(from_field_value <= "'.date("Y-m-d H:i:s", $to_tmstmp).'")' : '') . '
							)
						AND
							(field_id = ' . $this->field->id . ' AND custom_name = "' . $fields_group_name . '")
				';
			
			/* $sql = '
					SELECT
		    			object_id
		    		FROM
		    			content_fields_type_datetimerange_data 
		    		WHERE
		    				(
					    		' . ($days_of_week_search_sql ? '(' . implode(' OR ', $days_of_week_search_sql) . ') OR ' : '') . '
					    		
								(cycle_days_monday=0 AND cycle_days_tuesday=0 AND cycle_days_wednesday=0 AND cycle_days_thursday=0 AND cycle_days_friday=0 AND cycle_days_saturday=0 AND cycle_days_sunday=0)
							)
						AND
							(
								' . (($from_tmstmp && $to_tmstmp) ? '
								(TO_DAYS(to_field_value) >= TO_DAYS("' . date("Y-m-d", $from_tmstmp) . '") OR to_field_value = "9999-12-31 00:00:00") AND
								(TO_DAYS(from_field_value) <= TO_DAYS("' . date("Y-m-d", $to_tmstmp) . '") OR from_field_value = "0000-00-00 00:00:00") ' : '') . '

								' . (($from_tmstmp && !$to_tmstmp) ? '(TO_DAYS(to_field_value) >= TO_DAYS("' . date("Y-m-d", $from_tmstmp) . '") OR to_field_value = "9999-12-31 00:00:00")' : '') . '

								' . (($to_tmstmp && !$from_tmstmp) ? '(TO_DAYS(from_field_value) <= TO_DAYS("' . date("Y-m-d", $to_tmstmp) . '") OR from_field_value = "0000-00-00 00:00:00")' : '') . '
							)
						AND
							(field_id = ' . $this->field->id . ' AND custom_name = "' . $fields_group_name . '")
				'; */

			if ($from_tmstmp)
				$search_url_rebuild .= "from_tmstmp_" . $this->field->seo_name . '/' . $args["from_tmstmp_" . $this->field->seo_name] . '/';
			if ($to_tmstmp)
				$search_url_rebuild .= "to_tmstmp_" . $this->field->seo_name . '/' . $args["to_tmstmp_" . $this->field->seo_name] . '/';

	    	return array('AND' => str_replace("\n"," ",$sql));
		}
	}

	/**
	 * Special order by the nearest to NOW date
	 * @param string $direction - 
	 * 		when desc: order by the nearest to NOW date
	 * 		when asc: order by the farthest from NOW date
	 */
	public function orderby($object_ids, $group_custom_name, $content_field_id, $direction = 'desc', $nosticky = false)
	{
		$CI = &get_instance();
		$CI->load->model('content_fields', 'content_fields');
		
		$current_date = date('Y-m-d H:i:s');

		// Get all available records with dates
		$sql = '
				SELECT
					object_id, from_field_value, to_field_value, cycle_days_monday, cycle_days_tuesday, cycle_days_wednesday, cycle_days_thursday, cycle_days_friday, cycle_days_saturday, cycle_days_sunday
				FROM
					content_fields_type_datetimerange_data
				WHERE
					field_id = ' . $content_field_id . ' AND
			    	custom_name = "' . $group_custom_name . '" AND
			    	object_id IN (' . implode(', ', $object_ids) . ') AND
			    	to_field_value >= NOW()
		    	';
		$result_array = $CI->content_fields->db->query(str_replace("\n"," ",$sql))->result_array();

		$result_dates = array();
		foreach ($result_array AS $row) {
			$nearest_date = null;
			if (isset($result_dates[$row['object_id']]))
				$nearest_date = $result_dates[$row['object_id']];
			
			if (strtotime($row['from_field_value']) < strtotime($current_date))
				$start_date = strtotime($current_date);
			else
				$start_date = strtotime($row['from_field_value']);

			if (!$row['cycle_days_monday'] && !$row['cycle_days_tuesday'] && !$row['cycle_days_wednesday'] && !$row['cycle_days_thursday'] && !$row['cycle_days_friday'] && !$row['cycle_days_saturday'] && !$row['cycle_days_sunday']) {
				if (!isset($nearest_date) || (isset($nearest_date) && $nearest_date > $start_date))
					$nearest_date = $start_date;
			} else{
				if ($row['cycle_days_monday']) { $date = strtotime('next Monday', $start_date-1); if ((!$nearest_date || $nearest_date > $date) && $date <= strtotime($row['to_field_value'])) $nearest_date = $date; }
				if ($row['cycle_days_tuesday']) { $date = strtotime('next Tuesday', $start_date-1); if ((!$nearest_date || $nearest_date > $date) && $date <= strtotime($row['to_field_value'])) $nearest_date = $date; }
				if ($row['cycle_days_wednesday']) { $date = strtotime('next Wednesday', $start_date-1); if ((!$nearest_date || $nearest_date > $date) && $date <= strtotime($row['to_field_value'])) $nearest_date = $date; }
				if ($row['cycle_days_thursday']) { $date = strtotime('next Thursday', $start_date-1); if ((!$nearest_date || $nearest_date > $date) && $date <= strtotime($row['to_field_value'])) $nearest_date = $date; }
				if ($row['cycle_days_friday']) { $date = strtotime('next Friday', $start_date-1); if ((!$nearest_date || $nearest_date > $date) && $date <= strtotime($row['to_field_value'])) $nearest_date = $date; }
				if ($row['cycle_days_saturday']) { $date = strtotime('next Saturday', $start_date-1); if ((!$nearest_date || $nearest_date > $date) && $date <= strtotime($row['to_field_value'])) $nearest_date = $date; }
				if ($row['cycle_days_sunday']) { $date = strtotime('next Sunday', $start_date-1); if ((!$nearest_date || $nearest_date > $date) && $date <= strtotime($row['to_field_value'])) $nearest_date = $date; }
			}

			if ($nearest_date)
				$result_dates[$row['object_id']] = $nearest_date;
		}

		// Sort dates
		if ($direction == 'desc')
			asort($result_dates);
		else
			arsort($result_dates);
		return array_keys($result_dates);
	}

	public function getSomeNearestDates($how_many_dates = 20, $object_id = null, $group_custom_name = null)
	{
		if (!$object_id)
			$object_id = $this->field->object_id;
		if (!$group_custom_name)
			$group_custom_name = $this->field->group_custom_name;
		
		$content_field_id = $this->field->id;

		$CI = &get_instance();
		$CI->load->model('content_fields', 'content_fields');
		
		$current_date = date('Y-m-d');

		// Get all available records with dates
		$sql = '
				SELECT
					from_field_value, to_field_value, cycle_days_monday, cycle_days_tuesday, cycle_days_wednesday, cycle_days_thursday, cycle_days_friday, cycle_days_saturday, cycle_days_sunday
				FROM
					content_fields_type_datetimerange_data
				WHERE
					field_id = ' . $content_field_id . ' AND
			    	custom_name = "' . $group_custom_name . '" AND
			    	object_id = ' . $object_id . ' AND
			    	to_field_value >= NOW()
		    	';
		$result_array = $CI->content_fields->db->query(str_replace("\n"," ",$sql))->result_array();

		$result_dates = array();
		foreach ($result_array AS $row) {
			if (strtotime($row['from_field_value']) < strtotime($current_date))
				$start_date = strtotime($current_date);
			else
				$start_date = strtotime($row['from_field_value']);

			$end_date = strtotime($row['to_field_value']);

			while ($start_date <= $end_date) {
				if (!$row['cycle_days_monday'] && !$row['cycle_days_tuesday'] && !$row['cycle_days_wednesday'] && !$row['cycle_days_thursday'] && !$row['cycle_days_friday'] && !$row['cycle_days_saturday'] && !$row['cycle_days_sunday'])
					$result_dates[] = $start_date;
				else {
					if ($row['cycle_days_monday']) { $date = strtotime('next Monday', $start_date-1); if ($date <= $end_date) $result_dates[] = $date; }
					if ($row['cycle_days_tuesday']) { $date = strtotime('next Tuesday', $start_date-1); if ($date <= $end_date) $result_dates[] = $date; }
					if ($row['cycle_days_wednesday']) { $date = strtotime('next Wednesday', $start_date-1); if ($date <= $end_date) $result_dates[] = $date; }
					if ($row['cycle_days_thursday']) { $date = strtotime('next Thursday', $start_date-1); if ($date <= $end_date) $result_dates[] = $date; }
					if ($row['cycle_days_friday']) { $date = strtotime('next Friday', $start_date-1); if ($date <= $end_date) $result_dates[] = $date; }
					if ($row['cycle_days_saturday']) { $date = strtotime('next Saturday', $start_date-1); if ($date <= $end_date) $result_dates[] = $date; }
					if ($row['cycle_days_sunday']) { $date = strtotime('next Sunday', $start_date-1); if ($date <= $end_date) $result_dates[] = $date; }
				}
				$start_date += 86400;
			}
		}
		
		// Sort dates
		$result_dates = array_unique($result_dates);
		asort($result_dates);
		
		$result_dates = array_slice($result_dates, 0, $how_many_dates);
		//foreach ($result_dates AS $key=>$date)
			//$result_dates[$key] = date('Y-m-d H:i', $date);

		return $result_dates;
	}
}
?>