<?php
include_once(MODULES_PATH . 'ajax_files_upload/classes/files_upload.class.php');

class content_fieldsController extends controller
{
	public function index()
	{
    	$this->load->model('content_fields');
    	$fields = $this->content_fields->selectAllFields();

    	$view = $this->load->view();
    	$view->assign('fields', $fields);
        $view->display('content_fields/admin_manage_fields.tpl');
	}
	
	/**
	 * form validation function
	 *
	 * @param str $field_name
	 * @return bool
	 */
	public function check_field_name($field_name)
	{
		$this->load->model('content_fields');
		
		if ($this->content_fields->is_field_name($field_name)) {
			$this->form_validation->set_message('name');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	public function check_seo_field_name($field_name)
	{
		$this->load->model('content_fields');
		
		if ($this->content_fields->is_seo_field_name($field_name)) {
			$this->form_validation->set_message('seo_name');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	public function create()
	{
        $this->load->model('content_fields');
        
        // Let collect all avaliable types of field
        $field_types = $this->content_fields->getAllFieldTypes();

        if ($this->input->post('submit')) {
        	$this->form_validation->set_rules('name', LANG_FIELD_NAME, 'sanitize_string|required|max_length[255]|callback_check_field_name');
        	$this->form_validation->set_rules('frontend_name', LANG_FIELD_FRONTEND_NAME, 'sanitize_string|max_length[255]');
        	$this->form_validation->set_rules('frontend_mode', LANG_FIELD_FRONTEND_MODE, 'sanitize_string|max_length[255]');
        	$this->form_validation->set_rules('field_icon_image', LANG_FIELD_ICON_IMAGE, 'sanitize_string|max_length[255]');
        	$this->form_validation->set_rules('seo_name', LANG_FIELD_SEO_NAME, 'required|alpha_underscores|max_length[255]|callback_check_seo_field_name');
        	$this->form_validation->set_rules('type', LANG_FIELD_TYPE, 'sanitize_string|required');
        	$this->form_validation->set_rules('description', LANG_FIELD_DESCRIPTION, 'sanitize_string|max_length[1000]');
        	$this->form_validation->set_rules('required', LANG_FIELD_REQUIRED, 'is_checked');
        	$this->form_validation->set_rules('v_index_page', LANG_FIELD_VISIBILITY_INDEX, 'is_checked');
        	$this->form_validation->set_rules('v_types_page', LANG_FIELD_VISIBILITY_TYPES, 'is_checked');
        	$this->form_validation->set_rules('v_categories_page', LANG_FIELD_VISIBILITY_CATEGORIES, 'is_checked');
        	$this->form_validation->set_rules('v_search_page', LANG_FIELD_VISIBILITY_SEARCH, 'is_checked');

        	if ($this->form_validation->run() !== FALSE) {
        		// Complete field object and save it into DB
        		$field = $this->content_fields->buildFieldObj($this->form_validation->result_array());
        		if ($this->content_fields->saveField($field)) {
					$this->setSuccess(LANG_FIELD_CREATE_SUCCESS);
					redirect('admin/fields/');
				}
        	} else {
        		$field = $this->content_fields->buildFieldObj($this->form_validation->result_array());
        	}
        } else {
            $field = $this->content_fields->getNewField();
        }
        
        $users_content_server_path = $this->config->item('users_content_server_path');
        $users_content_array = $this->config->item('users_content');
        $icon_path_to_upload = $users_content_array['field_icon_image']['upload_to'];
		if (!($icons = directory_map($users_content_server_path . $icon_path_to_upload, true)))
        	$icons = array();
        foreach ($icons as $key=>$icon) {
        	if (!strpos($icon, '.png') && !strpos($icon, '.jpg') && !strpos($icon, '.jpeg') && !strpos($icon, '.gif') && !strpos($icon, '.PNG') && !strpos($icon, '.JPG') && !strpos($icon, '.JPEG') && !strpos($icon, '.GIF'))
        		unset($icons[$key]);
        }
        if ($icons)
        	asort($icons);

        /* foreach ($icons as $key=>$icon) {
        	rename($users_content_server_path . $icon_path_to_upload . $icon, $users_content_server_path . $icon_path_to_upload . str_replace('&', '_', $icon));
        } */
        
        registry::set('breadcrumbs', array(
    		'admin/fields/' => LANG_MANAGE_CONTENT_FIELDS,
    		LANG_CREATE_FIELD,
    	));

        $view = $this->load->view();
        
        $file_to_upload = new filesUpload;
        $file_to_upload->title = LANG_FIELD_ICON_IMAGE;
        $file_to_upload->upload_id = 'field_icon_image';
        $file_to_upload->current_file = $field->field_icon_image;
        $file_to_upload->after_upload_url = site_url('admin/fields/get_icon/');
        $config = $this->config->item('field_icon_image', 'users_content');
        $file_to_upload->attrs['width'] = explodeSize($config['size'], 'width');
        $file_to_upload->attrs['height'] = explodeSize($config['size'], 'height');
        $file_to_upload->attrs['existed_icons'] = $icons;
        $view->addJsFile('ajaxfileupload.js');
        $view->assign('image_upload_block', $file_to_upload);
        
        $view->assign('field', $field);
        $view->assign('field_types', $field_types);
        $view->display('content_fields/admin_field_settings.tpl');
	}
	
	public function get_icon()
	{
		if ($this->input->post('uploaded_file')) {
			$uploaded_file = $this->input->post('uploaded_file');
    		$users_content_server_path = $this->config->item('users_content_server_path');
    		$users_content_array = $this->config->item('users_content');
    		$icon_path_to_upload = $users_content_array['field_icon_image']['upload_to'];

			$this->load->library('image_lib');
			$destImageSize[] = explodeSize($users_content_array['field_icon_image']['size'], 'width') . '*' . explodeSize($users_content_array['field_icon_image']['size'], 'height');
			$destImageFolder[] = $users_content_server_path . $icon_path_to_upload;

			if ($this->image_lib->process_images('resize', $uploaded_file, $destImageFolder, $destImageSize)) {
				$file = $uploaded_file;
				$error = '';
			} else {
				$error = $this->image_lib->display_errors();
				$file = '';
			}

			// JQuery .post needs JSON response
			echo json_encode(array('error_msg' => _utf8_encode($error), 'file_name' => basename($file)));
		}
	}
	
	public function edit($field_id)
    {
		$this->load->model('content_fields');

		$this->content_fields->setFieldId($field_id);

        $field_types = $this->content_fields->getAllFieldTypes();

        if ($this->input->post('submit')) {
        	$this->form_validation->set_rules('name', LANG_FIELD_NAME, 'sanitize_string|required|max_length[255]|callback_check_field_name');
        	$this->form_validation->set_rules('frontend_name', LANG_FIELD_FRONTEND_NAME, 'sanitize_string|max_length[255]');
        	$this->form_validation->set_rules('frontend_mode', LANG_FIELD_FRONTEND_MODE, 'sanitize_string|max_length[255]');
        	$this->form_validation->set_rules('field_icon_image', LANG_FIELD_ICON_IMAGE, 'sanitize_string|max_length[255]');
        	$this->form_validation->set_rules('seo_name', LANG_FIELD_SEO_NAME, 'required|alpha_underscores|max_length[255]|callback_check_seo_field_name');
        	$this->form_validation->set_rules('description', LANG_FIELD_DESCRIPTION, 'sanitize_string|max_length[1000]');
        	$this->form_validation->set_rules('required', LANG_FIELD_REQUIRED, 'is_checked');
        	$this->form_validation->set_rules('orderby_enabled', LANG_FIELD_ORDERBY, 'is_checked');
        	$this->form_validation->set_rules('v_index_page', LANG_FIELD_VISIBILITY_INDEX, 'is_checked');
        	$this->form_validation->set_rules('v_types_page', LANG_FIELD_VISIBILITY_TYPES, 'is_checked');
        	$this->form_validation->set_rules('v_categories_page', LANG_FIELD_VISIBILITY_CATEGORIES, 'is_checked');
        	$this->form_validation->set_rules('v_search_page', LANG_FIELD_VISIBILITY_SEARCH, 'is_checked');
        	
        	if ($this->form_validation->run() !== FALSE) {
        		// Complete field object and save it into DB
        		$field = $this->content_fields->buildFieldObj($this->form_validation->result_array());
        		if ($this->content_fields->saveFieldById($field)) {
        			// Clean cache
					$this->cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('content_fields'));

					$this->setSuccess(LANG_FIELD_SAVE_SUCCESS);
					redirect('admin/fields/');
				}
        	} else {
        		$field = $this->content_fields->buildFieldObj($this->form_validation->result_array());
        	}
        } else {
            $field = $this->content_fields->getFieldById();
        }
        
        $users_content_server_path = $this->config->item('users_content_server_path');
        $users_content_array = $this->config->item('users_content');
        $icon_path_to_upload = $users_content_array['field_icon_image']['upload_to'];
        if (!($icons = directory_map($users_content_server_path . $icon_path_to_upload, true)))
        	$icons = array();
        foreach ($icons as $key=>$icon) {
        	if (!strpos($icon, '.png') && !strpos($icon, '.jpg') && !strpos($icon, '.jpeg') && !strpos($icon, '.gif') && !strpos($icon, '.PNG') && !strpos($icon, '.JPG') && !strpos($icon, '.JPEG') && !strpos($icon, '.GIF'))
        		unset($icons[$key]);
        }
        if ($icons)
        	asort($icons);
        
        registry::set('breadcrumbs', array(
    		'admin/fields/' => LANG_MANAGE_CONTENT_FIELDS,
    		LANG_EDIT_FIELD . ' "' . $field->name . '"',
    	));

        $view  = $this->load->view();
        
        $file_to_upload = new filesUpload;
        $file_to_upload->title = LANG_FIELD_ICON_IMAGE;
        $file_to_upload->upload_id = 'field_icon_image';
        $file_to_upload->current_file = $field->field_icon_image;
        $file_to_upload->after_upload_url = site_url('admin/fields/get_icon/');
        $config = $this->config->item('field_icon_image', 'users_content');
        $file_to_upload->attrs['width'] = explodeSize($config['size'], 'width');
        $file_to_upload->attrs['height'] = explodeSize($config['size'], 'height');
        $file_to_upload->attrs['existed_icons'] = $icons;
        $view->addJsFile('ajaxfileupload.js');
        $view->assign('image_upload_block', $file_to_upload);

        $view->assign('field', $field);
        $view->assign('field_types', $field_types);
        $view->display('content_fields/admin_field_settings.tpl');
    }
    
    public function delete($field_id)
    {
        $this->load->model('content_fields');

        $this->content_fields->setFieldId($field_id);

        if ($this->input->post('yes')) {
            if ($this->content_fields->deleteFieldById()) {
            	// Clean cache
				$this->cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('content_fields'));

            	$this->setSuccess(LANG_FIELD_DELETE_SUCCESS);
                redirect('admin/fields/');
            }
        }

        if ($this->input->post('no')) {
            redirect('admin/fields/');
        }

        if ( !$field = $this->content_fields->getFieldById()) {
            redirect('admin/fields/');
        }
        
        registry::set('breadcrumbs', array(
    		'admin/fields/' => LANG_MANAGE_CONTENT_FIELDS,
    		LANG_DELETE_FIELD . ' "' . $field->name . '"',
    	));

		$view  = $this->load->view();
		$view->assign('options', array($field_id => $field->name));
        $view->assign('heading', LANG_DELETE_FIELD);
        $view->assign('question', LANG_DELETE_FIELD_QUEST);
        $view->display('backend/delete_common_item.tpl');
    }
    
    public function configure($field_id)
    {
    	$this->load->model('content_fields');

		$this->content_fields->setFieldId($field_id);

		// Call configuration view directly from content field type class
        $this->content_fields->configureField();
    }
    
    public function copy($field_id)
    {
    	$this->load->model('content_fields');

		$this->content_fields->setFieldId($field_id);
		
    	if ($this->load->is_module_loaded('i18n')) {
			$this->load->model('languages', 'i18n');
			$this->languages->langAreasSwitchOff();
		}

        if ($this->content_fields->copyField()) {
	        if ($this->load->is_module_loaded('i18n')) {
				$this->languages->langAreasSwitchOn();
			}
        	$this->setSuccess(LANG_FIELD_COPY_SUCCESS);
        	redirect('admin/fields/');
        }
    }
    
    public function configure_search($field_id)
    {
    	$this->load->model('content_fields');
    	$this->content_fields->setFieldId($field_id);
    	
    	// Call configuration view directly from content field type class
        $this->content_fields->configureFieldSearch();
    }
    
    public function website_redirect($field_value_id)
    {
    	$this->load->model('content_fields');
    	$url = $this->content_fields->getWebsiteUrlById($field_value_id);
    	
    	// Some hostings return 302 HTTP error with 'Status: 200' header while redirect,
    	// so we have to get rid of it.
    	//header("Status: 200");

    	$url = trim($url, '/');

    	header('Location: ' . prep_url($url));
    }
    
    // possible actions: select, save, update, delete
    public function manage_field_value($action, $field_id, $field_custom_group, $object_id)
    {
    	$this->load->model('content_fields_groups');
    	$field = $this->content_fields_groups->getFieldObjectById($field_id, $field_custom_group, $object_id);
    	
    	switch ($action) {
    		case 'select':
    			$field->select($field_custom_group, $object_id);
    			echo json_encode($field);
    			break;
    	}
    }
}
?>