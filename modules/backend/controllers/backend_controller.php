<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class backendController extends Controller
{
    public function index()
    {
    	// Only logged in users allowed
    	if (!$this->session->userdata('user_id')) {
    		exit('Log in first!');
    	}
    	
    	$this->load->model('backend');
    	$content_access_obj = contentAcl::getInstance();
    	$view  = $this->load->view();
    	
    	if ($content_access_obj->isPermission('Manage all listings')) {
    		$active_listings_count = 0;
    		$blocked_listings_count = 0;
    		$suspended_listings_count = 0;
    		$unapproved_listings_count = 0;
    		$notpaid_listings_count = 0;
    		
    		$listings_status_count = $this->backend->getListingsSummary();
    		foreach ($listings_status_count AS $status_row) {
    			if ($status_row['status'] == 1)
    				$active_listings_count = $status_row['active_status'];
    			if ($status_row['status'] == 2)
    				$blocked_listings_count = $status_row['blocked_status'];
    			if ($status_row['status'] == 3)
    				$suspended_listings_count = $status_row['suspended_status'];
    			if ($status_row['status'] == 4)
    				$unapproved_listings_count = $status_row['unapproved_status'];
    			if ($status_row['status'] == 5)
    				$notpaid_listings_count = $status_row['not_paid_status'];
    		}
    		
    		$view->assign('active_listings_count', $active_listings_count);
    		$view->assign('blocked_listings_count', $blocked_listings_count);
    		$view->assign('suspended_listings_count', $suspended_listings_count);
    		$view->assign('unapproved_listings_count', $unapproved_listings_count);
    		$view->assign('notpaid_listings_count', $notpaid_listings_count);
    	}
    	if ($content_access_obj->isPermission('Manage users')) {
    		$active_users_count = 0;
    		$unverified_users_count = 0;
    		$blocked_users_count = 0;

	    	$users_status_count = $this->backend->getUsersSummary();
	    	foreach ($users_status_count AS $status_row) {
	    		if ($status_row['status'] == 1)
	    			$unverified_users_count = $status_row['unverified_status'];
	    		if ($status_row['status'] == 2)
	    			$active_users_count = $status_row['active_status'];
	    		if ($status_row['status'] == 3)
	    			$blocked_users_count = $status_row['blocked_status'];
	    	}
	    	$users_in_groups_count = $this->backend->getUsersGroupsSummary();
	    	$view->assign('active_users_count', $active_users_count);
	    	$view->assign('unverified_users_count', $unverified_users_count);
	    	$view->assign('blocked_users_count', $blocked_users_count);
			$view->assign('users_in_groups_count', $users_in_groups_count);
    	}
    	if ($content_access_obj->isPermission('Manage all listings') && $content_access_obj->isPermission('Manage ability to claim')) {
    		$unapproved_claims_count = $this->backend->getClaimsUnapproved();
    		$view->assign('unapproved_claims_count', $unapproved_claims_count);
    	}

    	if ($this->load->is_module_loaded('banners') && $content_access_obj->isPermission('Manage all banners')) {
    		$active_banners_count = 0;
    		$blocked_banners_count = 0;
    		$suspended_banners_count = 0;
    		$not_paid_banners_count = 0;
    		
	    	$this->load->model('banners', 'banners');
			$banners_status_count = $this->banners->getBannersSummary();
			foreach ($banners_status_count AS $status_row) {
				if ($status_row['status'] == 1)
					$active_banners_count = $status_row['active_status'];
				if ($status_row['status'] == 2)
					$blocked_banners_count = $status_row['blocked_status'];
				if ($status_row['status'] == 3)
					$suspended_banners_count = $status_row['suspended_status'];
				if ($status_row['status'] == 4)
					$not_paid_banners_count = $status_row['not_paid_status'];
			}
			$view->assign('active_banners_count', $active_banners_count);
			$view->assign('blocked_banners_count', $blocked_banners_count);
			$view->assign('suspended_banners_count', $suspended_banners_count);
			$view->assign('not_paid_banners_count', $not_paid_banners_count);
    	}

    	if ($this->load->is_module_loaded('payment') && $content_access_obj->isPermission('View all invoices') && $content_access_obj->isPermission('View all transactions')) {
    		$not_paid_invoices_count = 0;
    		$paid_invoices_count = 0;

			$this->load->model('payment', 'payment');
			$invoices_status_count = $this->payment->getInvoicesSummary();
			foreach ($invoices_status_count AS $status_row) {
				if ($status_row['status'] == 1)
					$not_paid_invoices_count = $status_row['not_paid_status'];
				if ($status_row['status'] == 2)
					$paid_invoices_count = $status_row['paid_status'];
			}
			$view->assign('not_paid_invoices_count', $not_paid_invoices_count);
			$view->assign('paid_invoices_count', $paid_invoices_count);
			
			$transactions_count = $this->payment->getTransactionsCount();
			$transactions_summary = $this->payment->getTransactionsSummary();
			$view->assign('transactions_count', $transactions_count);
			$view->assign('transactions_summary', $transactions_summary);
    	}
    	
    	// --------------------------------------------------------------------------------------------
    	if ($content_access_obj->isPermission('Manage self listings')) {
    		$my_listings_status_count = $this->backend->getMyListingsSummary();
    		$view->assign('my_listings_status_count', $my_listings_status_count);
    	}

    	if ($this->load->is_module_loaded('banners') && $content_access_obj->isPermission('Manage self banners')) {
	    	$this->load->model('banners', 'banners');
			$my_banners_status_count = $this->banners->getMyBannersSummary();
			$view->assign('my_banners_status_count', $my_banners_status_count);
    	}
		
    	if ($this->load->is_module_loaded('payment') && $content_access_obj->isPermission('View self invoices') && $content_access_obj->isPermission('View self transactions')) {
			$this->load->model('payment', 'payment');
			$my_invoices_status_count = $this->payment->getMyInvoicesSummary();
			$my_transactions_count = $this->payment->getMyTransactionsCount();
			$my_transactions_summary = $this->payment->getMyTransactionsSummary();
			$view->assign('my_invoices_status_count', $my_invoices_status_count);
			$view->assign('my_transactions_count', $my_transactions_count);
			$view->assign('my_transactions_summary', $my_transactions_summary);
    	}

        $view->display('backend/admin_index.tpl');
    }
    
    public function refresh_captcha()
    {
    	$this->load->plugin('captcha');
		$captcha = create_captcha($this);

		echo $captcha->image;
    }

    public function update_langs($updater_string)
    {
    	error_reporting(E_ALL);

    	$udir = ROOT . 'updates/' . $updater_string . '/';
    	if (is_dir($udir)) {
    		if (preg_match("/^update_v([0-9])([0-9x])([0-9x])_to_v([0-9])([0-9])([0-9])$/", $updater_string, $matches)) {
    			$uversion = $matches[4].'.'.$matches[5].'.'.$matches[6];
    			
    			// Clean cache
				$this->cache->clean(Zend_Cache::CLEANING_MODE_ALL);

	    		// Translate new language constants
	    		$this->load->model('backend');
	    		$this->backend->languagesFilesUpdate($uversion, $udir);

				events::callEvent('Modules list rebuild');
	    		
	    		exit("Languages update to ".$uversion." version completed!");
    		} else {
    			exit("'" . $updater_string . "' update url doesn't match format");
    		}
    	} else {
    		exit("'" . $updater_string . "' update folder doesn't exist!");
    	}
    }
    
    public function update($updater_string)
    {
    	// This is in order to see all mistakes during upgrade process
    	//error_reporting(E_ALL);

    	$udir = ROOT . 'updates/' . $updater_string . '/';
    	if (is_dir($udir)) {
    		if (preg_match("/^update_v([0-9])([0-9x])([0-9x])_to_v([0-9])([0-9])([0-9])$/", $updater_string, $matches)) {
    			$uversion = $matches[4].'.'.$matches[5].'.'.$matches[6];
    			
    			echo "Upgrade started, it may take up to some minutes, please, be patient<br /><br />";
    			
    			// Clean all cache
				$this->cache->clean(Zend_Cache::CLEANING_MODE_ALL);
				$this->load->helper('file');
				delete_files(BASEPATH . 'view/smarty/templates_c/', true);
	    		delete_files(BASEPATH . 'cache/');
				
				// Switch off lang areas
				if ($this->load->is_module_loaded('i18n')) {
		    		$this->load->model('languages', 'i18n');
		    		$this->languages->langAreasSwitchOff();
				}

    			// SQL queries execution
    			$update_sql_file = $udir . $updater_string . '.sql';
	    		if (is_file($update_sql_file)) {
	    			$this->load->plugin('sqlDumpParser');
	    			$queries = getQueriesFromFile($update_sql_file);
					foreach ($queries AS $query) {
						$this->db->query($query);
					}
	    		}
	    		
	    		// PHP instructions execution
    			$update_php_file = $udir . $updater_string . '.php';
	    		if (is_file($update_php_file)) {
	    			include_once($update_php_file);
	    		}
	    		
	    		// Switch on lang areas
				if ($this->load->is_module_loaded('i18n')) {
		    		$this->languages->langAreasSwitchOn();
				}
	    		
	    		// Translate new language constants
	    		$this->load->model('backend');
	    		$this->backend->languagesFilesUpdate($uversion, $udir);

				events::callEvent('Modules list rebuild');
	    		
	    		exit("Update to ".$uversion." version completed!");
    		} else {
    			exit("'" . $updater_string . "' update url doesn't match format");
    		}
    	} else {
    		exit("'" . $updater_string . "' update folder doesn't exist!");
    	}
    }
}
?>