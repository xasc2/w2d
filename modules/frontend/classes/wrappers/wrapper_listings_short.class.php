<?php

class listingsShortWrapperClass extends wrapperClass
{
	public function render()
	{
		// Look for content fields those may be rendered in header of the table
		if (!isset($this->params['content_fields']))
			$this->getContentFieldsOfWrapper();
		
		$CI = &get_instance();
		$view = $CI->load->view();
		$view->assign($this->params);
		$view->assign('listings_array', $this->params['items_array']);
		return $view->fetch('frontend/wrappers/wrapper_listings_short.tpl');
	}
}
?>