<?php

class listingsFullWrapperClass extends wrapperClass
{
	public function render()
	{
		$CI = &get_instance();
		$view = $CI->load->view();
		$view->assign($this->params);
		$view->assign('listings_array', $this->params['items_array']);
		return $view->fetch('frontend/wrappers/wrapper_listings_full.tpl');
	}
}
?>