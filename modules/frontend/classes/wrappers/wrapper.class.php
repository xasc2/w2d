<?php

abstract class wrapperClass
{
	protected $params = array();
	
	public function __construct($params)
	{
		$this->params = $params;
	}
	
	public function getContentFieldsOfWrapper()
	{
		$listings_array = $this->params['items_array'];

		$content_fields = array();
		$logo_enabled = false;
		$max_fields_count = 0;
		foreach ($listings_array AS $key=>$listing) {
			if ($listing->level->logo_enabled)
				$logo_enabled = true;
			if ($max_fields_count < count($listing->content_fields->getFieldsObjects())) {
				$max_fields_count = count($listing->content_fields->getFieldsObjects());
				$fields_objects = $listing->content_fields->getFieldsObjects();
				foreach ($fields_objects AS $field) {
					if (!in_array($field->field->seo_name, $content_fields)) {
						$content_fields[serialize($field)] = $field->field->seo_name;
					}
				}
			}
			
			$fields_objects = $listing->content_fields->getFieldsObjects();
			$_index1 = 0;
			foreach ($content_fields AS $field=>$seo_name) {
				if (!array_key_exists($seo_name, $fields_objects)) {
					$field = unserialize($field);
					$field->field->value = '';
					$field->field->currency = '';

					if ($_index1 === 0) {
						$fields_objects = array_merge(array($seo_name=>$field), $fields_objects);
						$_index1 = $seo_name;
					} else {
						$fields_objects = array_push_after($fields_objects, array($seo_name=>$field), $_index1);
					}
				} else {
					$_index1 = $seo_name;
				}
			}
			if ($fields_objects)
				$listings_array[$key]->content_fields->setFieldsObjects($fields_objects);
		}
		$this->params['max_fields_count'] = $max_fields_count;
		$this->params['content_fields'] = $content_fields;
		$this->params['logo_enabled'] = $logo_enabled;
		$this->params['items_array'] = $listings_array;
	}
	
	public function render_orderby()
	{
		$CI = &get_instance();
		$view = $CI->load->view();

		$this->getContentFieldsOfWrapper();
		$view->assign($this->params);

		$listings_array = $this->params['items_array'];
		$view->assign('listings_array', $listings_array);

		// Find is there ratings orderby
		$current_type = registry::get('current_type');
		if ($current_type) {
			$levels = $current_type->buildLevels();
			foreach ($levels AS $level)
				if ($level->ratings_enabled) {
					$view->assign('orderby_rating', true);
					break;
				}
		}

		// Find is there orderby distance
		if ($this->params['search_args']['where_radius'] && ($this->params['search_args']['where_search'] || $this->params['search_args']['predefined_location_id']))
			$view->assign('orderby_distance', true);
		
		return $view->fetch('frontend/wrappers/orderby.tpl');
	}
	
	/*public function render()
	{
		$CI = &get_instance();
		$view = $CI->load->view();
		$view->assign($this->params);
		if (isset($this->params['view_name']) && $view->template_exists('frontend/wrappers/wrapper_' . $this->params['view_name'] . '.tpl'))
			$template = 'frontend/wrappers/wrapper_' . $this->params['view_name'] . '.tpl';
		else 
			$template = 'frontend/wrappers/wrapper_default.tpl';
		return $view->fetch($template);
	}*/
}

/**
 * Add elements to an array after a specific index or key
 *
 * @param array $src
 * @param array $in
 * @param int $pos
 * @return array
 */
function array_push_after($src, $in, $pos)
{
	if (is_int($pos)) {
		$R = array_merge(array_slice($src, 0, $pos+1), $in, array_slice($src, $pos+1));
	} else {
		foreach($src as $k=>$v) {
			$R[$k] = $v;
			if ($k == $pos)
				$R=array_merge($R, $in);
		}
	}
	return $R;
}
?>