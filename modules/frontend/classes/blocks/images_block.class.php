<?php

class imagesBlockClass extends blockClass
{
	/**
	 * when items array doesn't passed into the block - it must be extracted here
	 *
	 */
	protected function getItems()
	{
		/* Params:
		
		existed_images - when we already have an array of existed images (IDs or objects)
		listings - array of IDs or objects
		images_limit [max] - int
		*/
		
		$CI = &get_instance();
		
		$images = array();
		if (isset($this->params['existed_images'])) {
			$images_ids = array();
			if (is_array($this->params['existed_images'])) {
				foreach ($this->params['existed_images'] AS $image)
					if (is_numeric($image))
						// if array of Ids
						$images_ids[] = $image;
					elseif (is_object($image))
						// if array of objects
						$images[] = $image;
			} elseif (is_object($this->params['existed_images']))
				// if single object
				$images[] = $this->params['existed_images'];
			elseif (is_numeric($this->params['existed_images']))
				// if single ID
				$images_ids[] = $this->params['existed_images'];
			elseif (is_string($this->params['existed_images']))
				// if string of IDs connected with ','
				$images_ids = array_filter(explode(',', $this->params['existed_images']));
			if (!$images && $images_ids) {
				$CI->load->model('images', 'listings');
				foreach ($images_ids AS $id) {
					$images[] = $CI->images->getImageById($id);
				}
			}
		} else {
			$CI->load->model('images', 'listings');
			if (isset($this->params['listings'])) {
				if (!is_array($this->params['listings']))
					$listings = array($this->params['listings']);
				else 
					$listings = $this->params['listings'];
				foreach ($listings AS $listing)
					if (is_object($listing))
						$images = array_merge($images, $CI->images->selectImages($listing->id));
					elseif (is_object($listing))
						$images = array_merge($images, $CI->images->selectImages($listing));
			} else 
				$images = $CI->images->selectImages();
		}
		if (isset($this->params['images_limit']) && $this->params['images_limit'] != 'max')
			$images = array_slice($images, 0, $this->params['images_limit']);

		return $images;
	}
	
	/**
	 * we overwrite parent's render() method in order to work with this specific listings block cache
	 *
	 * @return html
	 */
	public function render()
	{
		$CI = &get_instance();
		$block = $CI->load->view();

		// --------------------------------------------------------------------------------------------
		// Render special template or default
		// --------------------------------------------------------------------------------------------
		if (isset($this->params['block_template']) && $block->template_exists($this->params['block_template'])) {
        	$block_template = $this->params['block_template'];
        } else {
        	$block_template = 'frontend/blocks/default.tpl';
        }
        // --------------------------------------------------------------------------------------------

		if ($this->wrapper_object)
			$block->assign('wrapper_object', $this->wrapper_object);
		$block->assign($this->params);

		$cache_id = 'images_block_' . md5(serialize($this->params));
		if (!$cache = $CI->cache->load($cache_id)) {
			$html = $block->fetch($block_template);
			$CI->cache->save($html, $cache_id, array('listings'));
		} else {
	    	$html = $cache;
	    }
		
		return $html;
	}
}
?>