<?php
include_once(MODULES_PATH . 'acl/classes/content_acl.class.php');
include_once(MODULES_PATH . 'content_fields/classes/field.class.php');
include_once(MODULES_PATH . 'content_fields/classes/content_fields.class.php');
include_once(MODULES_PATH . 'content_fields/classes/search_content_fields.class.php');

class contactusController extends controller
{
    public function index()
    {
    	$this->load->model('content_pages', 'content_pages');
		$this->load->model('types', 'types_levels');
		$view = $this->load->view();

		// --------------------------------------------------------------------------------------------
		// Select types and levels in hierarchical array
		// --------------------------------------------------------------------------------------------
		$types = $this->types->getTypesLevels();
		$view->assign('types', $types);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Search block settings
		// --------------------------------------------------------------------------------------------
		$search_fields = new searchContentFields(GLOBAL_SEARCH_GROUP_CUSTOM_NAME, 0);
		$advanced_search_fields = new searchContentFields(GLOBAL_SEARCH_GROUP_CUSTOM_NAME, 0, 'advanced');

		$view->assign('search_fields', $search_fields);
		$view->assign('advanced_search_fields', $advanced_search_fields);
		// --------------------------------------------------------------------------------------------
		
		$content_fields = new contentFields(CONTACT_US_PAGE_GROUP_CUSTOM_NAME, 0);

    	if ($this->input->post('submit')) {
    		$this->form_validation->set_rules('captcha', LANG_CAPTCHA, 'callback_check_captcha');
            $this->form_validation->set_rules('name', LANG_CONTACTUS_NAME, 'sanitize_string|required|max_length[255]');
            $this->form_validation->set_rules('email', LANG_CONTACTUS_EMAIL, 'required|valid_email');
            $this->form_validation->set_rules('subject', LANG_CONTACTUS_SUBJECT, 'sanitize_string|required|max_length[255]');
            $this->form_validation->set_rules('message', LANG_CONTACTUS_BODY, 'sanitize_string|required|max_length[1500]');
            
            $content_fields->validate($this->form_validation);

    		if ($this->form_validation->run() !== FALSE) {
    			$form = $this->form_validation->result_array();

    			$this->load->library('swiftmail');
    			$system_settings = registry::get('system_settings');
    			$site_settings = registry::get('site_settings');
    			$subject = $form['subject'];
    			// Check this message for spam
    			$sprotector = new spamProtector();
    			if (!$sprotector->isSpam($this->input->ip_address(), $form['message'], $form['subject'], $form['name'], $form['email'])) {
    				// Attach information from content fields
    				$message = $form['message'];
    				$content_fields->getValuesFromForm($form);
    				$fields = $content_fields->getFieldsObjects();
    				foreach ($fields AS $seo_name=>$content_field) {
    					$field_name = $content_field->field->name;
    					$field_value = $content_fields->getFieldValueAsString($seo_name);
    					if ($field_value)
    						$message .= '
    						' . $field_name . ':
    						' . $field_value;
    				}
    				$this->swiftmail->create_message($subject, $message, array($form['email'] => $form['name']), array($system_settings['website_email'] => $site_settings['website_title']), array($form['email'] => $form['name']));
    				$this->swiftmail->send_message();
    			}

			   	$this->setSuccess(LANG_CONTACTUS_SUCCESS);
			   	redirect('contactus/');
			} else {
				$form = $this->form_validation->result_array();

				$view->assign('name', $form['name']);
				$view->assign('email', $form['email']);
				$view->assign('subject', $form['subject']);
				$view->assign('message', $form['message']);
			}
    	}

    	$this->load->plugin('captcha');
		$captcha = create_captcha($this);
		$view->assign('content_fields', $content_fields);
		$view->assign('captcha', $captcha);
    	$view->display('frontend/contactus_page.tpl');
    }
    
    /**
	 * validation function
	 *
	 * @param string $email
	 * @return bool
	 */
	public function check_captcha($captcha)
	{
		if ($this->session->userdata('captcha_word') != $captcha) {
			$this->form_validation->set_message('captcha');
			return FALSE;
		} else {
			return TRUE;
		}
	}
}
?>