<?php

include_once(MODULES_PATH . 'settings/classes/listings_views_set.class.php');
include_once(MODULES_PATH . 'content_pages/classes/content_page.class.php');
include_once(MODULES_PATH . 'acl/classes/content_acl.class.php');
include_once(MODULES_PATH . 'content_fields/classes/field.class.php');
include_once(MODULES_PATH . 'content_fields/classes/content_fields.class.php');
include_once(MODULES_PATH . 'content_fields/classes/search_content_fields.class.php');
include_once(MODULES_PATH . 'google_maps/classes/listings_markers.class.php');

class frontendController extends controller
{
	public function index($location)
	{
		$this->output->enable_profiler($this->config->item('debug_mode'));

		$this->load->model('types', 'types_levels');
		$this->load->model('settings', 'settings');
		$this->load->model('listings', 'listings');
		$view = $this->load->view();

		// --------------------------------------------------------------------------------------------
		// Select types and levels in hierarchical array
		// --------------------------------------------------------------------------------------------
		$types = $this->types->getTypesLevels();
		$view->assign('types', $types);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Get frontend settings - listings views object
		// --------------------------------------------------------------------------------------------
		$listings_views = $this->settings->selectViewsByTypes($types);
		$view->assign('listings_views', $listings_views);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Select listings from DB
		// --------------------------------------------------------------------------------------------
		$paginator = new pagination;
		$this->listings->setPaginator($paginator);
		$listings_of_type = array();
		foreach ($types AS $type) {
			$listings_view = $listings_views->getViewByTypeIdAndPage($type->id, 'index');
			if ($listings_number = $listings_view->getListingsNumberFromFormat()) {
				$paginator->setNumPerPage($listings_number);
				$listings_of_type[$type->id] = $this->listings->selectListings(array('page_name' => 'index', 'search_type' => $type, 'search_status' => 1, 'search_users_status' => 2, 'search_levels' => $listings_view->levels_visible, 'search_location' => registry::get('current_location')), $listings_view->order_by, $listings_view->order_direction, $listings_view->suborder_by, $listings_view->suborder_direction);
			} else 
				$listings_of_type[$type->id] = array();
		}
		$view->assign('listings_of_type', $listings_of_type);
		// --------------------------------------------------------------------------------------------

		$view->assign('current_type_id', 0);

		// --------------------------------------------------------------------------------------------
		// Search block settings
		// --------------------------------------------------------------------------------------------
		$base_url = site_url('search/');
		$search_fields = new searchContentFields(GLOBAL_SEARCH_GROUP_CUSTOM_NAME, 0);
		$advanced_search_fields = new searchContentFields(GLOBAL_SEARCH_GROUP_CUSTOM_NAME, 0, 'advanced');

		$view->assign('base_url', $base_url);
		$view->assign('search_fields', $search_fields);
		$view->assign('advanced_search_fields', $advanced_search_fields);
		// --------------------------------------------------------------------------------------------
		
		$this->session->set_userdata('source_page', array());

		$view->display('frontend/index.tpl');
	}

	public function listings($seo_title, $referer_id = null)
	{
		$this->output->enable_profiler($this->config->item('debug_mode'));

		$this->load->model('types', 'types_levels');
		$view = $this->load->view();

		$listing = registry::get('current_listing');
		$view->assign('listing', $listing);
		$view->assign('current_type', $listing->type);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Select types and levels in hierarchical array
		// --------------------------------------------------------------------------------------------
		$types = $this->types->getTypesLevels();
		$view->assign('types', $types);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Assign images, videos, files
		// --------------------------------------------------------------------------------------------
		if ($images = $listing->getAssignedImages()) {
			// Logo will be assigned as first image in the gallery
			if ($listing->level->logo_enabled && $listing->logo_file) {
				$images = array_merge(array($listing->logoImage()), $images);
			}
		}
		$view->assign('images', $images);
		
		$videos = $listing->getAssignedVideos();
		$view->assign('videos', $videos);
		
		$files = $listing->getAssignedFiles();
		$view->assign('files', $files);
		
		$available_options_count = 0;
		if ($listing->type->locations_enabled && $listing->locations_count())
			$available_options_count++;
		if ($listing->level->video_count && $videos)
			$available_options_count++;
		if ($listing->level->files_count && $files)
			$available_options_count++;
		if ($listing->level->reviews_mode != 'disabled')
			$available_options_count++;
		$view->assign('available_options_count', $available_options_count);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Build meta information fields of page
		// --------------------------------------------------------------------------------------------
		$view->assign('title', $listing->title());
		$view->assign('description', $listing->listing_meta_description);
		$view->assign('keywords', $listing->listing_keywords);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Search block settings
		// --------------------------------------------------------------------------------------------
		if ($listing->type->search_type == 'global') {
			$group_name = GLOBAL_SEARCH_GROUP_CUSTOM_NAME;
			$search_group_id = 0;
			$cache_index = GLOBAL_SEARCH_GROUP_CUSTOM_NAME;
			$base_url = site_url('search/');
		} else {
			$group_name = LOCAL_SEARCH_GROUP_CUSTOM_NAME;
			$search_group_id = $listing->type->id;
			$cache_index = LOCAL_SEARCH_GROUP_CUSTOM_NAME . '_' . $listing->type->id;
			$base_url = site_url('search/search_type/' . $listing->type->seo_name . '/');
			$view->assign('base_url', $base_url);
		}

		$search_fields = new searchContentFields($group_name, $search_group_id);
		$advanced_search_fields = new searchContentFields($group_name, $search_group_id, 'advanced');

		$view->assign('search_fields', $search_fields);
		$view->assign('advanced_search_fields', $advanced_search_fields);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// We may show 'prev/next listing' buttons by referer
		// and breadcrumbs building
		// --------------------------------------------------------------------------------------------
		$breadcrumbs_collection = new breadcrumbs;
		if ($referer_id) {
			$paginator = new pagination();
			$paginator->setRefererId($referer_id);
			$view->assign('search_results_paginator', $paginator);

			if ($params = $this->listings->getSearchParamsById($referer_id)) {
				$breadcrumbs_collection->setRefererParams($params);
				$breadcrumbs = $breadcrumbs_collection->buildBreadcrumbsFromReferer();
			}
		} else {
			$breadcrumbs = $breadcrumbs_collection->buildBreadcrumbsFromListing($listing);
		}
		if (isset($breadcrumbs) && $breadcrumbs)
			$view->assign('breadcrumbs', $breadcrumbs);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// We may choose special template for level, if not exist - for type, if not exist - standart template
		// --------------------------------------------------------------------------------------------
		if ($view->template_exists('frontend/listing_page-level-' . $listing->level->id . '.tpl')) {
			$template = 'frontend/listing_page-level-' . $listing->level->id . '.tpl';
		} elseif ($view->template_exists('frontend/listing_page-type-' . $listing->type->id . '.tpl')) {
			$template = 'frontend/listing_page-type-' . $listing->type->id . '.tpl';
		} else {
			$template = 'frontend/listing_page.tpl';
		}
		// --------------------------------------------------------------------------------------------
		
		$view->assign('canonical_url', site_url($listing->url()));
		
		$view->display($template);
	}

	public function download($file_id)
	{
		$this->load->model('files', 'listings');
		if (!($file = $this->files->getFileById($file_id))) {
			exit(0);
		}

		$users_content_server_path = rtrim($this->config->item('users_content_server_path'), '/');
		$users_content_settings = $this->config->item('users_content');
		// Read the file's contents
		if (is_file($file->file))
			$data = file_get_contents($file->file);
		elseif (is_file($users_content_server_path . '/' . $users_content_settings['listing_file']['upload_to'] . $file->file)) {
			$data = file_get_contents($users_content_server_path . '/' . $users_content_settings['listing_file']['upload_to'] . $file->file);
		}

		if ($file->file_format != 'undefined') {
			$name = $file->title . '.' . $file->file_format;
		} else {
			$info = pathinfo($file->file);
			$name = $file->title . '.' . $info['extension'];
		}

		$this->load->helper('download');
		force_download($name, $data);
	}
	
	public function print_listing($seo_title)
	{
		$this->load->model('types', 'types_levels');
		$view = $this->load->view();

		$listing = registry::get('current_listing');
		$view->assign('listing', $listing);
		$view->assign('current_type', $listing->type);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Select types and levels in hierarchical array
		// --------------------------------------------------------------------------------------------
		$types = $this->types->getTypesLevels();
		$view->assign('types', $types);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Assign images, videos, files
		// --------------------------------------------------------------------------------------------
		if ($images = $listing->getAssignedImages()) {
			if ($listing->level->logo_enabled && $listing->logo_file) {
				$logo_image = new listingImage();
				$logo_image->setImageFromArray(array('title' => $listing->title(), 'file' => $listing->logo_file));
				$images = array_merge(array($logo_image), $images);
			}
		}
		$view->assign('images', $images);
		
		$videos = $listing->getAssignedVideos();
		$view->assign('videos', $videos);
		
		$files = $listing->getAssignedFiles();
		$view->assign('files', $files);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Build meta information fields of page
		// --------------------------------------------------------------------------------------------
		$view->assign('title', $listing->title());
		// --------------------------------------------------------------------------------------------

		$view->display('frontend/listing_print.tpl');
	}
	
	public function listing_section($listing_id, $section_name)
	{
		$this->load->model('listings', 'listings');
		$listing = $this->listings->getListingById($listing_id);
		$view = $this->load->view();
		$view->assign('listing', $listing);

		switch ($section_name) {
			case 'images':
				$images = $listing->getAssignedImages();
				$view->assign('images', $images);
				$view->display('frontend/listing_images.tpl');
				break;

			case 'reviews':
				$view->display('frontend/listing_reviews.tpl');
				break;

			case 'map':
				$view->display('frontend/listing_map.tpl');
				break;

			case 'videos':
				$videos = $listing->getAssignedVideos();
				$view->assign('videos', $videos);
				$view->display('frontend/listing_videos.tpl');
				break;

			case 'files':
				$files = $listing->getAssignedFiles();
				$view->assign('files', $files);
				$view->display('frontend/listing_files.tpl');
				break;
		}
	}
	
	public function users($seo_title)
	{
		$this->output->enable_profiler($this->config->item('debug_mode'));

		$this->load->model('users', 'users');
		$this->load->model('settings', 'settings');
		$this->load->model('types', 'types_levels');
		$view = $this->load->view();

		// --------------------------------------------------------------------------------------------
		// Get user by its seo login or ID
		// --------------------------------------------------------------------------------------------
		/*if (!($user = $this->users->getUserByUrl($seo_title))) {
			exit("This user doesn't exist!");
		}
		$view->assign('user', $user);
		registry::set('current_user', $user);*/

		$user = registry::get('current_user');
		$view->assign('user', $user);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Build meta information fields of page
		// --------------------------------------------------------------------------------------------
		$view->assign('title', $user->login);
		if ($user->users_group->meta_enabled && $user->meta_description) {
			$view->assign('description', $user->meta_description);
			$view->assign('keywords', $user->meta_keywords);
		}
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Select types and levels in hierarchical array
		// --------------------------------------------------------------------------------------------
		$types = $this->types->getTypesLevels();
		$view->assign('types', $types);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Get frontend settings - listings views object
		// --------------------------------------------------------------------------------------------
		$listings_views = $this->settings->selectViewsByTypes($types);
		$view->assign('listings_views', $listings_views);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Search block settings
		// --------------------------------------------------------------------------------------------
		$base_url = site_url('search/');
		$search_fields = new searchContentFields(GLOBAL_SEARCH_GROUP_CUSTOM_NAME, 0);
		$advanced_search_fields = new searchContentFields(GLOBAL_SEARCH_GROUP_CUSTOM_NAME, 0, 'advanced');

		$view->assign('base_url', $base_url);
		$view->assign('search_fields', $search_fields);
		$view->assign('advanced_search_fields', $advanced_search_fields);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// We may choose special template for users group, if not exist - standart template
		// --------------------------------------------------------------------------------------------
		if ($view->template_exists('frontend/user_page-' . $user->users_group->id . '.tpl')) {
			$template = 'frontend/user_page-' . $user->users_group->id . '.tpl';
		} else {
			$template = 'frontend/user_page.tpl';
		}
		// --------------------------------------------------------------------------------------------
		
		$view->display($template);
	}
	
	public function print_user($seo_title)
	{
		$this->load->model('users', 'users');
		$this->load->model('settings', 'settings');
		$this->load->model('types', 'types_levels');
		$view = $this->load->view();
		
		// --------------------------------------------------------------------------------------------
		// Select types and levels in hierarchical array
		// --------------------------------------------------------------------------------------------
		$types = $this->types->getTypesLevels();
		$view->assign('types', $types);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Get frontend settings - listings views object
		// --------------------------------------------------------------------------------------------
		$listings_views = $this->settings->selectViewsByTypes($types);
		$view->assign('listings_views', $listings_views);
		// --------------------------------------------------------------------------------------------

		$user = registry::get('current_user');
		$view->assign('user', $user);
		// --------------------------------------------------------------------------------------------

		$view->display('frontend/user_print.tpl');
	}

	public function types($location, $seo_name, $argsString = '')
	{
		$this->output->enable_profiler($this->config->item('debug_mode'));
		
		$args = parseUrlArgs($argsString);
		$system_settings = registry::get('system_settings');
		
		$this->load->model('types', 'types_levels');
		$this->load->model('settings', 'settings');
		$this->load->model('listings', 'listings');
		$view = $this->load->view();

		$type = registry::get('current_type');
		$view->assign('type', $type);
		$view->assign('current_type', $type);

		// --------------------------------------------------------------------------------------------
		// Select types and levels in hierarchical array
		// --------------------------------------------------------------------------------------------
		$types = $this->types->getTypesLevels();
		$view->assign('types', $types);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Get frontend settings - listings views object
		// --------------------------------------------------------------------------------------------
		$listings_views = $this->settings->selectViewsByTypes($types);
		$listings_view = $listings_views->getViewByTypeIdAndPage($type->id, 'types');
		$view->assign('listings_views', $listings_views);
		$view->assign('view', $listings_view);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Orderby and its direction pagination block
		// --------------------------------------------------------------------------------------------
		if (isset($args['orderby']) && isset($args['direction'])) {
			$orderby = $args['orderby'];
			$direction = $args['direction'];
			$suborderby = false;
			$subdirection = false;
		} else {
			$orderby = $listings_view->order_by;
			$direction = $listings_view->order_direction;
			$suborderby = $listings_view->suborder_by;
			$subdirection = $listings_view->suborder_direction;
		}
		$view->assign('orderby', $orderby);
		$view->assign('direction', $direction);
		$view->assign('suborderby', $suborderby);
		$view->assign('subdirection', $subdirection);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Search block settings
		// --------------------------------------------------------------------------------------------
		if ($type->search_type == 'global') {
			$group_name = GLOBAL_SEARCH_GROUP_CUSTOM_NAME;
			$search_group_id = 0;
		} else {
			$group_name = LOCAL_SEARCH_GROUP_CUSTOM_NAME;
			$search_group_id = $type->id;
			$base_url = site_url("search/search_type/" . $type->seo_name);
			$view->assign('base_url', $base_url);
		}

		$search_fields = new searchContentFields($group_name, $search_group_id);
		$advanced_search_fields = new searchContentFields($group_name, $search_group_id, 'advanced');

		$view->assign('search_fields', $search_fields);
		$view->assign('advanced_search_fields', $advanced_search_fields);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Select listings from DB
		// --------------------------------------------------------------------------------------------
		$paginator_url = site_url($type->getUrl());
		$paginator = new pagination(array('args' => $args, 'url' => $paginator_url, 'num_per_page' => $listings_view->getListingsNumberFromFormat()));
		$this->listings->setPaginator($paginator);
		
		$select_args = array('page_name' => 'types', 'search_type' => $type, 'search_status' => 1, 'search_users_status' => 2, 'search_levels' => $listings_view->levels_visible, 'search_location' => registry::get('current_location'));
		$paginator->setRefererId($this->listings->storeSearchParams('types', uri_string(), $select_args, false, $search_group_id, $group_name, $orderby, $direction));
		$listings = $this->listings->selectListings($select_args, $orderby, $direction, $suborderby, $subdirection);
		// --------------------------------------------------------------------------------------------

		$view->assign('listings_paginator', $paginator);
		$view->assign('listings', $listings);
		$view->assign('order_url', $paginator_url);

		// --------------------------------------------------------------------------------------------
		// Build meta information fields of page
		// --------------------------------------------------------------------------------------------
		if ($type->meta_title)
			$view->assign('title', $type->meta_title);
		else 
			$view->assign('title', $type->name);
		if ($type->meta_description)
			$view->assign('description', $type->meta_description);
		//$view->assign('keywords', $type->listing_keywords);
		// --------------------------------------------------------------------------------------------
		
		$view->assign('canonical_url', $paginator_url);
		
		//$this->session->set_userdata('source_page', array(current_url() => $type->name));

		$view->display('frontend/types_page.tpl');
	}

	public function categories($location, $type_seo_name = '', $argsString = '')
	{
		$this->output->enable_profiler($this->config->item('debug_mode'));
		
		$system_settings = registry::get('system_settings');

		$this->load->model('types', 'types_levels');
		$this->load->model('settings', 'settings');
		$this->load->model('listings', 'listings');
		$view = $this->load->view();

		if (!($type = registry::get('current_type')))
			$current_type_id = 0;
		else
			$current_type_id = $type->id;
		$category = registry::get('current_category');
		$view->assign('type', $type);
		$view->assign('current_type', $type);
		$view->assign('current_category', $category);
		
		// --------------------------------------------------------------------------------------------
		if ($type_seo_name)
			$argsString = str_replace('type/' . $type_seo_name . '/', '', $argsString);
		$array = $category->getChainAsArray();
		foreach ($array AS $category) {
			$chain[] = $category->seo_name;
		}
		$argsString = str_replace(implode('/', $chain), '', $argsString);
		$args = parseUrlArgs($argsString);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Select types and levels in hierarchical array
		// --------------------------------------------------------------------------------------------
		$types = $this->types->getTypesLevels();
		$view->assign('types', $types);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Get frontend settings - listings views object
		// --------------------------------------------------------------------------------------------
		$listings_views = $this->settings->selectViewsByTypes($types);
		$listings_view = $listings_views->getViewByTypeIdAndPage($current_type_id, 'categories');
		$view->assign('listings_views', $listings_views);
		$view->assign('view', $listings_view);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Orderby and its direction pagination block
		// --------------------------------------------------------------------------------------------
		if (isset($args['orderby']) && isset($args['direction'])) {
			$orderby = $args['orderby'];
			$direction = $args['direction'];
			$suborderby = false;
			$subdirection = false;
		} else {
			$orderby = $listings_view->order_by;
			$direction = $listings_view->order_direction;
			$suborderby = $listings_view->suborder_by;
			$subdirection = $listings_view->suborder_direction;
		}
		$view->assign('orderby', $orderby);
		$view->assign('direction', $direction);
		$view->assign('suborderby', $suborderby);
		$view->assign('subdirection', $subdirection);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Search block settings
		// --------------------------------------------------------------------------------------------
		if (!$type || $type->search_type == 'global') {
			$group_name = GLOBAL_SEARCH_GROUP_CUSTOM_NAME;
		} else {
			$group_name = LOCAL_SEARCH_GROUP_CUSTOM_NAME;
			$base_url = site_url("search/search_type/" . $type->seo_name);
			$view->assign('base_url', $base_url);
		}

		$search_fields = new searchContentFields($group_name, $current_type_id);
		$advanced_search_fields = new searchContentFields($group_name, $current_type_id, 'advanced');

		$view->assign('search_fields', $search_fields);
		$view->assign('advanced_search_fields', $advanced_search_fields);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Select listings from DB
		// --------------------------------------------------------------------------------------------
		$paginator_url = site_url($category->getUrl());
		$paginator = new pagination(array('args' => $args, 'url' => $paginator_url, 'num_per_page' => $listings_view->getListingsNumberFromFormat()));
		$this->listings->setPaginator($paginator);
		
		$select_args = array('page_name' => 'categories', 'search_type' => $type, 'search_category' => $category->id, 'search_status' => 1, 'search_users_status' => 2, 'search_location' => registry::get('current_location'));
		$paginator->setRefererId($this->listings->storeSearchParams('categories', uri_string(), $select_args, false, $current_type_id, $group_name, $orderby, $direction));
		$listings = $this->listings->selectListings($select_args, $orderby, $direction, $suborderby, $subdirection, array());
		
		$view->assign('listings_paginator', $paginator);
		$view->assign('listings', $listings);
		$view->assign('order_url', $paginator_url);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Build meta information fields of page
		// --------------------------------------------------------------------------------------------
		if ($category->meta_title)
			$view->assign('title', $category->meta_title);
		else 
			$view->assign('title', $category->name);
		if ($category->meta_description)
			$view->assign('description', $category->meta_description);
		//$view->assign('keywords', $type->listing_keywords);
		// --------------------------------------------------------------------------------------------
		
		$view->assign('canonical_url', $paginator_url);
		
		$breadcrumbs_objects = $category->getChainAsArray();
		array_pop($breadcrumbs_objects);
		$breadcrumbs = array();
		foreach ($breadcrumbs_objects AS $crumb_category)
			$breadcrumbs[site_url($crumb_category->getUrl())] = $crumb_category->name;
		$view->assign('breadcrumbs', $breadcrumbs);
		
		//$this->session->set_userdata('source_page', array(current_url() => $category->name));

		$view->display('frontend/categories_page.tpl');
	}

	public function search($argsString = '')
	{
		$this->output->enable_profiler($this->config->item('debug_mode'));

		$args = parseUrlArgs($argsString);

		$system_settings = registry::get('system_settings');

		$this->load->model('settings', 'settings');
		$this->load->model('types', 'types_levels');
		$this->load->model('listings', 'listings');
		$view = $this->load->view();
		$view->assign('args', $args);
		
		// --------------------------------------------------------------------------------------------
		// Select types and levels in hierarchical array
		// --------------------------------------------------------------------------------------------
		$types = $this->types->getTypesLevels();
		$view->assign('types', $types);
		// --------------------------------------------------------------------------------------------
		
		if (isset($args['search_type'])) {
			$type = registry::get('current_type');
			
			$search_type_seoname = $args['search_type'];
			$view->assign('current_type', $type);
			$search_type_id = $type->id;
		} else {
			$search_type_id = 0;
			$type = null;
		}

		if (isset($args['use_advanced'])) {
			$use_advanced = true;
		} else {
			$use_advanced = false;
		}

		if (isset($args['search_category'])) {
			$categories_ids = array_filter(explode(',', urldecode(html_entity_decode($args['search_category']))));
			array_walk($categories_ids, "trim");
			$view->assign('search_categories_array', $categories_ids);
		}

		// --------------------------------------------------------------------------------------------
		// Store search params into registry (without params of content fields!)
		// --------------------------------------------------------------------------------------------
		$search_url = registry::get('current_search_params_url');
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Get frontend settings - listings views object
		// --------------------------------------------------------------------------------------------
		$listings_views = $this->settings->selectViewsByTypes($types);
		$listings_view = $listings_views->getViewByTypeIdAndPage($search_type_id, 'search');
		$view->assign('listings_views', $listings_views);
		$view->assign('view', $listings_view);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Orderby and its direction
		// --------------------------------------------------------------------------------------------
		if (isset($args['orderby']) && isset($args['direction'])) {
			$orderby = $args['orderby'];
			$direction = $args['direction'];
			$suborderby = false;
			$subdirection = false;
		} else {
			if ((!isset($args['where_radius']) || !$args['where_radius']) || ((!isset($args['where_search']) || !$args['where_search']) && (!isset($args['predefined_location_id']) || !$args['predefined_location_id']))) {
				$orderby = $listings_view->order_by;
				$direction = $listings_view->order_direction;
				$suborderby = $listings_view->suborder_by;
				$subdirection = $listings_view->suborder_direction;
			} else {
				// When search by radius - by default we must order by distance from center of search 
				$orderby = 'distance';
				$direction = 'asc';
				$suborderby = false;
				$subdirection = false;
			}
		}
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Search block settings
		// --------------------------------------------------------------------------------------------
		if (!$type || $type->search_type == 'global') {
			$group_name = GLOBAL_SEARCH_GROUP_CUSTOM_NAME;
		} else {
			$group_name = LOCAL_SEARCH_GROUP_CUSTOM_NAME;
			$base_url = site_url("search/search_type/" . $search_type_seoname);
			$view->assign('base_url', $base_url);
		}

		$search_fields = new searchContentFields($group_name, $search_type_id);
		$advanced_search_fields = new searchContentFields($group_name, $search_type_id, 'advanced');

		$search_sql_array = $search_fields->validateSearch(LISTINGS_LEVEL_GROUP_CUSTOM_NAME, $args, $use_advanced, $search_url_rebuild);
		$search_url .= $search_url_rebuild;
		registry::set('current_search_params_url', $search_url);
		$view->assign('search_fields', $search_fields);
		$view->assign('advanced_search_fields', $advanced_search_fields);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Select listings from DB
		// --------------------------------------------------------------------------------------------
		$paginator = new pagination(array('args' => $args, 'url' => site_url('search/' . $search_url), 'num_per_page' => $listings_view->getListingsNumberFromFormat()));
		$this->listings->setPaginator($paginator);

		$full_search_args = array_merge($args, array('page_name' => 'search', 'search_type' => $type, 'search_status' => 1, 'search_users_status' => 2, 'search_location' => registry::get('current_location')));
		$paginator->setRefererId($this->listings->storeSearchParams('search', uri_string(), $full_search_args, $use_advanced, $search_type_id, $group_name, $orderby, $direction));
		$listings = $this->listings->selectListings($full_search_args, $orderby, $direction, $suborderby, $subdirection, $search_sql_array);

		$view->assign('listings_paginator', $paginator);
		$view->assign('listings', $listings);
		$view->assign('order_url', site_url('search/' . $search_url));
		$view->assign('orderby', $orderby);
		$view->assign('direction', $direction);
		$view->assign('suborderby', $suborderby);
		$view->assign('subdirection', $subdirection);
		// --------------------------------------------------------------------------------------------
		
		$view->assign('canonical_url', site_url('search/' . $search_url));

		//$this->session->set_userdata('source_page', array(current_url() => LANG_BREADCRUMBS_SEARCH));

		$view->display('frontend/search_page.tpl');
	}

	/**
	 * This method processes additional content pages:
	 * 1. by seo name of node - so we will call propper content page
	 * 2. by existing template 'frontend/node-$node_url.tpl' - will call only that template (content page doesn't required) 
	 * 
	 * @param string $node_url
	 */
	public function node($node_url)
	{
		$this->load->model('content_pages', 'content_pages');
		$this->load->model('types', 'types_levels');
		$view = $this->load->view();

		// --------------------------------------------------------------------------------------------
		// Select types and levels in hierarchical array
		// --------------------------------------------------------------------------------------------
		$types = $this->types->getTypesLevels();
		$view->assign('types', $types);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Search block settings
		// --------------------------------------------------------------------------------------------
		$search_fields = new searchContentFields(GLOBAL_SEARCH_GROUP_CUSTOM_NAME, 0);
		$advanced_search_fields = new searchContentFields(GLOBAL_SEARCH_GROUP_CUSTOM_NAME, 0, 'advanced');
		
		$view->assign('search_fields', $search_fields);
		$view->assign('advanced_search_fields', $advanced_search_fields);
		// --------------------------------------------------------------------------------------------
		
		if ($node_id = $this->content_pages->getNodeIdByUrl($node_url)) {
			// Node by SeoName
			$this->content_pages->setNodeId($node_id);
	
			$node = $this->content_pages->getNodeById();
			$view->assign('node', $node);
	
			if ($node->meta_title)
				$view->assign('title', $node->meta_title);
			else
				$view->assign('title', $node->title);
			if ($node->meta_description)
				$view->assign('description', $node->meta_description);

			if ($view->template_exists('frontend/node-' . $node_id . '.tpl')) {
	        	$template = 'frontend/node-' . $node_id . '.tpl';
	        } else {
	        	$template = 'frontend/node.tpl';
	        }
	        $view->display($template);
		} elseif ($view->template_exists('frontend/node-' . $node_url . '.tpl')) {
			// Node by template
			$template = 'frontend/node-' . $node_url . '.tpl';
			$view->display($template);
		} else 
			show_404();
	}

	public function quick_list($argsString = '')
	{
		$this->output->enable_profiler($this->config->item('debug_mode'));
		
		$args = parseUrlArgs($argsString);
		
		$system_settings = registry::get('system_settings');

		$this->load->model('types', 'types_levels');
		$this->load->model('listings', 'listings');
		$this->load->model('settings', 'settings');
		$view = $this->load->view();

		// --------------------------------------------------------------------------------------------
		// Select types and levels in hierarchical array
		// --------------------------------------------------------------------------------------------
		$types = $this->types->getTypesLevels();
		$view->assign('types', $types);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Get frontend settings - listings views object
		// --------------------------------------------------------------------------------------------
		$listings_views = $this->settings->selectViewsByTypes($types);
		$listings_view = $listings_views->getViewByTypeIdAndPage(0, 'quicklist');
		$view->assign('listings_views', $listings_views);
		$view->assign('view', $listings_view);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Orderby and its direction
		// --------------------------------------------------------------------------------------------
		if (isset($args['orderby']) && isset($args['direction'])) {
			$orderby = $args['orderby'];
			$direction = $args['direction'];
			$suborderby = false;
			$subdirection = false;
		} else {
			$orderby = $listings_view->order_by;
			$direction = $listings_view->order_direction;
			$suborderby = $listings_view->suborder_by;
			$subdirection = $listings_view->suborder_direction;
		}
		$view->assign('orderby', $orderby);
		$view->assign('direction', $direction);
		$view->assign('suborderby', $suborderby);
		$view->assign('subdirection', $subdirection);
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Extract cookies into favourites array
		// --------------------------------------------------------------------------------------------
		$quick_list_ids = checkQuickList();
		// --------------------------------------------------------------------------------------------
		
		// --------------------------------------------------------------------------------------------
		// Select listings from DB
		// --------------------------------------------------------------------------------------------
		$listings = array();
		$paginator_url = site_url('quick_list/');
		$paginator = new pagination(array('args' => $args, 'url' => $paginator_url, 'num_per_page' => LISTINGS_PER_PAGE_ON_QUICKLIST_PAGE));
		if ($quick_list_ids) {
			$this->listings->setPaginator($paginator);
			
			$select_args = array('search_by_ids' => $quick_list_ids, 'search_status' => 1, 'search_users_status' => 2);
			$paginator->setRefererId($this->listings->storeSearchParams('quicklist', uri_string(), $select_args, false, 0, GLOBAL_SEARCH_GROUP_CUSTOM_NAME, $orderby, $direction));
			$listings = $this->listings->selectListings($select_args, $orderby, $direction, $suborderby, $subdirection);
		}
		$view->assign('order_url', $paginator_url);
		$view->assign('listings_paginator', $paginator);
		$view->assign('listings', $listings);
		// --------------------------------------------------------------------------------------------

		// --------------------------------------------------------------------------------------------
		// Search block settings
		// --------------------------------------------------------------------------------------------
		$search_fields = new searchContentFields(GLOBAL_SEARCH_GROUP_CUSTOM_NAME, 0);
		$advanced_search_fields = new searchContentFields(GLOBAL_SEARCH_GROUP_CUSTOM_NAME, 0, 'advanced');

		$view->assign('search_fields', $search_fields);
		$view->assign('advanced_search_fields', $advanced_search_fields);
		// --------------------------------------------------------------------------------------------

		//$this->session->set_userdata('source_page', array(current_url() => LANG_QUICK_LIST));

		$view->display('frontend/quick_list.tpl');
	}
	
	public function advertise()
	{
		$this->load->model('types', 'types_levels');
		$view = $this->load->view();

		// --------------------------------------------------------------------------------------------
		// Select types and levels in hierarchical array
		// --------------------------------------------------------------------------------------------
		$types = $this->types->getTypesLevels();

		// Levels listings prices
		if ($this->session->userdata('user_id'))
	    	$listings_prices = registry::get('listings_prices');
	    else 
	    	$listings_prices = registry::get('default_listings_prices');

		if ($listings_prices) {
			foreach ($types AS $type_id=>$type) {
				foreach ($type->levels AS $level_id=>$level) {
					foreach ($listings_prices AS $price) {
						if ($level->id == $price['level_id']) {
							$types[$type_id]->levels[$level_id]->setPrice($price['value'], $price['currency']);
						}
					}
				}
			}
		}
		
		if ($this->load->is_module_loaded('packages')) {
			$this->load->model('packages', 'packages');
			$packages = $this->packages->getAllPackages();
			if ($this->session->userdata('user_id'))
				$packages_prices = registry::get('packages_prices');
			else
				$packages_prices = registry::get('default_packages_prices');
			$view->assign('packages', $packages);
			$view->assign('packages_prices', $packages_prices);
		}

		$view->assign('types', $types);
		$view->assign('listings_prices', $listings_prices);
		$view->display('frontend/advertise_with_us.tpl');
	}
}
?>