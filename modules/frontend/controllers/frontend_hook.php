<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function getCurrentType($CI)
{
	$current_route_args = registry::get('current_route_args');
	$location = $current_route_args[0];
	$seo_name = $current_route_args[1];
	
	$CI->load->model('types', 'types_levels');
	
	if (!$type = $CI->types->getTypeBySeoName($seo_name))
		exit("This type doesn't exist!");

	registry::set('current_type', $type);
}

function getCurrentCategory($CI)
{
	$current_route_args = registry::get('current_route_args');
	$location = $current_route_args[0];
	$type_seo_name = $current_route_args[1];
	$argsString = $current_route_args[2];

	$CI->load->model('categories', 'categories');
	$CI->load->model('types', 'types_levels');

	if ($type_seo_name) {
		// Local
		if (!($type = $CI->types->getTypeBySeoName($type_seo_name))) {
			exit(0);
		}
		registry::set('current_type', $type);
		$CI->categories->setTypeId($type->id);

		$argsString = str_replace('type/' . $type_seo_name . '/', '', $argsString);
	}
	$categories_db = $CI->categories->selectCategoriesFromDB();
	$selected_categories_chain = array();
	$a = explode('/', $argsString);
	foreach ($a AS $category_seo_name) {
		foreach ($categories_db AS $category_row) {
			if ($category_row['seo_name'] == $category_seo_name) {
	   			$selected_categories_chain[] = $category_row['seo_name'];
	   		}
		}
	}
	
	if (!$selected_categories_chain)
		exit("This category doesn't exist!");

	if ($category = $CI->categories->getCategoryBySeoName(implode('/', $selected_categories_chain))) {
		$category->buildChildren();
		registry::set('current_category', $category);
	}
}

function getCurrentListing($CI)
{
	$current_route_args = registry::get('current_route_args');
	$seo_title = $current_route_args[0];

	$CI->load->model('listings', 'listings');

	// --------------------------------------------------------------------------------------------
	// Get listing by its seo name or ID
	// --------------------------------------------------------------------------------------------
	if (!($listing_row = $CI->listings->getListingRowByUrl($seo_title))) {
		exit("This listing doesn't exist or wasn't translated!");
	}
	$listing_id = $listing_row['id'];
	$listing = $CI->listings->getListingById($listing_id, false);
	if ($listing->status != 1) {
		exit('This listing was blocked or active period had expired!');
	}
	if ($listing->user->status != 2) {
		exit('The owner of listing was blocked or unverified!');
	}
	if (count($listing->categories_array()) == 1) {
		$CI->load->model('categories', 'categories');
		$one_category = array_pop($listing->categories_array());
		$CI->categories->setTypeId($listing->type->id);
		$current_category = $CI->categories->getCategoryById($one_category->id);
		registry::set('current_category', $current_category);
	}

	registry::set('current_listing', $listing);
	registry::set('current_type', $listing->type);
}

function getCurrentUser($CI)
{
	$current_route_args = registry::get('current_route_args');
	$seo_title = $current_route_args[0];
	
	$CI->load->model('users', 'users');
	
	if (!($user = $CI->users->getUserByUrl($seo_title))) {
		exit("This user doesn't exist!");
	}
	registry::set('current_user', $user);
}

function getCurrentSearchParams($CI)
{
	$current_route_args = registry::get('current_route_args');
	$argsString = $current_route_args[0];
	
	$args = parseUrlArgs($argsString);

	$search_url = '';
	if (isset($args['what_search'])) {
		$what_search = $args['what_search'];
		$search_url .= 'what_search/' . $what_search . '/';
	}
	if (isset($args['what_match'])) {
		$what_match = $args['what_match'];
		$search_url .= 'what_match/' . $what_match . '/';
	}
	if (isset($args['where_search'])) {
		$where_search = $args['where_search'];
		$search_url .= 'where_search/' . $where_search . '/';
	}
	if (isset($args['zip_search'])) {
		$zip_search = $args['zip_search'];
		$search_url .= 'zip_search/' . $zip_search . '/';
	}
	if (isset($args['predefined_location_id'])) {
		$predefined_location_id = $args['predefined_location_id'];
		$search_url .= 'predefined_location_id/' . $predefined_location_id . '/';
	}
	if (isset($args['where_radius'])) {
		$where_radius = $args['where_radius'];
		$search_url .= 'where_radius/' . $where_radius . '/';
	}
	if (isset($args['search_type'])) {
		$CI->load->model('types', 'types_levels');
		if (is_numeric($args['search_type'])) {
			$search_type_id = $args['search_type'];
			$search_url .= 'search_type/' . $search_type_id . '/';
	
			if (!($type = $CI->types->getTypeById($search_type_id))) {
				exit('No such ID of type!');
			}
		} else {
			$search_type_seoname = $args['search_type'];
			$search_url .= 'search_type/' . $search_type_seoname . '/';

			if (!($type = $CI->types->getTypeBySeoName($search_type_seoname))) {
				exit('No such seo name of type!');
			}
			$search_type_id = $type->id;
		}
		registry::set('current_type', $type);
	} else {
		$type = null;
	}

	if (isset($args['use_advanced'])) {
		$use_advanced = true;
		$search_url .= 'use_advanced/true/';
	} else {
		$use_advanced = false;
	}
	
	if (isset($args['search_claimed_listings'])) {
		$search_claimed_listings = $args['search_claimed_listings'];
		$search_url .= 'search_claimed_listings/' . $search_claimed_listings . '/';
	}

	if (isset($args['search_category'])) {
		$search_category = $args['search_category'];
		$search_url .= 'search_category/' . $search_category . '/';
		
		$categories_ids = array_filter(explode(',', urldecode(html_entity_decode($args['search_category']))));
		array_walk($categories_ids, "trim");
		
		if (count($categories_ids) == 1) {
			$CI->load->model('categories', 'categories');
			if (isset($search_type_id))
				$CI->categories->setTypeId($search_type_id);
			$current_category = $CI->categories->getCategoryById(array_pop($categories_ids));
			registry::set('current_category', $current_category);
		}
	}
	if (isset($args['search_owner'])) {
		$search_owner = $args['search_owner'];
		$search_url .= 'search_owner/' . $search_owner . '/';
	}
	if (isset($args['search_status'])) {
		$search_status = $args['search_status'];
		$search_url .= 'search_status/' . $search_status . '/';
	}
	if (isset($args["search_order_date"])) {
		$search_order_date = $args['search_order_date'];
		$search_url .= 'search_order_date/' . $search_order_date . '/';
	}
	if (isset($args['search_from_order_date'])) {
		$search_from_order_date = $args['search_from_order_date'];
		$search_url .= 'search_from_order_date/' . $search_from_order_date . '/';
	}
	if (isset($args['search_to_order_date'])) {
		$search_to_order_date = $args['search_to_order_date'];
		$search_url .= 'search_to_order_date/' . $search_to_order_date . '/';
	}
	if (isset($args["search_creation_date"])) {
		$search_creation_date = $args['search_creation_date'];
		$search_url .= 'search_creation_date/' . $search_creation_date . '/';
	}
	if (isset($args['search_from_creation_date'])) {
		$search_from_creation_date = $args['search_from_creation_date'];
		$search_url .= 'search_from_creation_date/' . $search_from_creation_date . '/';
	}
	if (isset($args['search_to_creation_date'])) {
		$search_to_creation_date = $args['search_to_creation_date'];
		$search_url .= 'search_to_creation_date/' . $search_to_creation_date . '/';
	}
	
	registry::set('current_search_params_url', $search_url);
	registry::set('current_search_params_array', $args);
}

function checkQuickList()
{
	if (isset($_COOKIE['favourites']))
		$favourites = explode('*', $_COOKIE['favourites']);
	else
		$favourites = array();
	$favourites = array_values(array_filter($favourites));
	$favourites_array = array();
	foreach ($favourites AS $listing_id)
		if (is_numeric($listing_id))
			$favourites_array[] = $listing_id;
	return $favourites_array;
}

function checkIsRegistrationAllowed()
{
	$CI = &get_instance();
	$CI->load->model('users_groups', 'users');
	$all_users_groups = $CI->users_groups->getUsersGroups();

	$users_groups_allowed = array();
	foreach ($all_users_groups AS $group_item)
		if ($group_item->may_register)
			$users_groups_allowed[$group_item->id] = $group_item;

	return $users_groups_allowed;
}

?>