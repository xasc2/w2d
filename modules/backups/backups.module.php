<?php
class backupsModule
{
	public $title = "Backups";
	public $version = "1.0";
	public $description = "Import/export backups";
	
	public $lang_files = "backups.php";

	public function routes()
	{
		$route['admin/backups/export'] = array(
			'action' => 'export',
			'access' => 'Edit system settings',
			'title' => LANG_EXPORT_BACKUPS_MENU,
		);
		
		$route['admin/backups/import'] = array(
			'action' => 'import',
			'access' => 'Edit system settings',
			'title' => LANG_IMPORT_BACKUPS_MENU,
		);

		return $route;
	}

	public function menu()
	{
		$menu['Backups'] = array(
			'weight' => 151,
			'children' => array(
				LANG_EXPORT_BACKUPS_MENU => array(
					'weight' => 1,
					'url' => 'admin/backups/export/',
					'access' => 'Edit system settings',
				),
				LANG_IMPORT_BACKUPS_MENU => array(
					'weight' => 2,
					'url' => 'admin/backups/import/',
					'access' => 'Edit system settings',
				)
			),
		);

		return $menu;
	}
}
?>