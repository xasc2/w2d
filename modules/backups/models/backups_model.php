<?php

class backupsModel extends model
{
	/**
	 * All zip and sql files from 'backups/' folder
	 */
	public function getExistedBackups()
	{
		$backups = directory_map('backups', true);
		$result_array = array();
		foreach ($backups AS $backup) {
			$backup_file_path = 'backups' . '/' . $backup;
			if (is_file($backup_file_path) && (pathinfo($backup_file_path, PATHINFO_EXTENSION) == 'sql' || pathinfo($backup_file_path, PATHINFO_EXTENSION) == 'zip')) {
				$result_array[] = $backup;
			}
		}
		return $result_array;
	}
	
	public function createBackup($label)
	{
		$system_settings = registry::get('system_settings');
		$backup_file_name = 'backup-v' . $system_settings['W2D_VERSION'] . '-' . date('Ymd') . '-' . $label . '-' . (md5($label));
		if ($this->createMySQLDump($backup_file_name)) {
			$return_backup_file_name = 'backups' . '/' . 'db-'.$backup_file_name . '.sql';

			$folders_to_zip = array(
				$this->config->item('users_content_server_path'),
				LANGPATH
			);
			if ($this->zipFilesFolders($folders_to_zip, $backup_file_name, 'backups' . '/' . $backup_file_name . '.zip'))
				$return_backup_file_name = 'backups' . '/' . $backup_file_name . '.zip';
			// --------------------------------------------------------------------------------------------
	    	// In order to avoid 'MYSQL server has gone away' problem
	    	// --------------------------------------------------------------------------------------------
	    	$this->db->reconnect();
	    	// --------------------------------------------------------------------------------------------
		} else 
			return false;
		return $return_backup_file_name;
	}
	
	/**
	 * accepts sql or zip file inside 'backups/' folder
	 * 
	 * @param string $file_name
	 */
	public function importBackup($file_name)
	{
		$backup_file_path = 'backups' . '/' . $file_name;
		if (pathinfo($backup_file_path, PATHINFO_EXTENSION) == 'zip') {
			if ($this->unzipBackup($this->config->item('users_content_server_path'), $backup_file_path)) {
				$sql_file = 'db-' . basename($backup_file_path, ".zip") . '.sql';
				if (is_file('users_content' . '/' . $sql_file)) {
					if ($this->importMySQLDump('users_content' . '/' . $sql_file))
						unlink('users_content' . '/' . $sql_file);
				}

				$target_folders = array('users_content', 'languages');
				foreach ($target_folders AS $folder)
					$this->recurse_move(ROOT . 'users_content' . '/' . $folder, ROOT . $folder);
				// --------------------------------------------------------------------------------------------
		    	// In order to avoid 'MYSQL server has gone away' problem
		    	// --------------------------------------------------------------------------------------------
		    	$this->db->reconnect();
		    	// --------------------------------------------------------------------------------------------
				return true;
			} else 
				return false;
		} elseif (pathinfo($backup_file_path, PATHINFO_EXTENSION) == 'sql') {
			$this->importMySQLDump($backup_file_path);
			// --------------------------------------------------------------------------------------------
	    	// In order to avoid 'MYSQL server has gone away' problem
	    	// --------------------------------------------------------------------------------------------
	    	$this->db->reconnect();
	    	// --------------------------------------------------------------------------------------------
			return true;
		} else
			return false;
	}
	
	/**
	 * ';#\n' - this is special delimiter to execute multi queries
	 * @param string $backup_file_name
	 */
	public function createMySQLDump($backup_file_name)
	{
		$tables = $this->db->list_tables();
		
		$return = '';
		foreach($tables as $table)
		{
			$result = $this->db->query('SELECT * FROM '.$table);
			$result_array = $result->result_array();
			$num_fields = $result->num_fields();
			
			$return.= "DROP TABLE IF EXISTS ".$table.";#\n";
			$row = $this->db->query('SHOW CREATE TABLE '.$table)->row_array();
			$return.= "\n\n".$row["Create Table"].";#\n\n";
			
			foreach ($result_array AS $row_array) {
				$return.= 'INSERT INTO '.$table.' VALUES(';
				
				$i = 0;
				foreach ($row_array AS $column=>$value) {
					$i++;
					$value = addslashes($value);
					str_replace("\n", "\\n", $value);
					if (isset($value)) { $return.= '"'.$value.'"' ; } else { $return.= '""'; }
					if ($i<$num_fields) { $return.= ','; }
				}
				$return.= ");#\n";
			}
			$return.="\n\n\n";
		}
		  
		if ($return) {
			if (!is_dir('backups'))
				mkdir('backups');
			$handle = fopen('backups' . '/' . 'db-'.$backup_file_name.'.sql', 'w+');
			fwrite($handle, $return);
			return fclose($handle);
		}
	}
	
	public function importMySQLDump($file_path)
	{
		if (is_file($file_path)) {
			// We need to save root user record
			$this->db->select();
			$this->db->from('users');
			$this->db->where('id', 1);
			$root_user_row = $this->db->get()->row_array();
			
			$this->load->plugin('sqlDumpParser');
			$queries = getQueriesFromFile($file_path, "#");
			foreach ($queries AS $query) {
				$this->db->query($query);
			}
			
			// Here root user record will be restored
			$this->db->set('login', $root_user_row['login']);
			$this->db->set('seo_login', $root_user_row['seo_login']);
			$this->db->set('meta_description', $root_user_row['meta_description']);
			$this->db->set('meta_keywords', $root_user_row['meta_keywords']);
			$this->db->set('password', $root_user_row['password']);
			$this->db->set('user_logo_image', $root_user_row['user_logo_image']);
			$this->db->set('email', $root_user_row['email']);
			$this->db->set('registration_date', $root_user_row['registration_date']);
			$this->db->set('registration_hash', $root_user_row['registration_hash']);
			if (isset($root_user_row['facebook_uid']))
				$this->db->set('facebook_uid', $root_user_row['facebook_uid']);
			if (isset($root_user_row['use_facebook_logo']))
				$this->db->set('use_facebook_logo', $root_user_row['use_facebook_logo']);
			if (isset($root_user_row['facebook_logo_file']))
				$this->db->set('facebook_logo_file', $root_user_row['facebook_logo_file']);
			$this->db->where('id', 1);
			$this->db->update('users');
			
			return true;
		}
	}
	
	public function zipFilesFolders($source_folders, $backup_file_name, $destination, $plus_db_dump = true)
	{
		if (!extension_loaded('zip')) {
			return false;
		}
		
		$zip = new ZipArchive();
		if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
			return false;
		}

		foreach ($source_folders AS $source) {
			$source = str_replace('\\', '/', realpath($source));
			if (is_dir($source) === true) {
				$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
				foreach ($files as $file) {
					$file = str_replace('\\', '/', realpath($file));
					if (is_dir($file) === true) {
						$zip->addEmptyDir(basename($source) . '/' . str_replace($source . '/', '', $file . '/'));
					} else if (is_file($file) === true) {
						$zip->addFromString(basename($source) . '/' . str_replace($source . '/', '', $file), file_get_contents($file));
					}
				}
			} else if (is_file($source) === true) {
				$zip->addFromString(basename($source), file_get_contents($source));
			}
		}

		if ($plus_db_dump) {
			$db_dump = 'backups' . '/' . 'db-'.$backup_file_name.'.sql';
			$zip->addFromString(basename($db_dump), file_get_contents($db_dump));
		}
		
		return $zip->close();
	}
	
	public function unzipBackup($target, $backup_file_path)
	{
		if (!extension_loaded('zip') || !file_exists($backup_file_path)) {
			return false;
		}

		$zip = new ZipArchive();
		if (!$zip->open($backup_file_path)) {
			return false;
		}

		$zip->extractTo($target);
		return $zip->close();
	}

	public function checkSystemBackupVersion($file_name)
	{
		$system_settings = registry::get('system_settings');
		
		$backup_file_path = 'backups' . '/' . $file_name;
		if (pathinfo($backup_file_path, PATHINFO_EXTENSION) == 'zip')
			$a = explode('-', str_replace('backup-v', '', $file_name));
		elseif (pathinfo($backup_file_path, PATHINFO_EXTENSION) == 'sql')
			$a = explode('-', str_replace('db-backup-v', '', $file_name));

		return (!version_compare($a[0], $system_settings['W2D_VERSION']));
	}
	
	public function recurse_move($src, $dst) { 
		$dir = opendir($src); 
		@mkdir($dst);
		while(false !== ($file = readdir($dir))) {
			if (($file != '.') && ($file != '..')) { 
				if (is_dir($src . '/' . $file)) {
					$this->recurse_move($src . '/' . $file, $dst . '/' . $file); 
				} else {
					copy($src . '/' . $file, $dst . '/' . $file); 
				}
			}
		}
		closedir($dir);
		$this->rrmdir($src); 
    }
    
    public function rrmdir($dir) {
    	if (is_dir($dir)) {
    		$files = scandir($dir);
    		foreach ($files as $file)
    			if ($file != "." && $file != "..")
    				$this->rrmdir($dir . '/' . $file);
			rmdir($dir);
    	} else
    		if (file_exists($dir))
    			unlink($dir);
    }
}
?>