<?php

class backupsController extends controller
{
    public function export()
    {
    	if ($this->input->post('submit')) {
    		$this->form_validation->set_rules('label', LANG_BACKUPS_LABEL, 'alpha_dash|required|max_length[60]');
    		$this->form_validation->set_rules('download', LANG_DOWNLOAD_BACKUP_CHECKBOX, 'is_checked');
    		if ($this->form_validation->run() != FALSE) {
    			$this->load->model('backups');
    			if ($backup_file_name = $this->backups->createBackup($this->form_validation->set_value('label'))) {
    				$this->setSuccess(LANG_BACKUPS_CREATION_SUCCESS);
    				if ($this->form_validation->set_value('download')) {
						if (is_file($backup_file_name)) {
							$data = file_get_contents($backup_file_name);
							$this->load->helper('download');
							force_download($backup_file_name, $data);
						}
    				}
    			} else 
    				$this->setError(LANG_BACKUPS_ERROR);
    		}
    		redirect('admin/backups/export/');
    	}

		$view = $this->load->view();
		$view->display('backups/export.tpl');
    }
    
    public function import()
    {
    	$this->load->model('backups');
    	$backups_files = $this->backups->getExistedBackups();
    	
    	if ($this->input->post('submit_flag')) {
    		$this->form_validation->set_rules('backup_file', LANG_BACKUPS_EXISTED_FILE, 'sanitize_string');
    		$this->form_validation->set_rules('upload_backup_file', LANG_BACKUPS_UPLOADED_FILE);
    		if ($this->form_validation->run() != FALSE) {
    			// First of all check if backup was uploaded, then check for existed files
	    		$this->load->library('upload', array(
			    	'upload_path' => 'backups' . DIRECTORY_SEPARATOR,
			    	'allowed_types' => 'sql|zip'
			    ));
			    if (isset($_FILES['upload_backup_file']) && $this->upload->do_upload('upload_backup_file') && $this->upload->display_errors('', '') !== FALSE) {
			    	if ($this->upload->display_errors('', ''))
			    		$this->setError($this->upload->display_errors('', ''));
			    	else {
				    	$file_attrs = $this->upload->data();
				    	if (!$this->backups->checkSystemBackupVersion($file_attrs['file_name']))
				    		$this->setError(LANG_BACKUPS_VERSION_ERROR);
				    	elseif ($this->backups->importBackup($file_attrs['file_name'])) {
				    		// Clean cache
							$this->cache->clean(Zend_Cache::CLEANING_MODE_ALL);

	    					$this->setSuccess(LANG_BACKUPS_IMPORT_SUCCESS);
				    	} else
				    		$this->setError(LANG_BACKUPS_ERROR);
			    	}
    			} elseif ($this->form_validation->set_value('backup_file')) {
    				if (!$this->backups->checkSystemBackupVersion($this->form_validation->set_value('backup_file')))
				    	$this->setError(LANG_BACKUPS_VERSION_ERROR);
    				elseif ($this->backups->importBackup($this->form_validation->set_value('backup_file'))) {
    					// Clean cache
						$this->cache->clean(Zend_Cache::CLEANING_MODE_ALL);

    					$this->setSuccess(LANG_BACKUPS_IMPORT_SUCCESS);
    				}
    			} else 
    				$this->setError(LANG_BACKUPS_SELECT_ERROR);
    		}
    		redirect('admin/backups/import/');
    	}

    	$view = $this->load->view();
    	$view->assign('backups_files', $backups_files);
		$view->display('backups/import.tpl');
    }
}
?>