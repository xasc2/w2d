<?php

class pagination
{
    protected $_num_per_page = null;               // Rows per each page
    protected $_curr_page = 0;                     // Current Page number
    protected $_pages_num = 0;                     // Total number of pages
    protected $_num_per_page_line = 10;            // Pages per each line
    protected $_curr_page_line = 0;                // Current page line number
    protected $_page_lines_num = 0;                // Total number of page lines
    protected $_num_rows = 0;                      // Total number of rows
    protected $_start_row = 0;                     // Current start row on current page
    protected $_url = '';

    protected $_from;
    protected $_result_query;
    protected $_select_query;
    protected $_referer_id;

    public function __construct($config = null)
    {
    	if (isset($config['args']))
    		$args = $config['args'];

    	if (isset($config['args']['page'])) {
    		$page = $args['page'];
    	} else {
    		$page = 1;
    	}

        if (isset($config['num_per_page'])) {
            $this->_num_per_page = $config['num_per_page'];
        }
        if (isset($config['num_per_page_line'])) {
            $this->_num_per_page_line = $config['num_per_page_line'];
        }
        if (isset($config['url'])) {
            $this->_url = $config['url'];
            if (isset($args['orderby']) && isset($args['direction'])) {
				$this->_url .= 'orderby/' . $args['orderby'] . '/direction/' . $args['direction'] . '/';
			}
        }

		$this->_curr_page = $page;
		$this->_curr_page_line = ceil($this->_curr_page/$this->_num_per_page_line);
    }
    
    public function setUrl($url)
    {
    	$this->_url = $url;
    }
    
    public function setNumPerPage($num_per_page)
    {
    	$this->_num_per_page = $num_per_page;
    }

    /**
     * Sets the number of rows from DB count(*)
     *
     * @param integer $count_rows
     */
    public function setCount($count_rows)
    {
    	$this->_num_rows = $count_rows;
    	if ($this->_num_per_page) {
	        $this->_start_row = ($this->_curr_page-1)*$this->_num_per_page;
	        $this->_pages_num = ceil($this->_num_rows/$this->_num_per_page);
	        $this->_page_lines_num = ceil($this->_pages_num/$this->_num_per_page_line);
    	} else {
    		$this->_start_row = 0;
    	}
    }
    
    public function getResultIds($result_array)
    {
    	return array_slice($result_array, $this->_start_row, $this->_num_per_page);
    }

    public function placeLinksToHtml()
    {
        if ($this->_pages_num > 1) {
            $link = $this->_url;
            
            if ($this->_curr_page-1 != 1)
            	$prev_link = $link . 'page/' . ($this->_curr_page - 1). '/';
            else
            	$prev_link = $link;
            $next_link = $link . 'page/' . ($this->_curr_page + 1). '/';
            if (($this->_curr_page_line - 1) * $this->_num_per_page_line != 1)
            	$prev_line_link = $link . 'page/' . (($this->_curr_page_line - 1) * $this->_num_per_page_line). '/';
            else
            	$prev_line_link = $link;
            $next_line_link = $link . 'page/' . ($this->_curr_page_line * $this->_num_per_page_line + 1). '/';
            $start_page = ($this->_curr_page_line-1)*$this->_num_per_page_line + 1;
            $end_page = (($start_page+$this->_num_per_page_line) < $this->_pages_num) ? ($start_page+$this->_num_per_page_line-1) : $this->_pages_num;

            $view = new view();
            $view->assign('pages_num', $this->_pages_num);
            $view->assign('pages_lines_num', $this->_page_lines_num);
            $view->assign('prev_line_link', $prev_line_link);
            $view->assign('prev_link', $prev_link);
            $view->assign('curr_page_line', $this->_curr_page_line);
            $view->assign('curr_page', $this->_curr_page);
            $view->assign('start_page', $start_page);
            $view->assign('end_page', $end_page);
            $view->assign('link', $link);
            $view->assign('next_link', $next_link);
            $view->assign('next_line_link', $next_line_link);
            return $view->fetch('settings/paginator/paginator.tpl');
        }
    }
    
    public function getUrl()
    {
    	return $this->_url;
    }
    
    public function getPage()
    {
    	if ($this->_num_rows) {
    		return $this->_curr_page;
    	} else {
    		return 0;
    	}
    }
    
    public function count()
    {
    	return $this->_num_rows;
    }
    
    public function countPages()
    {
    	return $this->_pages_num;
    }
    
    public function selectPageBlock()
    {
    	if ($this->_pages_num > 1) {
	    	if ($this->_pages_num>100 && $this->_pages_num<1000) {
	    		$multiplier = 10;
	    	} elseif ($this->_pages_num>1000 && $this->_pages_num<10000) {
	    		$multiplier = 100;
	    	} elseif ($this->_pages_num>10000 && $this->_pages_num<100000) {
	    		$multiplier = 1000;
	    	} else {
	    		$multiplier = 1;
	    	}

	    	$view = new view();
	    	
            $view->assign('pages_num', $this->_pages_num);
            $view->assign('multiplier', $multiplier);
            $view->assign('curr_page', $this->_curr_page);
            $view->assign('link', $this->_url);
            return $view->fetch('settings/paginator/paginator_pager.tpl');
    	}
    }
    
    public function setRefererId($id)
    {
    	$this->_referer_id = $id;
    }
    
	public function getRefererId()
    {
    	return $this->_referer_id;
    }

    public function showPrevNextButtons($current_id)
    {
    	$CI = &get_instance();
    	$CI->load->model('listings', 'listings');

    	// Prev/Next buttons don't needed on listing page from quick list
    	if (($params = $CI->listings->getSearchParamsById($this->getRefererId())) && $params['referer_page'] != 'quicklist') {
	    	$search_args = $params['args'];
	    	$search_type_id = $params['search_type_id'];
	    	$use_advanced = $params['use_advanced'];
	    	$search_fields_group_name = $params['search_fields_group_name'];
	    	$orderby = $params['orderby'];
	    	$direction = $params['direction'];
	    	$suborderby = $params['suborderby'];
	    	$subdirection = $params['subdirection'];

	    	if ($orderby != 'random') {
	    		include_once(MODULES_PATH . 'content_fields/classes/field.class.php');
				include_once(MODULES_PATH . 'content_fields/classes/content_fields.class.php');
				include_once(MODULES_PATH . 'content_fields/classes/search_content_fields.class.php');

		    	$search_fields = new searchContentFields($search_fields_group_name, $search_type_id);
		    	$search_sql_array = $search_fields->validateSearch(LISTINGS_LEVEL_GROUP_CUSTOM_NAME, $search_args, $use_advanced, $search_url_rebuild);
		    	$listings_ids = $CI->listings->selectListings($search_args, $orderby, $direction, $suborderby, $subdirection, $search_sql_array, array(), true, true);
	
		    	$listings_count = count($listings_ids);
		    	if ($listings_count > 1) {
			    	$prev_listing = null;
			    	$next_listing = null;
			    	$key = array_search($current_id, $listings_ids);
			    	$listing_number = $key+1;
			    	if (isset($listings_ids[$key-1])) {
			    		$prev_listing = $CI->listings->getListingById($listings_ids[$key-1]);
			    		$prev_listing->setRefererId($this->getRefererId());
			    	}
			    	if (isset($listings_ids[$key+1])) {
			    		$next_listing = $CI->listings->getListingById($listings_ids[$key+1]);
			    		$next_listing->setRefererId($this->getRefererId());
			    	}
			    	
			    	$view = new view();
		            $view->assign('prev_listing', $prev_listing);
		            $view->assign('next_listing', $next_listing);
		            $view->assign('listing_number', $listing_number);
		            $view->assign('listings_count', $listings_count);
		            return $view->fetch('settings/paginator/paginator_prev_next.tpl');
		    	}
	    	}
    	}
    }
    
    public function getRefererUrl($current_id)
    {
    	$CI = &get_instance();
    	$CI->load->model('listings', 'listings');

    	if ($params = $CI->listings->getSearchParamsById($this->getRefererId())) {
    		return site_url($params['args_string']);
    	} else
    		return false;
    }
}
?>