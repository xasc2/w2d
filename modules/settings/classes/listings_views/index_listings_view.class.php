<?php

class indexListingsView extends listingsView
{
	public $page_name = LANG_INDEX_PAGE_TH;
	public $page_key = 'index';

	public function setTypeId($type = null)
	{
		if ($type)
			$this->type_id = $type->id;
	}
	
	public function setDefaults()
	{
		$this->view = 'semitable';
		$this->format = '3*1';
	}
}
?>