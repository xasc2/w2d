<?php

class categoriesListingsView extends listingsView
{
	public $page_name = LANG_CATEGORIES_PAGE_TH;
	public $page_key = 'categories';

	public function setTypeId($type = null)
	{
		if ($type && $type->categories_type == 'local')
			$this->type_id = $type->id;
	}

	public function setDefaults()
	{
		$this->view = 'full';
		$this->format = '10';
	}
}
?>