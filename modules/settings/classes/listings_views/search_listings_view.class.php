<?php

class searchListingsView extends listingsView
{
	public $page_name = LANG_SEARCH_PAGE_TH;
	public $page_key = 'search';

	public function setTypeId($type = null)
	{
		if ($type && $type->search_type == 'local')
			$this->type_id = $type->id;
	}

	public function setDefaults()
	{
		$this->view = 'full';
		$this->format = '10';
	}
}
?>