<?php
class breadcrumbs
{
	public $params;
	
    public function setRefererParams($params)
    {
    	$this->params = $params;
    }
    
	public function buildBreadcrumbsFromReferer()
    {
    	$args = $this->params['args'];

    	switch ($this->params['referer_page']) {
    		case 'types' :
	    		if (is_numeric($args['search_type'])) {
	    			$CI = &get_instance();
	    			$CI->load->model('types', 'types_levels');
	    			$type = $CI->types->getTypeById($args['search_type']);
	    		} elseif (is_object($args['search_type'])) {
	    			$type = $args['search_type'];
	    		} elseif (is_string($args['search_type'])) {
	    			$CI = &get_instance();
	    			$CI->load->model('types', 'types_levels');
	    			$type = $CI->types->getTypeBySeoName($args['search_type']);
	    		}
	    		if (isset($type)) {
	    			return array(site_url($type->getBaseUrl()) => $type->name);
	    		} else 
	    			return false;
    			break;
    		case 'categories' :
    			if (isset($args['search_type']))
	    			if (is_numeric($args['search_type'])) {
		    			$CI = &get_instance();
		    			$CI->load->model('types', 'types_levels');
		    			$type = $CI->types->getTypeById($args['search_type']);
		    		} elseif (is_object($args['search_type'])) {
		    			$type = $args['search_type'];
		    		} elseif (is_string($args['search_type'])) {
		    			$CI = &get_instance();
		    			$CI->load->model('types', 'types_levels');
		    			$type = $CI->types->getTypeBySeoName($args['search_type']);
		    		}
    			
	    		if (is_numeric($args['search_category'])) {
	    			$CI = &get_instance();
	    			$CI->load->model('categories', 'categories');
	    			if (isset($type))
	    				$CI->categories->setTypeId($type->id);
	    			$category = $CI->categories->getCategoryById($args['search_category']);
	    		} elseif (is_string($args['search_category'])) {
	    			$CI = &get_instance();
	    			$CI->load->model('categories', 'categories');
	    			if (isset($type))
	    				$CI->categories->setTypeId($type->id);
	    			$category = $CI->categories->getCategoryBySeoName($args['search_category']);
	    		} elseif (is_object($args['search_category'])) {
	    			$category = $args['search_category'];
	    		}
	    		if (isset($category)) {
	    			$breadcrumbs = array();
	    			$categories_chain = $category->getChainAsArray();
	    			foreach ($categories_chain AS $category) {
	    				$breadcrumbs = array_merge($breadcrumbs, array(site_url($category->getUrl()) => $category->name));
	    			}
	    			return $breadcrumbs;
	    		} else 
	    			return false;
	    		break;
	    	case 'search' :
	    		return array(site_url($this->params['args_string']) => LANG_BREADCRUMBS_SEARCH);
	    		break;
	    	case 'quicklist' :
	    		return array(site_url($this->params['args_string']) => LANG_QUICK_LIST);
	    		break;
    	}
    }
    
    public function buildBreadcrumbsFromListing($listing, $build_by_category = false)
    {
    	if ($build_by_category) {
		    if (count($listing->categories_array()) == 1) {
				$CI->load->model('categories', 'categories');
				$CI->categories->setTypeId($listing->type->id);
				$one_category = array_pop($listing->categories_array());
				$category = $CI->categories->getCategoryById($one_category->id);
				return array(site_url($category->getUrl()) => $category->name);
			} else 
				return false;
    	} else
    		return array(site_url($listing->type->getBaseUrl()) => $listing->type->name);
    }
}
?>