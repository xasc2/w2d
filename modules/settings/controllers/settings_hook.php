<?php
include_once(MODULES_PATH . 'settings/classes/pagination.php');
include_once(MODULES_PATH . 'settings/classes/breadcrumbs.php');
include_once(MODULES_PATH . 'settings/classes/spam_protector.php');

function runAutoBlocker($CI)
{
	$system_settings = registry::get('system_settings');

	// Clean old cache files
	if ($CI->config->item('enable_cache') && intval($system_settings['cache_remover_timestamp']) <= mktime()) {
		$CI->cache->clean(Zend_Cache::CLEANING_MODE_OLD);

		// --------------------------------------------------------------------------------------------
		// In order to avoid 'MYSQL server has gone away' problem
		// --------------------------------------------------------------------------------------------
		$CI->db->reconnect();
		// --------------------------------------------------------------------------------------------

		$CI->load->model('settings', 'settings');
		$CI->settings->setCacheRemoverTimer(CACHE_LIFETIME);
	}

	// Process all functions those affect on auto blocker run
	if (intval($system_settings['auto_blocker_timestamp']) <= mktime()) {
		events::callEvent('Auto blocker run');

		// Seconds till next auto blocking cycle
		$block_set_time = $CI->config->item('auto_block_timer');

		// --------------------------------------------------------------------------------------------
		// In order to avoid 'MYSQL server has gone away' problem
		// --------------------------------------------------------------------------------------------
		$CI->db->reconnect();
		// --------------------------------------------------------------------------------------------

		$CI->load->model('settings', 'settings');
		$CI->settings->setAutoBlockerTimer($block_set_time);
	}
}

function selectSiteSettings($CI)
{
	// Select site settings
	$query = $CI->db->get('site_settings');
	$settings = array();
	foreach ($query->result() as $row) {
		$settings[$row->name] = $row->value;
	}
	registry::set('site_settings', $settings);
}
?>