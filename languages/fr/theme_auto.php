<?php

$language['LANG_FRONTEND_SEARCH_BY_MAKE_MODEL'] = "Recherche par marque/modèle";
$language['LANG_CHOOSE_MAKE_MODEL'] = "Choisissez la marque et le modèle";
$language['LANG_CHOOSE_MAKE_MODEL_DESCRIPTION'] = "Cliquez sur 'choisir ce modèle' lien";
$language['LANG_CHOOSE_MODEL'] = "choisir ce modèle";
$language['LANG_LISTING_SELECTED_MAKE_MODEL'] = "marque et le modèle sélectionné:";
?>