<?php

$language['LANG_SEARCH_BUTTON'] = "Rechercher";
$language['LANG_HOME_BUTTON'] = "Accueil";
$language['LANG_BACK_BUTTON'] = "Retour";
$language['LANG_SELECTED_LOCATION'] = "Emplacement";
$language['LANG_ALL_LOCATIONS_LINK'] = "Parcourir dans tous les lieux";
$language['LANG_MANAGE_QUICK_LIST'] = "Gérer votre liste rapide";
$language['LANG_EMAIL_FRIEND_MOBILE'] = "Envoyer à un ami";
$language['LANG_EMAIL_OWNER_MOBILE'] = "Envoyer à propriétaire fiche";
$language['LANG_USER_ALL_LISTINGS'] = "Toutes les annonces de l'utilisateur";
$language['LANG_REPORT_ADMIN_MOBILE'] = "Signaler cette annonce";
$language['LANG_LISTING_MAP'] = "Voir la carte";
$language['LANG_LISTING_USER_PAGE'] = "Auteur de la page";
$language['LANG_LISTING_RATE_THIS'] = "Noter cette";
?>