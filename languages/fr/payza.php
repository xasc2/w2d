<?php
	
$language['LANG_PAYZA_SETTINGS'] = "Payza paramètres";
$language['LANG_PAYZA_REDIRECT_TITLE'] = "Vous allez être redirigé vers Payza";
$language['LANG_PAYZA_REDIRECT_MANUAL'] = "Si vous n'êtes pas redirigé automatiquement vers Payza dans les 5 secondes ...";
$language['LANG_PAYZA_EMAIL'] = "Email affaires Payza";
$language['LANG_PAYZA_SECURITYCODE'] = "Code de sécurité Payza.";
$language['LANG_PAYZA_SECURITYCODE_DESCR'] = "devrait être utilisée pour confirmer que l'IPN reçues provenaient de Payza Comparez cela à l'IPN dans le code de sécurité de votre compte Payza.";
?>