<?php

$language['LANG_PAYFAST_SETTINGS'] = "PayFast paramètres";
$language['LANG_PAYFAST_REDIRECT_TITLE'] = "Vous allez être redirigé vers le site PayFast";
$language['LANG_PAYFAST_REDIRECT_MANUAL'] = "Si vous n'êtes pas automatiquement redirigé vers PayFast dans les 5 secondes ...";
$language['LANG_PAYFAST_MID'] = "Marchand PayFast ID";
$language['LANG_PAYFAST_MKEY'] = "clé de marchand PayFast";
?>