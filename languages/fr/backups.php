<?php

$language['LANG_BACKUPS_LABEL'] = "label de sauvegarde";
$language['LANG_BACKUPS_LABEL_DESCR'] = "Seuls les symboles alphanumériques et dashs reconnus";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX'] = "Télécharger de sauvegarde après la création";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX_DESCR'] = "+ sauvegarde téléchargement après la création";
$language['LANG_BACKUPS_CREATION_SUCCESS'] = "Nouvelle sauvegarde a été créé avec succès!";
$language['LANG_BACKUPS_ERROR'] = "Une erreur s'est accoured!";
$language['LANG_BACKUPS_EXISTED_FILE'] = "Le fichier existe";
$language['LANG_BACKUPS_UPLOADED_FILE'] = "Téléchargement de fichier";
$language['LANG_BACKUPS_VERSION_ERROR'] = "La version du système de sauvegarde n'est pas égal avec la version actuelle du système";
$language['LANG_BACKUPS_SELECT_ERROR'] = "Choisissez le fichier de sauvegarde existe ou télécharger le fichier!";
$language['LANG_BACKUPS_IMPORT_SUCCESS'] = "Backup a été importée avec succès";
$language['LANG_CREATE_NEW_BACKUP'] = "Créer nouvelle sauvegarde";
$language['LANG_CREATE_NEW_BACKUP_DESCR'] = "sauvegarde sera créé dans <i>'backups/'</i> dossier Aussi, vous pouvez le télécharger automatiquement";
$language['LANG_CREATE_BACKUP_BUTTON'] = "Créer une sauvegarde";
$language['LANG_IMPORT_BACKUP'] = "Importer sauvegarde";
$language['LANG_IMPORT_BACKUP_DESCR'] = "Choisissez sauvegarde existe depuis <i>'backups/'</i> dossier ou télécharger le fichier de sauvegarde <br /> Note 1:. Actuelle login, e-mail et mot de passe de l'utilisateur root sera sauvé. <br /> Note 2: base de données actuelle, «users_content / '<i> </ i> et <i>« langues /' </ i> dossiers seront entièrement écrasés, recommandé de faire une sauvegarde du contenu existé - ";
$language['LANG_CREATE_NEW_BACKUP_LINK'] = "font de sauvegarde";
$language['LANG_BACKUP_WARNING_MSG'] = "Fortement recommandé de faire nouvelle sauvegarde avant d'importer existait Souhaitez-vous faire maintenant nouvelle sauvegarde";
$language['LANG_BACKUP_WARNING_QUESTION'] = "Créer nouvelle sauvegarde?";
$language['LANG_SELECT_BACKUP'] = "Choisir un fichier de sauvegarde";
$language['LANG_NO_BACKUPS'] = "Aucune sauvegarde existé!";
$language['LANG_UPLOAD_BACKUP'] = "Télécharger le fichier de sauvegarde";
$language['LANG_IMPORT_BACKUP_BUTTON'] = "Importer sauvegarde";
$language['LANG_IMPORT_BACKUPS_MENU'] = "Importer des sauvegardes";
$language['LANG_EXPORT_BACKUPS_MENU'] = "Exporter sauvegardes";
?>