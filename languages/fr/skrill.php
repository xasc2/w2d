<?php

$language['LANG_SKRILL_REDIRECT_TITLE'] = "Vous allez être redirigé vers Skrill";
$language['LANG_SKRILL_REDIRECT_MANUAL'] = "Si vous n'êtes pas automatiquement redirigé vers Skrill dans les 5 secondes ...";
$language['LANG_SKRILL_EMAIL'] = "Email marchand Skrill";
$language['LANG_SKRILL_SWORD'] = "mot secret Skrill";
$language['LANG_SKRILL_SWORD_DESCR'] = "Connexion à votre compte Skrill et aller sur 'Look and Feel' section. Enregistrer le mot secret et l'utiliser dans le processus de vérification IPN.";
$language['LANG_SKRILL_SETTINGS'] = "Skrill paramètres";
?>
