<?php

$language['LANG_MANAGE_CONTENT_PAGES'] = "Gérer les pages de contenu";
$language['LANG_CREATE_CONTENT_PAGE'] = "Créer une page de contenu";
$language['LANG_EDIT_CONTENT_PAGE'] = "Modifier la page de contenu";
$language['LANG_DELETE_CONTENT_PAGE'] = "contenu de la page Supprimer";
$language['LANG_DELETE_CONTENT_PAGE_QUESTION'] = "Voulez-vous vraiment supprimer cette page de contenu?";
$language['LANG_PAGE_URL'] = "url de la page";
$language['LANG_PAGE_TITLE'] = "Titre de la page";
$language['LANG_PAGE_ID'] = "Numéro Id Page";
$language['LANG_CREATE_PAGE'] = "créer une page";
$language['LANG_PAGES'] = "pages";
$language['LANG_PAGE_PUBLIC_URL'] = "URL de la page publique";
$language['LANG_BUTTON_CREATE_NEW_PAGE'] = "Créer une nouvelle page";
$language['LANG_TOP_MENU_TH'] = "Top Menu";
$language['LANG_CREATION_DATE_TH'] = "Date de création";
$language['LANG_ENABLE_LINK_IN_MENU'] = "Activer le lien contenu de la page dans le menu supérieur";
$language['LANG_VIEW_CONTENT_PAGE'] = "contenu de la page Aperçu";
$language['LANG_EDIT_CONTENT_PAGE_OPTION'] = "page d'édition de contenu";
$language['LANG_DELETE_CONTENT_PAGE_OPTION'] = "supprimer la page de contenu";
$language['LANG_CONTENT_PAGE_INFO'] = "Informations sur la page de contenu";
$language['LANG_URL_TD'] = "Url";
$language['LANG_TITLE_TD'] = "Titre";
$language['LANG_CREATION_DATE_TD'] = "Date de création";
$language['LANG_ADDITIONAL_FIELDS'] = "champs de contenu additionnel";
// Controller constants
$language['LANG_IS_UNIQUE_NODE'] = "Le nœud de cette url est déjà existé!";
$language['LANG_PAGES_ORDER_SAVE_SUCCESS'] = "Contenu pages ordonnance a été enregistré avec succès!";
$language['LANG_CONTENT_PAGE_CREATE_SUCCESS_1'] = "page de contenu";
$language['LANG_CONTENT_PAGE_CREATE_SUCCESS_2'] = "a été créé avec succès!";
$language['LANG_CONTENT_PAGE_SAVE_SUCCESS_1'] = "page de contenu";
$language['LANG_CONTENT_PAGE_SAVE_SUCCESS_2'] = "a été enregistré avec succès!";
$language['LANG_CONTENT_PAGE_DELETE_SUCCESS'] = "page de contenu a été supprimé avec succès!";
$language['LANG_MANAGE_CONTENT_PAGES_TITLE'] = "Gérer les pages de contenu";
$language['LANG_CREATE_CONTENT_PAGE_TITLE'] = "Créer une page de contenu";
$language['LANG_EDIT_CONTENT_PAGE_TITLE'] = "Modifier la page de contenu";
$language['LANG_DELETE_CONTENT_PAGE_TITLE'] = "contenu de la page Supprimer";
$language['LANG_CONTENT_PAGES_MENU'] = "Les pages de contenu";
$language['LANG_CREATE_CONTENT_PAGE_MENU'] = "Créer nouvelle page de contenu";
$language['LANG_MANAGE_CONTENT_PAGE_MENU'] = "Gérer les pages de contenu";


/* Update to 3.2.0 */


$language['LANG_BOTTOM_MENU_TH'] = "Bottom Menu";
$language['LANG_IS_PHP_CODE_TH'] = "Code PHP ou un modèle";
$language['LANG_ENABLE_LINK_IN_TOP_MENU'] = "Lien vers page Set contenu dans le menu du haut";
$language['LANG_ENABLE_LINK_IN_BOTTOM_MENU'] = "Lien vers page Set contenu dans le menu en bas";
$language['LANG_PAGE_INPUT_PARAMS'] = "Paramètres de contenu de page d'entrée";
$language['LANG_PAGE_INPUT_PARAMS_DESCR'] = "Voici contenu de la page des paramètres de l'URL reçoit, puis ils passent dans le code PHP de la page Saisissez chaque paramètre sur une ligne séparée.";
$language['LANG_PAGE_OUTPUT_PARAMS'] = "Paramètres de contenu de page de sortie";
$language['LANG_PAGE_OUTPUT_PARAMS_DESCR'] = "Voici contenu de la page des paramètres envoie à partir du code PHP dans Smarty Saisissez chaque paramètre sur une ligne séparée Utilisez la syntaxe comme l'intérieur des modèles <i>{\$value}</i>.";
$language['LANG_PAGE_PHP_CODE'] = "Code PHP";
$language['LANG_PAGE_PHP_CODE_DESCR'] = "Ce code PHP s'exécute comme fonction de contrôleur d'habitude.";
$language['LANG_PAGE_TEMPLATE_FILE'] = "Nom de fichier de modèle";
$language['LANG_PAGE_TEMPLATE_FILE_DESCR'] = "fichier de modèle Ensemble intérieur <i>themes/{your_theme}/views/frontend/'</i> dossier.";
$language['LANG_CONTENT_PAGE_EXECUTION_RESULT'] = "Le résultat de l'exécution de code PHP et HTML/smarty code";
?>