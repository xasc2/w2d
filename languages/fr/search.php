<?php

$language['LANG_CONFIGURE_FIELD_SEARCH'] = 'configure field search';
$language['LANG_MANAGE_SEARCH_FIELDS_GROUPS_BY_TYPE'] = 'Manage fields of search groups by listings types';
$language['LANG_SEARCH_SAVE_MSG'] = 'Search fields group is saving!';
$language['LANG_SEARCH_MODE'] = 'Search mode';
$language['LANG_QUICK_SEARCH_MODE'] = 'Quick mode';
$language['LANG_ADVANCED_SEARCH_MODE'] = 'Advanced mode';
$language['LANG_MANAGE_GLOBAL_SEARCH_TITLE'] = 'Manage global search fields';
$language['LANG_MANAGE_SEARCH_BYTYPE_TITLE'] = 'Manage search by type';
$language['LANG_MANAGE_GLOBAL_SEARCH_MENU'] = 'Manage global search';
$language['LANG_MANAGE_SEARCH_BYTYPE_MENU'] = 'Manage search by type';
$language['LANG_SEARCH_MENU'] = 'Search';
$language['LANG_ALL_CONTENT_FIELDS'] = 'All Content Fields';
$language['LANG_FIELDS_OF_GROUP'] = 'Fields of this Group';
$language['LANG_SEARCH_FOR_ALL_TYPES'] = "Recherche sur tous les types";
$language['LANG_SEARCH_LISTINGS'] = "Recherche d'annonces";
$language['LANG_SEARCH_WHAT'] = "Qu'est-ce";
$language['LANG_SEARCH_WHAT_DESCR'] = "mots-clés dans les titres, la description ou les méta-données";
$language['LANG_SEARCH_WHERE'] = "Lorsque";
$language['LANG_SEARCH_WHERE_DESCR'] = "Adresse, emplacements, Zip ou un index postal";
$language['LANG_SEARCH_IN_RADIUS'] = "Rechercher dans un rayon";
$language['LANG_SEARCH_IN_RADIUS_MILES'] = "miles";
$language['LANG_ADVANCED_SEARCH'] = "Recherche avancée";
$language['LANG_SEARCH_BY_TITLE'] = "par titre";
$language['LANG_SEARCH_CATEGORIES'] = "Catégories";
$language['LANG_SEARCH_CATEGORY_ANY'] = "Recherche dans une catégorie";
$language['LANG_SEARCH_BY_OWNER'] = "par le propriétaire";
$language['LANG_SEARCH_BY_STATUS'] = "selon le statut";
$language['LANG_SEARCH_BY_TYPE'] = "Par type de";
$language['LANG_SEARCH_BY_LEVEL'] = "Selon le niveau de type de";
$language['LANG_SEARCH_BY_CREATION_DATE'] = "par date de création";
$language['LANG_SEARCH_BY_CREATION_DATE_FROM'] = "De";
$language['LANG_SEARCH_BY_CREATION_DATE_TO'] = "Pour";
$language['LANG_BUTTON_SEARCH_LISTINGS'] = "Recherche d'annonces";
$language['LANG_SEARCH_RESULTS_1'] = "Le formulaire de recherche tableau des résultats";
$language['LANG_SEARCH_RESULTS_2'] = "annonces";


/* Update to 3.2.0 */


$language['LANG_FRONTEND_SEARCH_LISTINGS'] = "annonces Rechercher";
$language['LANG_FRONTEND_SEARCH_IN_CATEGORIES'] = "Rechercher dans les catégories";
$language['LANG_FRONTEND_SEARCH_IN_LOCATIONS'] = "Rechercher dans les endroits";


/* Update to 3.3.0 */


$language['LANG_SEARCH_WHAT_OPION'] = "option";
$language['LANG_SEARCH_WHERE_OPION'] = "en rayon";

?>