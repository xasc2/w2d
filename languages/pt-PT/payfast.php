<?php

$language['LANG_PAYFAST_SETTINGS'] = "PayFast configurações";
$language['LANG_PAYFAST_REDIRECT_TITLE'] = "Você será redirecionado para o site PayFast";
$language['LANG_PAYFAST_REDIRECT_MANUAL'] = "Se você não está automaticamente redirecionado para PayFast dentro de 5 segundos ...";
$language['LANG_PAYFAST_MID'] = "Mercador PayFast ID";
$language['LANG_PAYFAST_MKEY'] = "Key Merchant PayFast";

?>