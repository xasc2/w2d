<?php

$language['LANG_PAYZA_SETTINGS'] = "Payza configurações";
$language['LANG_PAYZA_REDIRECT_TITLE'] = "Você será redirecionado para Payza";
$language['LANG_PAYZA_REDIRECT_MANUAL'] = "Se você não for redirecionado automaticamente para Payza dentro de 5 segundos ...";
$language['LANG_PAYZA_EMAIL'] = "Email Payza Negócios";
$language['LANG_PAYZA_SECURITYCODE'] = "Código de segurança Payza.";
$language['LANG_PAYZA_SECURITYCODE_DESCR'] = "deve ser usado para confirmar que o IPN recebeu veio do Payza Compare isso com o Código de Segurança IPN na sua conta Payza.";
?>