<?php

$language['LANG_BACKUPS_LABEL'] = "rótulo Backup";
$language['LANG_BACKUPS_LABEL_DESCR'] = "Apenas alfa-numéricos símbolos e dashs aceitos";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX'] = "backup Baixar depois da criação";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX_DESCR'] = "+ backup de download após a criação";
$language['LANG_BACKUPS_CREATION_SUCCESS'] = "Novo backup foi criado com sucesso!";
$language['LANG_BACKUPS_ERROR'] = "Um erro accoured!";
$language['LANG_BACKUPS_EXISTED_FILE'] = "file existiu";
$language['LANG_BACKUPS_UPLOADED_FILE'] = "Enviado arquivo";
$language['LANG_BACKUPS_VERSION_ERROR'] = "Versão do sistema em backup não é igual a da versão atual do sistema";
$language['LANG_BACKUPS_SELECT_ERROR'] = "Escolha existia arquivo de backup ou fazer upload de arquivos";
$language['LANG_BACKUPS_IMPORT_SUCCESS'] = "Backup foi importado com sucesso!";
$language['LANG_CREATE_NEW_BACKUP'] = "Criar novo backup";
$language['LANG_CREATE_NEW_BACKUP_DESCR'] = "Backup será criada em <i>'backups/'</i> pasta Também você pode auto-baixá-lo.";
$language['LANG_CREATE_BACKUP_BUTTON'] = "Criar backup";
$language['LANG_IMPORT_BACKUP'] = "backup Importar";
$language['LANG_IMPORT_BACKUP_DESCR'] = "Escolha de backup existia desde <i>'backups/'</i> pasta ou enviar arquivos de backup.<br />Nota 1: Atual email login e senha do usuário root será salvo.<br />Nota 2: banco de dados atual, <i>'users_content/'</i> e <i>'línguas/'</i> pastas será totalmente substituído, recomenda-se fazer backup de conteúdo existia - ";
$language['LANG_CREATE_NEW_BACKUP_LINK'] = "fazer backup";
$language['LANG_BACKUP_WARNING_MSG'] = "Altamente recomendado para fazer novo backup antes de importar existia Você deseja fazer backup de novo agora?";
$language['LANG_BACKUP_WARNING_QUESTION'] = "Criar novo backup?";
$language['LANG_SELECT_BACKUP'] = "Escolher arquivo de backup";
$language['LANG_NO_BACKUPS'] = "Não há backups existia!";
$language['LANG_UPLOAD_BACKUP'] = "Enviar arquivo de backup";
$language['LANG_IMPORT_BACKUP_BUTTON'] = "backup Importar";
$language['LANG_IMPORT_BACKUPS_MENU'] = "backups de Importação";
$language['LANG_EXPORT_BACKUPS_MENU'] = "backups de Exportação";

?>