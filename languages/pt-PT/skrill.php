<?php

$language['LANG_SKRILL_REDIRECT_TITLE'] = "Você será redirecionado para Skrill";
$language['LANG_SKRILL_REDIRECT_MANUAL'] = "Se você não está automaticamente redirecionado para Skrill dentro de 5 segundos ...";
$language['LANG_SKRILL_EMAIL'] = "e-mail comerciante Skrill";
$language['LANG_SKRILL_SWORD'] = "palavra secreta Skrill";
$language['LANG_SKRILL_SWORD_DESCR'] = "Login em sua conta Skrill e ir para a 'Look and Feel' seção. Registrar a palavra secreta e usá-lo no processo de verificação IPN.";
$language['LANG_SKRILL_SETTINGS'] = "Skrill configurações";
?>
