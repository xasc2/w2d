<?php

$language['LANG_SEARCH_BUTTON'] = "Pesquisar";
$language['LANG_HOME_BUTTON'] = "Home";
$language['LANG_BACK_BUTTON'] = "Voltar";
$language['LANG_SELECTED_LOCATION'] = "Local";
$language['LANG_ALL_LOCATIONS_LINK'] = "Procurar em todos os locais";
$language['LANG_MANAGE_QUICK_LIST'] = "Gerenciar sua lista rápida";
$language['LANG_EMAIL_FRIEND_MOBILE'] = "Enviar a um amigo";
$language['LANG_EMAIL_OWNER_MOBILE'] = "E-mail para o dono de listagem";
$language['LANG_USER_ALL_LISTINGS'] = "Todos os anúncios do usuário";
$language['LANG_REPORT_ADMIN_MOBILE'] = "Denunciar este anúncio";
$language['LANG_LISTING_MAP'] = "Ver Mapa";
$language['LANG_LISTING_USER_PAGE'] = "Página do Autor";
$language['LANG_LISTING_RATE_THIS'] = "Classificar este";
?>