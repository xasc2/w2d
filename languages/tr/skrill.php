<?php

$language['LANG_SKRILL_REDIRECT_TITLE'] = "Sen Skrill yönlendirileceksiniz";
$language['LANG_SKRILL_REDIRECT_MANUAL'] = "otomatik olarak 5 saniye içinde Skrill için yönlendirilmez iseniz";
$language['LANG_SKRILL_EMAIL'] = "Skrill tüccar e-posta";
$language['LANG_SKRILL_SWORD'] = "Skrill gizli kelime";
$language['LANG_SKRILL_SWORD_DESCR'] = "Lütfen Skrill hesabınıza giriş yapıp 'Bak ve Hisset' bölümüne gidin. Gizli Word Kayıt ve IPN doğrulama işleminde kullanabilirsiniz.";
$language['LANG_SKRILL_SETTINGS'] = "Skrill ayarları";
?>
