<?php

$language['LANG_PAYZA_SETTINGS'] = "Payza ayarları";
$language['LANG_PAYZA_REDIRECT_TITLE'] = "Payza için yönlendirilmiş olacaktır";
$language['LANG_PAYZA_REDIRECT_MANUAL'] = "5 saniye içinde otomatik olarak Payza yönlendirilmez iseniz ...";
$language['LANG_PAYZA_EMAIL'] = "Payza E-posta";
$language['LANG_PAYZA_SECURITYCODE'] = "Payza Güvenlik kodu.";
$language['LANG_PAYZA_SECURITYCODE_DESCR'] = "IPN aldı Payza geldiğini onaylamak için kullanılır olmalı Payza hesabınıza IPN Güvenlik Kodu karşılaştırın.";
?>