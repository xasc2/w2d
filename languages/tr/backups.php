<?php

$language['LANG_BACKUPS_LABEL'] = "Yedek etiket";
$language['LANG_BACKUPS_LABEL_DESCR'] = "kabul Sadece alfa-sayısal semboller ve dashs";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX'] = "oluşturulduktan sonra yükle yedekleme";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX_DESCR'] = "oluşturulduktan sonra + indir yedekleme";
$language['LANG_BACKUPS_CREATION_SUCCESS'] = "Yeni yedekleme başarıyla oluşturuldu!";
$language['LANG_BACKUPS_ERROR'] = "Bir hata accoured etti!";
$language['LANG_BACKUPS_EXISTED_FILE'] = "Yaşadığını dosyası";
$language['LANG_BACKUPS_UPLOADED_FILE'] = "Dosya yüklendi";
$language['LANG_BACKUPS_VERSION_ERROR'] = "yedekleme sistemi Sürüm sisteminin güncel sürümü ile eşit değil!";
$language['LANG_BACKUPS_SELECT_ERROR'] = "varolan yedekleme dosyası seçin veya dosya yükleyin!";
$language['LANG_BACKUPS_IMPORT_SUCCESS'] = "Yedekleme başarıyla ithal edildi!";
$language['LANG_CREATE_NEW_BACKUP'] = "Yeni bir yedekleme oluştur";
$language['LANG_CREATE_NEW_BACKUP_DESCR'] = "Yedek <i>'yedekleme/'</i> oluşturulur klasörünüze Ayrıca bunu otomatik indirebilirsiniz.";
$language['LANG_CREATE_BACKUP_BUTTON'] = "Yedek Oluştur";
$language['LANG_IMPORT_BACKUP'] = "İthalat yedekleme";
$language['LANG_IMPORT_BACKUP_DESCR'] = "beri var yedeği seçin <i>'yedekleme/'</i> klasör veya yedekleme dosya upload.<br />Not 1: Geçerli giriş, e-posta ve root parolasını kaydedilecektir.<br />Not 2: Geçerli veritabanı, <i>'users_content/'</i> ve <i>'languages/'</i> klasörleri varolan içerik yedekleme yapmak için önerilen, tamamen üzerine yazılır - ";
$language['LANG_CREATE_NEW_BACKUP_LINK'] = "yedek oluştur";
$language['LANG_BACKUP_WARNING_MSG'] = "Kesinlikle sen var almadan önce yeni bir yedekleme yapmak için önerilen şimdi yeni bir yedekleme yapmak ister misiniz.?";
$language['LANG_BACKUP_WARNING_QUESTION'] = "Yeni bir yedekleme oluştur?";
$language['LANG_SELECT_BACKUP'] = "yedekleme dosyası seçin";
$language['LANG_NO_BACKUPS'] = "Hayır yedekleri var!";
$language['LANG_UPLOAD_BACKUP'] = "yedekleme dosyası yükle";
$language['LANG_IMPORT_BACKUP_BUTTON'] = "İthalat yedekleme";
$language['LANG_IMPORT_BACKUPS_MENU'] = "İthalat yedekleri";
$language['LANG_EXPORT_BACKUPS_MENU'] = "İhracat yedekleri";

?>