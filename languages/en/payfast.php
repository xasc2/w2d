<?php
	

$language['LANG_PAYFAST_SETTINGS'] = "PayFast settings";
$language['LANG_PAYFAST_REDIRECT_TITLE'] = "You will be redirected to the PayFast website";
$language['LANG_PAYFAST_REDIRECT_MANUAL'] = "If you are not automatically redirected to PayFast within 5 seconds...";
$language['LANG_PAYFAST_MID'] = "PayFast Merchant ID";
$language['LANG_PAYFAST_MKEY'] = "PayFast Merchant Key";

?>