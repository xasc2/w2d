<?php

$language['LANG_2CHECKOUT_REDIRECT_TITLE'] = "You will be redirected to 2Checkout";
$language['LANG_2CHECKOUT_REDIRECT_MANUAL'] = "If you are not automatically redirected to 2Checkout within 5 seconds...";
$language['LANG_2CHECKOUT_SID'] = "2CheckOut sid";
$language['LANG_2CHECKOUT_SWORD'] = "2CheckOut secret word";
$language['LANG_2CHECKOUT_SWORD_DESCR'] = "Login into your 2checkout.com account and go to 'Look and Feel' section. At the bottom enter the Secret Word and use it in the IPN verification process. Browse to the 'Notifications'->'Settings' tab, enable the 'Order Created' notification option and enter 'http://www.yoursite/tcheckout/ipn/' in the 'URL' field";
$language['LANG_2CHECKOUT_SETTINGS'] = "2CheckOut settings";
?>