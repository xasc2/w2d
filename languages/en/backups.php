<?php

$language['LANG_BACKUPS_LABEL'] = "Backup label";
$language['LANG_BACKUPS_LABEL_DESCR'] = "Only alpha-numeric symbols and dashs accepted";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX'] = "Download backup after creation";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX_DESCR'] = "+ download backup after creation";
$language['LANG_BACKUPS_CREATION_SUCCESS'] = "New backup was created successfully!";
$language['LANG_BACKUPS_ERROR'] = "An error has accoured!";
$language['LANG_BACKUPS_EXISTED_FILE'] = "Existed file";
$language['LANG_BACKUPS_UPLOADED_FILE'] = "Uploaded file";
$language['LANG_BACKUPS_VERSION_ERROR'] = "Version of the system in backup doesn't equal with current version of the system!";
$language['LANG_BACKUPS_SELECT_ERROR'] = "Choose existed backup file or upload file!";
$language['LANG_BACKUPS_IMPORT_SUCCESS'] = "Backup was imported successfully!";
$language['LANG_CREATE_NEW_BACKUP'] = "Create new backup";
$language['LANG_CREATE_NEW_BACKUP_DESCR'] = "Backup will be created in <i>'backups/'</i> folder. Also you may auto-download it.";
$language['LANG_CREATE_BACKUP_BUTTON'] = "Create backup";
$language['LANG_IMPORT_BACKUP'] = "Import backup";
$language['LANG_IMPORT_BACKUP_DESCR'] = "Choose existed backup from <i>'backups/'</i> folder or upload backup file.<br />Note 1: current login, email and password of root user will be saved.<br />Note 2: current database, <i>'users_content/'</i> and <i>'languages/'</i> folders will be fully overwritten, recommended to make backup from existed content -";
$language['LANG_CREATE_NEW_BACKUP_LINK'] = "make backup";
$language['LANG_BACKUP_WARNING_MSG'] = "Strongly recommended to make new backup before you import existed. Do you wish to make new backup now?";
$language['LANG_BACKUP_WARNING_QUESTION'] = "Create new backup?";
$language['LANG_SELECT_BACKUP'] = "Choose backup file";
$language['LANG_NO_BACKUPS'] = "No backups existed!";
$language['LANG_UPLOAD_BACKUP'] = "Upload backup file";
$language['LANG_IMPORT_BACKUP_BUTTON'] = "Import backup";
$language['LANG_IMPORT_BACKUPS_MENU'] = "Import backups";
$language['LANG_EXPORT_BACKUPS_MENU'] = "Export backups";

?>