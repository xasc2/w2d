<?php

$language['LANG_MANAGE_CONTENT_PAGES'] = "Manage content pages";
$language['LANG_CREATE_CONTENT_PAGE'] = "Create content page";
$language['LANG_EDIT_CONTENT_PAGE'] = "Edit content page";
$language['LANG_DELETE_CONTENT_PAGE'] = "Delete content page";
$language['LANG_DELETE_CONTENT_PAGE_QUESTION'] = "Do you really want delete this content page?";
$language['LANG_PAGE_URL'] = "Page url";
$language['LANG_PAGE_TITLE'] = "Page title";
$language['LANG_PAGE_ID'] = "Page Id number";
$language['LANG_CREATE_PAGE'] = "create page";
$language['LANG_PAGES'] = "pages";
$language['LANG_PAGE_PUBLIC_URL'] = "Page public url";
$language['LANG_BUTTON_CREATE_NEW_PAGE'] = "Create new page";
$language['LANG_TOP_MENU_TH'] = "Top Menu";
$language['LANG_CREATION_DATE_TH'] = "Creation date";
$language['LANG_VIEW_CONTENT_PAGE'] = "Preview content page";
$language['LANG_EDIT_CONTENT_PAGE_OPTION'] = "edit content page";
$language['LANG_DELETE_CONTENT_PAGE_OPTION'] = "delete content page";
$language['LANG_CONTENT_PAGE_INFO'] = "Content page info";
$language['LANG_URL_TD'] = "Url";
$language['LANG_TITLE_TD'] = "Title";
$language['LANG_CREATION_DATE_TD'] = "Creation Date";
$language['LANG_ADDITIONAL_FIELDS'] = "Additional content fields";
// Controller constants
$language['LANG_IS_UNIQUE_NODE'] = "Page with this url is already existed!";
$language['LANG_PAGES_ORDER_SAVE_SUCCESS'] = "Content pages order was successfully saved!";
$language['LANG_CONTENT_PAGE_CREATE_SUCCESS_1'] = "Content page";
$language['LANG_CONTENT_PAGE_CREATE_SUCCESS_2'] = "was created successfully!";
$language['LANG_CONTENT_PAGE_SAVE_SUCCESS_1'] = "Content page";
$language['LANG_CONTENT_PAGE_SAVE_SUCCESS_2'] = "was saved successfully!";
$language['LANG_CONTENT_PAGE_DELETE_SUCCESS'] = "Content page was deleted successfully!";
$language['LANG_MANAGE_CONTENT_PAGES_TITLE'] = "Manage content pages";
$language['LANG_CREATE_CONTENT_PAGE_TITLE'] = "Create content page";
$language['LANG_EDIT_CONTENT_PAGE_TITLE'] = "Edit content page";
$language['LANG_DELETE_CONTENT_PAGE_TITLE'] = "Delete content page";
$language['LANG_CONTENT_PAGES_MENU'] = "Content pages";
$language['LANG_CREATE_CONTENT_PAGE_MENU'] = "Create new content page";
$language['LANG_MANAGE_CONTENT_PAGE_MENU'] = "Manage content pages";


$language['LANG_BOTTOM_MENU_TH'] = "Bottom Menu";
$language['LANG_IS_PHP_CODE_TH'] = "PHP Code or template";
$language['LANG_ENABLE_LINK_IN_TOP_MENU'] = "Set content page link in the top menu";
$language['LANG_ENABLE_LINK_IN_BOTTOM_MENU'] = "Set content page link in the bottom menu";
$language['LANG_PAGE_INPUT_PARAMS'] = "Content page input parameters";
$language['LANG_PAGE_INPUT_PARAMS_DESCR'] = "These parameters content page receives from URL, then they pass into PHP code of the page. Enter each parameter on separate line.";
$language['LANG_PAGE_OUTPUT_PARAMS'] = "Content page output parameters";
$language['LANG_PAGE_OUTPUT_PARAMS_DESCR'] = "These parameters content page sends from PHP Code into smarty template. Enter each parameter on separate line. Use such syntax inside templates <i>{\$value}</i>";
$language['LANG_PAGE_PHP_CODE'] = "PHP Code";
$language['LANG_PAGE_PHP_CODE_DESCR'] = "This PHP code executes as usual controller function.";
$language['LANG_PAGE_TEMPLATE_FILE'] = "Template file name";
$language['LANG_PAGE_TEMPLATE_FILE_DESCR'] = "Set template file inside <i>'themes/{your_theme}/views/frontend/'</i> folder.";
$language['LANG_CONTENT_PAGE_EXECUTION_RESULT'] = "The result of execution of PHP Code and html/smarty code";
?>