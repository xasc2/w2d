<?php
	

$language['LANG_PAYZA_SETTINGS'] = "Payza settings";
$language['LANG_PAYZA_REDIRECT_TITLE'] = "You will be redirected to Payza";
$language['LANG_PAYZA_REDIRECT_MANUAL'] = "If you are not automatically redirected to Payza within 5 seconds...";
$language['LANG_PAYZA_EMAIL'] = "Payza Business Email";
$language['LANG_PAYZA_SECURITYCODE'] = "Payza Security code.";
$language['LANG_PAYZA_SECURITYCODE_DESCR'] = "Should be used to confirm that the IPN received came from Payza. Compare it to the IPN Security Code in your Payza account.";
?>