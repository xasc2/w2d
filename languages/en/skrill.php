<?php

$language['LANG_SKRILL_REDIRECT_TITLE'] = "You will be redirected to Skrill";
$language['LANG_SKRILL_REDIRECT_MANUAL'] = "If you are not automatically redirected to Skrill within 5 seconds...";
$language['LANG_SKRILL_EMAIL'] = "Skrill merchant email";
$language['LANG_SKRILL_SWORD'] = "Skrill secret word";
$language['LANG_SKRILL_SWORD_DESCR'] = "Login into your Skrill account and go to 'Look and Feel' section. Register the Secret Word and use it in the IPN verification process.";
$language['LANG_SKRILL_SETTINGS'] = "Skrill settings";
?>
