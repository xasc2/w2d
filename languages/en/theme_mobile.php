<?php

$language['LANG_SEARCH_BUTTON'] = "Search";
$language['LANG_HOME_BUTTON'] = "Home";
$language['LANG_BACK_BUTTON'] = "Back";
$language['LANG_SELECTED_LOCATION'] = "Location";
$language['LANG_ALL_LOCATIONS_LINK'] = "Browse in all locations";
$language['LANG_MANAGE_QUICK_LIST'] = "Manage your quick list";
$language['LANG_EMAIL_FRIEND_MOBILE'] = "Email to friend";
$language['LANG_EMAIL_OWNER_MOBILE'] = "Email to listing owner";
$language['LANG_USER_ALL_LISTINGS'] = "All listings of user";
$language['LANG_REPORT_ADMIN_MOBILE'] = "Report this listing";
$language['LANG_LISTING_MAP'] = "View Map";
$language['LANG_LISTING_USER_PAGE'] = "Author's page";
$language['LANG_LISTING_RATE_THIS'] = "Rate this";
?>