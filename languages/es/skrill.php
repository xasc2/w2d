<?php

$language['LANG_SKRILL_REDIRECT_TITLE'] = "Usted será redirigido a Skrill";
$language['LANG_SKRILL_REDIRECT_MANUAL'] = "Si no se redirigen automáticamente a Skrill en 5 segundos ...";
$language['LANG_SKRILL_EMAIL'] = "email comerciante Skrill";
$language['LANG_SKRILL_SWORD'] = "palabra secreta Skrill";
$language['LANG_SKRILL_SWORD_DESCR'] = "Inicio de sesión en su cuenta de Skrill e ir a 'Look and Feel' sección. Registrar la palabra secreta y usarla en el proceso de verificación IPN.";
$language['LANG_SKRILL_SETTINGS'] = "Skrill ajustes";
?>
