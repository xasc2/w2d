<?php

$language['LANG_BACKUPS_LABEL'] = "etiqueta de copia de seguridad";
$language['LANG_BACKUPS_LABEL_DESCR'] = "Solo alfanuméricos y símbolos dashs aceptados";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX'] = "backup Descargar después de la creación";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX_DESCR'] = "+ copia de seguridad de descarga después de la creación";
$language['LANG_BACKUPS_CREATION_SUCCESS'] = "copia de seguridad se ha creado correctamente";
$language['LANG_BACKUPS_ERROR'] = "Un error ha accoured";
$language['LANG_BACKUPS_EXISTED_FILE'] = "El archivo existe";
$language['LANG_BACKUPS_UPLOADED_FILE'] = "Subida de archivo";
$language['LANG_BACKUPS_VERSION_ERROR'] = "La versión de copia de seguridad en el sistema no es igual a la versión actual del sistema";
$language['LANG_BACKUPS_SELECT_ERROR'] = "Seleccionar archivo de copia de seguridad existente o subir un archivo";
$language['LANG_BACKUPS_IMPORT_SUCCESS'] = "Copia de seguridad se ha importado correctamente";
$language['LANG_CREATE_NEW_BACKUP'] = "Crear nueva copia de seguridad";
$language['LANG_CREATE_NEW_BACKUP_DESCR'] = "Copia de seguridad se creará en <i>'backups/'</i> Carpeta También usted puede auto-descarga.";
$language['LANG_CREATE_BACKUP_BUTTON'] = "Crear copia de seguridad";
$language['LANG_IMPORT_BACKUP'] = "backup importación";
$language['LANG_IMPORT_BACKUP_DESCR'] = "Seleccionar copia de seguridad existente de <i>'backups/'</i> carpeta o subir el archivo de copia de seguridad <br/>. Nota 1: Inicio de sesión actual de correo electrónico y la contraseña del usuario root será salvo.<br /> Nota 2: base de datos actual, <i>'users_content/'</i> y <i>'languages/'</i> carpetas estarán completamente sobrescrito, recomienda hacer copia de seguridad del contenido existente - ";
$language['LANG_CREATE_NEW_BACKUP_LINK'] = "Hacer copia de seguridad";
$language['LANG_BACKUP_WARNING_MSG'] = "Totalmente recomendable hacer copia de seguridad de nuevo antes de importar existido ¿Desea hacer copia de seguridad de nuevo ahora.?";
$language['LANG_BACKUP_WARNING_QUESTION'] = "Crear nueva copia de seguridad?";
$language['LANG_SELECT_BACKUP'] = "Seleccionar archivo de copia de seguridad";
$language['LANG_NO_BACKUPS'] = "No hay copias de seguridad existente";
$language['LANG_UPLOAD_BACKUP'] = "Cargar archivo de copia de seguridad";
$language['LANG_IMPORT_BACKUP_BUTTON'] = "backup importación";
$language['LANG_IMPORT_BACKUPS_MENU'] = "Importar copias de seguridad";
$language['LANG_EXPORT_BACKUPS_MENU'] = "copias de seguridad a la exportación";
?>