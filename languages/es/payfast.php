<?php

$language['LANG_PAYFAST_SETTINGS'] = "PayFast ajustes";
$language['LANG_PAYFAST_REDIRECT_TITLE'] = "Usted será redirigido a la página web PayFast";
$language['LANG_PAYFAST_REDIRECT_MANUAL'] = "Si no se redirigen automáticamente a PayFast en 5 segundos ...";
$language['LANG_PAYFAST_MID'] = "El mercader PayFast ID";
$language['LANG_PAYFAST_MKEY'] = "clave de comerciante PayFast";

?>