<?php

$language['LANG_PAYZA_SETTINGS'] = "Payza ajustes";
$language['LANG_PAYZA_REDIRECT_TITLE'] = "Usted será redirigido a Payza";
$language['LANG_PAYZA_REDIRECT_MANUAL'] = "Si no se redirigen automáticamente a Payza en 5 segundos ...";
$language['LANG_PAYZA_EMAIL'] = "El correo electrónico de negocios Payza";
$language['LANG_PAYZA_SECURITYCODE'] = "El código de seguridad Payza.";
$language['LANG_PAYZA_SECURITYCODE_DESCR'] = "Debe ser utilizado para confirmar que el IPN recibió provino de Payza Compárelo con el Código de Seguridad IPN en su cuenta de Payza..";
?>