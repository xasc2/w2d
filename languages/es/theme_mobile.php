<?php

$language['LANG_SEARCH_BUTTON'] = "Buscar";
$language['LANG_HOME_BUTTON'] = "Home";
$language['LANG_BACK_BUTTON'] = "Volver";
$language['LANG_SELECTED_LOCATION'] = "Ubicación";
$language['LANG_ALL_LOCATIONS_LINK'] = "Buscar en todos los lugares";
$language['LANG_MANAGE_QUICK_LIST'] = "Administrar tu lista rápida";
$language['LANG_EMAIL_FRIEND_MOBILE'] = "Enviar a un amigo";
$language['LANG_EMAIL_OWNER_MOBILE'] = "Enviar a dueño del artículo";
$language['LANG_USER_ALL_LISTINGS'] = "Todos los anuncios de usuario";
$language['LANG_REPORT_ADMIN_MOBILE'] = "Reporta este anuncio de";
$language['LANG_LISTING_MAP'] = "Ver Mapa";
$language['LANG_LISTING_USER_PAGE'] = "Página del autor";
$language['LANG_LISTING_RATE_THIS'] = "Tasa de esto";
?>