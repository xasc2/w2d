<?php

$language['LANG_PAYFAST_SETTINGS'] = "Настройки PayFast";
$language['LANG_PAYFAST_REDIRECT_TITLE'] = "Вы будете перенаправлены на сайт PayFast";
$language['LANG_PAYFAST_REDIRECT_MANUAL'] = "Если вы автоматически не перенаправлены на сайт PayFast в течении пяти секунд...";
$language['LANG_PAYFAST_MID'] = "PayFast Merchant ID";
$language['LANG_PAYFAST_MKEY'] = "PayFast Merchant Key";
?>