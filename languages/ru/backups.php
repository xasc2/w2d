<?php

$language['LANG_BACKUPS_LABEL'] = "Метка бэкапа";
$language['LANG_BACKUPS_LABEL_DESCR'] = "Только буквенно-цифровых символов и dashs принята";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX'] = "Скачать бэкап";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX_DESCR'] = "+ скачать бэкап";
$language['LANG_BACKUPS_CREATION_SUCCESS'] = "Новая резервная копия была успешно создана!";
$language['LANG_BACKUPS_ERROR'] = "Произошла ошибка!";
$language['LANG_BACKUPS_EXISTED_FILE'] = "существующий на сервере файл бэкапа";
$language['LANG_BACKUPS_UPLOADED_FILE'] = "загруженный файл бэкапа";
$language['LANG_BACKUPS_VERSION_ERROR'] = "Версия системы резервного копирования не совпадает с текущей версией системы!";
$language['LANG_BACKUPS_SELECT_ERROR'] = "Выберите существующий файл резервной копии или загрузите файл!";
$language['LANG_BACKUPS_IMPORT_SUCCESS'] = "Бэкап был успешно импортирован!";
$language['LANG_CREATE_NEW_BACKUP'] = "Создать новый бэкап";
$language['LANG_CREATE_NEW_BACKUP_DESCR'] = "Бэкап будет создан в <i>'backups/'</i> папке. Также вы можете автоматически скачать его.";
$language['LANG_CREATE_BACKUP_BUTTON'] = "Создать резервную копию";
$language['LANG_IMPORT_BACKUP'] = "Импорт бэкапа";
$language['LANG_IMPORT_BACKUP_DESCR'] = "Выберите существующий файл бэкапа из папки <i>'backups/'</i> или загрузите файл.<br />Примечание 1: Текущий логин, адрес электронной почты и пароль для root пользователя будут сохранены.<br />Примечание 2: текущая база данных, папкb <i>'users_content/'</i> и <i>'languages/'</i> будут полностью перезаписаны, рекомендуется сделать резервную копию с существовующим контентом - ";
$language['LANG_CREATE_NEW_BACKUP_LINK'] = "создать бэкап";
$language['LANG_BACKUP_WARNING_MSG'] = "Настоятельно рекомендуется сделать новую резервную копию прежде чем импортировать бэкап. Вы хотите сделать новую резервную копию сейчас?";
$language['LANG_BACKUP_WARNING_QUESTION'] = "Создать новую резервную копию?";
$language['LANG_SELECT_BACKUP'] = "Выберите файл резервной копии";
$language['LANG_NO_BACKUPS'] = "Не выбран файл резервной копии!";
$language['LANG_UPLOAD_BACKUP'] = "Загрузить файл резервной копии";
$language['LANG_IMPORT_BACKUP_BUTTON'] = "Импорт бэкапа";
$language['LANG_IMPORT_BACKUPS_MENU'] = "Импорт бэкапа";
$language['LANG_EXPORT_BACKUPS_MENU'] = "Экспорт бэкапа";

?>