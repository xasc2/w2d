<?php

$language['LANG_PAYZA_SETTINGS'] = "Payza Настройки";
$language['LANG_PAYZA_REDIRECT_TITLE'] = "Вы будете перенаправлены на Payza";
$language['LANG_PAYZA_REDIRECT_MANUAL'] = "Если вы автоматически не перенаправлены на Payza в течение 5 секунд ...";
$language['LANG_PAYZA_EMAIL'] = "Payza бизнес Email";
$language['LANG_PAYZA_SECURITYCODE'] = "Payza защитный код.";
$language['LANG_PAYZA_SECURITYCODE_DESCR'] = "Должен быть использован для подтверждения того, что IPN был получен от Payza, сравнивается с защитным кодом IPN в аккаунте Payza.";
?>