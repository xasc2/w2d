<?php

$language['LANG_MANAGE_CONTENT_PAGES'] = "Управление содержанием страниц";
$language['LANG_CREATE_CONTENT_PAGE'] = "Создание контента страницы";
$language['LANG_EDIT_CONTENT_PAGE'] = "Изменить содержание страницы";
$language['LANG_DELETE_CONTENT_PAGE'] = "Удалить содержимое страницы";
$language['LANG_DELETE_CONTENT_PAGE_QUESTION'] = "Вы действительно хотите удалить это содержимое страницы?";
$language['LANG_PAGE_URL'] = "URL страницы";
$language['LANG_PAGE_TITLE'] = "Название страницы";
$language['LANG_PAGE_ID'] = "Страница идентификационный номер";
$language['LANG_CREATE_PAGE'] = "Создать страницу";
$language['LANG_PAGES'] = "Страницы";
$language['LANG_PAGE_PUBLIC_URL'] = "Страница общественной URL";
$language['LANG_BUTTON_CREATE_NEW_PAGE'] = "Создать новую страницу";
$language['LANG_TOP_MENU_TH'] = "Верхнее меню";
$language['LANG_CREATION_DATE_TH'] = "Дата создания";
$language['LANG_ENABLE_LINK_IN_MENU'] = "Включить ссылку содержание страницы в верхнем меню";
$language['LANG_VIEW_CONTENT_PAGE'] = "Предварительный просмотр содержимого страницы";
$language['LANG_EDIT_CONTENT_PAGE_OPTION'] = "редактировать содержимое страницы";
$language['LANG_DELETE_CONTENT_PAGE_OPTION'] = "удалять содержимое страницы";
$language['LANG_CONTENT_PAGE_INFO'] = "Информация Содержание страницы";
$language['LANG_URL_TD'] = "Url";
$language['LANG_TITLE_TD'] = "Название";
$language['LANG_CREATION_DATE_TD'] = "Дата создания";
$language['LANG_ADDITIONAL_FIELDS'] = "Дополнительные поля содержания";
// Controller constants
$language['LANG_IS_UNIQUE_NODE'] = "Узел с этим URL уже существовали!";
$language['LANG_PAGES_ORDER_SAVE_SUCCESS'] = "порядок содержания страниц был сохранен!";
$language['LANG_CONTENT_PAGE_CREATE_SUCCESS_1'] = "Содержание страницы";
$language['LANG_CONTENT_PAGE_CREATE_SUCCESS_2'] = "был успешно создан!";
$language['LANG_CONTENT_PAGE_SAVE_SUCCESS_1'] = "Содержание страницы";
$language['LANG_CONTENT_PAGE_SAVE_SUCCESS_2'] = "был успешно сохранены!";
$language['LANG_CONTENT_PAGE_DELETE_SUCCESS'] = "Содержание страницы успешно удалены!";
$language['LANG_MANAGE_CONTENT_PAGES_TITLE'] = "Управление содержанием страниц";
$language['LANG_CREATE_CONTENT_PAGE_TITLE'] = "Создание контента страницы";
$language['LANG_EDIT_CONTENT_PAGE_TITLE'] = "Изменить содержание страницы";
$language['LANG_DELETE_CONTENT_PAGE_TITLE'] = "Удалить содержимое страницы";
$language['LANG_CONTENT_PAGES_MENU'] = "Содержание страницы";
$language['LANG_CREATE_CONTENT_PAGE_MENU'] = "Создать новую страницу содержания";
$language['LANG_MANAGE_CONTENT_PAGE_MENU'] = "Управление содержанием страниц";


/* Update to 3.2.0 */


$language['LANG_BOTTOM_MENU_TH'] = "Нижнее меню";
$language['LANG_IS_PHP_CODE_TH'] = "PHP код или tpl шаблон";
$language['LANG_ENABLE_LINK_IN_TOP_MENU'] = "Ссыдка на страницу в вверхнем меню";
$language['LANG_ENABLE_LINK_IN_BOTTOM_MENU'] = "Ссыдка на страницу в нижнем меню";
$language['LANG_PAGE_INPUT_PARAMS'] = "Входящие аргументы";
$language['LANG_PAGE_INPUT_PARAMS_DESCR'] = "Эти аргументы страница получает из URL, затем они передаются в PHP код. Введите каждый аргумент на отдельной строке.";
$language['LANG_PAGE_OUTPUT_PARAMS'] = "Выходные аргументы";
$language['LANG_PAGE_OUTPUT_PARAMS_DESCR'] = "Эти аргументы страница отправляет в smarty шаблон. Введите каждый аргумент на отдельной строке. Используйте следующий синтакс в шаблонах <i>{\$value}</i>";
$language['LANG_PAGE_PHP_CODE'] = "PHP код";
$language['LANG_PAGE_PHP_CODE_DESCR'] = "Этот код выполняется как обычная функция в контроллере.";
$language['LANG_PAGE_TEMPLATE_FILE'] = "Имя шаблона";
$language['LANG_PAGE_TEMPLATE_FILE_DESCR'] = "Поместите файл шаблона в папке <i>'themes/{your_theme}/views/frontend/'</i>.";
$language['LANG_CONTENT_PAGE_EXECUTION_RESULT'] = "Результат выполнения PHP кода и html/smarty кода";
?>