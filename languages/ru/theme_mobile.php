<?php

$language['LANG_SEARCH_BUTTON'] = "Поиск";
$language['LANG_HOME_BUTTON'] = "Home";
$language['LANG_BACK_BUTTON'] = "Назад";
$language['LANG_SELECTED_LOCATION'] = "Локация";
$language['LANG_ALL_LOCATIONS_LINK'] = "Во всех локациях";
$language['LANG_MANAGE_QUICK_LIST'] = "Добавить/Удалить закладку";
$language['LANG_EMAIL_FRIEND_MOBILE'] = "Написать другу";
$language['LANG_EMAIL_OWNER_MOBILE'] = "Написать автору";
$language['LANG_USER_ALL_LISTINGS'] = "Все листинги автора";
$language['LANG_REPORT_ADMIN_MOBILE'] = "Пожаловаться на листинг";
$language['LANG_LISTING_MAP'] = "Карта";
$language['LANG_LISTING_USER_PAGE'] = "Страница автора";
$language['LANG_LISTING_RATE_THIS'] = "Оценить";
?>