<?php

$language['LANG_SKRILL_REDIRECT_TITLE'] = "Вы будете перенаправлены в систему Skrill";
$language['LANG_SKRILL_REDIRECT_MANUAL'] = "Если вы автоматически не были перенапралены в систему Skrill в течении 5 секунд...";
$language['LANG_SKRILL_EMAIL'] = "Email продавца";
$language['LANG_SKRILL_SWORD'] = "Секретное слово";
$language['LANG_SKRILL_SWORD_DESCR'] = "Войдите в ваш аккаунт в Skrill и зайдите в секцию 'Look and Feel'. Зарегистрируйте секретное слово и используйте его при IPN верификации.";
$language['LANG_SKRILL_SETTINGS'] = "Настройки Skrill";
?>
