<?php

$language['LANG_BACKUPS_LABEL'] = "Backup-Label";
$language['LANG_BACKUPS_LABEL_DESCR'] = "Nur alphanumerische Zeichen und dashs akzeptiert";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX'] = "Download Backup nach Erstellung";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX_DESCR'] = "+ download Backup nach Erstellung";
$language['LANG_BACKUPS_CREATION_SUCCESS'] = "Neues Backup wurde erfolgreich erstellt";
$language['LANG_BACKUPS_ERROR'] = "Ein Fehler ist accoured!";
$language['LANG_BACKUPS_EXISTED_FILE'] = "Existierende Datei";
$language['LANG_BACKUPS_UPLOADED_FILE'] = "file Hochgeladen";
$language['LANG_BACKUPS_VERSION_ERROR'] = "Version des Systems in Backup nicht gleich mit der aktuellen Version des Systems";
$language['LANG_BACKUPS_SELECT_ERROR'] = "Wählen Sie bestanden Backup-Datei oder Datei-Upload";
$language['LANG_BACKUPS_IMPORT_SUCCESS'] = "Backup wurde erfolgreich importiert";
$language['LANG_CREATE_NEW_BACKUP'] = "Neues Backup";
$language['LANG_CREATE_NEW_BACKUP_DESCR'] = "Backup wird in <i>'backups/'</i> erstellt werden. Ordner Auch Sie können auto-Software herunterzuladen.";
$language['LANG_CREATE_BACKUP_BUTTON'] = "Backup erstellen";
$language['LANG_IMPORT_BACKUP'] = "Import backup";
$language['LANG_IMPORT_BACKUP_DESCR'] = "Wählen Sie bestanden Sicherung von <i>'backups/'</i> Ordner oder laden Backup-Datei.<br />Anmerkung 1: Aktuelle Login, E-Mail und das Passwort des Root-Benutzer gespeichert werden.<br />Anmerkung 2: aktuelle Datenbank, <i>'users_content/'</i> und <i>'languages​​/'</i> Ordner werden vollständig überschrieben, empfohlen, Backup aus existierten Inhalt machen - ";
$language['LANG_CREATE_NEW_BACKUP_LINK'] = "Sicherungskopien";
$language['LANG_BACKUP_WARNING_MSG'] = "dringend empfohlen, neue Backup machen, bevor Sie gab zu importieren Möchten Sie neue Backup jetzt machen.";
$language['LANG_BACKUP_WARNING_QUESTION'] = "Neues Backup?";
$language['LANG_SELECT_BACKUP'] = "Wählen Sie Backup-Datei";
$language['LANG_NO_BACKUPS'] = "Keine Backups existiert!";
$language['LANG_UPLOAD_BACKUP'] = "Upload Backup-Datei";
$language['LANG_IMPORT_BACKUP_BUTTON'] = "Import backup";
$language['LANG_IMPORT_BACKUPS_MENU'] = "Import Backups";
$language['LANG_EXPORT_BACKUPS_MENU'] = "Export Backups";

?>