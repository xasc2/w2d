<?php
	
$language['LANG_PAYZA_SETTINGS'] = "Payza Einstellungen";
$language['LANG_PAYZA_REDIRECT_TITLE'] = "Sie werden auf Payza umgeleitet werden";
$language['LANG_PAYZA_REDIRECT_MANUAL'] = "Wenn Sie nicht automatisch weitergeleitet, um innerhalb von 5 Sekunden Payza ...";
$language['LANG_PAYZA_EMAIL'] = "Payza Business Email";
$language['LANG_PAYZA_SECURITYCODE'] = "Payza Security code.";
$language['LANG_PAYZA_SECURITYCODE_DESCR'] = "Sollte benutzt werden, um zu bestätigen, dass die IPN erhielt von Payza kam sein Vergleichen Sie es mit dem IPN Security Code in Ihre Payza Konto.";
?>