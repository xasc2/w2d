<?php

$language['LANG_PAYFAST_SETTINGS'] = "PayFast Einstellungen";
$language['LANG_PAYFAST_REDIRECT_TITLE'] = "Sie werden auf die PayFast Website umgeleitet werden";
$language['LANG_PAYFAST_REDIRECT_MANUAL'] = "Wenn Sie nicht automatisch umgeleitet werden innerhalb von 5 Sekunden PayFast ...";
$language['LANG_PAYFAST_MID'] = "PayFast Merchant ID";
$language['LANG_PAYFAST_MKEY'] = "PayFast Händler Key";
?>