<?php

$language['LANG_SEARCH_BUTTON'] = "Suche";
$language['LANG_HOME_BUTTON'] = "Home";
$language['LANG_BACK_BUTTON'] = "Zurück";
$language['LANG_SELECTED_LOCATION'] = "Ort";
$language['LANG_ALL_LOCATIONS_LINK'] = "an allen Standorten Durchsuchen";
$language['LANG_MANAGE_QUICK_LIST'] = "Eintrag kurze Liste";
$language['LANG_EMAIL_FRIEND_MOBILE'] = "E-Mail an einen Freund";
$language['LANG_EMAIL_OWNER_MOBILE'] = "E-Mail zur Notierung Besitzer";
$language['LANG_USER_ALL_LISTINGS'] = "Alle Inserate des Benutzers";
$language['LANG_REPORT_ADMIN_MOBILE'] = "Diese Website melden";
$language['LANG_LISTING_MAP'] = "View Map";
$language['LANG_LISTING_USER_PAGE'] = "Autor der Seite";
$language['LANG_LISTING_RATE_THIS'] = "Dieses";
?>