<?php

$language['LANG_MANAGE_CONTENT_PAGES'] = "Verwalten Content-Seiten";
$language['LANG_CREATE_CONTENT_PAGE'] = "Inhalt erstellen Seite";
$language['LANG_EDIT_CONTENT_PAGE'] = "Edit Content-Seite";
$language['LANG_DELETE_CONTENT_PAGE'] = "Löschen Content-Seite";
$language['LANG_DELETE_CONTENT_PAGE_QUESTION'] = "Wollen Sie wirklich löschen dieser Content-Seite?";
$language['LANG_PAGE_URL'] = "Seiten-URL";
$language['LANG_PAGE_TITLE'] = "Seitentitel";
$language['LANG_PAGE_ID'] = "Page Id-Nummer";
$language['LANG_CREATE_PAGE'] = "erstellen Seite";
$language['LANG_PAGES'] = "Seiten";
$language['LANG_PAGE_PUBLIC_URL'] = "Page öffentliche URL";
$language['LANG_BUTTON_CREATE_NEW_PAGE'] = "Neue Seite erstellen";
$language['LANG_TOP_MENU_TH'] = "Top-Menü";
$language['LANG_CREATION_DATE_TH'] = "Erstellungsdatum";
$language['LANG_ENABLE_LINK_IN_MENU'] = "Enable Content-Seite Link im oberen Menü";
$language['LANG_VIEW_CONTENT_PAGE'] = "Vorschau Content-Seite";
$language['LANG_EDIT_CONTENT_PAGE_OPTION'] = "Inhalt bearbeiten Seite";
$language['LANG_DELETE_CONTENT_PAGE_OPTION'] = "löschen Content-Seite";
$language['LANG_CONTENT_PAGE_INFO'] = "Inhalt Seite info";
$language['LANG_URL_TD'] = "URL";
$language['LANG_TITLE_TD'] = "Titel";
$language['LANG_CREATION_DATE_TD'] = "Erstellungsdatum";
$language['LANG_ADDITIONAL_FIELDS'] = "Zusätzlicher Inhalt Felder";
// Controller constants
$language['LANG_IS_UNIQUE_NODE'] = "Der Knoten mit dieser URL ist bereits vorhanden!";
$language['LANG_PAGES_ORDER_SAVE_SUCCESS'] = "Content-Seiten Ordnung wurde erfolgreich gespeichert!";
$language['LANG_CONTENT_PAGE_CREATE_SUCCESS_1'] = "Inhalt Seite";
$language['LANG_CONTENT_PAGE_CREATE_SUCCESS_2'] = "wurde erfolgreich eingerichtet!";
$language['LANG_CONTENT_PAGE_SAVE_SUCCESS_1'] = "Inhalt Seite";
$language['LANG_CONTENT_PAGE_SAVE_SUCCESS_2'] = "wurde erfolgreich gespeichert!";
$language['LANG_CONTENT_PAGE_DELETE_SUCCESS'] = "Inhalt Seite wurde erfolgreich gelöscht!";
$language['LANG_MANAGE_CONTENT_PAGES_TITLE'] = "Verwalten Content-Seiten";
$language['LANG_CREATE_CONTENT_PAGE_TITLE'] = "Inhalt erstellen Seite";
$language['LANG_EDIT_CONTENT_PAGE_TITLE'] = "Edit Content-Seite";
$language['LANG_DELETE_CONTENT_PAGE_TITLE'] = "Löschen Content-Seite";
$language['LANG_CONTENT_PAGES_MENU'] = "Content-Seiten";
$language['LANG_CREATE_CONTENT_PAGE_MENU'] = "Erstellen Sie neue Content-Seite";
$language['LANG_MANAGE_CONTENT_PAGE_MENU'] = "Verwalten Content-Seiten";


/* Update to 3.2.0 */


$language['LANG_BOTTOM_MENU_TH'] = "Bottom Menu";
$language['LANG_IS_PHP_CODE_TH'] = "PHP-Code oder eine Vorlage";
$language['LANG_ENABLE_LINK_IN_TOP_MENU'] = "Set Content-Seite Link in der oberen Menüleiste";
$language['LANG_ENABLE_LINK_IN_BOTTOM_MENU'] = "Set Content-Seite Link im unteren Menü";
$language['LANG_PAGE_INPUT_PARAMS'] = "Content-Seite Eingangsparameter";
$language['LANG_PAGE_INPUT_PARAMS_DESCR'] = "Diese Parameter Content-Seite erhält von URL, dann werden sie in PHP-Code der Seite übergeben Geben Sie jeden Parameter auf separaten Zeile..";
$language['LANG_PAGE_OUTPUT_PARAMS'] = "Content-Seite Ausgangsparameter";
$language['LANG_PAGE_OUTPUT_PARAMS_DESCR'] = "Diese Parameter Content-Seite sendet von PHP-Code in smarty template Geben Sie jeden Parameter separate. Zeile verwenden diese Syntax in Vorlagen <i>{\$value}</i>.";
$language['LANG_PAGE_PHP_CODE'] = "PHP-Code";
$language['LANG_PAGE_PHP_CODE_DESCR'] = "Dieser PHP-Code ausführt wie gewohnt Controller-Funktion.";
$language['LANG_PAGE_TEMPLATE_FILE'] = "Template Dateiname";
$language['LANG_PAGE_TEMPLATE_FILE_DESCR'] = "Set-Vorlage-Datei in <i>'themes/{your_theme}/views/frontend/'</i> Ordner.";
$language['LANG_CONTENT_PAGE_EXECUTION_RESULT'] = "Das Ergebnis der Ausführung von PHP-Code und HTML/smarty-Code";

?>