<?php

$language['LANG_SKRILL_REDIRECT_TITLE'] = "Du wirst Skrill umgeleitet werden";
$language['LANG_SKRILL_REDIRECT_MANUAL'] = "Wenn Sie nicht automatisch umgeleitet werden innerhalb von 5 Sekunden Skrill ...";
$language['LANG_SKRILL_EMAIL'] = "Skrill Händlers E-Mail";
$language['LANG_SKRILL_SWORD'] = "Skrill geheime Wort";
$language['LANG_SKRILL_SWORD_DESCR'] = "Login in Ihr Skrill-Konto und gehen Sie auf 'Look and Feel' Bereich. Registrieren dem geheimen Wort und verwenden Sie es in der IPN Verifikation.";
$language['LANG_SKRILL_SETTINGS'] = "Skrill Einstellungen";
?>
