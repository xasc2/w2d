<?php

$language['LANG_SEARCH_BUTTON'] = "Cerca";
$language['LANG_HOME_BUTTON'] = "Home";
$language['LANG_BACK_BUTTON'] = "Indietro";
$language['LANG_SELECTED_LOCATION'] = "Posizione";
$language['LANG_ALL_LOCATIONS_LINK'] = "Cerca in tutte le sedi";
$language['LANG_MANAGE_QUICK_LIST'] = "Gestione lista veloce";
$language['LANG_EMAIL_FRIEND_MOBILE'] = "E-mail ad un amico";
$language['LANG_EMAIL_OWNER_MOBILE'] = "E-mail al proprietario lista";
$language['LANG_USER_ALL_LISTINGS'] = "Tutti gli annunci di utente";
$language['LANG_REPORT_ADMIN_MOBILE'] = "Segnala questo annuncio";
$language['LANG_LISTING_MAP'] = "Visualizza Mappa";
$language['LANG_LISTING_USER_PAGE'] = "La pagina dell'autore";
$language['LANG_LISTING_RATE_THIS'] = "Vota questo";
?>