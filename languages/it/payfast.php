<?php

$language['LANG_PAYFAST_SETTINGS'] = "PayFast impostazioni";
$language['LANG_PAYFAST_REDIRECT_TITLE'] = "Sarai reindirizzato al sito web PayFast";
$language['LANG_PAYFAST_REDIRECT_MANUAL'] = "Se non sei reindirizzato automaticamente alla PayFast entro 5 secondi ...";
$language['LANG_PAYFAST_MID'] = "Mercante PayFast ID";
$language['LANG_PAYFAST_MKEY'] = "codice commerciante PayFast";

?>