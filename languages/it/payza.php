<?php

$language['LANG_PAYZA_SETTINGS'] = "Payza impostazioni";
$language['LANG_PAYZA_REDIRECT_TITLE'] = "Sarai reindirizzato a Payza";
$language['LANG_PAYZA_REDIRECT_MANUAL'] = "Se non sei automaticamente reindirizzato al Payza entro 5 secondi ...";
$language['LANG_PAYZA_EMAIL'] = "Business Email Payza";
$language['LANG_PAYZA_SECURITYCODE'] = "Codice di sicurezza Payza.";
$language['LANG_PAYZA_SECURITYCODE_DESCR'] = "dovrebbe essere usato per confermare che l'IPN ha ricevuto proveniva da Payza confrontarlo con il codice di sicurezza IPN sul tuo conto Payza..";
?>