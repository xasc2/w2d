<?php

$language['LANG_BACKUPS_LABEL'] = "label di backup";
$language['LANG_BACKUPS_LABEL_DESCR'] = "solo caratteri alfanumerici e simboli dashs accettati";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX'] = "Scarica il backup dopo la creazione";
$language['LANG_DOWNLOAD_BACKUP_CHECKBOX_DESCR'] = "+ backup scaricare dopo la creazione";
$language['LANG_BACKUPS_CREATION_SUCCESS'] = "Nuovo backup è stato creato con successo!";
$language['LANG_BACKUPS_ERROR'] = "Errore accoured!";
$language['LANG_BACKUPS_EXISTED_FILE'] = "file esiste";
$language['LANG_BACKUPS_UPLOADED_FILE'] = "Caricato il file";
$language['LANG_BACKUPS_VERSION_ERROR'] = "Versione del sistema di backup non è uguale con la versione corrente del sistema";
$language['LANG_BACKUPS_SELECT_ERROR'] = "Scegli file di backup esistente o caricare il file!";
$language['LANG_BACKUPS_IMPORT_SUCCESS'] = "Backup è stato importato con successo";
$language['LANG_CREATE_NEW_BACKUP'] = "Crea nuovo backup";
$language['LANG_CREATE_NEW_BACKUP_DESCR'] = "Il backup verrà creato in <i>'backup/'</i> cartella Inoltre si può auto-scarica.";
$language['LANG_CREATE_BACKUP_BUTTON'] = "Crea backup";
$language['LANG_IMPORT_BACKUP'] = "backup Importa";
$language['LANG_IMPORT_BACKUP_DESCR'] = "Scegli il backup esiste da <i>'backup/'</i> cartella o caricare file di backup.<br />Nota 1: Accesso corrente, e-mail e la password dell'utente root sarà salvato.<br />Nota 2: database corrente, <i>'users_content/'</i> e '<i>languages/'</i> cartelle sarà completamente sovrascritto, consiglia di fare il backup dal contenuto esistito - ";
$language['LANG_CREATE_NEW_BACKUP_LINK'] = "fare il backup";
$language['LANG_BACKUP_WARNING_MSG'] = "consiglia vivamente di fare il backup prima di importare nuovi esisteva Volete fare nuovo backup ora.?";
$language['LANG_BACKUP_WARNING_QUESTION'] = "Crea nuovo backup?";
$language['LANG_SELECT_BACKUP'] = "Scegli file di backup";
$language['LANG_NO_BACKUPS'] = "Nessun backup esistito!";
$language['LANG_UPLOAD_BACKUP'] = "Carica file di backup";
$language['LANG_IMPORT_BACKUP_BUTTON'] = "backup Importa";
$language['LANG_IMPORT_BACKUPS_MENU'] = "backup Importa";
$language['LANG_EXPORT_BACKUPS_MENU'] = "backup esportazione";

?>