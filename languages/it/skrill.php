<?php

$language['LANG_SKRILL_REDIRECT_TITLE'] = "Si verrà reindirizzati alla Skrill";
$language['LANG_SKRILL_REDIRECT_MANUAL'] = "Se non sei reindirizzato automaticamente alla Skrill entro 5 secondi ...";
$language['LANG_SKRILL_EMAIL'] = "email mercante Skrill";
$language['LANG_SKRILL_SWORD'] = "parola segreta Skrill";
$language['LANG_SKRILL_SWORD_DESCR'] = "Accedi al tuo account Skrill e andare a 'look and feel' la sezione. Registrare la Parola Segreta e utilizzarlo nel processo di verifica IPN.";
$language['LANG_SKRILL_SETTINGS'] = "Skrill impostazioni";
?>
