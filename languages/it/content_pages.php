<?php

$language['LANG_MANAGE_CONTENT_PAGES'] = "Gestisci le pagine di contenuto";
$language['LANG_CREATE_CONTENT_PAGE'] = "Creare il contenuto della pagina";
$language['LANG_EDIT_CONTENT_PAGE'] = "Modifica il contenuto della pagina";
$language['LANG_DELETE_CONTENT_PAGE'] = "Elimina il contenuto della pagina";
$language['LANG_DELETE_CONTENT_PAGE_QUESTION'] = "Sei sicuro di voler cancellare questa pagina di contenuto?";
$language['LANG_PAGE_URL'] = "URL della pagina";
$language['LANG_PAGE_TITLE'] = "Titolo della pagina";
$language['LANG_PAGE_ID'] = "Pagina Id numero";
$language['LANG_CREATE_PAGE'] = "creare pagina";
$language['LANG_PAGES'] = "pagine";
$language['LANG_PAGE_PUBLIC_URL'] = "URL della pagina pubblica";
$language['LANG_BUTTON_CREATE_NEW_PAGE'] = "Crea nuova pagina";
$language['LANG_TOP_MENU_TH'] = "Top Menu";
$language['LANG_CREATION_DATE_TH'] = "Data di creazione";
$language['LANG_ENABLE_LINK_IN_MENU'] = "Abilita pagina link contenuto nel menu in alto";
$language['LANG_VIEW_CONTENT_PAGE'] = "Anteprima contenuto della pagina";
$language['LANG_EDIT_CONTENT_PAGE_OPTION'] = "modificare il contenuto della pagina";
$language['LANG_DELETE_CONTENT_PAGE_OPTION'] = "cancellare il contenuto della pagina";
$language['LANG_CONTENT_PAGE_INFO'] = "Contenuto della pagina info";
$language['LANG_URL_TD'] = "Url";
$language['LANG_TITLE_TD'] = "Titolo";
$language['LANG_CREATION_DATE_TD'] = "Data di creazione";
$language['LANG_ADDITIONAL_FIELDS'] = "campi di contenuti aggiuntivi";
// Controller constants
$language['LANG_IS_UNIQUE_NODE'] = "Il nodo con questo URL è già esistito!";
$language['LANG_PAGES_ORDER_SAVE_SUCCESS'] = "Contenuti per pagine, è stato salvato con successo!";
$language['LANG_CONTENT_PAGE_CREATE_SUCCESS_1'] = "Contenuto della pagina";
$language['LANG_CONTENT_PAGE_CREATE_SUCCESS_2'] = "è stato creato con successo!";
$language['LANG_CONTENT_PAGE_SAVE_SUCCESS_1'] = "Contenuto della pagina";
$language['LANG_CONTENT_PAGE_SAVE_SUCCESS_2'] = "è stato salvato con successo!";
$language['LANG_CONTENT_PAGE_DELETE_SUCCESS'] = "Contenuto della pagina è stato cancellato con successo!";
$language['LANG_MANAGE_CONTENT_PAGES_TITLE'] = "Gestisci le pagine di contenuto";
$language['LANG_CREATE_CONTENT_PAGE_TITLE'] = "Creare il contenuto della pagina";
$language['LANG_EDIT_CONTENT_PAGE_TITLE'] = "Modifica il contenuto della pagina";
$language['LANG_DELETE_CONTENT_PAGE_TITLE'] = "Elimina il contenuto della pagina";
$language['LANG_CONTENT_PAGES_MENU'] = "Pagine di Contenuti";
$language['LANG_CREATE_CONTENT_PAGE_MENU'] = "Crea nuova pagina di contenuto";
$language['LANG_MANAGE_CONTENT_PAGE_MENU'] = "Gestisci le pagine di contenuto";


/* Update to 3.2.0 */


$language['LANG_BOTTOM_MENU_TH'] = "Menu inferiore";
$language['LANG_IS_PHP_CODE_TH'] = "codice PHP o un modello";
$language['LANG_ENABLE_LINK_IN_TOP_MENU'] = "Il contenuto collegamento Imposta pagina nel menu in alto";
$language['LANG_ENABLE_LINK_IN_BOTTOM_MENU'] = "Il contenuto collegamento Imposta pagina nel menu in basso";
$language['LANG_PAGE_INPUT_PARAMS'] = "Contenuto pagina parametri di input";
$language['LANG_PAGE_INPUT_PARAMS_DESCR'] = "Questi parametri contenuti pagina riceve da URL, poi passano in codice PHP della pagina Immettere ogni parametro su una riga diversa.";
$language['LANG_PAGE_OUTPUT_PARAMS'] = "Contenuto pagina parametri di output";
$language['LANG_PAGE_OUTPUT_PARAMS_DESCR'] = "Questi parametri contenuti pagina invia da codice PHP nel template smarty Immettere ogni parametro su una riga diversa Utilizzare una sintassi simile all'interno dei modelli <i>{\$valore}</i>.";
$language['LANG_PAGE_PHP_CODE'] = "Codice PHP";
$language['LANG_PAGE_PHP_CODE_DESCR'] = "Questo codice PHP viene eseguito come funzione normale controller.";
$language['LANG_PAGE_TEMPLATE_FILE'] = "nome del file modello";
$language['LANG_PAGE_TEMPLATE_FILE_DESCR'] = "Imposta file di modello interno <i>'themes/{your_theme}/views/frontend/'</i> cartella.";
$language['LANG_CONTENT_PAGE_EXECUTION_RESULT'] = "Il risultato della esecuzione di codice PHP e HTML/smarty codice";
?>