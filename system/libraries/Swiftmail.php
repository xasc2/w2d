<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Swiftmail {
	// base
	var $CI = FALSE;
	var $config = array(
			'use_smtp_mail'=>'',
			'smtp_host'=>'',
			'smtp_port'=>'',
			'smtp_user'=>'',
			'smtp_pass'=>'',
			'smtp_use_ssl'=>'',
			'use_sendmail'=>'',
			'sendmail_path'=>'',
			'sendmail_parameters'=>''
	);
	// default transport type
	var $transport_type = 'mail';

	// state
	var $initialized = FALSE;
	
	// abstraction mode variables
	var $messages = array();
	var $mailer = FALSE;
	
	
	public function __construct($config = array())
	{
		include_once(BASEPATH . 'libraries/swiftmailer/swift_required.php');

		$this->config = $config;

		$this->CI = &get_instance();
		if ($this->CI->config->item('use_smtp_mail') && $this->CI->config->item('smtp_host') && $this->CI->config->item('smtp_port')) {
			$this->config = array(
					'smtp_host' => $this->CI->config->item('smtp_host'),
					'smtp_port' => $this->CI->config->item('smtp_port'),
					'smtp_user' => $this->CI->config->item('smtp_username'),
					'smtp_pass' => $this->CI->config->item('smtp_password'),
					'smtp_use_ssl' => $this->CI->config->item('smtp_use_ssl'),
			);
			$this->transport_type = 'smtp';
		}
		
		if (isset($this->config['use_sendmail']) && $this->config['use_sendmail'])
			$this->transport_type = 'sendmail';

		$this->initialize_abstraction_mode();
		
		$this->initialized = TRUE;
	}

	private function create_smtp_transport()
	{
		/* Initialize transport */
		$this->transport = Swift_SmtpTransport::newInstance($this->config['smtp_host'], $this->config['smtp_port']);
	
		if($this->config['smtp_user'])
			$this->transport->setUsername($this->config['smtp_user']);
	
		if($this->config['smtp_pass'])
			$this->transport->setPassword($this->config['smtp_pass']);
	
		// configure ssl
		if($this->config['smtp_use_ssl'])
			$this->transport->setEncryption('ssl');
	
		return TRUE;
	}
	
	private function create_sendmail_transport()
	{
		if(!isset($this->config['sendmail_path']) || !$this->config['sendmail_path']) return FALSE;
	
		$this->transport = Swift_SendmailTransport::newInstance($this->config['sendmail_path'] . ($this->config['sendmail_parameters'] ? ' ' . $this->config['sendmail_parameters'] : ''));
	
		return FALSE;
	}
	
	private function create_mail_transport()
	{
		$this->transport = Swift_MailTransport::newInstance('');
	
		return FALSE;
	}
	
	private function initialize_abstraction_mode()
	{
		switch($this->transport_type)
		{
			case 'smtp':
				$this->create_smtp_transport();
				break;
			case 'sendmail':
				$this->create_sendmail_transport();
				break;
			case 'mail':
				$this->create_mail_transport();
				break;
		}
	}
	
	public function create_message($message_subject = FALSE, $message_body = FALSE, $message_from = FALSE, $message_to = FALSE, $reply_to = FALSE, $content_type = 'text/plain', $message_identifier = FALSE)
	{
		if(!$this->initialized) return false;
		if(!$message_identifier) $message_identifier = 'default';
	
		// initialize message
		$this->messages[$message_identifier] = Swift_Message::newInstance();
	
		// set subject / body
		if($message_subject) $this->messages[$message_identifier]->setSubject($message_subject);
		if($message_body) $this->messages[$message_identifier]->setBody($message_body);
		$this->messages[$message_identifier]->setEncoder(Swift_Encoding::get8BitEncoding());
	
		// set from addresses - array('email@address.com' => 'sender from name')
		if($message_from)
		{
			$this->messages[$message_identifier]->setFrom($message_from);
		}
	
		// set recipient addresses - array('email@address.com' => 'recipient name')
		if($message_to)
		{
			$this->messages[$message_identifier]->setTo($message_to);
		}

		if($reply_to)
		{
			$this->messages[$message_identifier]->setReplyTo($reply_to);
		}
	
		if($content_type)
		{
			$this->messages[$message_identifier]->setContentType($content_type);
		}
	
		return TRUE;
	}
	
	public function add_attachment($message_identifier = FALSE, $filepath = FALSE)
	{
		if(!$this->initialized) return false;
		if(!$message_identifier) $message_identifier = 'default';
		if(!$filepath) return FALSE;
		if(!$this->messages[$message_identifier]) $this->create_message($message_identifier);
	
		$this->messages[$message_identifier]->attach(Swift_Attachment::fromPath($filepath));
	}
	
	public function add_part($message_identifier = FALSE, $message_part_content = FALSE, $message_part_mime = 'text/html')
	{
		if(!$this->initialized) return false;
		if(!$message_identifier) $message_identifier = 'default';
		if(!$this->messages[$message_identifier]) $this->create_message($message_identifier);
	
		if($message_part_content) $this->messages[$message_identifier]->addPart($message_part_content, $message_part_mime);
	}
	
	public function send_message($message_identifier = FALSE)
	{
		if(!$this->initialized) return false;
		if(!$message_identifier) $message_identifier = 'default';
		if(!$this->messages[$message_identifier]) return FALSE;
		if(!$this->transport) return FALSE;
	
		// set up mailer
		if(!$this->mailer) $this->mailer = Swift_Mailer::newInstance($this->transport);
	
		// send the message
		return $this->mailer->send($this->messages[$message_identifier]);
	}
	
	public function send_all_messages()
	{
		if(!$this->initialized) return false;
		if(count($this->messages) == 0) return FALSE;
		if(!$this->transport) return FALSE;
	
		// set up mailer
		if(!$this->mailer) $this->mailer = Swift_Mailer::newInstance($this->transport);
	
		// loop through messages
		$results = array();
		foreach($this->messages as $message_identifier => $message)
		{
			// send
			$results[$message_identifier] = $this->mailer->send($message);
		}
	
		return $results;
	}
}