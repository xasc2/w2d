<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class events 
{
	private static $events = array();
	
	public static function setEvent($event_name, $file, $module_name, $function_name, $theme, $weight)
	{
		self::$events[] = array('name' => $event_name, 'file' => $file, 'module' => $module_name, 'function_name' => $function_name, 'theme' => $theme, 'weight' => $weight);

		// Sort events by weights
		$sortAux = array();
		$count = 1000;
		foreach(self::$events as $key=>$event_item) {
			if (!isset($event_item['weight']) || !is_numeric($event_item['weight'])) {
				$event_item['weight'] = $count;
				$count++;
				self::$events[$key]['weight'] = $event_item['weight'];
			}
			$sortAux[] = $event_item['weight'];
		}
		if (!empty($sortAux))
			array_multisort($sortAux, SORT_ASC, SORT_NUMERIC, self::$events);
	}
	
	public static function callEvent()
	{
		// Move event's name to the end of the args array
		$args = func_get_args();
		$called_event_name = $args[0];
		if (count($args) > 1) {
			array_shift($args);
		}
		$args[] = $called_event_name;

		if (!empty(self::$events)) {
			foreach (self::$events AS $key=>$event) {
				if ($event['name'] == $called_event_name) {
					Controller::run_function($event['file'], $event['module'], $event['function_name'], $event['theme'], $args);
				}
			}
		}
	}
}

function callEvent() {
	// func_get_arg() does not appear to be allowed to be used as a function argument itself
	$args = func_get_args();
	return call_user_func_array(array('events', 'callEvent'), $args);
}