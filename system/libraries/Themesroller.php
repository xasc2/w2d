<?php

class themesroller
{
    private static $themes_list = array();
    private $default_theme_name = 'default';
    private $mobile_theme_name = 'mobile';
    private $system_theme_name;
    private $language_theme_name;
    private $url_theme_name;

    private $current_theme_name = null;
    private $current_theme_instance = null;

    /**
     * Themes from DB table
     * @param array $array
     */
    public function setThemesList($array)
    {
        self::$themes_list = $array;
    }

    /**
     * These 2 methods use when new themes folders appear or disappear in 'themes/' directory
     */
    public function addThemeToList($array)
    {
        self::$themes_list[] = $array;
    }
    public function removeThemeFromList($dir)
    {
    	if (isset(self::$themes_list[$dir]))
        	unset(self::$themes_list[$dir]);
    }


	public function getThemesList()
    {
        return self::$themes_list;
    }
    
    public function setSystemThemeName($system_theme_name)
    {
    	$this->system_theme_name = $system_theme_name;
    }

    public function getSystemThemeName()
    {
    	return $this->system_theme_name;
    }
    
	public function getLanguageThemeName()
    {
    	$CI = &get_instance();
        if ($CI->load->is_module_loaded("i18n")) {
        	// Load custom theme of current language
        	$current_language_db_obj = registry::get('current_language_db_obj');
        	$this->language_theme_name = $current_language_db_obj->custom_theme;
        	return $this->language_theme_name;
        } else 
        	return false;
    }
    
    public function checkURLSegments()
    {
    	// Load theme according to URL's additional segment
        $url_segments = registry::get('url_additional_segments_matches');
        if (is_array($url_segments) && array_key_exists('theme', $url_segments)) {
        	$this->url_theme_name = $url_segments['theme'];
        	return $this->url_theme_name;
        } else 
        	return false;
    }
    
    public function getDefaultTheme()
    {
    	return $this->default_theme_name;
    }
    
	public function isMobile() {
		if(isset($_SERVER["HTTP_X_WAP_PROFILE"])) {
			return true;
		}
		if(isset($_SERVER["HTTP_ACCEPT"]) && preg_match("/wap\.|\.wap/i",$_SERVER["HTTP_ACCEPT"])) {
			return true;
		}
		if(isset($_SERVER["HTTP_USER_AGENT"])){
			$user_agents = array("Googlebot-Mobile", "midp", "j2me", "avantg", "docomo", "novarra", "palmos", "palmsource", "240x320", "opwv", "chtml", "pda", "windows\ ce", "mmp\/", "blackberry", "mib\/", "symbian", "wireless", "nokia", "hand", "mobi", "phone", "cdm", "up\.b", "audio", "SIE\-", "SEC\-", "samsung", "HTC", "mot\-", "mitsu", "sagem", "sony", "alcatel", "lg", "erics", "vx", "NEC", "philips", "mmm", "xx", "panasonic", "sharp", "wap", "sch", "rover", "pocket", "benq", "java", "pt", "pg", "vox", "amoi", "bird", "compal", "kg", "voda", "sany", "kdd", "dbt", "sendo", "sgh", "gradi", "jb", "\d\d\di", "moto");
			foreach($user_agents as $user_string){
				if(preg_match("/".$user_string."/i",$_SERVER["HTTP_USER_AGENT"])) {
					return true;
				}
			}
		}
		// Apple's product are a bit different so lets see if they are using iphone, ipad or ipod device
		if(isset($_SERVER["HTTP_USER_AGENT"]) && (preg_match("/iphone/i",$_SERVER["HTTP_USER_AGENT"]) || preg_match("/ipad/i",$_SERVER["HTTP_USER_AGENT"]) || preg_match("/ipod/i",$_SERVER["HTTP_USER_AGENT"]))) {
			return true;
		}
		// None of the above? Then it's probably not a mobile device.
		return false;
	}
    
    public function theme()
    {
    	if ($this->current_theme_name)
    		return $this->current_theme_name;
    	elseif ($this->checkURLSegments())
    		$this->current_theme_name = $this->url_theme_name;
    	elseif ($this->isMobile() && is_dir(THEMES_PATH . $this->mobile_theme_name) && (isset(self::$themes_list[$this->mobile_theme_name]) && self::$themes_list[$this->mobile_theme_name]['installed']))
    		$this->current_theme_name = $this->mobile_theme_name;
    	elseif ($this->getLanguageThemeName())
    		$this->current_theme_name = $this->language_theme_name;
    	elseif ($this->system_theme_name)
    		$this->current_theme_name = $this->system_theme_name;
    	else 
    		$this->current_theme_name = $this->getDefaultTheme();

    	$CI = &get_instance();
    	if (strpos($CI->uri->uri_string, 'update') === FALSE && (!isset(self::$themes_list[$this->current_theme_name]) || !self::$themes_list[$this->current_theme_name]['installed'])) {
    		//exit("Install this theme first!");
    		if (!$_POST)
    			echo "You are using uninstalled design theme or something was broken in current theme. Go into settings, select one of your already installed design themes and click to save changes!.<br />";
    		return $this->getDefaultTheme();
    	}

    	return $this->current_theme_name;
    }
    
    public function getThemesViewsPaths()
    {
    	// Check selected theme, if available - smarty will look for templates in that folder initially,
        // also set default theme - smarty will look for templates in that folder in the second order
        $themes = array();
        if (is_dir(THEMES_PATH . $this->theme() . DIRECTORY_SEPARATOR . 'views'))
        	$themes[] = THEMES_PATH . $this->theme() . DIRECTORY_SEPARATOR . 'views';
        $themes[] = THEMES_PATH . $this->default_theme_name . DIRECTORY_SEPARATOR . 'views';
        return $themes;
    }
    
    public function getAllSamePathsInThemes($path_part)
    {
    	$paths = array();
    	$themes_list = $this->getThemesList();
   		foreach ($themes_list AS $theme) {
   			$path = 'themes' . DIRECTORY_SEPARATOR . $theme['dir'] . DIRECTORY_SEPARATOR . $path_part . DIRECTORY_SEPARATOR;
   			if (is_dir($path))
   				$paths[] = $path;
   		}
   		return $paths;
    }
    
    public function themeInstance()
    {
    	if ($this->current_theme_instance)
    		return $this->current_theme_instance;
    	
    	$theme_dir = $this->theme();
    	if (is_file(THEMES_PATH . $theme_dir . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . $theme_dir . '.theme.php')) {
    		$theme_file = THEMES_PATH . $theme_dir . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . $theme_dir . '.theme.php';
    		$theme_class = $theme_dir . 'Theme';
    	} else {
    		$theme_file = THEMES_PATH . $this->default_theme_name . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . $this->default_theme_name . '.theme.php';
    		$theme_class = $this->default_theme_name . 'Theme';
    	}
    	include_once($theme_file);
		$this->current_theme_instance = new $theme_class;

		return $this->current_theme_instance;
    }
    
    public function getThemeHooks()
    {
    	$theme_dir = $this->theme();

    	if (is_file(THEMES_PATH . $theme_dir . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . $theme_dir . '.theme.php')) {
    		$hooks_file = THEMES_PATH . $theme_dir . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . $theme_dir . '_hook.php';
    		$module_name = $theme_dir;
    	} else {
    		$hooks_file = THEMES_PATH . $this->default_theme_name . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . $this->default_theme_name . '_hook.php';
    		$module_name = $this->default_theme_name;
    	}
    	
		$theme_instance = $this->themeInstance();
		$hook = array();
		if (method_exists($theme_instance, 'hooks')) {
			$hook[$module_name] = $theme_instance->hooks();
		}

		if (is_file($hooks_file))
			include_once($hooks_file);

    	return $hook;
    }
    
    public function loadLanguageFiles()
    {
    	$theme_dir = $this->theme();
		$loading_language = registry::get('current_language');

    	if (is_dir(LANGPATH . $loading_language)) {
			$language = array();
			/**
			 * $language_files_array = array('modules' => array(array()), 'themes' => array(array()))
			 */
			$language_files_array = registry::get('language_files_array');

			// Scan languages directory
			$languages_map = directory_map(LANGPATH . $loading_language);
			foreach ($languages_map AS $file) {
				// Include themes lang files
				if (isset($language_files_array['themes'][$theme_dir]))
					foreach ($language_files_array['themes'][$theme_dir] AS $db_file) {
						// Is there such file as in the DB
						if ($db_file == $file) {
							if (is_file(LANGPATH . $loading_language . DIRECTORY_SEPARATOR . $file)) {
								include_once(LANGPATH . $loading_language . DIRECTORY_SEPARATOR . $file);
							} else {
								// Load default english file
								include_once(LANGPATH . 'en' . DIRECTORY_SEPARATOR . $file);
							}
						}
					}
			}

			// --------------------------------------------------------------------------------------------
			// If translation mode enabled - reverce language vars with its keys
			// --------------------------------------------------------------------------------------------
			$config = load_class('Config');
			if ($config->item('translation_mode')) {
				foreach ($language AS $var=>$string)
					$language[$var] = $var;
			}
			
			// --------------------------------------------------------------------------------------------
			// Define lang constants
			// --------------------------------------------------------------------------------------------
			foreach ($language AS $key=>$var) {
				define($key, $var);
			}

			// --------------------------------------------------------------------------------------------
			// Load langauge constants into views environment
			// --------------------------------------------------------------------------------------------
			view::setLanguageVars($language);

			log_message('debug', "Theme language file was loaded");
		} else {
			show_error('Theme language file was not found in the language directory');
		}
    }
}

?>