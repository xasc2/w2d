<?php
include_once(BASEPATH . 'richtext_editor/ckeditor/ckeditor.php');
require_once(BASEPATH . 'richtext_editor/ckfinder/ckfinder.php');

class richtextEditor
{
	public $ckeditor;
	public $instanceName;
	public $instanceValue;
	
	public function __construct($instanceName, $instanceValue = '')
	{
		$this->instanceName = $instanceName;
		$this->instanceValue = $instanceValue;
		
		$this->ckeditor = new CKEditor();
		$this->ckeditor->basePath	= base_url() . 'system/richtext_editor/ckeditor/';

		$this->config('filebrowserBrowseUrl', base_url() . 'system/richtext_editor/ckfinder/ckfinder.html');
		$this->config('filebrowserImageBrowseUrl', base_url() . 'system/richtext_editor/ckfinder/ckfinder.html?type=Images');
		$this->config('filebrowserImageUploadUrl', base_url() . 'system/richtext_editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images');
		
		$language_code = registry::get('current_language');
		if ($language_code && $language_code != 'en')
			$this->ckeditor->config['language'] = $language_code; 

        $CI = &get_instance();
        // Set users content folder paths (server and http)
        $_SESSION['upload_files_server_path'] = rtrim($CI->config->item('users_content_server_path'), '/');
        $_SESSION['upload_files_http_path'] = trim($CI->config->item('users_content_http_path'), '/');
	}
	
	public function CreateHtml()
	{
		$this->ckeditor->editor($this->instanceName, $this->instanceValue);
	}
	
	public function disableImageUpload()
	{
		$this->config('filebrowserBrowseUrl', false);
		$this->config('filebrowserImageBrowseUrl', false);
		$this->config('filebrowserImageUploadUrl', false);
	}
	
	public function config($item, $value)
	{
		$this->ckeditor->config[$item] = $value;
	}
}
?>