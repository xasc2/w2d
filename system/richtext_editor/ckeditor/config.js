﻿/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	config.toolbar = 'Default';
	 
	config.toolbar_Default =
	[
		{ name: 'document', items : [ 'Source' ] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','-','Undo','Redo' ] },
		{ name: 'insert', items : [ 'Image','Table','HorizontalRule','Smiley'] },
		{ name: 'styles', items : [ 'FontSize','Format','Font' ] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','Blockquote' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] }
	];

	config.toolbar_Review =
		[
		 { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','-','RemoveFormat','-','TextColor' ] },
		 { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
		 { name: 'links', items : [ 'Link','Unlink'] },
		 { name: 'insert', items : [ 'Image','Smiley'] }
		 ];
};
