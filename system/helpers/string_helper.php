<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter String Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/string_helper.html
 */

// ------------------------------------------------------------------------

/**
 * Trim Slashes
 *
 * Removes any leading/traling slashes from a string:
 *
 * /this/that/theother/
 *
 * becomes:
 *
 * this/that/theother
 *
 * @access	public
 * @param	string
 * @return	string
 */	
if ( ! function_exists('trim_slashes'))
{
	function trim_slashes($str)
	{
		return trim($str, '');
	} 
}
	
// ------------------------------------------------------------------------

/**
 * Strip Slashes
 *
 * Removes slashes contained in a string or in an array
 *
 * @access	public
 * @param	mixed	string or array
 * @return	mixed	string or array
 */	
if ( ! function_exists('strip_slashes'))
{
	function strip_slashes($str)
	{
		if (is_array($str))
		{	
			foreach ($str as $key => $val)
			{
				$str[$key] = strip_slashes($val);
			}
		}
		else
		{
			$str = stripslashes($str);
		}
	
		return $str;
	}
}

// ------------------------------------------------------------------------

/**
 * Strip Quotes
 *
 * Removes single and double quotes from a string
 *
 * @access	public
 * @param	string
 * @return	string
 */	
if ( ! function_exists('strip_quotes'))
{
	function strip_quotes($str)
	{
		return str_replace(array('"', "'"), '', $str);
	}
}

// ------------------------------------------------------------------------

/**
 * Quotes to Entities
 *
 * Converts single and double quotes to entities
 *
 * @access	public
 * @param	string
 * @return	string
 */	
if ( ! function_exists('quotes_to_entities'))
{
	function quotes_to_entities($str)
	{	
		return str_replace(array("\'","\"","'",'"'), array("&#39;","&quot;","&#39;","&quot;"), $str);
	}
}

// ------------------------------------------------------------------------
/**
 * Reduce Double Slashes
 *
 * Converts double slashes in a string to a single slash,
 * except those found in http://
 *
 * http://www.some-site.com//index.php
 *
 * becomes:
 *
 * http://www.some-site.com/index.php
 *
 * @access	public
 * @param	string
 * @return	string
 */	
if ( ! function_exists('reduce_double_slashes'))
{
	function reduce_double_slashes($str)
	{
		return preg_replace("#([^:])//+#", "\\1/", $str);
	}
}
	
// ------------------------------------------------------------------------

/**
 * Reduce Multiples
 *
 * Reduces multiple instances of a particular character.  Example:
 *
 * Fred, Bill,, Joe, Jimmy
 *
 * becomes:
 *
 * Fred, Bill, Joe, Jimmy
 *
 * @access	public
 * @param	string
 * @param	string	the character you wish to reduce
 * @param	bool	TRUE/FALSE - whether to trim the character from the beginning/end
 * @return	string
 */	
if ( ! function_exists('reduce_multiples'))
{
	function reduce_multiples($str, $character = ',', $trim = FALSE)
	{
		$str = preg_replace('#'.preg_quote($character, '#').'{2,}#', $character, $str);

		if ($trim === TRUE)
		{
			$str = trim($str, $character);
		}

		return $str;
	}
}
	
// ------------------------------------------------------------------------

/**
 * Create a Random String
 *
 * Useful for generating passwords or hashes.
 *
 * @access	public
 * @param	string 	type of random string.  Options: alunum, numeric, nozero, unique
 * @param	integer	number of characters
 * @return	string
 */
if ( ! function_exists('random_string'))
{	
	function random_string($type = 'alnum', $len = 8)
	{					
		switch($type)
		{
			case 'alnum'	:
			case 'numeric'	:
			case 'nozero'	:
		
					switch ($type)
					{
						case 'alnum'	:	$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
							break;
						case 'numeric'	:	$pool = '0123456789';
							break;
						case 'nozero'	:	$pool = '123456789';
							break;
					}

					$str = '';
					for ($i=0; $i < $len; $i++)
					{
						$str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
					}
					return $str;
			  break;
			case 'unique' : return md5(uniqid(mt_rand()));
			  break;
		}
	}
}

// ------------------------------------------------------------------------

/**
 * Alternator
 *
 * Allows strings to be alternated.  See docs...
 *
 * @access	public
 * @param	string (as many parameters as needed)
 * @return	string
 */	
if ( ! function_exists('alternator'))
{
	function alternator()
	{
		static $i;	

		if (func_num_args() == 0)
		{
			$i = 0;
			return '';
		}
		$args = func_get_args();
		return $args[($i++ % count($args))];
	}
}

// ------------------------------------------------------------------------

/**
 * Repeater function
 *
 * @access	public
 * @param	string
 * @param	integer	number of repeats
 * @return	string
 */	
if ( ! function_exists('repeater'))
{
	function repeater($data, $num = 1)
	{
		return (($num > 0) ? str_repeat($data, $num) : '');
	} 
}

// ------------------------------------------------------------------------

/**
 * Transfers string to userfriendly seo format
 *
 */	
if ( ! function_exists('friendly_seo_string'))
{
	function friendly_seo_string($string, $separator = '-')
	{
		/* $string = strtolower($string);
		$defaultDiacriticsRemovalMap = array(
			array('base'=>'A', 'letters'=>'[\x0041\x24B6\xFF21\x00C0\x00C1\x00C2\x1EA6\x1EA4\x1EAA\x1EA8\x00C3\x0100\x0102\x1EB0\x1EAE\x1EB4\x1EB2\x0226\x01E0\x00C4\x01DE\x1EA2\x00C5\x01FA\x01CD\x0200\x0202\x1EA0\x1EAC\x1EB6\x1E00\x0104\x023A\x2C6F]'),
			array('base'=>'AA', 'letters'=>'[\xA732]'),
			array('base'=>'AE', 'letters'=>'[\x00C6\x01FC\x01E2]'),
			array('base'=>'AO', 'letters'=>'[\xA734]'),
			array('base'=>'AU', 'letters'=>'[\xA736]'),
			array('base'=>'AV', 'letters'=>'[\xA738\xA73A]'),
			array('base'=>'AY', 'letters'=>'[\xA73C]'),
			array('base'=>'B',  'letters'=>'[\x0042\x24B7\xFF22\x1E02\x1E04\x1E06\x0243\x0182\x0181]'),
			array('base'=>'C',  'letters'=>'[\x0043\x24B8\xFF23\x0106\x0108\x010A\x010C\x00C7\x1E08\x0187\x023B\xA73E]'),
			array('base'=>'D',  'letters'=>'[\x0044\x24B9\xFF24\x1E0A\x010E\x1E0C\x1E10\x1E12\x1E0E\x0110\x018B\x018A\x0189\xA779]'),
			array('base'=>'DZ', 'letters'=>'[\x01F1\x01C4]'),
			array('base'=>'Dz', 'letters'=>'[\x01F2\x01C5]'),
			array('base'=>'E',  'letters'=>'[\x0045\x24BA\xFF25\x00C8\x00C9\x00CA\x1EC0\x1EBE\x1EC4\x1EC2\x1EBC\x0112\x1E14\x1E16\x0114\x0116\x00CB\x1EBA\x011A\x0204\x0206\x1EB8\x1EC6\x0228\x1E1C\x0118\x1E18\x1E1A\x0190\x018E]'),
			array('base'=>'F',  'letters'=>'[\x0046\x24BB\xFF26\x1E1E\x0191\xA77B]'),
			array('base'=>'G',  'letters'=>'[\x0047\x24BC\xFF27\x01F4\x011C\x1E20\x011E\x0120\x01E6\x0122\x01E4\x0193\xA7A0\xA77D\xA77E]'),
			array('base'=>'H',  'letters'=>'[\x0048\x24BD\xFF28\x0124\x1E22\x1E26\x021E\x1E24\x1E28\x1E2A\x0126\x2C67\x2C75\xA78D]'),
			array('base'=>'I',  'letters'=>'[\x0049\x24BE\xFF29\x00CC\x00CD\x00CE\x0128\x012A\x012C\x0130\x00CF\x1E2E\x1EC8\x01CF\x0208\x020A\x1ECA\x012E\x1E2C\x0197]'),
			array('base'=>'J',  'letters'=>'[\x004A\x24BF\xFF2A\x0134\x0248]'),
			array('base'=>'K',  'letters'=>'[\x004B\x24C0\xFF2B\x1E30\x01E8\x1E32\x0136\x1E34\x0198\x2C69\xA740\xA742\xA744\xA7A2]'),
			array('base'=>'L',  'letters'=>'[\x004C\x24C1\xFF2C\x013F\x0139\x013D\x1E36\x1E38\x013B\x1E3C\x1E3A\x0141\x023D\x2C62\x2C60\xA748\xA746\xA780]'),
			array('base'=>'LJ', 'letters'=>'[\x01C7]'),
			array('base'=>'Lj', 'letters'=>'[\x01C8]'),
			array('base'=>'M',  'letters'=>'[\x004D\x24C2\xFF2D\x1E3E\x1E40\x1E42\x2C6E\x019C]'),
			array('base'=>'N',  'letters'=>'[\x004E\x24C3\xFF2E\x01F8\x0143\x00D1\x1E44\x0147\x1E46\x0145\x1E4A\x1E48\x0220\x019D\xA790\xA7A4]'),
			array('base'=>'NJ', 'letters'=>'[\x01CA]'),
			array('base'=>'Nj', 'letters'=>'[\x01CB]'),
			array('base'=>'O',  'letters'=>'[\x004F\x24C4\xFF2F\x00D2\x00D3\x00D4\x1ED2\x1ED0\x1ED6\x1ED4\x00D5\x1E4C\x022C\x1E4E\x014C\x1E50\x1E52\x014E\x022E\x0230\x00D6\x022A\x1ECE\x0150\x01D1\x020C\x020E\x01A0\x1EDC\x1EDA\x1EE0\x1EDE\x1EE2\x1ECC\x1ED8\x01EA\x01EC\x00D8\x01FE\x0186\x019F\xA74A\xA74C]'),
			array('base'=>'OI', 'letters'=>'[\x01A2]'),
			array('base'=>'OO', 'letters'=>'[\xA74E]'),
			array('base'=>'OU', 'letters'=>'[\x0222]'),
			array('base'=>'P',  'letters'=>'[\x0050\x24C5\xFF30\x1E54\x1E56\x01A4\x2C63\xA750\xA752\xA754]'),
			array('base'=>'Q',  'letters'=>'[\x0051\x24C6\xFF31\xA756\xA758\x024A]'),
			array('base'=>'R',  'letters'=>'[\x0052\x24C7\xFF32\x0154\x1E58\x0158\x0210\x0212\x1E5A\x1E5C\x0156\x1E5E\x024C\x2C64\xA75A\xA7A6\xA782]'),
			array('base'=>'S',  'letters'=>'[\x0053\x24C8\xFF33\x1E9E\x015A\x1E64\x015C\x1E60\x0160\x1E66\x1E62\x1E68\x0218\x015E\x2C7E\xA7A8\xA784]'),
			array('base'=>'T',  'letters'=>'[\x0054\x24C9\xFF34\x1E6A\x0164\x1E6C\x021A\x0162\x1E70\x1E6E\x0166\x01AC\x01AE\x023E\xA786]'),
			array('base'=>'TZ', 'letters'=>'[\xA728]'),
			array('base'=>'U',  'letters'=>'[\x0055\x24CA\xFF35\x00D9\x00DA\x00DB\x0168\x1E78\x016A\x1E7A\x016C\x00DC\x01DB\x01D7\x01D5\x01D9\x1EE6\x016E\x0170\x01D3\x0214\x0216\x01AF\x1EEA\x1EE8\x1EEE\x1EEC\x1EF0\x1EE4\x1E72\x0172\x1E76\x1E74\x0244]'),
			array('base'=>'V',  'letters'=>'[\x0056\x24CB\xFF36\x1E7C\x1E7E\x01B2\xA75E\x0245]'),
			array('base'=>'VY', 'letters'=>'[\xA760]'),
			array('base'=>'W',  'letters'=>'[\x0057\x24CC\xFF37\x1E80\x1E82\x0174\x1E86\x1E84\x1E88\x2C72]'),
			array('base'=>'X',  'letters'=>'[\x0058\x24CD\xFF38\x1E8A\x1E8C]'),
			array('base'=>'Y',  'letters'=>'[\x0059\x24CE\xFF39\x1EF2\x00DD\x0176\x1EF8\x0232\x1E8E\x0178\x1EF6\x1EF4\x01B3\x024E\x1EFE]'),
			array('base'=>'Z',  'letters'=>'[\x005A\x24CF\xFF3A\x0179\x1E90\x017B\x017D\x1E92\x1E94\x01B5\x0224\x2C7F\x2C6B\xA762]'),
			array('base'=>'a',  'letters'=>'[\x0061\x24D0\xFF41\x1E9A\x00E0\x00E1\x00E2\x1EA7\x1EA5\x1EAB\x1EA9\x00E3\x0101\x0103\x1EB1\x1EAF\x1EB5\x1EB3\x0227\x01E1\x00E4\x01DF\x1EA3\x00E5\x01FB\x01CE\x0201\x0203\x1EA1\x1EAD\x1EB7\x1E01\x0105\x2C65\x0250]'),
			array('base'=>'aa', 'letters'=>'[\xA733]'),
			array('base'=>'ae', 'letters'=>'[\x00E6\x01FD\x01E3]'),
			array('base'=>'ao', 'letters'=>'[\xA735]'),
			array('base'=>'au', 'letters'=>'[\xA737]'),
			array('base'=>'av', 'letters'=>'[\xA739\xA73B]'),
			array('base'=>'ay', 'letters'=>'[\xA73D]'),
			array('base'=>'b',  'letters'=>'[\x0062\x24D1\xFF42\x1E03\x1E05\x1E07\x0180\x0183\x0253]'),
			array('base'=>'c',  'letters'=>'[\x0063\x24D2\xFF43\x0107\x0109\x010B\x010D\x00E7\x1E09\x0188\x023C\xA73F\x2184]'),
			array('base'=>'d',  'letters'=>'[\x0064\x24D3\xFF44\x1E0B\x010F\x1E0D\x1E11\x1E13\x1E0F\x0111\x018C\x0256\x0257\xA77A]'),
			array('base'=>'dz', 'letters'=>'[\x01F3\x01C6]'),
			array('base'=>'e',  'letters'=>'[\x0065\x24D4\xFF45\x00E8\x00E9\x00EA\x1EC1\x1EBF\x1EC5\x1EC3\x1EBD\x0113\x1E15\x1E17\x0115\x0117\x00EB\x1EBB\x011B\x0205\x0207\x1EB9\x1EC7\x0229\x1E1D\x0119\x1E19\x1E1B\x0247\x025B\x01DD]'),
			array('base'=>'f',  'letters'=>'[\x0066\x24D5\xFF46\x1E1F\x0192\xA77C]'),
			array('base'=>'g',  'letters'=>'[\x0067\x24D6\xFF47\x01F5\x011D\x1E21\x011F\x0121\x01E7\x0123\x01E5\x0260\xA7A1\x1D79\xA77F]'),
			array('base'=>'h',  'letters'=>'[\x0068\x24D7\xFF48\x0125\x1E23\x1E27\x021F\x1E25\x1E29\x1E2B\x1E96\x0127\x2C68\x2C76\x0265]'),
			array('base'=>'hv', 'letters'=>'[\x0195]'),
			array('base'=>'i',  'letters'=>'[\x0069\x24D8\xFF49\x00EC\x00ED\x00EE\x0129\x012B\x012D\x00EF\x1E2F\x1EC9\x01D0\x0209\x020B\x1ECB\x012F\x1E2D\x0268\x0131]'),
			array('base'=>'j',  'letters'=>'[\x006A\x24D9\xFF4A\x0135\x01F0\x0249]'),
			array('base'=>'k',  'letters'=>'[\x006B\x24DA\xFF4B\x1E31\x01E9\x1E33\x0137\x1E35\x0199\x2C6A\xA741\xA743\xA745\xA7A3]'),
			array('base'=>'l',  'letters'=>'[\x006C\x24DB\xFF4C\x0140\x013A\x013E\x1E37\x1E39\x013C\x1E3D\x1E3B\x017F\x0142\x019A\x026B\x2C61\xA749\xA781\xA747]'),
			array('base'=>'lj', 'letters'=>'[\x01C9]'),
			array('base'=>'m',  'letters'=>'[\x006D\x24DC\xFF4D\x1E3F\x1E41\x1E43\x0271\x026F]'),
			array('base'=>'n',  'letters'=>'[\x006E\x24DD\xFF4E\x01F9\x0144\x00F1\x1E45\x0148\x1E47\x0146\x1E4B\x1E49\x019E\x0272\x0149\xA791\xA7A5]'),
			array('base'=>'nj', 'letters'=>'[\x01CC]'),
			array('base'=>'o',  'letters'=>'[\x006F\x24DE\xFF4F\x00F2\x00F3\x00F4\x1ED3\x1ED1\x1ED7\x1ED5\x00F5\x1E4D\x022D\x1E4F\x014D\x1E51\x1E53\x014F\x022F\x0231\x00F6\x022B\x1ECF\x0151\x01D2\x020D\x020F\x01A1\x1EDD\x1EDB\x1EE1\x1EDF\x1EE3\x1ECD\x1ED9\x01EB\x01ED\x00F8\x01FF\x0254\xA74B\xA74D\x0275]'),
			array('base'=>'oi', 'letters'=>'[\x01A3]'),
			array('base'=>'ou', 'letters'=>'[\x0223]'),
			array('base'=>'oo', 'letters'=>'[\xA74F]'),
			array('base'=>'p', 'letters'=>'[\x0070\x24DF\xFF50\x1E55\x1E57\x01A5\x1D7D\xA751\xA753\xA755]'),
			array('base'=>'q', 'letters'=>'[\x0071\x24E0\xFF51\x024B\xA757\xA759]'),
			array('base'=>'r', 'letters'=>'[\x0072\x24E1\xFF52\x0155\x1E59\x0159\x0211\x0213\x1E5B\x1E5D\x0157\x1E5F\x024D\x027D\xA75B\xA7A7\xA783]'),
			array('base'=>'s', 'letters'=>'[\x0073\x24E2\xFF53\x00DF\x015B\x1E65\x015D\x1E61\x0161\x1E67\x1E63\x1E69\x0219\x015F\x023F\xA7A9\xA785\x1E9B]'),
			array('base'=>'t', 'letters'=>'[\x0074\x24E3\xFF54\x1E6B\x1E97\x0165\x1E6D\x021B\x0163\x1E71\x1E6F\x0167\x01AD\x0288\x2C66\xA787]'),
			array('base'=>'tz', 'letters'=>'[\xA729]'),
			array('base'=>'u', 'letters'=>'[\x0075\x24E4\xFF55\x00F9\x00FA\x00FB\x0169\x1E79\x016B\x1E7B\x016D\x00FC\x01DC\x01D8\x01D6\x01DA\x1EE7\x016F\x0171\x01D4\x0215\x0217\x01B0\x1EEB\x1EE9\x1EEF\x1EED\x1EF1\x1EE5\x1E73\x0173\x1E77\x1E75\x0289]'),
			array('base'=>'v', 'letters'=>'[\x0076\x24E5\xFF56\x1E7D\x1E7F\x028B\xA75F\x028C]'),
			array('base'=>'vy', 'letters'=>'[\xA761]'),
			array('base'=>'w', 'letters'=>'[\x0077\x24E6\xFF57\x1E81\x1E83\x0175\x1E87\x1E85\x1E98\x1E89\x2C73]'),
			array('base'=>'x', 'letters'=>'[\x0078\x24E7\xFF58\x1E8B\x1E8D]'),
			array('base'=>'y', 'letters'=>'[\x0079\x24E8\xFF59\x1EF3\x00FD\x0177\x1EF9\x0233\x1E8F\x00FF\x1EF7\x1E99\x1EF5\x01B4\x024F\x1EFF]'),
			array('base'=>'z', 'letters'=>'[\x007A\x24E9\xFF5A\x017A\x1E91\x017C\x017E\x1E93\x1E95\x01B6\x0225\x0240\x2C6C\xA763]')
		);
		
		foreach ($defaultDiacriticsRemovalMap AS $item) {
			$string = preg_replace($item['letters'] . 'u', $item['base'], $string);
		}
		
		$string = preg_replace("/\W/i", $separator, $string);
		
		return trim($string, $separator); */
		
		$string = preg_replace("`\[.*\]`U","", $string);
		$string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i', $separator, $string);
		$string = htmlentities($string, ENT_COMPAT, 'utf-8');
		$string = preg_replace("`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string);
		$string = preg_replace(array("`[^a-z0-9]`i","`[-]+`") , $separator, $string);
		
		
		
		return strtolower(trim($string, $separator));
		
		
		/*$string = trim($string);
		$string = strtolower($string); // convert to lowercase text
		// Recommendation URL: http://www.webcheatsheet.com/php/regular_expressions.php
		// Only space, letters, numbers and underscore are allowed
		$string = trim(preg_replace("/[^ A-Za-z0-9-]u", " ", $string));*/
		
		/*
		"t" (ASCII 9 (0x09)), a tab.
		"n" (ASCII 10 (0x0A)), a new line (line feed).
		"r" (ASCII 13 (0x0D)), a carriage return. 
		*/

		//$string = preg_replace("/[ tnr]+/u", "-", $string);
		//$string = str_replace(" ", $separator, $string);
		//$string = preg_replace("/[ -]+/u", "-", $string);

		//return $string;
	}
}

// ------------------------------------------------------------------------

/**
 * It is the analog of str_split for multibyte strings
 *
 */	
if ( ! function_exists('mb_str_split'))
{
	function mb_str_split($str, $length = 1) {
		if ($length < 1) return FALSE;
		
		$result = array();
		
		for ($i = 0; $i < mb_strlen($str); $i += $length) {
			$result[] = mb_substr($str, $i, $length);
		}
		
		return $result;
	}
}


// ------------------------------------------------------------------------

if ( ! function_exists('explodeSize'))
{
	function explodeSize($value, $part) {
    	if ($part === 'width')
    		$part = 0;
    	if ($part === 'height')
    		$part = 1;

        $a = explode('*', $value);
        return $a[$part];
    }
}


/* End of file string_helper.php */
/* Location: ./system/helpers/string_helper.php */