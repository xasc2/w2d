<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * parses .sql dump file and returns all queries in array
 *
 */


// OLD function - not working with backups
/*function getQueriesFromFile($file)
{
	$sql = file_get_contents($file);

	$queries  = array();
	$strlen   = strlen($sql);
	$position = 0;
	$query    = '';

	for (; $position < $strlen; ++$position) {
		$char = $sql{$position};

		switch ($char) 
		{
			case '-':
				if ( substr($sql, $position, 3) !== '-- ' ) {
					$query .= $char;
					break;
				}

			case '#':
				while ($char !== "\r" && $char !== "\n" && $position < $strlen - 1)
					$char = $sql{++$position};
				break;

			case '`':
			case '\'':
			case '"':
				$quote  = $char;
				$query .= $quote;

				while ($position < $strlen - 1) {
					$char = $sql{ ++$position };

					if ($char === '\\') {
						$query .= $char;

						if ($position < $strlen - 1) {
							$char   = $sql{++$position};
							$query .= $char;

							if ($position < $strlen - 1)
								$char = $sql{++$position};
						} else {
							break;
						}
					}

					if ($char === $quote)
						break;

					$query .= $char;
				}

				$query .= $quote;
				break;

			case ';':
				$query = trim($query);
				if ($query)
					$queries[] = $query;
				$query = '';
				break;

			default:
				$query .= $char;
				break;
		}
	}

	$query = trim($query);
	if ($query)
		$queries[] = $query;

	return $queries;
}*/



// Implementation of phpBB methods
function remove_comments(&$output)
{
   $lines = explode("\n", $output);
   $output = "";

   // try to keep mem. use down
   $linecount = count($lines);

   $in_comment = false;
   for($i = 0; $i < $linecount; $i++)
   {
      if( preg_match("/^\/\*/", preg_quote($lines[$i])) )
      {
         $in_comment = true;
      }

      if( !$in_comment )
      {
         $output .= $lines[$i] . "\n";
      }

      if( preg_match("/\*\/$/", preg_quote($lines[$i])) )
      {
         $in_comment = false;
      }
   }

   unset($lines);
   return $output;
}

//
// remove_remarks will strip the sql comment lines out of an uploaded sql file
//
function remove_remarks($sql)
{
   $lines = explode("\n", $sql);

   // try to keep mem. use down
   $sql = "";

   $linecount = count($lines);
   $output = "";

   for ($i = 0; $i < $linecount; $i++)
   {
      if (($i != ($linecount - 1)) || (strlen($lines[$i]) > 0))
      {
         if (isset($lines[$i][0]) && $lines[$i][0] != "#")
         {
            $output .= $lines[$i] . "\n";
         }
         else
         {
            $output .= "\n";
         }
         // Trading a bit of speed for lower mem. use here.
         $lines[$i] = "";
      }
   }

   return $output;

}

//
// split_sql_file will split an uploaded sql file into single sql statements.
// Note: expects trim() to have already been run on $sql.
//
function split_sql_file($sql, $end_of_query)
{
	$delimiter = ";" . $end_of_query . "\n";
	
   // Split up our string into "possible" SQL statements.
   //$tokens = explode($delimiter, $sql);
   $tokens = preg_split("/;" . $end_of_query . "(\r\n|\n|\r)/", $sql);

   // try to save mem.
   $sql = "";
   $output = array();

   // we don't actually care about the matches preg gives us.
   $matches = array();

   // this is faster than calling count($oktens) every time thru the loop.
   $token_count = count($tokens);
   for ($i = 0; $i < $token_count; $i++)
   {
      // Don't wanna add an empty string as the last thing in the array.
      if (($i != ($token_count - 1)) || (strlen($tokens[$i] > 0)))
      {
         // This is the total number of single quotes in the token.
         $total_quotes = preg_match_all("/'/", $tokens[$i], $matches);
         // Counts single quotes that are preceded by an odd number of backslashes,
         // which means they're escaped quotes.
         $escaped_quotes = preg_match_all("/(?<!\\\\)(\\\\\\\\)*\\\\'/", $tokens[$i], $matches);

         $unescaped_quotes = $total_quotes - $escaped_quotes;

         // If the number of unescaped quotes is even, then the delimiter did NOT occur inside a string literal.
         if (($unescaped_quotes % 2) == 0)
         {
            // It's a complete sql statement.
            $output[] = $tokens[$i];
            // save memory.
            $tokens[$i] = "";
         }
         else
         {
            // incomplete sql statement. keep adding tokens until we have a complete one.
            // $temp will hold what we have so far.
            $temp = $tokens[$i] . $delimiter;
            // save memory..
            $tokens[$i] = "";

            // Do we have a complete statement yet?
            $complete_stmt = false;

            for ($j = $i + 1; (!$complete_stmt && ($j < $token_count)); $j++)
            {
               // This is the total number of single quotes in the token.
               $total_quotes = preg_match_all("/'/", $tokens[$j], $matches);
               // Counts single quotes that are preceded by an odd number of backslashes,
               // which means they're escaped quotes.
               $escaped_quotes = preg_match_all("/(?<!\\\\)(\\\\\\\\)*\\\\'/", $tokens[$j], $matches);

               $unescaped_quotes = $total_quotes - $escaped_quotes;

               if (($unescaped_quotes % 2) == 1)
               {
                  // odd number of unescaped quotes. In combination with the previous incomplete
                  // statement(s), we now have a complete statement. (2 odds always make an even)
                  $output[] = $temp . $tokens[$j];

                  // save memory.
                  $tokens[$j] = "";
                  $temp = "";

                  // exit the loop.
                  $complete_stmt = true;
                  // make sure the outer loop continues at the right point.
                  $i = $j;
               }
               else
               {
                  // even number of unescaped quotes. We still don't have a complete statement.
                  // (1 odd and 1 even always make an odd)
                  $temp .= $tokens[$j] . $delimiter;
                  // save memory.
                  $tokens[$j] = "";
               }

            } // for..
         } // else
      }
   }

   return $output;
}


function getQueriesFromFile($file, $end_of_query = "")
{
	$sql = file_get_contents($file);
	$sql = remove_remarks($sql);
	$sql = split_sql_file($sql, $end_of_query);
	return $sql;
}

?>