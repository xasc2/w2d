<?php
include_once(BASEPATH . 'view/smarty/libs/Smarty.class.php');

/**
 * helps to run other helpers functions inside smarty teplates
 * using $VH assigned object
 * 
 * Example: use $VH->anchor() 
 * instead of anchor()
 *
 */
class view_helper
{
	public function __call($name, $args)
	{
		if (function_exists($name))
			return call_user_func_array($name, $args);
	}
}


class view extends Smarty
{
    private static $_css_files = array();
    private static $_js_scripts = array();
    private static $_ex_js_scripts = array();
    private static $_ex_css_files = array();
    
    private static $_system_settings = null;
    private static $_site_settings = null;

    private static $_triggers = array();
    private static $lang_vars = array();

    private $curr_module;
    
    private $_theme;
    private $_default_theme;

    public function __construct()
    {
    	if (is_null(self::$_system_settings) && is_null(self::$_site_settings))
    	{
    		self::$_system_settings = registry::get('system_settings');
    		self::$_site_settings = registry::get('site_settings');
    	}

        //      Initialize Smarty
        parent::__construct();
        
        $CI = &get_instance();
        if ($CI->load->is_module_loaded("i18n"))
        	$current_language_db_obj = registry::get('current_language_db_obj');

        $themes_roller = registry::get('themes_roller');
        $this->_theme = $themes_roller->theme();
        $this->_default_theme = $themes_roller->getDefaultTheme();

        // --------------------------------------------------------------------------------------------
        // Check selected theme, if available - smarty will look for templates in that folder initially,
        // also set default theme - smarty will look for templates in that folder in the second order
        $this->template_dir  = $themes_roller->getThemesViewsPaths();
        // --------------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------------
        // Smarty requires different compile directories for each theme
        $c_dir = BASEPATH . 'view'.DIRECTORY_SEPARATOR.'smarty'.DIRECTORY_SEPARATOR.'templates_c'.DIRECTORY_SEPARATOR.$this->_theme;
        if (!is_dir($c_dir))
        	@mkdir($c_dir, 0777);
        // --------------------------------------------------------------------------------------------

        $this->compile_dir   = $c_dir;
        $this->config_dir    = BASEPATH . 'view' . DIRECTORY_SEPARATOR . 'smarty' . DIRECTORY_SEPARATOR . 'configs';

        // Initialize system vars - if they weren't assigned yet
        if (!isset($this->tpl_vars['VH'])) {
        	// Assign base object
        	$this->compile_check = $CI->config->item('smarty_compile_check');
        	$this->assign('CI', $CI);
        	
        	// Assign content ACL object
        	$content_access_obj = contentAcl::getInstance();
        	$this->assign('content_access_obj', $content_access_obj);
        	
        	// Assign system and site settings
        	$this->assign('system_settings', self::$_system_settings);
        	$this->assign('site_settings', self::$_site_settings);
        	
        	// Assign current language code
        	$this->assign('current_language', registry::get('current_language'));

        	// Assign view helper
			$VH = new view_helper;
			$this->assign('VH', $VH);
			
			// Assign language strings
        	$this->assign(self::$lang_vars);
        	
        	// Assign path to public content (js, css, images)
        	$path = base_url() . 'themes/' . $this->_theme . '/';
	        $this->assign('public_path', $path);
	        registry::set('public_path', $path);

        	// Assign users upload public path
        	$users_content_path = $CI->config->item('users_content_http_path');
        	$this->assign('users_content', trim($users_content_path, '/'));
        	
        	// Assign current uri
        	$this->assign('route', implode('/', $CI->router->uri->segments));
        	// Assign current route template
        	$this->assign('route_template', registry::get('current_route'));
        	
        	// Assign 1st part of title tag, extracted from controller attributes
        	$controller_attrs = registry::get('controller_attrs');
        	if (isset($controller_attrs['title']))
        		$this->assign('title', $controller_attrs['title']);

        	// Assign decimals and thousands separator from language settings
        	// or default from config.php
        	if ($CI->load->is_module_loaded("i18n")) {
        		$this->assign('decimals_separator', $current_language_db_obj->decimals_separator);
        		$this->assign('thousands_separator', $current_language_db_obj->thousands_separator);
        	} else {
        		$this->assign('decimals_separator', $CI->config->item('decimals_separator'));
        		$this->assign('thousands_separator', $CI->config->item('thousands_separator'));
        	}
        	
        	// Assign session user id
        	$this->assign_by_ref('session_user_id', $CI->session->userdata('user_id'));

        	// Assign current location/type/category/listing/user/serach params from registry
        	$this->assign('current_location', registry::get('current_location'));
        	$this->assign('current_type', registry::get('current_type'));
        	$this->assign('current_category', registry::get('current_category'));
        	$this->assign('current_listing', registry::get('current_listing'));
        	$this->assign('current_user', registry::get('current_user'));
        	$this->assign('current_search_params_url', registry::get('current_search_params'));
        	
        	// Assign self view object
        	$this->assign_by_ref('smarty_obj', $this);
        }
        $this->assign_by_ref('css_files', self::$_css_files);
        $this->assign_by_ref('js_scripts', self::$_js_scripts);
        $this->assign_by_ref('ex_js_scripts', self::$_ex_js_scripts);
    }
    
    public function getFileInTheme($file_name, $with_base_url = true)
    {
    	if (is_file(THEMES_PATH . $this->_theme . DIRECTORY_SEPARATOR . $file_name)) {
    		return ($with_base_url ? base_url() : '') . 'themes/' . $this->_theme . '/' . $file_name;
    	} else {
    		return ($with_base_url ? base_url() : '') . 'themes/' . $this->_default_theme . '/' . $file_name;
    	}
    }

    public function addCssFile($file, $media = 'screen')
    {
    	$file_path = $this->getFileInTheme('css/' . $file);

    	if (!array_key_exists($file_path, array_keys(self::$_css_files))) {
        	self::$_css_files[$file_path] = $media;
    	}
    }
    public function addExternalCssFile($file)
    {
    	if (!in_array($file, self::$_ex_css_files))
        	self::$_ex_css_files[] = $file;
    }

    public function addJsFile($file)
    {
    	if (!in_array($file, self::$_js_scripts)) {
    		$file_path = $this->getFileInTheme('js/' . $file, false);
    		
        	self::$_js_scripts[] = $file_path;
    	}
    }
    public function addExternalJsFile($file)
    {
    	if (!in_array($file, self::$_ex_js_scripts))
        	self::$_ex_js_scripts[] = $file;
    }
    
    public function removeJsFile($file)
    {
    	$file_path = base_url() . 'themes/' . $this->_theme . '/js/' . $file;
    	if (($key = array_search($file, self::$_js_scripts)) !== false)
        	unset(self::$_js_scripts[$key]);
        	
		$file_path = base_url() . 'themes/' . $this->_default_theme . '/js/' . $file;
		if (($key = array_search($file, self::$_js_scripts)) !== false)
        	unset(self::$_js_scripts[$key]);
    }
    
    public function clearCssJs()
    {
    	self::$_css_files = array();
    	self::$_js_scripts = array();
    	self::$_ex_js_scripts = array();
    }

    public function display($template = null, $cache_id = null, $compile_id = null)
    {
        if (empty($template)) {
	    	$template = $this->template;
    	}

	    $CI = &get_instance();
    	$CI->events->callEvent('onDisplay', $template, $this->curr_module);
        $output = parent::fetch($template);

        echo $output;
        return $output;
    }
    
    public function fetch($template = null, $cache_id = null, $compile_id = null, $display = false)
    {
    	if (empty($template)) {
	    	$template = $this->template;
    	}
    	
    	$CI = &get_instance();
    	$CI->events->callEvent('onFetch', $template, $this->curr_module);
        return parent::fetch($template);
    }

    public static function setLanguageVars($vars)
    {
    	self::$lang_vars = array_merge($vars, self::$lang_vars);
    }
}
?>