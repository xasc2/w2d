<?php /* Smarty version 2.6.26, created on 2014-01-16 08:08:42
         compiled from frontend/footer.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'nl2br', 'frontend/footer.tpl', 7, false),)), $this); ?>
					<tr>
						<td class="footer_line" colspan="3">
							<?php echo $this->_tpl_vars['VH']->render_banner('footer'); ?>

							<?php echo $this->_tpl_vars['VH']->js_advertisement_append('footer'); ?>


							<div id="footer">
								<?php echo ((is_array($_tmp=$this->_tpl_vars['site_settings']['footer_copyrights'])) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>

								<?php echo $this->_tpl_vars['VH']->buildContentPagesMenu_bottom($this->_tpl_vars['CI']); ?>

							</div>
						</td>
					</tr>
				</table>
		</div>
		<!-- Content Ends -->
	</div>

<?php if ($this->_tpl_vars['system_settings']['google_analytics_profile_id']): ?>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">
	_uacct = "<?php echo $this->_tpl_vars['system_settings']['google_analytics_account_id']; ?>
";
	urchinTracker();
</script>
<?php endif; ?>
</body>
</html>