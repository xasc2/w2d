<?php /* Smarty version 2.6.26, created on 2014-01-16 08:08:42
         compiled from frontend/blocks/reviews_latest.tpl */ ?>
<?php if ($this->_tpl_vars['reviews_block']->reviews_count): ?>
<?php $this->assign('objects_table', $this->_tpl_vars['reviews_block']->objects_table); ?>
<?php $this->assign('object_id', $this->_tpl_vars['reviews_block']->objects_ids); ?>

<div id="reviews_div">
	<h1>Latest comments</h1>
		<ul class="root_reviews_block">
			<?php $_from = $this->_tpl_vars['reviews_block']->reviews_structured_array; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['review']):
?>
				<?php echo $this->_tpl_vars['review']->view(false,$this->_tpl_vars['review_item_template']); ?>

			<?php endforeach; endif; unset($_from); ?>
		</ul>
</div>
<?php endif; ?>