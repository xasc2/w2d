<?php

$language['LANG_FRONTEND_SEARCH_BY_MAKE_MODEL'] = "Buscar por marca/modelo";
$language['LANG_CHOOSE_MAKE_MODEL'] = "Seleccione la marca y modelo";
$language['LANG_CHOOSE_MAKE_MODEL_DESCRIPTION'] = "Haga clic en 'elegir este modelo de' enlace";
$language['LANG_CHOOSE_MODEL'] = "elegir este modelo";
$language['LANG_LISTING_SELECTED_MAKE_MODEL'] = "marca seleccionada y el modelo:";
?>