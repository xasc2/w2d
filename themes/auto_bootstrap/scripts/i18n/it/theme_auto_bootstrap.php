<?php

$language['LANG_FRONTEND_SEARCH_BY_MAKE_MODEL'] = "Ricerca per marca/modello";
$language['LANG_CHOOSE_MAKE_MODEL'] = "Scegli marca e modello";
$language['LANG_CHOOSE_MAKE_MODEL_DESCRIPTION'] = "Clicca 'scegliere questo modello di' link";
$language['LANG_CHOOSE_MODEL'] = "scegliere questo modello";
$language['LANG_LISTING_SELECTED_MAKE_MODEL'] = "marca e modello selezionato:";
?>