<?php

$language['LANG_FRONTEND_SEARCH_BY_MAKE_MODEL'] = "Suche nach Marke/Modell";
$language['LANG_CHOOSE_MAKE_MODEL'] = "Wählen Sie Marke und Modell";
$language['LANG_CHOOSE_MAKE_MODEL_DESCRIPTION'] = "Klicken Sie auf 'Wählen Sie dieses Modell' link";
$language['LANG_CHOOSE_MODEL'] = "Wählen Sie dieses Modell";
$language['LANG_LISTING_SELECTED_MAKE_MODEL'] = "Ausgewählte Marke und Modell:";
?>