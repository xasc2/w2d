<?php
class auto_bootstrapTheme
{
	public $title = "Vechicles bootstrap theme";
	public $version = "1";
	public $description = "Ideally for cars, boats, bikes, motorcycles, other classifieds. Based on Twitter Bootstrap CSS framework.";
	public $req_version = '3.3';
	
	public $lang_files = "theme_auto_bootstrap.php";

	public function hooks()
	{
		$hook['auto_bootstrap_theme_frontend_addJsFiles'] = array(
			'exclusions' => array(
				'install/:any',
				'locations/ajax_autocomplete_request/',
				'refresh_captcha/',
				'locations/get_locations_path_by_id/',
				'login/(.*)',
				'admin/(.*)',
				'install/(.*)',
				'ajax/(.*)'
			),
		);
		
		$hook['auto_bootstrap_theme_backend_addJsFiles'] = array(
			'inclusions' => array(
				'login/(.*)',
				'admin/(.*)',
				'install/(.*)',
			),
			'exclusions' => array(
				'ajax/(.*)'
			),
		);
		
		return $hook;
	}
}
?>