{include file="frontend/header.tpl"}

			<div class="row-fluid">
			 	<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>
	
				<div class="span6" id="center_part">
      				<div id="content_wrapper">
      					{if $CI->load->is_module_loaded('rss')}
                       	<div class="rss_icon"">
                       		<a href="{$VH->getRssUrl()}" title="{$VH->getRssTitle()}">
                       			<img src="{$public_path}images/feed.png" />
                       		</a>
                       	</div>
                        {/if}

      					{if $current_type->seo_name == 'classifieds'}
	      					{render_frontend_block
	                        	block_type='listings'
								block_template="frontend/blocks/slider_classifieds.tpl"
								search_type='classifieds'
								only_with_logos=true
								limit=10
								search_status=1
								search_users_status=2
								orderby=random
								slider_height=190
	                        }
                        {/if}

						<ul class="breadcrumb">
      						<li><a href="{$VH->index_url()}">{$LANG_HOME_PAGE}</a> <span class="divider">/</span></li>
      						<li class="active">{$type->name}</li>
      					</ul>

                    	{render_frontend_block
                        	block_type='listings'
                        	block_template='frontend/blocks/with_paginator.tpl'
                        	items_array=$listings
                        	view_name=$view->view
                        	view_format=$view->format
                        	type=$type
                        	listings_paginator=$listings_paginator
                        	order_url=$order_url
                        	orderby=$orderby
                        	direction=$direction
                        }
                 	</div>
                </div>
                <div class="span3" id="right_sidebar">
					{include file="frontend/right-sidebar.tpl"}
				</div>
			</div>

{include file="frontend/footer.tpl"}