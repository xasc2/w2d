{if $items_array|@count}
	<div class="sidebar_block similar_listings">
		<h4>Upcoming events</h4>
		{foreach from=$items_array item=listing}
			{assign var=event_field value=$listing->content_fields->getField(event_dates)}
			{assign var=event_field_date value=$event_field->getSomeNearestDates($listing->id, "listing_level", 1)}
			{if $event_field_date|@count > 0}
			<div class="block_item" id="upcoming_event_{$listing->id}">
				{if $listing->level->logo_enabled && $listing->logo_file}
				<div class="block_item_img">
					<a title="{$listing->title()}" href="{$VH->site_url($listing->url())}">
						<img src="{$users_content}/users_images/thmbs_cropped/{$listing->logo_file}" alt="{$listing->title()}" />
					</a>
				</div>
				{/if}
				<div class="event_date">
					<div class="event_month">{$VH->date('M', $event_field_date[0])}</div>
					<div class="event_day">{$VH->date('j', $event_field_date[0])}</div>
				</div>
				<div>
					<a title="{$listing->title()}" href="{$VH->site_url($listing->url())}">{$listing->title()}</a>
				</div>
				<div class="clear_float"></div>
			</div>
			{/if}
		{/foreach}
	</div>
{/if}