
<!-- LEFT SIDEBAR -->

					<!-- Calendar -->
						{if $CI->load->is_module_loaded('calendar')}
							{$VH->attachCalendar($CI)}
						{/if}
					<!-- /Calendar -->

					<!-- Login Block Starts -->
						{if !$session_user_id && $content_access_obj->isPermission('Edit self profile')}
						<div class="well sidebar-nav">
							<div class="nav-header"><h4>{$LANG_LOGIN_HEADER}</h4></div>
							<form id="login_form" action="{$VH->site_url('login')}" method="post" class="form-signin">
								{$LANG_LOGIN_EMAIL}<span class="red_asterisk">*</span>:
								<input type="text" name="email" size="25" placeholder="{$LANG_LOGIN_EMAIL}">

								{$LANG_LOGIN_PASSWORD}<span class="red_asterisk">*</span>:
								<input type="password" name="password" size="25" placeholder="{$LANG_LOGIN_PASSWORD}">

								<label class="checkbox">
									<input type="checkbox" name="remember_me"> {$LANG_REMEMBER_ME}
								</label>
								<input type="submit" name="submit" class="btn btn-info" value="{$LANG_BUTTON_LOGIN}">
								<div class="login_block_link"><a href="{$VH->site_url('register')}" rel="nofollow">{$LANG_CREATE_ACCOUNT}</a></div>
								<div class="login_block_link"><a href="{$VH->site_url('pass_recovery_step1')}" rel="nofollow">{$LANG_FORGOT_PASS}</a></div>
								
								{$VH->callEvent('Login processors block')}
							</form>
						</div>
						{/if}
					<!-- Login Block Ends -->

						{if $VH->checkQuickList()}
						<div id="quick_list">
							<a href="{$VH->site_url('quick_list')}">{$LANG_QUICK_LIST} ({$VH->checkQuickList()|@count})</a>
						</div>
						{/if}
						
					<!-- QR Code -->
						<div style="text-align: center">
							<img src="http://chart.apis.google.com/chart?cht=qr&chs=200x200&chl=MECARD:N:{$VH->urlencode($site_settings.website_title)};URL:{if $system_settings.enable_contactus_page}{$VH->str_replace('http://', '', $VH->site_url('contactus'))}{else}{$VH->str_replace('http://', '', $VH->site_url())}{/if};;" />
						</div>
					<!-- /QR Code -->
					
						{$VH->render_banner('left_sidebar')}
						{$VH->js_advertisement_append('left_sidebar')}

<!-- /LEFT SIDEBAR -->
