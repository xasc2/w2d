{include file="frontend/header.tpl"}

{assign var="listing_id" value=$listing->id}
{assign var="listing_unique_id" value=$listing->getUniqueId()}
{assign var="user_unique_id" value=$listing->user->getUniqueId()}
{assign var="user_login" value=$listing->user->login}

			<div class="row-fluid">
			 	<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>
	
				<div class="span6" id="center_part">
					<div id="content_wrapper" itemscope itemtype="http://schema.org/WebPage">
					{$VH->render_banner('listing_header')}
					{$VH->js_advertisement_append('listing_header')}
					
      				{if $listing->title != 'untranslated'}
						<script language="Javascript" type="text/javascript">
						$(document).ready(function() {ldelim}
							$('a.lightbox_image').live('mouseover', function(){ldelim}
								$('a.lightbox_image, a.hidden_imgs').lightBox({ldelim}
									overlayOpacity: 0.75,
									imageLoading: '{$public_path}images/lightbox-ico-loading.gif',
									imageBtnClose: '{$public_path}images/lightbox-btn-close.gif',
									imageBtnPrev: '{$public_path}images/lightbox-btn-prev.gif',
									imageBtnNext: '{$public_path}images/lightbox-btn-next.gif'
								{rdelim});
							{rdelim});

							$('#show_reviews').click(function() {ldelim}
								ajax_loader_show();
								$.post('{$VH->site_url("listing/$listing_id/reviews")}',
									function(data) {ldelim}
										$('#reviews_container').html(data);
										ajax_loader_hide();
										$('#show_reviews').parent().hide();
									{rdelim},
									"html");
							{rdelim});
						{rdelim});
						</script>

						{if $search_results_paginator}
							{$search_results_paginator->showPrevNextButtons($listing->id)}
							<meta itemprop="isPartOf" content="{$search_results_paginator->getRefererUrl($listing->id)}" />
						{/if}
						<ul class="breadcrumb" itemprop="breadcrumb">
      						<li><a href="{$VH->index_url()}">{$LANG_HOME_PAGE}</a> <span class="divider">/</span></li>
      						{foreach from=$breadcrumbs item="source_page" key="source_url"}
								{if $source_page}
								<li><a href="{$source_url}">{$source_page}</a> <span class="divider">/</span></li>
								{/if}
							{/foreach}
      						<li class="active">{$listing->title()}</li>
      					</ul>

						<div id="listing_header_block">
	                        <div id="listing_header">
	                        	<iframe src="http://www.facebook.com/plugins/like.php?href={$VH->site_url($listing->url())}&amp;layout=standard&amp;show_faces=false&amp;width=345&amp;action=like&amp;colorscheme=light" scrolling="no" frameborder="0" allowTransparency="true" style="border:none; overflow:hidden; width:345px; height:45px"></iframe>

	                        	<h3 class="listing_title" itemprop="name">{$listing->title()}</h3>
		                        <div class="listing_author">
		                        	<span itemprop="author" itemscope itemtype="http://schema.org/Person">
		                        		<meta itemprop="image" content="{$listing->user->profileImageUrl()}">
										{$LANG_SUMITTED_1}
										{if $listing->user->users_group->is_own_page && $listing->user->users_group->id != 1}
											<a href="{$listing->user->profileUrl()}" title="{$LANG_VIEW_USER_PAGE_OPTION}" itemprop="url">
												{$listing->user->login}
											</a>
										{else}
											<strong itemprop="name">{$listing->user->login}</strong>
										{/if}
									</span> {$LANG_SUMITTED_2} <time itemprop="dateCreated" datetime="{$listing->order_date|date_format:"%Y-%m-%dT%H:%M"}">{$listing->order_date|date_format:"%D %H:%M"}</time>
								</div>
		                        {if $listing->type->categories_type != 'disabled' && $listing->level->categories_number && $listing->categories_array()|@count}
			                        <div class="listing_page_categories">{$LANG_SUMITTED_3} 
			                        {foreach from=$listing->categories_array() item=category}
			                        	<a href="{$VH->site_url($category->getUrl())}" class="listing_cat_link">{$category->name}</a>&nbsp;&nbsp;
			                        {/foreach}
			                        </div>
		                        {/if}
		                        <div class="clear_float"></div>

		                        {if $listing->level->ratings_enabled}
		                        	{assign var=avg_rating value=$listing->getRatings()}
		                        	<div {if $avg_rating->ratings_count}itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"{/if}>
									{$avg_rating->view()}
									</div>
								{/if}
		                        <div class="clear_float"></div>
		                        <div class="px5"></div>
		                        
		                        {if ($listing->level->logo_enabled && $listing->logo_file) || $images|@count}
		                        <div id="listing_logo">
		                        	{if $listing->level->logo_enabled && $listing->logo_file}
		                        		{assign var=logo value=$listing->logo_file}
		                        	{else}
		                        		{assign var=logo value=$images[0]->file}
		                        	{/if}
									<a href="{$users_content}/users_images/images/{$logo}" class="lightbox_image" title="{$listing->title()}" itemprop="image"><img src="{$users_content}/users_images/thmbs_big/{$logo}" alt="{$listing->title()}"  /></a>
								</div>
								{/if}
							</div>

							<div id="listing_options_panel">
								{if $listing->level->social_bookmarks_enabled}
									<a class="a2a_dd" href="http://www.addtoany.com/share_save"><img src="http://static.addtoany.com/buttons/share_save_171_16.png" width="171" height="16" border="0" alt="Share/Bookmark"/></a><script type="text/javascript" src="http://static.addtoany.com/menu/page.js"></script>
								{/if}
								{if $listing->user->id == $session_user_id || $content_access_obj->isPermission('Manage all listings')}
									<span class="listing_options_panel_item edit"><a href="{$VH->site_url("admin/listings/edit/$listing_id")}">{$LANG_EDIT_LISTING_OPTION}</a></span>
								{/if}
								{if $listing->level->option_print}
									<span class="listing_options_panel_item print"><a href="javascript: void(0);" onClick="$.jqURL.loc('{$VH->site_url("print_listing/$listing_unique_id/")}', {ldelim}w:590,h:750,wintype:'_blank'{rdelim}); return false;">{$LANG_PRINT_PAGE}</a></span>
								{/if}
								{if $listing->level->option_pdf}
									<span class="listing_options_panel_item pdf"><a href="http://pdfmyurl.com/?url={$VH->site_url("listings/$listing_unique_id")}">{$LANG_PDF_PAGE}</a></span>
								{/if}
								{if $listing->level->option_quick_list}
									<a href="javascript: void(0);" class="add_to_favourites" listingid="{$listing->id}" title="{$LANG_ADD_REMOVE_QUICK_LIST}"></a>&nbsp;<a href="javascript: void(0);" class="add_to_favourites_button">{$LANG_ADD_REMOVE_QUICK_LIST}</a>&nbsp;
								{/if}
								{if $listing->level->option_email_friend}
									<span class="listing_options_panel_item friend"><a href="{$VH->site_url("email/send/listing_id/$listing_id/target/friend/")}" class="nyroModal">{$LANG_EMAIL_FRIEND}</a></span>
								{/if}
								{if $listing->level->option_email_owner}
									<span class="listing_options_panel_item owner"><a href="{$VH->site_url("email/send/listing_id/$listing_id/target/owner/")}" class="nyroModal">{$LANG_EMAIL_OWNER}</a></span>
								{/if}
								<span class="user_options_panel_item all_listings"><a href="{$VH->site_url("search/search_owner/$user_login")}" title="{$LANG_USER_VIEW_ALL_LISTINGS}">{$LANG_USER_VIEW_ALL_LISTINGS}</a></span>
								{if $listing->level->option_report}
		                        	<span class="listing_options_panel_item report"><a href="{$VH->site_url("email/send/listing_id/$listing_id/target/report/")}" class="nyroModal">{$LANG_REPORT_ADMIN}</a></span>
		                        {/if}
								{if $listing->type->locations_enabled && $listing->locations_count() && $listing->level->maps_enabled}
									<span class="listing_options_panel_item map"><a href="#addresses-tab" id="addresses">{$LANG_LISTING_ADDRESSES_OPTION}</a></span>
								{/if}
								{if $listing->level->video_count && $listing->getAssignedVideos()}
									<span class="listing_options_panel_item videos"><a href="#videos-tab" id="videos">{$LANG_LISTING_VIDEOS_OPTION}</a></span>
								{/if}
								{if $listing->level->files_count && $listing->getAssignedFiles()}
									<span class="listing_options_panel_item files"><a href="#files-tab" id="files">{$LANG_LISTING_FILES_OPTION}</a></span>
								{/if}
								{if $listing->level->reviews_mode && $listing->level->reviews_mode != 'disabled'}
		                        	<span class="listing_options_panel_item reviews"><a href="#reviews-tab" id="reviews">{$listing->getReviewsCount()} {if $listing->level->reviews_mode == 'reviews'}{$LANG_REVIEWS}{else}{$LANG_COMMENTS}{/if}</a></span>
		                        {/if}
		                        {if $content_access_obj->isPermission("Claim on listings") && $listing->claim_row.ability_to_claim}
		                        	<span class="listing_options_panel_item claim"><a href="{$VH->site_url("claim/$listing_unique_id")}">{$LANG_CLAIM_LISTING_OPTION}</a></span>
		                        {/if}
		                        {if $listing->user->users_group->is_own_page && $listing->user->users_group->id != 1}
		                        	<span class="listing_options_panel_item author"><a href="{$VH->site_url("users/$user_unique_id")}">{$LANG_VIEW_USER_PAGE_OPTION}</a></span>
		                        {/if}
		                        {if $content_access_obj->isPermission('Change listing level') && $listing->type->buildLevels()|@count > 1}
		                        	<span class="listing_options_panel_item upgrade">{$LANG_LEVEL_TH}: {$listing->level->name} (<a href="{$VH->site_url("admin/listings/change_level/$listing_id")}">{$LANG_CHANGE_LISTING_LEVEL_OPTION}</a>)</span>
		                        {/if}
		                        {if $listing->level->raiseup_enabled}
	                             	<span class="listing_options_panel_item raiseup"><a href="{$VH->site_url("admin/listings/raiseup/$listing_id")}">{$LANG_RAISE_LISTINGS_UP}</a></span>
	                            {/if}
							</div>
							<div class="clear_float"></div>
						</div>

						{if $listing->level->images_count && $images|@count}
						<div style="width: 100%" itemscope itemtype="http://schema.org/ImageGallery">
						{render_frontend_block
							block_type='images'
							block_template='frontend/blocks/images_listing_slider_anything.tpl'
							existed_images=$images
							slider_height=145
						}
						</div>
						{/if}

						{if $listing->listing_keywords}<meta itemprop="keywords" content="{$listing->listing_keywords}" />{/if}

						{if $listing->listing_description || ($listing->content_fields->fieldsCount() && $listing->content_fields->isAnyValue())}
						{if $listing->listing_description}
						<h4>{$LANG_LISTING_SHORT_DESCRIPTION}</h4>
						{/if}
						<span itemprop="description">
							{if $listing->listing_description}
							<div id="listing_description">
								{if $listing->level->description_mode == 'richtext'}
								{$listing->listing_description}
								{else}
								{$listing->listing_description|nl2br}
								{/if}
							</div>
							{/if}
	
							{if $listing->content_fields->fieldsCount() && $listing->content_fields->isAnyValue()}
								<h4>{$LANG_LISTING_INFORMATION}</h4>
			                   	{$listing->content_fields->outputMode('tablerow')}
			                   	<div class="px10"></div>
							{/if}
						</span>
						{/if}
						
						{if $listing->level->images_count && $images|@count}
							{render_frontend_block
					      		block_type='images'
					      		block_template='frontend/blocks/images_straight_gallery.tpl'
					      		existed_images=$images
							}
						{else}
							{render_frontend_block
					      		block_type='images'
					      		block_template='frontend/blocks/images_straight_gallery.tpl'
					      		existed_images=$listing->logoImage()
							}
						{/if}

						{if $listing->type->locations_enabled && $listing->locations_count()}
						<div id="addresses-tab">
							<h4 id="map">{$LANG_LISTING_ADDRESS}</h4>
							<div class="listing_address_block">
								{assign var="i" value=1}
								{foreach from=$listing->locations_array() item=location}
								<span itemprop="contentLocation" itemscope itemtype="http://schema.org/Place">
									<meta itemprop="name" content="{$listing->title()}">
									<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
										{if $location->address_line_1 || $location->address_line_2}<meta itemprop="streetAddress" content="{$location->address_line_1} {$location->address_line_2}">{/if}
										{if $location->location()}<meta itemprop="addressLocality" content="{$location->location()}">{/if}
										{if $location->zip_or_postal_index}<meta itemprop="postalCode" content="{$location->zip_or_postal_index}">{/if}
									</span>
									{if $listing->level->maps_enabled && $listing->locations_count(true)}
									<span itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
										<meta itemprop="latitude" content="{$location->map_coords_1}">
										<meta itemprop="longitude" content="{$location->map_coords_2}">
									</span>
									{/if}
								</span>
								<address>
									{if $listing->locations_count()>1}<span class="address_label">{$LANG_LISTING_ADDRESS} {$i++}:</span> {/if}{$location->compileAddress()}
								</address>
								{/foreach}
							</div>
							{if $listing->level->maps_enabled && $listing->locations_count(true)}
							<div id="map"></div>
							{render_frontend_block
								block_type='map_and_markers'
								block_template='frontend/blocks/map_standart_directions.tpl'
								existed_listings=$listing->id
								map_width=$listing->level->explodeSize('maps_size', 'width')
								show_anchors=false
								show_links=false
							}
							{/if}
						</div>
						{/if}

						{if $listing->level->video_count && $listing->getAssignedVideos()}
						<div id="videos-tab" itemscope itemtype="http://schema.org/VideoGallery">
							<h4 id="videos">{$LANG_LISTING_VIDEOS}</h4>
							{foreach from=$videos item=video}
							<div class="listing_videos_block" itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
								<div>
									<meta itemprop="thumbnail" content="http://img.youtube.com/vi/{$video->video_code}/0.jpg" />
									<meta itemprop="thumbnailUrl" content="http://img.youtube.com/vi/{$video->video_code}/0.jpg" />
									<meta itemprop="embedUrl" content="http://www.youtube.com/v/{$video->video_code}&hl=en&fs=1&" />
									<object width="{$listing->level->explodeSize('video_size', 'width')}" height="{$listing->level->explodeSize('video_size', 'height')}"><param name="movie" value="http://www.youtube.com/v/{$video->video_code}&hl=en&fs=1&"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/{$video->video_code}&hl=en&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="{$listing->level->explodeSize('video_size', 'width')}" height="{$listing->level->explodeSize('video_size', 'height')}"></embed></object>
								</div>
								{if $video->title}
								<div class="listing_videos_title" itemprop="name">
									{$video->title}
								</div>
								{/if}
							</div>
							{/foreach}
						</div>
						{/if}
							
						{if $listing->level->files_count && $listing->getAssignedFiles()}
						<div id="files-tab">
							<h4 id="files">{$LANG_LISTING_FILES}</h4>
							<div class="listing_files_block" itemtype="http://schema.org/CollectionPage">
								{foreach from=$files item=file}
								{assign var="file_id" value=$file->id}
								<span class="listing_file_item" style="background: url('{$public_path}images/file_types/{$file->file_format}.png') 0 0 no-repeat;"><a href="{$VH->site_url("download/$file_id")}" target="_blank" itemprop="url">{$file->title} ({$file->file_size})</a></span>
								{/foreach}
							</div>
						</div>
						{/if}
							
						{if $listing->level->reviews_mode && $listing->level->reviews_mode != 'disabled'}
						<div id="reviews-tab">
							{if $listing->getReviewsCount() && $listing->level->ratings_enabled && $avg_rating->ratings_count}
							<span itemscope itemtype="http://data-vocabulary.org/Review-aggregate">
								<meta itemprop="itemreviewed" content="{$listing->title()}" />
								{if $listing->logo_file}<meta itemprop="photo" content="{$users_content}/users_images/images/{$listing->logo_file}" />{/if}
								<meta itemprop="count" content="{$listing->getReviewsCount()}" />
								<meta itemprop="votes" content="{$avg_rating->ratings_count}" />
								<span itemprop="rating" itemscope itemtype="http://data-vocabulary.org/Rating">
									<meta itemprop="average" content="{$avg_rating->avg_value}" />
									<meta itemprop="best" content="5" />
								</span>
							</span>
							{/if}

							{render_frontend_block
								block_type='reviews_comments'
								block_template='frontend/blocks/reviews_comments_add.tpl'
								objects_table='listings'
								objects_ids=$listing->id
								comment_area_enabled=true
								reviews_mode=$listing->level->reviews_mode
								admin_mode=false
								is_richtext=$listing->level->reviews_richtext_enabled
							}
						</div>
						{/if}
					{else}
						<div class="alert alert-block">
							<h4>{$LANG_LISTING_TRANSLATION_ERROR}</h4>
						</div>
					{/if}

					</div>
				</div>
				<div class="span3" id="right_sidebar">
					{include file="frontend/right-sidebar.tpl"}
				</div>
			</div>

{include file="frontend/footer.tpl"}