{include file="frontend/header.tpl"}

			<div class="row-fluid">
			 	<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>
	
				<div class="span6" id="center_part">
      				<div id="content_wrapper">
      					{if $CI->load->is_module_loaded('rss')}
	                        <div class="rss_icon"">
	                        	<a href="{$VH->getRssUrl()}" title="{$VH->getRssTitle()}">
	                        		<img src="{$public_path}images/feed.png" />
	                        	</a>
	                        </div>
						{/if}
      					<ul class="breadcrumb">
      						<li><a href="{$VH->index_url()}">{$LANG_HOME_PAGE}</a> <span class="divider">/</span></li>
      						<li class="active">{$LANG_BREADCRUMBS_SEARCH}</li>
      					</ul>
      				
						{if $listings|@count}
							{render_frontend_block
	                        	block_type='listings'
	                        	block_template='frontend/blocks/with_paginator.tpl'
	                        	search_args=$args
	                        	items_array=$listings
	                        	view_name=$view->view
	                        	view_format=$view->format
	                        	type=$type
	                        	listings_paginator=$listings_paginator
	                        	order_url=$order_url
	                        	orderby=$orderby
	                        	direction=$direction
	                        }
                        {else}
							<ul>
								<li>{$LANG_NO_SEARCH}</li>
							</ul>
						{/if}
					</div>
				</div>
                <div class="span3" id="right_sidebar">
					{include file="frontend/right-sidebar.tpl"}
				</div>
			</div>

{include file="frontend/footer.tpl"}