		<div class="navbar navbar-fixed-top navbar-inverse">
			<div class="navbar-inner">
				<ul class="nav">
					<li><a href="{$VH->site_url()}"><img src="{$public_path}images/treeview_img/house.png"> {$LANG_HOME_MENU}</a></li>
					<li class="divider-vertical"></li>
					<li><a href="{$VH->site_url('admin')}"><img src="{$public_path}images/buttons/user.png"> {$LANG_MY_ACCOUNT_LINK}</a></li>
					{if $content_access_obj->isPermission('Edit self profile')}
					<li class="divider-vertical"></li>
					<li><a href="{$VH->site_url('admin/users/my_profile/')}"><img src="{$public_path}images/treeview_img/user.png"> {$LANG_EDIT_MY_PROFILE_MENU} <span class="label label-info">{$CI->session->userdata('user_login')}</span></a></li>
					{/if}
					{if $content_access_obj->isPermission('Manage self listings') || $content_access_obj->isPermission('Create listings')}
					<li class="divider-vertical"></li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><b class="caret"></b> {$LANG_LISTINGS_MENU}</a>
						<ul class="dropdown-menu">
							{if $content_access_obj->isPermission('Create listings')}
							<li><a href="{$VH->site_url('admin/listings/create/')}"><img src="{$public_path}images/treeview_img/new_listing.png"> {$LANG_CREATE_LISTING_MENU}</a></li>
							{/if}
							{if $content_access_obj->isPermission('Manage self listings')}
							<li><a href="{$VH->site_url('admin/listings/my/')}"><img src="{$public_path}images/treeview_img/listings.png"> {$LANG_VIEW_MY_LISTING_MENU} <span class="badge badge-info">{$CI->listings->getMyListingsCount()}</span></a></li>
							{/if}
						</ul>
					</li>
					{/if}
					{if $CI->load->is_module_loaded('banners') && ($content_access_obj->isPermission('Manage self banners') || $content_access_obj->isPermission('Create banners'))}
					<li class="divider-vertical"></li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><b class="caret"></b> {$LANG_BANNERS_MENU}</a>
						<ul class="dropdown-menu">
							{if $content_access_obj->isPermission('Create banners')}
							<li><a href="{$VH->site_url('admin/banners/create/')}"><img src="{$public_path}images/treeview_img/image_add.png"> {$LANG_BANNERS_CREATE_BANNER_MENU}</a></li>
							{/if}
							{if $content_access_obj->isPermission('Manage self banners')}
							<li><a href="{$VH->site_url('admin/banners/my/')}"><img src="{$public_path}images/treeview_img/images.png"> {$LANG_BANNERS_MY_MENU} <span class="badge badge-info">{$CI->banners->getMyBannersCount()}</span></a></li>
							{/if}
						</ul>
					</li>
					{/if}
					{if $CI->load->is_module_loaded('payment') && ($content_access_obj->isPermission('View self invoices') || $content_access_obj->isPermission('View self transactions'))}
					<li class="divider-vertical"></li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><b class="caret"></b> {$LANG_PAYEMENT_MENU}</a>
						<ul class="dropdown-menu">
							{if $content_access_obj->isPermission('View self invoices')}
							<li><a href="{$VH->site_url('admin/payment/invoices/my/')}"><img src="{$public_path}images/treeview_img/invoices.png"> {$LANG_VIEW_MY_INVOICES_MENU} <span class="badge badge-info">{$CI->payment->getMyInvoicesCount()}</span></a></li>
							{/if}
							{if $content_access_obj->isPermission('View self transactions')}
							<li><a href="{$VH->site_url('admin/payment/transactions/my/')}"><img src="{$public_path}images/treeview_img/transactions.png"> {$LANG_VIEW_MY_TRANSACTIONS_MENU} <span class="badge badge-info">{$CI->payment->getMyTransactionsCount()}</span></a></li>
							{/if}
						</ul>
					</li>
					{/if}
					{if $CI->load->is_module_loaded('packages') && $content_access_obj->isPermission('Use packages')}
					<li class="divider-vertical"></li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><b class="caret"></b> {$LANG_PACKAGES_MENU}</a>
						<ul class="dropdown-menu">
							<li><a href="{$VH->site_url('admin/packages/add/')}"><img src="{$public_path}images/treeview_img/package_add.png"> {$LANG_ADD_MY_PACKAGE}</a></li>
							<li><a href="{$VH->site_url('admin/packages/my/')}"><img src="{$public_path}images/treeview_img/package.png"> {$LANG_VIEW_MY_PACKAGES} <span class="badge badge-info">{$CI->packages->getMyPackagesCount()}</span></a></li>
						</ul>
					</li>
					{/if}
					{if $CI->load->is_module_loaded('discount_coupons') && $content_access_obj->isPermission('Use coupons')}
					<li class="divider-vertical"></li>
					<li><a href="{$VH->site_url('admin/coupons/my/')}"><img src="{$public_path}images/treeview_img/tag_blue.png"> {$LANG_VIEW_MY_COUPONS} <span class="badge badge-info">{$CI->discount_coupons->getMyCouponsCount()}</span></a></li>
					{/if}
					<li class="divider-vertical"></li>
					<li><a href="{$VH->site_url('logout/')}"><img src="{$public_path}images/treeview_img/door_in.png"> {$LANG_LOGOUT_MENU}</a></li>
				</ul>
			</div>
		</div>