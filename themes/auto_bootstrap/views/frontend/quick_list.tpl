{include file="frontend/header.tpl"}

<script language="Javascript" type="text/javascript">
	$(document).ready(function() {ldelim}
		$(".remove_favourite").click(function() {ldelim}
			if ((str = $.cookie("favourites")) != null) {ldelim}
				var favourites_array = str.split('*');
			{rdelim} else {ldelim}
				var favourites_array = new Array();
			{rdelim}

			id = $(this).attr('id');
			if ((key = in_array(id, favourites_array)) !== false) {ldelim}
				favourites_array[key] = undefined;
				$.cookie("favourites", favourites_array.join('*'), {ldelim}expires: 365, path: "/"{rdelim});
				$(this).parent().slideUp("normal", function() {ldelim}
					$(this).remove();
				{rdelim});
				show_msg('{addslashes string=$LANG_QUICK_FROM_LIST_SUCCESS}', 'success');
			{rdelim}
			return false;
		{rdelim});
	{rdelim});
</script>

			<div class="row-fluid">
			 	<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>
	
				<div class="span6" id="center_part">
      				<div id="content_wrapper">

                        <h2>{$LANG_QUICK_LIST} ({$VH->checkQuickList()|@count}):</h2>
                        <div class="quicklist_listings">
                        	{render_frontend_block
								block_type='listings'
	                        	block_template='frontend/blocks/with_paginator.tpl'
	                        	items_array=$listings
	                        	view_name=$view->view
	                        	view_format=$view->format
	                        	listings_paginator=$listings_paginator
	                        	order_url=$order_url
	                        	orderby=$orderby
	                        	direction=$direction
	                        }
						</div>
					</div>
				</div>
                <div class="span3" id="right_sidebar">
					{include file="frontend/right-sidebar.tpl"}
				</div>
			</div>

{include file="frontend/footer.tpl"}