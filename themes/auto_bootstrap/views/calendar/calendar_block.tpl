	<script language="javascript" type="text/javascript">
		$(document).ready( function() {ldelim}
			$("#datepicker").datepicker({ldelim}
				changeMonth: true,
				changeYear: true,
				onSelect: function(dateText) {ldelim}
					window.location.href = '{$url}'+$.datepicker.formatDate('yy-mm-dd', $("#datepicker").datepicker("getDate"));
				{rdelim}
			{rdelim});
			{if ($defaultDate)}
			$("#datepicker").datepicker('setDate', $.datepicker.parseDate('@', '{$defaultDate}000'));
			{/if}
			$("#datepicker").datepicker("option", $.datepicker.regional["{$current_language}"]);
		{rdelim});
	</script>
	<div class="nav-header"><h5>{$name}</h5></div>
	<div id="datepicker" style="margin-bottom: 20px;"></div>
	