{include file="backend/admin_header.tpl"}

				<script language="javascript" type="text/javascript">
                // Global url string
                {if $base_url}
                	var global_js_url = '{$base_url}';
                {else}
                	var global_js_url = '{$VH->site_url("admin/listings/search/")}';
                {/if}
                
                // Command variable, needs for delete, block listings buttons
                var action_cmd;
                
                // Is advanced search in use? Attach to global_js_url
                var use_advanced = '';
                
                // Was advanced search block loaded?
                var advanced_loaded = false;

                // Scripts code array - will be evaluated when advanced search attached
                var scripts = new Array();
                
                var default_what = '{addslashes string=$LANG_SEARCH_WHAT} ({addslashes string=$LANG_SEARCH_WHAT_DESCR})';
                var default_where = '{addslashes string=$LANG_SEARCH_WHERE} ({addslashes string=$LANG_SEARCH_WHERE_DESCR})';
                
                var use_districts = {$system_settings.geocoded_locations_mode_districts};
                var use_provinces = {$system_settings.geocoded_locations_mode_provinces};
                
                var ajax_autocomplete_request = '{$VH->site_url("ajax/locations/autocomplete_request/")}';

                $(document).ready(function() {ldelim}
                	{if (!$current_type) || ($current_type && $current_type->search_type != 'disabled')}
	                	{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_where_search) || (!$system_settings.single_type_structure && $current_type && $current_type->where_search && $current_type->locations_enabled)}
                		{if $system_settings.predefined_locations_mode != 'only'}
	                		$('#where_search').autocomplete({ldelim}
				               	source: function(request, response) {ldelim}
				               		{if $system_settings.predefined_locations_mode != 'only'}
				               			{if $system_settings.predefined_locations_mode != 'disabled'}
				               				// geocode + ajax from DB
				               				geocodeAddress(request.term, response, ajax_autocomplete_request, false);
				               			{else}
				               				// only geocode
				               				geocodeAddress(request.term, response, false, false);
				               			{/if}
				               		{elseif $system_settings.predefined_locations_mode != 'disabled'}
				               			// only ajax from DB
				               			$.post(ajax_autocomplete_request, {ldelim}query: request.term{rdelim}, function(data) {ldelim}
					               			if (data = jQuery.parseJSON(data))
					               				response(data);
					               		{rdelim});
				               		{/if}

				               		/*{if $system_settings.predefined_locations_mode != 'only'}
				               			geocodeAddress(request.term, response);
				               		{elseif $system_settings.predefined_locations_mode != 'disabled'}
					               		$.post('{$VH->site_url("ajax/locations/autocomplete_request/")}', {ldelim}query: request.term{rdelim}, function(data) {ldelim}
					               			if (data = jQuery.parseJSON(data))
					               				response(data);
					               		{rdelim});
				               		{/if}*/
								{rdelim},
								focus: function(event, ui) {ldelim}
									$(this).val(ui.item.label);
									return false;
								{rdelim},
								select: function(event, ui) {ldelim}
									$(this).val(ui.item.label);
									return false;
								{rdelim},
								minLength: 2,
								delay: 600
							{rdelim});
			                $('#where_radius_slider').slider({ldelim}
								min: 0,
								max: 10,
								range: "min",
								value: $("#where_radius_label").val(),
								slide: function(event, ui) {ldelim}
									$("#where_radius_label").val(ui.value);
									$("#where_radius").val(ui.value);
								{rdelim}
							{rdelim});
							if ($("#where_search").val() == default_where)
								$("#where_search").addClass("highlight_search_input");
						{/if}
						{/if}
	
						{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_categories_search) || (!$system_settings.single_type_structure && $current_type && $current_type->categories_search)}
		                /*$("#search_by_category_tree").bind("change_state.jstree", function (event, data) {ldelim}
							var categories_list = [];
							$.each(data.inst.get_checked(), function() {ldelim}
								categories_list.push($(this).attr('id').replace("search_", ""));
							{rdelim});
							$("#search_category").val(categories_list.join(','));
							return false;
						{rdelim});

		                $("#search_category_button, #search_by_category_tree").hover(
		                	function() {ldelim}
			                	$("#search_by_category_tree").show();
			                	var pos = $("#search_category_button").offset();
			                	$("#search_by_category_tree").css({ldelim}'top': parseInt(pos.top)+parseInt(14)+'px', 'left': parseInt(pos.left)+parseInt(5)+'px'{rdelim});
			                	return false;
		                	{rdelim},
		                	function() {ldelim}
		                		$("#search_by_category_tree").hide();
		                	{rdelim}
		                );*/
		                {/if}
	
						{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_what_search) || (!$system_settings.single_type_structure && $current_type && $current_type->what_search)}
						$("#what_search").blur( function() {ldelim}
							if ($(this).val() == '' )
								$(this).val(default_what).addClass('highlight_search_input');
						{rdelim});
						$("#what_search").focus( function() {ldelim}
							if ($(this).val() == default_what)
								$(this).val('').removeClass('highlight_search_input');
						{rdelim});
	
						if ($("#what_search").val() == default_what)
							$("#what_search").addClass("highlight_search_input");
						{/if}
	
						{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_where_search) || (!$system_settings.single_type_structure && $current_type && $current_type->where_search && $current_type->locations_enabled)}
						$("#where_search").blur( function() {ldelim}
							if ($(this).val() == '' )
								$(this).val(default_where).addClass('highlight_search_input');
						{rdelim});
						$("#where_search").focus( function() {ldelim}
							if ($(this).val() == default_where)
								$(this).val('').removeClass('highlight_search_input');
						{rdelim});
						
						/*var where_advanced_opened = false;
						$("#where_advanced").click( function() {ldelim}
							$("#where_advanced_block").slideToggle(200);
							if (where_advanced_opened) {ldelim}
								$("#where_advanced img").attr('src', '{$public_path}images/icons/add.png');
								where_advanced_opened = false;
							{rdelim} else {ldelim}
								$("#where_advanced img").attr('src', '{$public_path}images/icons/delete.png');
								where_advanced_opened = true;
							{rdelim}
							return false;
						{rdelim});*/
						{/if}
						
						// Form submit event
						$("#search_form").submit( function() {ldelim}
							if ($("#search_type").val() != 0)
								global_js_url = global_js_url + 'search_type/' + urlencode($("#search_type").val()) + '/';

							{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_what_search) || (!$system_settings.single_type_structure && $current_type && $current_type->what_search)}
							if ($("#what_search").val() != '' && $("#what_search").val() != default_what) {ldelim}
	                			global_js_url = global_js_url + 'what_search/' + urlencode($("#what_search").val()) + '/';
	                		{rdelim}
	                		{/if}
	
	                		{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_where_search) || (!$system_settings.single_type_structure && $current_type && $current_type->where_search && $current_type->locations_enabled)}
	                		/*if ($("#where_search").val() != '' && $("#where_search").val() != default_where) {ldelim}
	                			global_js_url = global_js_url + 'where_search/' + urlencode($("#where_search").val()) + '/';
	                			if (where_advanced_opened && parseInt($("#where_radius").val())>0) {ldelim}
	                				global_js_url = global_js_url + 'where_radius/' + $("#where_radius").val() + '/';
	                			{rdelim}
	                		{rdelim}*/
	                		{/if}
		                		
	                		{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_where_search) || (!$system_settings.single_type_structure && $current_type && $current_type->where_search && $current_type->locations_enabled)}
		                		{if $system_settings.predefined_locations_mode != 'only'}
			                		if ($("#where_search").val() != '' && $("#where_search").val() != default_where) {ldelim}
			                			global_js_url = global_js_url + 'where_search/' + urlencode($("#where_search").val()) + '/';
			                			if (parseInt($("#where_radius").val())>0) {ldelim}
			                				global_js_url = global_js_url + 'where_radius/' + $("#where_radius").val() + '/';
			                			{rdelim}
			                		{rdelim}
			                		if ($("#predefined_location_id").val() != '') {ldelim}
			                			global_js_url = global_js_url + 'predefined_location_id/' + urlencode($("#predefined_location_id").val()) + '/';
			                		{rdelim}
		                		{else}
			                		var predefined_location_id;
									$(".location_dropdown_list").each(function() {ldelim}
										if ($(this).find("option:selected").val() != '')
											predefined_location_id = $(this).find("option:selected").val();
									{rdelim});
									if (predefined_location_id) {ldelim}
			                			global_js_url = global_js_url + 'predefined_location_id/' + urlencode(predefined_location_id) + '/';
			                			if (parseInt($("#where_radius").val())>0) {ldelim}
			                				global_js_url = global_js_url + 'where_radius/' + $("#where_radius").val() + '/';
			                			{rdelim}
			                		{rdelim}
		                		{/if}
	                		{/if}
	                		
	                		{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_categories_search) || (!$system_settings.single_type_structure && $current_type && $current_type->categories_search)}
	                		if ($("#search_category").val() != '') {ldelim}
	                			global_js_url = global_js_url + 'search_category/' + $("#search_category").val() + '/';
	                		{rdelim}
	                		{/if}

	                		if ($("input[name=search_claimed_listings]:checked").val() != 'any') {ldelim}
	                			global_js_url = global_js_url + 'search_claimed_listings/' + $("input[name=search_claimed_listings]:checked").val() + '/';
	                		{rdelim}
	
	                		global_js_url = global_js_url + use_advanced;
	                		window.location.href = global_js_url;
							return false;
						{rdelim});
	
						{if $advanced_search_fields->fieldsCount() || $content_access_obj->isPermission('Manage all listings')}
						var advanced_opened = false;
						$(".advanced_search").bind('click', function() {ldelim}
							if (advanced_opened) {ldelim}
								$(".advanced_search img").attr('src', '{$public_path}images/icons/accept.png');
								$("#advanced_search_block").hide();
								advanced_opened = false;
								use_advanced = '';
							{rdelim} else {ldelim}
								$(".advanced_search img").attr('src', '{$public_path}images/icons/delete.png');
								$("#advanced_search_block").show();
								advanced_opened = true;
	
								if (!advanced_loaded) {ldelim}
									ajax_loader_show();
									$.post('{$VH->site_url("ajax/listings/build_advanced_search/")}', {ldelim}type_id: $("#search_type").val(), args: '{$VH->json_encode($args)}'{rdelim}, 
										function(data) {ldelim}
											$("#advanced_search_block").html(parseScript(data));
											evalScripts(data);
											advanced_loaded = true;
											ajax_loader_hide();
										{rdelim}
									);
								{rdelim}
								use_advanced = 'use_advanced/true/';
							{rdelim}
							return false;
						{rdelim});
						{if $args.use_advanced}
						$('.advanced_search').triggerHandler('click');
						{/if}
						{/if}
					{/if}
					
					$("#search_type").change( function() {ldelim}
						if ($(this).val() != 0)
							global_js_url = '{$clean_url}search_type/' + $(this).val() + '/';
						else
							global_js_url = '{$clean_url}';
						window.location.href = global_js_url;
					{rdelim});
                {rdelim});

                function submit_listings_form()
                {ldelim}
                	$("#listings_form").attr("action", '{$VH->site_url('admin/listings/')}' + action_cmd + '/');
                	return true;
                {rdelim}
                </script>

                <div class="content">
                     <h3>{$LANG_SEARCH_LISTINGS}</h3>

                     <form id="search_form" action="" method="post">
	                    {if !$system_settings.single_type_structure}
	                    <div class="search_filter_standart">
		                    {$LANG_SEARCH_BY_TYPE}:
		                    <select id="search_type" name="search_type">
								<option value="0">- - - {$LANG_SEARCH_FOR_ALL_TYPES} - - -</option>
								{foreach from=$types item=type}
								<option value="{$type->id}" {if $args.search_type == $type->id}selected{/if}>{$type->name}</option>
								{/foreach}
							</select>
						</div>
						<div class="clear_float"></div>
						{else}
						<input type="hidden" id="search_type" name="search_type" value="0" />
						{/if}

                     	<div>
                     		{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_what_search) || (!$system_settings.single_type_structure && $current_type && $current_type->what_search)}
                     		<div class="search_filter_standart">
                     			<div>
                     				<input type="text" class="what_where_search_input" name="what_search" id="what_search" value="{if $args.what_search}{$args.what_search}{else}{$LANG_SEARCH_WHAT} ({$LANG_SEARCH_WHAT_DESCR}){/if}" />
                     			</div>
                     		</div>
                     		{/if}

                     		{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_categories_search) || (!$system_settings.single_type_structure && $current_type && $current_type->categories_search)}
                     		<div class="search_filter_standart">
                     			Search in categories
			                   	{render_frontend_block
									block_type='categories'
									block_template="frontend/blocks/categories_search_dropboxes.tpl"
									type=$current_type->id
									search_categories_array=$search_categories_array
									is_counter=false
									max_depth=1
									dropboxes_count=2
									no_cache=true
								}
							</div>
			                {/if}

                     		{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_where_search) || (!$system_settings.single_type_structure && $current_type && $current_type->where_search && $current_type->locations_enabled)}
                     		<div class="search_filter_standart">
                     			<div>
	                     			{if $system_settings.predefined_locations_mode != 'only'}
	                     				<input type="text" class="what_where_search_input" name="where_search" id="where_search" value="{if $args.where_search}{$args.where_search}{elseif $current_location}{$current_location->getChainAsString()}{else}{$LANG_SEARCH_WHERE} ({$LANG_SEARCH_WHERE_DESCR}){/if}" />
	                     				<input type="hidden" name="predefined_location_id" id="predefined_location_id" value="{if $args.predefined_location_id}{$args.predefined_location_id}{/if}" />
	                     			{else}
		                     			Search in locations
										{if $args.predefined_location_id}
											{$VH->renderLocationDropBoxes($args.predefined_location_id)}
										{elseif $current_location}
											{$VH->renderLocationDropBoxes($current_location->id)}
										{else}
											{$VH->renderLocationDropBoxes()}
										{/if}
									{/if}

                     				<div class="px5"></div>
                     				{$LANG_SEARCH_IN_RADIUS}
                     				<select name="where_radius" id="where_radius" style="min-width: 40px;">
                     					<option value=0>0</option>
                     					<option value=1 {if $args.where_radius==1}selected{/if}>1</option>
                     					<option value=2 {if $args.where_radius==2}selected{/if}>2</option>
                     					<option value=3 {if $args.where_radius==3}selected{/if}>3</option>
                     					<option value=4 {if $args.where_radius==4}selected{/if}>4</option>
                     					<option value=5 {if $args.where_radius==5}selected{/if}>5</option>
                     					<option value=6 {if $args.where_radius==6}selected{/if}>6</option>
                     					<option value=7 {if $args.where_radius==7}selected{/if}>7</option>
                     					<option value=8 {if $args.where_radius==8}selected{/if}>8</option>
                     					<option value=9 {if $args.where_radius==9}selected{/if}>9</option>
                     					<option value=10 {if $args.where_radius==10}selected{/if}>10</option>
                     					<option value=15 {if $args.where_radius==15}selected{/if}>15</option>
                     					<option value=20 {if $args.where_radius==20}selected{/if}>20</option>
                     				</select>
									{if $system_settings.search_in_raduis_measure == 'miles'}
										{$LANG_SEARCH_IN_RADIUS_MILES}
									{else}
										{$LANG_SEARCH_IN_RADIUS_KILOMETRES}
									{/if}
	                     		</div>
                     		</div>
                     		{/if}

                     		{if $advanced_search_fields->fieldsCount() || $content_access_obj->isPermission('Manage all listings')}
                     		<div class="search_filter_standart">
	                     		<div id="advanced_search_button">
	                     			<a href="javascript: void(0);" class="toggle advanced_search"><img src="{$public_path}images/icons/accept.png"></a> <a href="javascript: void(0);" class="toggle advanced_search">{$LANG_ADVANCED_SEARCH}</a>
	                     		</div>
	                     	</div>
	                     	{/if}
	                     	<div class="clear_float"></div>
	                    </div>
	                     	
                     	<div class="search_block">
                     		{if $content_access_obj->isPermission('Manage ability to claim')}
                     		<div class="search_item">
                     			<label><input type="radio" name="search_claimed_listings" value="any" {if !$args.search_claimed_listings}checked{/if} /> {$LANG_SEARCH_ANY_LISTINGS}</label>
                     			<label><input type="radio" name="search_claimed_listings" value="ability_to_claim" {if $args.search_claimed_listings == 'ability_to_claim'}checked{/if} /> {$LANG_LISTINGS_ABILITY_TO_CLAIM_STATUS}</label>
                     			<label><input type="radio" name="search_claimed_listings" value="claimed" {if $args.search_claimed_listings == 'claimed'}checked{/if} /> {$LANG_LISTINGS_CLAIMED_STATUS}</label>
                     			<label><input type="radio" name="search_claimed_listings" value="approved_claim" {if $args.search_claimed_listings == 'approved_claim'}checked{/if} /> {$LANG_LISTINGS_APPROVED_CLAIM_STATUS}</label>
                     		</div>
                     		{/if}

                     		{$search_fields->inputMode($args)}
                     	</div>
                     	<div class="clear_float"></div>
                     	<div id="advanced_search_block" style="display: none;"></div>
                     	<div class="clear_float"></div>
                     	<div class="search_item_button">
	                    	<input type="submit" class="button search_button" id="process_search" value="{$LANG_BUTTON_SEARCH_LISTINGS}">
	                    </div>
                     </form>

                     <div class="search_results_title">
                     	{$LANG_SEARCH_RESULTS_1} ({$listings_count} {$LANG_SEARCH_RESULTS_2}):
                     </div>

                     {if $listings|@count > 0}
                     <form id="listings_form" action="" method="post" onSubmit="submit_listings_form();">
                     <table class="standardTable" border="0" cellpadding="2" cellspacing="2">
                       <tr>
                         <th><input type="checkbox"></th>
                         <!--<th>{$LANG_LOGO_TH}</th>-->
                         <th>{asc_desc_insert base_url=$search_url orderby='title' orderby_query=$orderby direction=$direction title=$LANG_TITLE_TH}</th>
                         <th>{$LANG_OWNER_TH}</th>
                         {if !$system_settings.single_type_structure}
                         <th>{$LANG_TYPE_TH}</th>
                         {/if}
                         <th>{$LANG_LEVEL_TH}</th>
                         <th>{$LANG_STATUS_TH}</th>
                         <th>{asc_desc_insert base_url=$search_url orderby='creation_date' orderby_query=$orderby direction=$direction title=$LANG_CREATION_DATE_TH}</th>
                         <th>{asc_desc_insert base_url=$search_url orderby='expiration_date' orderby_query=$orderby direction=$direction title=$LANG_EXPIRATION_DATE_TH}</th>
                         <th>{$LANG_OPTIONS_TH}</th>
                       </tr>
                       {foreach from=$listings item=listing}
                       {assign var="listing_id" value=$listing->id}
					   {assign var="listing_owner_id" value=$listing->owner_id}
					   {assign var="listing_owner_login" value=$listing->user->login}
                       <tr>
                         <td>
                    	  	<input type="checkbox" name="cb_{$listing->id}" value="{$listing->id}">
                    	 </td>
                         <!--<td>
                         {if $listing->level->logo_enabled}
                         	 <a href="{$VH->site_url("admin/listings/edit/$listing_id")}" title="{$LANG_EDIT_LISTING_OPTION}"><img src="{$users_content}/users_images/logos/{$listing->logo_file}" /></a>&nbsp;
                         {/if}
                         </td>-->
                         <td>
                             <a href="{$VH->site_url("admin/listings/view/$listing_id")}" title="{$LANG_VIEW_LISTING_OPTION}">{$listing->title()}</a>&nbsp;
                             {if $listing->level->raiseup_enabled && $listing->was_raisedup_times > 0}
                             <span title="{$LANG_LISTINGS_WAS_RAISED_UP}"><img src="{$public_path}images/buttons/raise_up.png"></span>
                             {/if}
                         </td>
                         <td>
                         	 {if $listing_owner_id != 1 && $listing_owner_id != $session_user_id && $content_access_obj->getUserAccess($listing_owner_id, 'View all users')}
                             <a href="{$VH->site_url("admin/users/view/$listing_owner_id")}" title="{$LANG_VIEW_USER_OPTION}">{$listing_owner_login}</a>&nbsp;
                             {else}
                             {$listing_owner_login}
                             {/if}

                             {if $content_access_obj->isPermission('Manage ability to claim')}
                             	 {if !$listing->claim_row.approved}
									{if !$listing->claim_row.to_user_id && $listing->claim_row.ability_to_claim}
										<br />(<i>May be claimed</i>)
									{elseif $listing->claim_row.to_user_id && $listing->claim_user}
										{assign var=user_id value=$listing->claim_user->id}
										<br />(<i>{$LANG_LISTING_SHORT_CLAIMED_BY} <a href="{$VH->site_url("admin/users/view/$user_id")}">{$listing->claim_user->login}</a><br />
											<nobr><a href="{$VH->site_url("admin/listings/approve_claim/$listing_id")}"><img src="{$public_path}images/icons/accept.png" /> {$LANG_LISTING_CLAIME_APPROVE}</a></nobr>
											<nobr><a href="{$VH->site_url("admin/listings/decline_claim/$listing_id")}"><img src="{$public_path}images/icons/delete.png" /> {$LANG_LISTING_CLAIME_DECLINE}</a></nobr>
										</i>)
									{/if}
								 {elseif $listing->claim_user}
								 	{$LANG_LISTING_SHORT_CLAIMED_FROM} {$listing->claim_from_user->login}
									<br />(<i><nobr><a href="{$VH->site_url("admin/listings/decline_claim/$listing_id")}"><img src="{$public_path}images/icons/delete.png" /> {$LANG_LISTING_CLAIM_ROLL_BACK}</a></nobr></i>)
								 {/if}
							 {/if}
                         </td>
                         {if !$system_settings.single_type_structure}
                         <td>
                         	{if $content_access_obj->isPermission('Manage all listings')}
                         		<a href="{$VH->site_url("admin/listings/change_type/$listing_id")}" title="{$LANG_CHANGE_LISTING_TYPE_OPTION}">{$listing->type->name}</a>
                         	{else}
                         		{$listing->type->name}
                         	{/if}
                         </td>
                         {/if}
                         <td>
                         	<nobr>
                            {if $content_access_obj->isPermission('Change listing level') && $listing->type->buildLevels()|@count > 1}
								<a href="{$VH->site_url("admin/listings/change_level/$listing_id")}" title="{$LANG_CHANGE_LISTING_LEVEL_OPTION}">{$listing->level->name}</a> <a href="{$VH->site_url("admin/listings/change_level/$listing_id")}" title="{$LANG_CHANGE_LISTING_LEVEL_OPTION}"><img src="{$public_path}/images/icons/upgrade.png" /></a>
							{else}
								{$listing->level->name}
							{/if}
							</nobr>
                         </td>
                         <td>
                         	{if $listing->status == 1}<nobr><a href="{$VH->site_url("admin/listings/change_status/$listing_id")}" class="status_active" title="{$LANG_CHANGE_LISTING_STATUS_OPTION}">{$LANG_STATUS_ACTIVE}</a></nobr>{/if}
                         	{if $listing->status == 2}<nobr><a href="{$VH->site_url("admin/listings/change_status/$listing_id")}" class="status_blocked" title="{$LANG_CHANGE_LISTING_STATUS_OPTION}">{$LANG_STATUS_BLOCKED}</a></nobr>{/if}
                         	{if $listing->status == 3}<nobr><a href="{$VH->site_url("admin/listings/change_status/$listing_id")}" class="status_suspended" title="{$LANG_CHANGE_LISTING_STATUS_OPTION}">{$LANG_STATUS_SUSPENDED}</a>&nbsp;<a href="{$VH->site_url("admin/listings/prolong/$listing_id")}" title="{$LANG_PROLONG_ACTION}"><img src="{$public_path}images/icons/date_add.png"></a></nobr>{/if}
                         	{if $listing->status == 4}<nobr><a href="{$VH->site_url("admin/listings/change_status/$listing_id")}" class="status_unapproved" title="{$LANG_CHANGE_LISTING_STATUS_OPTION}">{$LANG_STATUS_UNAPPROVED}</a></nobr>{/if}
                         	{if $listing->status == 5}<nobr><a href="{$VH->site_url("admin/listings/change_status/$listing_id")}" class="status_notpaid" title="{$LANG_CHANGE_LISTING_STATUS_OPTION}">{$LANG_STATUS_NOTPAID}</a>{if $listing_owner_id == $session_user_id}&nbsp;<a href="{$VH->site_url("admin/payment/invoices/my/")}" title="{$LANG_VIEW_MY_INVOICES_MENU}"><img src="{$public_path}images/buttons/money_add.png"></a>{/if}</nobr>{/if}
                         	&nbsp;
                         </td>
                         <td>
                             {$listing->creation_date|date_format:"%D %T"}&nbsp;
                         </td>
                         <td>
                            {if !$listing->level->active_years && !$listing->level->active_months && !$listing->level->active_days && !$listing->level->allow_to_edit_active_period}
                         		<span class="green">{$LANG_ETERNAL}</span>
                         	{else}
                         		{$listing->expiration_date|date_format:"%D %T"}&nbsp;
                         	{/if}
                         </td>
                         <td>
                         	<nobr>
                         	 {if $listing->status == 1}
                         	 {assign var=listing_unique_id value=$listing->getUniqueId()}
                         	 <a href="{$VH->site_url("listings/$listing_unique_id")}" title="{$LANG_LISTING_LOOK_FRONTEND}"><img src="{$public_path}/images/buttons/house_go.png" /></a>
                         	 {/if}
                         	 <a href="{$VH->site_url("admin/listings/view/$listing_id")}" title="{$LANG_VIEW_LISTING_OPTION}"><img src="{$public_path}images/buttons/page.png"></a>
                             <a href="{$VH->site_url("admin/listings/edit/$listing_id")}" title="{$LANG_EDIT_LISTING_OPTION}"><img src="{$public_path}images/buttons/page_edit.png"></a>
                             <a href="{$VH->site_url("admin/listings/delete/$listing_id")}" title="{$LANG_DELETE_LISTING_OPTION}"><img src="{$public_path}images/buttons/page_delete.png"></a>
                             {if $listing->level->images_count > 0}
                             <a href="{$VH->site_url("admin/listings/images/$listing_id")}" title="{$LANG_LISTING_IMAGES_OPTION}"><img src="{$public_path}images/buttons/images.png"></a>
                             {/if}
                             {if $listing->level->video_count > 0}
                             <a href="{$VH->site_url("admin/listings/videos/$listing_id")}" title="{$LANG_LISTING_VIDEOS_OPTION}"><img src="{$public_path}images/buttons/videos.png"></a>
                             {/if}
                             {if $listing->level->files_count > 0}
                             <a href="{$VH->site_url("admin/listings/files/$listing_id")}" title="{$LANG_LISTING_FILES_OPTION}"><img src="{$public_path}images/buttons/page_link.png"></a>
                             {/if}
                             {if ($system_settings.google_analytics_profile_id && $system_settings.google_analytics_email && $system_settings.google_analytics_password) && ($content_access_obj->isPermission('View all statistics') || ($content_access_obj->isPermission('View self statistics') && $content_access_obj->checkListingAccess($listing_id)))}
                             <a href="{$VH->site_url("admin/listings/statistics/$listing_id")}" title="{$LANG_LISTING_STATISTICS_OPTION}"><img src="{$public_path}images/buttons/chart_bar.png"></a>
                             {/if}
                             {if $listing->level->ratings_enabled && $content_access_obj->isPermission('Manage all ratings')}
                             <a href="{$VH->site_url("admin/ratings/listings/$listing_id")}" title="{$LANG_LISTING_RATINGS_OPTION}"><img src="{$public_path}images/icons/star.png"></a>
                             {/if}
                             {if $listing->level->reviews_mode && $listing->level->reviews_mode != 'disabled' && $content_access_obj->isPermission('Manage all reviews')}
                             <a href="{$VH->site_url("admin/reviews/listings/$listing_id")}" title="{$LANG_LISTING_REVIEWS_OPTION}"><img src="{$public_path}images/icons/comments.png"></a>
                             {/if}
                             {if $listing->level->raiseup_enabled}
                             <a href="{$VH->site_url("admin/listings/raiseup/$listing_id")}" title="{$LANG_RAISE_LISTINGS_UP}"><img src="{$public_path}images/buttons/raise_up.png"></a>
                             {/if}
                            </nobr>
                         </td>
                       </tr>
                       {/foreach}
                     </table>
                     {$LANG_WITH_SELECTED}:
	                 <select name="table_action" onchange="action_cmd=this.options[this.selectedIndex].value; submit_listings_form(); this.form.submit()">
	                 	<option value="">{$LANG_CHOOSE_ACTION}</option>
	                 	<option value="delete">{$LANG_BUTTON_DELETE_LISTINGS}</option>
	                 	<option value="activate">{$LANG_BUTTON_ACTIVATE_LISTINGS}</option>
	                 	<option value="block">{$LANG_BUTTON_BLOCK_LISTINGS}</option>
	                 </select>
                     </form>
                     {$paginator}
                     {/if}
                </div>

{include file="backend/admin_footer.tpl"}