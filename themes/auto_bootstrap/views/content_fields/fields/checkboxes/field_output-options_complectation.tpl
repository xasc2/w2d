{if $current_listing}{assign var="frontend_mode" value="both"}{else}{assign var="frontend_mode" value=$field->frontend_mode}{/if}

					{if $checkboxes|@count != 0}
					<div class="content_field_output field_type_{$field->type}  field_{$field->seo_name}">
						{if $frontend_mode == 'name'}
							<strong>{$field->name}</strong>: 
						{elseif $frontend_mode == 'frontend_name' && ($field->frontend_name || $field->name)}
							<strong>{$field->frontend_name}</strong>: 
						{elseif $frontend_mode == 'icon' && $field->field_icon_image}
							<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
						{elseif $frontend_mode == 'both'}
						{if ($field->frontend_name || $field->name) && $field->field_icon_image}
							<strong><img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> {if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}</strong>: 
							{elseif $field->field_icon_image}
							<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
							{else}
							<strong>{if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}</strong>: 
							{/if}
						{/if}
						<table width="100%" cellspacing="0">
						<tr>
							{assign var=td_padding_pixels value=15}
							{assign var=i value=0}
		
							<td style="padding-right: {$td_padding_pixels}px" valign="top">
								<div>
									{section name=key loop=$checkboxes start=0 step=3}
										<div class="checkboxex_complectation_item">{$checkboxes[key].option_name}</div>
									{/section}
								</div>
							</td>
									
							<td style="padding-right: {$td_padding_pixels}px" valign="top">
								<div>
									{section name=key loop=$checkboxes start=1 step=3}
										<div class="checkboxex_complectation_item">{$checkboxes[key].option_name}</div>
									{/section}
								</div>
							</td>
							
							<td style="padding-right: {$td_padding_pixels}px" valign="top">
								<div>
									{section name=key loop=$checkboxes start=2 step=3}
										<div class="checkboxex_complectation_item">{$checkboxes[key].option_name}</div>
									{/section}
								</div>
							</td>
						</tr>
						</table>
					</div>
					{/if}