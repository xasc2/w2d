
<!-- MAIN MENU -->
			<script language="JavaScript" type="text/javascript" src="{$smarty_obj->getFileInTheme('js/admin_menu.js')}"></script>
			<div class="well sidebar-nav">
				<div class="nav-header"><h4>{$LANG_MAIN_MENU}</h4></div>
				<ul id="browser" class="filetree">
					{$main_menu_list}
				</ul>
			</div>

			<script language="javascript" type="text/javascript">
				<!-- smarty 'insert.route.php' function -->
				{insert name="route"}

				var sinonims = new Array();
				{$sinonims_sinonim_input}
				var urls = new Array();
				{$sinonims_url_input}
			</script>
<!-- /MAIN MENU -->