<ul class="pager">
	{if $prev_listing}
	<li class="previous">
		<a href="{$VH->site_url($prev_listing->url())}" title="{$prev_listing->title()}">{$LANG_PAGINATOR_PREV_LISTING}</a>
	</li>
	{else}
	<li class="previous disabled">
		<a href="javascript:void(0);">{$LANG_PAGINATOR_PREV_LISTING}</a>
	</li>
	{/if}
	<b>{$LANG_PAGINATOR_LISTING_1} {$listing_number} {$LANG_PAGINATOR_LISTING_2} {$listings_count}</b>
	{if $next_listing}
	<li class="next">
		<a href="{$VH->site_url($next_listing->url())}" title="{$next_listing->title()}">{$LANG_PAGINATOR_NEXT_LISTING}</a>
	</li>
	{else}
	<li class="next disabled">
		<a href="javascript:void(0);">{$LANG_PAGINATOR_NEXT_LISTING}</a>
	</li>
	{/if}
</ul>