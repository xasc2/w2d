<!DOCTYPE html>
<html lang="{$current_language}" xml:lang="{$current_language}" xmlns ="http://www.w3.org/1999/xhtml">
	<head>
		<title>{$title}{if $site_settings.website_title} - {$site_settings.website_title}{/if}</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
{foreach from=$css_files item=css_media key=css_file}
		<link rel="stylesheet" href="{$css_file}" media="{$css_media}" type="text/css" />
{/foreach}
{foreach from=$ex_css_files item=ex_css_item}
		<link rel="stylesheet" href="{$ex_css_item}" type="text/css" />
{/foreach}
{foreach from=$ex_js_scripts item=ex_js_item}
		<script language="JavaScript" type="text/javascript" src="{$ex_js_item}"></script>
{/foreach}
{if !$CI->config->item('combine_static_files') || $CI->config->item('combine_static_files') === null}
	{foreach from=$js_scripts item=js_item}
			<script language="JavaScript" type="text/javascript" src="{$VH->base_url()}{$js_item}"></script>
	{/foreach}
{else}
	<script language="JavaScript" type="text/javascript" src="{$VH->base_url()},{$VH->implode(',', $js_scripts)}"></script>
{/if}
		<link rel="shortcut icon" href="{$public_path}images/favicon.ico" >
	</head>
	<body {if $session_user_id}style="padding-top: 40px;"{/if}>
		<!--[if lt IE 9]>
		<script>
			document.createElement('header');
			document.createElement('section');
			document.createElement('footer');
			document.createElement('nav');
			document.createElement('article');
			document.createElement('aside');
			document.createElement('details');
			document.createElement('figcaption');
			document.createElement('figure');
		</script>
		<![endif]-->
		{$VH->buildMessagesBlock()}
		<div id="ajax_loader"><img src="{$public_path}images/ajax-loader.gif"></div>

		{if $session_user_id}
		{include file="frontend/users_main_menu.tpl"}
		{/if}

		<!-- Wrapper Starts -->
		<div class="container-fluid" style="min-height: 700px;">
			<!-- Header Starts -->
			<div class="row-fluid" style="margin-bottom: 10px">
				<!-- Logo Starts -->
				<div class="span4">
					{if $system_settings.site_logo_file}
					<a href="{$VH->index_url()}">
						<img src="{$users_content}/users_images/site_logo/{$system_settings.site_logo_file}" />
					</a>
					{/if}
				</div>
				<!-- Logo Ends -->
			
				<!-- I18n panels Starts -->
				<div class="span8">
					{if $CI->load->is_module_loaded('i18n')}
						{$VH->buildLanguagesPanels($CI)}
					{/if}
				</div>
				<!-- I18n panels Ends -->
			</div>
	
			<div class="row-fluid">
			{if $CI->router->fetch_module() != 'authorization' && $CI->router->fetch_module() != 'install'}
				{assign var=menu_html value=$VH->buildAdminMenu($CI, 50)}
				{if $menu_html}
				<div class="span3">
					{$menu_html}
				</div>
				{/if}
			{/if}
				
				<!-- Content Starts -->
				<div {if $menu_html}class="span9"{/if}>
					<div class="row-fluid">
						{$VH->buildBreadcrumbs($CI)}