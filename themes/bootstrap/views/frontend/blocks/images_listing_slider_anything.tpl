{if $items_array|@count}
<script language="JavaScript" type="text/javascript">
$(function() {ldelim}
	if ($(window).width() > 1024)
		var slides_number = 6;
	else
		var slides_number = 4;

	$('#images_slider').anythingSlider({ldelim}
		resizeContents: true,
		theme: 'minimalist-round',
		buildNavigation: false,
		buildStartStop: false,
		showMultiple: slides_number,
		changeBy: 1,
		infiniteSlides: false,
		expand: true
	{rdelim});
	$('#images_slider').show();
{rdelim});
</script>

					<script language="Javascript" type="text/javascript">
						$(document).ready(function() {ldelim}
							$(".img_small").click(function() {ldelim}
								if ($("#listing_logo").find("img").attr("src") != "{$public_path}images/lightbox-ico-loading.gif") {ldelim}
									var img_small = $(this).clone();
									// retrieve basename of image file
									var file_name = img_small.find("img").attr('src').replace(/^.*\/|\.*$/g, '');

									// Set the size of big image and place ajax loader there
									var big_image_width = $("#listing_logo").find("img").width();
									var big_image_height = $("#listing_logo").find("img").height();
									
									$(".lightbox_image").html("<img src='{$public_path}images/lightbox-ico-loading.gif' />");
									$("#listing_logo").css("width", big_image_width);
									$("#listing_logo").css("height", big_image_height);
									$(".lightbox_image").css("position", "relative");
									$(".lightbox_image").css("top", (big_image_height/2)-20);
									$(".lightbox_image").css("left", (big_image_width/2)-20);

									// Remove thmb of logo from lighbox images
									$(".hidden_divs").each(function() {ldelim}
										if (file_name == $(this).find("a").attr('href').replace(/^.*\/|\.*$/g, ''))
											$(this).find("a").removeClass("hidden_imgs");
										else
											$(this).find("a").addClass("hidden_imgs");
									{rdelim});

									// Load new image into big image container
									var img = new Image();
							        $(img).load(function () {ldelim}
							            $(this).hide();
							            img_small.html(this);
							            img_small.removeClass("img_small").addClass("lightbox_image");
							            $("#listing_logo").html(img_small);
							            $("#listing_logo").css("width", img_small.find("img").width());
										$("#listing_logo").css("height", img_small.find("img").height());
							            $(this).fadeIn();
							        {rdelim}).attr('src', '{$users_content}/users_images/thmbs_big/'+file_name);
								{rdelim}
								return false;
							{rdelim});
						{rdelim});
						</script>

						<div class="px10"></div>
						<div style="height: {$slider_height}px; padding-bottom: 20px">
							<ul id="images_slider" style="display: none;">
								{foreach from=$items_array item=image}
									<li>
										<div style="border: 0; margin: 5px 3px 0;">
											<a href="{$users_content}/users_images/images/{$image->file}" class="img_small" title="{$image->title}"><img src="{$users_content}/users_images/thmbs_small/{$image->file}" itemprop="thumbnailUrl" /></a>
											<div class="hidden_divs" style="display:none"><a href="{$users_content}/users_images/images/{$image->file}" {if $image->file != $listing->logo_file}class="hidden_imgs"{/if} itemprop="image"></a></div>
										</div>
									</li>
								{/foreach}
							</ul>
						</div>
{/if}