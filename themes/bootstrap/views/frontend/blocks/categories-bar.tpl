
<!-- CATEGORIES BLOCK -->

				{if $categories_tree|@count}
					<div class="nav-header"><h5>{$LANG_CATEGORIES}{if $type->categories_type == 'local'} {$LANG_IN} {$type->name}{/if}</h5></div>
					<div class="left_sidebar_categories">
						{include file='frontend/categories/normal.tpl' assign=template}
						{foreach from=$categories_tree item=category}
							{$category->render($template, $is_counter, $max_depth, $current_category->seo_name, 'class="active_category"')}
						{/foreach}
					</div>
				{/if}
	                
<!-- /CATEGORIES BLOCK -->
