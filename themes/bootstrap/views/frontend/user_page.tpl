{include file="frontend/header.tpl"}
{assign var="user_id" value=$user->id}
{assign var="user_unique_id" value=$user->getUniqueId()}
{assign var="user_login" value=$user->login}

			<div class="row" id="search_bar">
				{include file="frontend/search_block.tpl"}
			</div>

			<div class="row">
			 	<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>
	
				<div class="span9">
      				<div id="content_wrapper" itemscope itemtype="http://schema.org/ProfilePage">
      					<ul class="breadcrumb" itemprop="breadcrumb">
      						<li><a href="{$VH->index_url()}">{$LANG_HOME_PAGE}</a> <span class="divider">/</span></li>
      						<li class="active">{$user->login}</li>
      					</ul>

						<div id="user_header_block">
	                        <div class="user_header">
	                        	<iframe src="http://www.facebook.com/plugins/like.php?href={$VH->site_url($user->profileUrl())}&amp;layout=standard&amp;show_faces=false&amp;width=345&amp;action=like&amp;colorscheme=light" scrolling="no" frameborder="0" allowTransparency="true" style="border:none; overflow:hidden; width:345px; height:45px"></iframe>

	                        	<h2 class="user_login" itemprop="name">{$user->login}</h2>
	                        	<div class="listing_author">{$LANG_USER_REGISTERED} {$user->registration_date|date_format:"%D %H:%M"}</div>
	                        	<div class="px5"></div>

		                        {if $user->users_group->logo_enabled && ($user->user_logo_image || $user->facebook_logo_file)}
		                        <div id="user_logo">
									<img src="{$user->profileImageUrl()}" itemprop="image" />
								</div>
								{/if}
							</div>

							<div id="user_options_panel">
								<a class="a2a_dd" href="http://www.addtoany.com/share_save"><img src="http://static.addtoany.com/buttons/share_save_171_16.png" width="171" height="16" border="0" alt="Share/Bookmark"/></a><script type="text/javascript" src="http://static.addtoany.com/menu/page.js"></script>
								<span class="user_options_panel_item print"><a href="javascript: void(0);" onClick="$.jqURL.loc('{$VH->site_url("print_user/$user_id/")}', {ldelim}w:590,h:750,wintype:'_blank'{rdelim}); return false;">{$LANG_PRINT_PAGE}</a></span>
								<span class="user_options_panel_item pdf"><a href="http://pdfmyurl.com/?url={$VH->site_url($user->profileUrl())}">{$LANG_PDF_PAGE}</a></span>
								<span class="user_options_panel_item owner"><a href="{$VH->site_url("email/send/user_id/$user_id")}" class="nyroModal" title="{$LANG_SEND_EMAIL_TO_USER}">{$LANG_SEND_EMAIL_TO_USER}</a></span>
							</div>
							<div class="clear_float"></div>
						</div>

						{if $user->content_fields->fieldsCount() && $user->content_fields->isAnyValue()}
							<div class="px10"></div>
							<h3>{$LANG_LISTING_INFORMATION}</h3>
							<span itemprop="description">{$user->content_fields->outputMode()}</span>
						{/if}

						{foreach from=$types item=type}
                        	{assign var=type_id value=$type->id}
                        	{assign var=view value=$listings_views->getViewByTypeIdAndPage($type_id, 'types')}
							{render_frontend_block
	                        	block_type='listings'
	                        	block_template='frontend/blocks/listings_of_user.tpl'
	                        	page_name='types'
	                        	view_name=$view->view
                        		view_format=$view->format
	                        	search_type=$type
	                        	search_owner=$user->login
	                        	search_location=$current_location
	                        	orderby='l.order_date'
	                        	search_status=1
	                        	search_users_status=2
	                        	limit=3
	                        }
						{/foreach}
                 	</div>
                </div>
			</div>

{include file="frontend/footer.tpl"}