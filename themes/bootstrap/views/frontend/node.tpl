{include file="frontend/header.tpl"}

			<div class="row" id="search_bar">
				{include file="frontend/search_block.tpl"}
			</div>

			<div class="row">
				<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>

				<div class="span9">
					<div id="content_wrapper">
						<h2>{$node->title}</h2>
						{$node->outputMode()}
					</div>
				</div>
			</div>

{include file="frontend/footer.tpl"}