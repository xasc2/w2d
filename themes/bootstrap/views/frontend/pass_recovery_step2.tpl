{include file="frontend/header.tpl"}

			<div class="row" id="search_bar">
				{include file="frontend/search_block.tpl"}
			</div>

			<div class="row">
			 	<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>
	
				<div class="span9">
      				<div id="content_wrapper">
      					{$VH->validation_errors()}
                         <h2>{$LANG_PASSWORD_RECOVERY2}</h2>

                         <form action="" method="post">
	                     <div class="admin_option">
	                          <div class="admin_option_name">
	                          	{$LANG_PASSWORD}<span class="red_asterisk">*</span>
	                          </div>
	                          <div class="admin_option_description">
	                          	{$LANG_PASSWORD_DESCR}
	                          </div>
	                          <input type=password name="password" size="45" class="admin_option_input">
	                          <div class="admin_option_name">
	                          	{$LANG_PASSWORD_REPEAT}<span class="red_asterisk">*</span>
	                          </div>
	                          <input type=password name="repeat_password" size="45" class="admin_option_input">
	                     </div>
						 <div class="px5"></div>
	                     <input class="btn btn-info" type=submit name="submit" value="{$LANG_BUTTON_PASSWORD_RECOVERY2}">
	                     </form>
                 	</div>
                </div>
			</div>

{include file="frontend/footer.tpl"}