{assign var="listing_id" value=$listing->id}
{assign var="listing_unique_id" value=$listing->getUniqueId()}

										<span id="listing_id-{$listing_unique_id}"></span>
										{if $listing->level->sticky}
		                         			<div class="sticky_icon" title="{$LANG_STICKY_TITLE}"></div>
		                         		{/if}
	                         			<div class="clear_float"></div>
										{if $listing->level->logo_enabled}
										{if $listing->logo_file}<meta itemprop="image" content="{$users_content}/users_images/images/{$listing->logo_file}" />{/if}
										<a href="{$VH->site_url($listing->url())}">
			                         		<div class="img_div_border listing_semitable_logo" style="width: {$listing->level->explodeSize('logo_size', 'width')}px; height: {$listing->level->explodeSize('logo_size', 'height')}px;">
			                         			{if $listing->level->featured}<div class="ribbon" style="width: {$listing->level->explodeSize('logo_size', 'width')}px; height: {$listing->level->explodeSize('logo_size', 'height')}px;"></div>{/if}
												<span class="img_div_helper"></span>{if $listing->logo_file}<img src="{$users_content}/users_images/logos/{$listing->logo_file}" alt="{$listing->title()}" style="max-width: {$listing->level->explodeSize('logo_size', 'width')}px; max-height: {$listing->level->explodeSize('logo_size', 'height')}px;" />{else}<img src="{$public_path}/images/default_logo.jpg" style="max-width: {$listing->level->explodeSize('logo_size', 'width')}px; max-height: {$listing->level->explodeSize('logo_size', 'height')}px" />{/if}
											</div>
										</a>
										{/if}

										{if $listing->level->ratings_enabled}
											{assign var=avg_rating value=$listing->getRatings()}
											<div {if $avg_rating->ratings_count}itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"{/if}>
											{$avg_rating->view()}
											</div>
										{/if}
										{if $listing->level->reviews_mode && $listing->level->reviews_mode != 'disabled'}
		                         		<div class="stat">
		                         			<a href="{$VH->site_url($listing->url())}#reviews-tab" title="{if $listing->level->reviews_mode == 'reviews'}{$LANG_READ_REVIEWS}{else}{$LANG_READ_COMMENTS}{/if}">{$listing->getReviewsCount()}&nbsp;{if $listing->level->reviews_mode == 'reviews'}{$LANG_REVIEWS}{else}{$LANG_COMMENTS}{/if}</a>
		                         		</div>
		                         		{/if}
		                         		<div class="clear_float"></div>

										<div class="listing_title listing_title_small">
	                         				<a href="{$VH->site_url($listing->url())}" itemprop="url">{$listing->title()}</a>
	                         			</div>
	                         			{if $listing->type->categories_type != 'disabled' && $listing->level->categories_number && $listing->categories_array()|@count}
		                         			<div class="listing_categories_semitable">
		                         				{$LANG_SUMITTED_3}&nbsp;
		                         				{foreach from=$listing->categories_array() item=category}
		                         				{assign var="category_seo_name" value=$category->seo_name}
		                         					<a href="{$VH->site_url($category->getUrl())}" class="listing_cat_link">{$category->name}</a>&nbsp;&nbsp;
		                         				{/foreach}
		                         			</div>
	                         			{/if}
	                         			{$listing->outputMode(semitable)}