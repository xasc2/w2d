{assign var="listing_id" value=$listing->id}
{assign var="listing_unique_id" value=$listing->getUniqueId()}

		                         				<td class="content_field_output">
		                         					{if $listing->level->featured}<b>{/if}
		                         					<time itemprop="datePublished" datetime="{$listing->order_date|date_format:"%Y-%m-%dT%H:%M"}">
			                         					{$listing->order_date|date_format:"%D"}<br />
			                         					{$listing->order_date|date_format:"%T"}
			                         				</time>
		                         					{if $listing->level->featured}</b>{/if}

		                         					{if $listing->level->sticky}
		                         					<div class="clear_float"></div>
		                         					<div class="sticky_icon" title="{$LANG_STICKY_TITLE}"></div>
		                         					<div class="clear_float"></div>
		                         					{/if}
		                         				</td>
	                         					<td align="center" style="padding: 5px 0;" {if $listing->level->logo_enabled}width="{$listing->level->explodeSize('logo_size', 'width')}px"{/if}>
	                         						{if $listing->level->logo_enabled}
	                         						{if $listing->logo_file}<meta itemprop="image" content="{$users_content}/users_images/images/{$listing->logo_file}" />{/if}
	                         						<a href="{$VH->site_url($listing->url())}">
		                         						{if $listing->level->featured}<div class="ribbon" style="width: {$listing->level->explodeSize('logo_size', 'width')}px; height: {$listing->level->explodeSize('logo_size', 'height')}px;"></div>{/if}
						                         		<div class="img_div_border" style="margin:0; border-width:2px; width: {$listing->level->explodeSize('logo_size', 'width')}px; height: {$listing->level->explodeSize('logo_size', 'height')}px;">
															<span class="img_div_helper"></span>{if $listing->logo_file}<img src="{$users_content}/users_images/logos/{$listing->logo_file}" alt="{$listing->title()}" style="max-width: {$listing->level->explodeSize('logo_size', 'width')}px; max-height: {$listing->level->explodeSize('logo_size', 'height')}px;" />{else}<img src="{$public_path}/images/default_logo.jpg" style="max-width: {$listing->level->explodeSize('logo_size', 'width')}px; max-height: {$listing->level->explodeSize('logo_size', 'height')}px" />{/if}
														</div>
													</a>
													{/if}
												</td>
												<td class="content_field_output">
													{if $listing->level->featured}<b>{/if}
		                         					<a href="{$VH->site_url($listing->url())}" itemprop="url">{$listing->title()}</a>
		                         					{if $listing->level->featured}</b>{/if}
												</td>
												{$listing->outputMode(short)}