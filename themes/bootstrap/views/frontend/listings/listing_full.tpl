{assign var="listing_id" value=$listing->id}
{assign var="listing_unique_id" value=$listing->getUniqueId()}
{assign var="user_unique_id" value=$listing->user->getUniqueId()}

									<div id="listing_id-{$listing_unique_id}" class="listing_preview {if $listing->level->featured}featured{/if}" itemscope itemtype="http://schema.org/WebPage">
										<div class="listing_head">
											{if $listing->level->sticky}
		                         				<div class="sticky_icon" title="{$LANG_STICKY_TITLE}"></div>
		                         			{/if}

		                         			<div class="listing_title">
		                         				<a href="{$VH->site_url($listing->url())}" itemprop="url">{$listing->title()}</a>
		                         			</div>
		                         			
		                         			<div class="listing_full_options_panel">
				                         		{if $listing->level->images_count && $listing->getAssignedImages()}
													<a href="{$VH->site_url($listing->url())}#images" title="{$LANG_LISTING_IMAGES_OPTION}"><img src="{$public_path}/images/buttons/images.png" /></a>&nbsp;
												{/if}
												{if $listing->level->video_count && $listing->getAssignedVideos()}
													<a href="{$VH->site_url($listing->url())}#videos-tab" title="{$LANG_LISTING_VIDEOS_OPTION}"><img src="{$public_path}/images/buttons/videos.png" /></a>&nbsp;
												{/if}
												{if $listing->level->files_count && $listing->getAssignedFiles()}
													<a href="{$VH->site_url($listing->url())}#files-tab" title="{$LANG_LISTING_FILES_OPTION}"><img src="{$public_path}/images/buttons/page_link.png" /></a>&nbsp;
												{/if}
												{if $listing->type->locations_enabled && $listing->locations_count(true) && $listing->level->maps_enabled}
													<a href="{$VH->site_url($listing->url())}#addresses-tab" id="open_iw_{$listing_unique_id}" title="{$LANG_MAP}"><img src="{$public_path}/images/buttons/map.png" /></a>&nbsp;
												{/if}
												{if $listing->level->option_quick_list}
													<a href="javascript: void(0);" class="add_to_favourites" listingid="{$listing->id}" title="{$LANG_ADD_REMOVE_QUICK_LIST}"></a>&nbsp;
												{/if}
												{if $listing->level->option_email_friend}
													<a href="{$VH->site_url("email/send/listing_id/$listing_id/target/friend/")}" class="nyroModal" title="{$LANG_EMAIL_FRIEND}"><img src="{$public_path}/images/icons/email_go.png"></a>&nbsp;
												{/if}
			                         		</div>
		                         			<div class="listing_author">
		                         				<span itemprop="author" itemscope itemtype="http://schema.org/Person">
			                        				<meta itemprop="image" content="{$listing->user->profileImageUrl()}">
													{$LANG_SUMITTED_1}
													{if $listing->user->users_group->is_own_page && $listing->user->users_group->id != 1}
														<a href="{$VH->site_url("users/$user_unique_id")}" title="{$LANG_VIEW_USER_PAGE_OPTION}" itemprop="url">
															{$listing->user->login}
														</a>
													{else}
														<strong itemprop="name">{$listing->user->login}</strong>
													{/if}
												</span> {$LANG_SUMITTED_2} <time itemprop="datePublished" datetime="{$listing->order_date|date_format:"%Y-%m-%dT%H:%M"}">{$listing->order_date|date_format:"%D %H:%M"}</time>
											</div>
		                         			{if $listing->type->categories_type != 'disabled' && $listing->level->categories_number && $listing->categories_array()|@count}
			                         			<div class="listing_categories">
			                         				{$LANG_SUMITTED_3}&nbsp;
			                         				{foreach from=$listing->categories_array() item=category}
			                         				{assign var=category_url value=$category->getUrl()}
			                         					<a href="{$VH->site_url($category->getUrl())}" class="listing_cat_link">{$category->name}</a>&nbsp;&nbsp;
			                         				{/foreach}
			                         			</div>
		                         			{/if}
		                         			<div class="clear_float"></div>

		                         			{if $listing->level->ratings_enabled}
												{assign var=avg_rating value=$listing->getRatings()}
												<div {if $avg_rating->ratings_count}itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"{/if}>
												{$avg_rating->view()}
												</div>
											{/if}
											{if $listing->level->reviews_mode && $listing->level->reviews_mode != 'disabled'}
		                         			<div class="stat">
		                         				<a href="{$VH->site_url($listing->url())}#reviews-tab" title="{if $listing->level->reviews_mode == 'reviews'}{$LANG_READ_REVIEWS}{else}{$LANG_READ_COMMENTS}{/if}">{$listing->getReviewsCount()} {if $listing->level->reviews_mode == 'reviews'}{$LANG_REVIEWS}{else}{$LANG_COMMENTS}{/if}</a> <a href="{$VH->site_url($listing->url())}#reviews-tab" title="{if $listing->level->reviews_mode == 'reviews'}{$LANG_READ_REVIEWS}{else}{$LANG_READ_COMMENTS}{/if}"><img src="{$public_path}/images/icons/comments.png" /></a>
		                         			</div>
		                         			{/if}
		                         			<div class="clear_float"></div>
	                         			</div>

										{if $listing->level->logo_enabled}
										{if $listing->logo_file}<meta itemprop="image" content="{$users_content}/users_images/images/{$listing->logo_file}" />{/if}
										<a href="{$VH->site_url($listing->url())}">
			                         		<div class="img_div_border listing_full_logo" style="width: {$listing->level->explodeSize('logo_size', 'width')}px; height: {$listing->level->explodeSize('logo_size', 'height')}px;">
			                         			{if $listing->level->featured}<div class="ribbon" style="width: {$listing->level->explodeSize('logo_size', 'width')}px; height: {$listing->level->explodeSize('logo_size', 'height')}px;"></div>{/if}
												<span class="img_div_helper"></span>{if $listing->logo_file}<img src="{$users_content}/users_images/logos/{$listing->logo_file}" alt="{$listing->title()}" style="max-width: {$listing->level->explodeSize('logo_size', 'width')}px; max-height: {$listing->level->explodeSize('logo_size', 'height')}px;" />{else}<img src="{$public_path}/images/default_logo.jpg" style="max-width: {$listing->level->explodeSize('logo_size', 'width')}px; max-height: {$listing->level->explodeSize('logo_size', 'height')}px" />{/if}
											</div>
										</a>
										{/if}
										<div class="listing_description">
											{if $listing->type->locations_enabled && $listing->locations_count()}
											<div class="listing_address_block">
												{assign var="i" value=1}
												{foreach from=$listing->locations_array() item=location}
												<span itemprop="contentLocation" itemscope itemtype="http://schema.org/Place">
													<meta itemprop="name" content="{$listing->title()}">
													<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
														{if $location->address_line_1 || $location->address_line_2}<meta itemprop="streetAddress" content="{$location->address_line_1} {$location->address_line_2}">{/if}
														{if $location->location()}<meta itemprop="addressLocality" content="{$location->location()}">{/if}
														{if $location->zip_or_postal_index}<meta itemprop="postalCode" content="{$location->zip_or_postal_index}">{/if}
													</span>
													{if $listing->level->maps_enabled && $listing->locations_count(true)}
													<span itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
														<meta itemprop="latitude" content="{$location->map_coords_1}">
														<meta itemprop="longitude" content="{$location->map_coords_2}">
													</span>
													{/if}
												</span>
												<address>{if $location->calcDistanceFromCenter()}<span class="address_label" style="color: #006699">{$location->calcDistanceFromCenter()}</span>&nbsp;&nbsp;{/if} {if $listing->locations_count()>1}<span class="address_label">{$LANG_LISTING_ADDRESS} {$i++}:</span> {/if}{$location->compileAddress()}</address>
												{/foreach}
											</div>
											{/if}
											<span itemprop="description">
												{if $listing->level->description_mode == 'richtext'}
												{$listing->listing_description_teaser()}
												{else}
												{$listing->listing_description_teaser()|nl2br}
												{/if}
											</span>
		                         		</div>
	                         			<div class="clear_float"></div>
	                         			{$listing->outputMode(full)}
	                         			<a href="{$VH->site_url($listing->url())}" class="view_listing_link">{$LANG_VIEW_LISTING} >>></a>
	                         		</div>