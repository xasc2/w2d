{if $listings_array|@count}
	<div class="index_types_listings">
	{foreach from=$listings_array item=listing}
		{$listing->view($view_name)}
	{/foreach}
	</div>
{/if}