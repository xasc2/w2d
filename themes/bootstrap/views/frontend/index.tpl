{include file="frontend/header.tpl"}

			<div class="row" id="search_bar">
				{include file="frontend/search_block.tpl"}
			</div>

			<div class="row">
			 	<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>
	
				<div class="span9">
					<div id="content_wrapper">
						{if $CI->load->is_module_loaded('rss')}
						<div id="index_header">
							<div class="rss_icon">
								<a href="{$VH->getRssUrl()}" title="{$VH->getRssTitle()}">
									<img src="{$public_path}images/feed.png" />
								</a>
							</div>
						</div>
						{/if}
	
						<div class="index_listings">
							{foreach from=$types item=type}
								{assign var=type_id value=$type->id}
								{assign var=view value=$listings_views->getViewByTypeIdAndPage($type_id, 'index')}
								{render_frontend_block
									block_type='listings'
									block_template='frontend/blocks/for_index.tpl'
									items_array=$listings_of_type[$type_id]
									view_name=$view->view
									view_format=$view->format
									type=$type
								}
							{/foreach}
						</div>
					</div>
				</div>
			</div>

{include file="frontend/footer.tpl"}