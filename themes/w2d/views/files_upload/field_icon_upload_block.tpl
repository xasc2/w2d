					<script>
						$(document).ready(function(){ldelim}
							var first_change_bool = false;
							var first_change_value = '{if $current_file}{$current_file}{/if}';
							$('#existed_icons').change(function() {ldelim}
								if ($(this).val() != 0) {ldelim}
									if (!first_change_bool) {ldelim}
										first_change_value = $('#{$upload_id}').val();
										first_change_bool = true;
									{rdelim}
									$('#{$upload_id}').val($(this).val());
									$('#img').attr('src', '{$upload_to}'+$(this).val());
									$('#img_div_border').show();
								{rdelim} else {ldelim}
									$('#{$upload_id}').val(first_change_value);
									$('#img').attr('src', '{$upload_to}'+first_change_value);
									first_change_bool = false;
									if (first_change_value)
										$('#img_div_border').show();
								{rdelim}
							{rdelim});
							$('.upload_button').click(function() {ldelim}
								first_change_bool = false;
								$('#existed_icons option[value="0"]').attr("selected", "selected");
							{rdelim})
						{rdelim});
					</script>
					<div class="admin_option">
						<div class="admin_option_name">
							{$title}
						</div>
						<div class="admin_option_description">
							{$LANG_MAX_IMAGE_SIZE} {$attrs.width}*{$attrs.height}px, {$LANG_MAX_FILE_SIZE} {$max_upload_filesize}. {$LANG_SUPPORTED_FORMAT}: {$VH->str_replace('|', ', ', $allowed_types)}
						</div>
						<div id="img_wrapper">
							<input id="{$upload_id}_browse" type="file" size="45" name="{$upload_id}_browse"><br>
							<input type="button" class="upload_button button" onclick="return ajaxImageFileUpload('{$upload_id}', '{$VH->site_url("ajax/files_upload/$upload_id")}', '{$after_upload_url}', '{$upload_to}', '{$error_file_choose}');" value="{$LANG_BUTTON_UPLOAD_IMAGE}">
							<div class="px5"></div>
							<div id="img_div_border" class="img_div_border" style="{if !$current_file}display:none; {/if}width: {$attrs.width}px; height: {$attrs.height}px;">
								<span class="img_div_helper"></span><img id="img" src="{if $current_file}{$upload_to}{$current_file}{/if}" style="max-width: {$attrs.width}px; max-height: {$attrs.height}px;" />
								<input type="hidden" name="{$upload_id}" id="{$upload_id}" value="{if $current_file}{$current_file}{/if}">
							</div>
							<div class="px5"></div>
							<div class="admin_option_name">
								{$LANG_FIELD_ICON_IMAGE_SELECT}
							</div>
							<select id="existed_icons">
								<option value="0">{$LANG_FIELD_ICON_IMAGE_USE_UPLOADED}</option>
								{foreach from=$attrs.existed_icons item=icon}
								<option value="{$icon}" {if $current_file && $current_file == $icon}selected{/if}>{$icon}</option>
								{/foreach}
							</select>
						</div>
						<img id="loading" src="{$public_path}images/ajax-loader.gif" style="display: none;">
					</div>