<script language="JavaScript" type="text/javascript">
jQuery( function($) {ldelim}
	$("#search_form").submit( function() {ldelim}
		if ($("#{$from_index}").val() != '')
			global_js_url = global_js_url + $("#{$from_index}").attr('id') + '/' + $("#{$from_index}").val() + '/';
		if ($("#{$to_index}").val() != '')
			global_js_url = global_js_url + $("#{$to_index}").attr('id') + '/' + $("#{$to_index}").val() + '/';
			
		window.location.href = global_js_url;
		return false;
	{rdelim});
{rdelim});
</script>

							<div class="search_item search_field_type_{$field->type} search_field_{$field->seo_name}">
								<label>
								{if $field->field_icon_image && ($field->frontend_mode == 'icon' || $field->frontend_mode == 'both')}
									<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
								{/if}
								{if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}
								</label>
								<div class="search_field_from">
	                     			<span>{$LANG_FROM}</span>
	                     			<select id="{$from_index}" name="{$from_index}">
	                     				<option value="">{$LANG_MIN}</option>
	                     				{foreach from=$min_max_options item=option}
	                     				<option value="{$option.option_name}" {if $args[$from_index] == $option.option_name}selected{/if}>{$VH->number_format($option.option_name, 2, $decimals_separator, $thousands_separator)}</option>
	                     				{/foreach}
	                     			</select>
                     			</div>
                     			<div class="search_field_to">
                     				<span>{$LANG_TO}</span>
	                     			<select id="{$to_index}" name="{$to_index}">
	                     				<option value="">{$LANG_MAX}</option>
	                     				{foreach from=$min_max_options item=option}
	                     				<option value="{$option.option_name}" {if $args[$to_index] == $option.option_name}selected{/if}>{$VH->number_format($option.option_name, 2, $decimals_separator, $thousands_separator)}</option>
	                     				{/foreach}
	                     			</select>
                     			</div>
                     			<div class="clear_float"></div>
                     		</div>