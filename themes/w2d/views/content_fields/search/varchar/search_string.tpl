<script language="JavaScript" type="text/javascript">
jQuery( function($) {ldelim}
	$("#search_form").submit( function() {ldelim}
		if ($("#{$field_index}").val())
			global_js_url = global_js_url + $("#{$field_index}").attr('id') + '/' + $("#{$field_index}").val() + '/';
			
		window.location.href = global_js_url;
		return false;
	{rdelim});
{rdelim});
</script>

							<div class="search_item search_field_type_{$field->type} search_field_{$field->seo_name}">
								<label>
								{if $field->field_icon_image && ($field->frontend_mode == 'icon' || $field->frontend_mode == 'both')}
									<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
								{/if}
								{if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}
								</label>
								<input type="text" name="{$field_index}" id="{$field_index}" value="{$args[$field_index]}" size="{$max_length}" maxlength="{$max_length}">
                     		</div>