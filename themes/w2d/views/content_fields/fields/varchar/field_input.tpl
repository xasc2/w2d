{assign var="field_value_id" value=$field->field_value_id}

			<div class="admin_option_name">
				{$field->name}{if $field->required}<span class="red_asterisk">*</span>{/if}
				{if !$is_numeric && $is_translatable && $system_settings.language_areas_enabled}
					{if $field_value_id != 'new'} 
						{translate_content table='content_fields_type_varchar_data' field='field_value' row_id=$field_value_id}
					{else}
						{assign var=virtual_id value=$VH->rand()}
						{translate_content table='content_fields_type_varchar_data' field='field_value' row_id=$field_value_id virtual_id=$virtual_id}
						<input type="hidden" name="field_virtual_id_{$field->seo_name}" value="{$virtual_id}">
					{/if}
				{/if}
			</div>
			<div class="admin_option_description">
				{$field->description|nl2br}
			</div>
			<input type="text" name="field_{$field->seo_name}" value="{$value}" size="{$field_length}" maxlength="{$max_length}" class="admin_option_input">
			<br />
			<br />