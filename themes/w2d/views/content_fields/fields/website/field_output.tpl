{if $current_listing}{assign var="frontend_mode" value="both"}{else}{assign var="frontend_mode" value=$field->frontend_mode}{/if}

					{if $field->value != ''}
					<div class="content_field_output field_type_{$field->type}  field_{$field->seo_name}">
						{if $frontend_mode == 'name'}
							<strong>{$field->name}</strong>: 
						{elseif $frontend_mode == 'frontend_name' && ($field->frontend_name || $field->name)}
							<strong>{$field->frontend_name}</strong>: 
						{elseif $frontend_mode == 'icon' && $field->field_icon_image}
							<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
						{elseif $frontend_mode == 'both'}
							{if ($field->frontend_name || $field->name) && $field->field_icon_image}
							<strong><img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> {if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}</strong>: 
							{elseif $field->field_icon_image}
							<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
							{else}
							<strong>{if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}</strong>: 
							{/if}
						{/if}
						{if !$enable_redirect}
							<a href="{$value}" target="_blank" title="{$value}">{$value}</a>
						{else}
							<a href="{$VH->site_url("redirect/$field_value_id")}" rel="nofollow" target="_blank" title="{$value}">{$value}</a>
						{/if}
					</div>
					{/if}