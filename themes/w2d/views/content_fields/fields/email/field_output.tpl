{if $current_listing}{assign var="frontend_mode" value="both"}{else}{assign var="frontend_mode" value=$field->frontend_mode}{/if}

					{if $field->value != ''}
					<div class="content_field_output field_type_{$field->type}  field_{$field->seo_name}">
						{if $field->frontend_mode == 'name'}
							<strong>{$field->name}</strong>:<br />
						{elseif $field->frontend_mode == 'frontend_name' && $field->frontend_name}
							<strong>{$field->frontend_name}</strong>:<br />
						{elseif $field->frontend_mode == 'icon' && $field->field_icon_image}
							<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
						{elseif $field->frontend_mode == 'both'}
							{if $field->frontend_name && $field->field_icon_image}
							<strong><img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> {$field->frontend_name}</strong>:<br />
							{elseif $field->frontend_name}
							<strong>{$field->frontend_name}</strong>:<br />
							{elseif $field->field_icon_image}
							<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
							{/if}
						{/if}
						<span class="email_field-{$field->seo_name}">
							<a id="email_{$field->seo_name}"></a>
							<script type="text/javascript">
								var email='{$part1}'+'{$part2}';
								$("#email_{$field->seo_name}").attr('href', 'mailto: '+email).text(email);
							</script>
						</span>
					</div>
					{/if}