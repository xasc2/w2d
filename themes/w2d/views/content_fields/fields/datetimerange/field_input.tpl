			<div class="admin_option_name">
				{$field->name}{if $field->required}<span class="red_asterisk">*</span>{/if}
			</div>
			<div class="admin_option_description">
				{$field->description|nl2br}
			</div>

			<script language="javascript" type="text/javascript">
				var lclDate = new Date();
				var timezone_offset = lclDate.getTimezoneOffset()*60;

				var events_array_{$field->seo_name} = [];
				{foreach from=$field->value item=event}
				events_array_{$field->seo_name}.push({ldelim}
					id: {$event.id},
					title: "{$VH->addslashes($event.title)}",
					description: "{$VH->addslashes($event.description)}",
					start_date: {$event.start_date},
					start: {$event.start_date},
					end_date: {$event.end_date},
					end: {$event.end_date},
					cycle_days_monday: {if $event.cycle_days_monday}true{else}false{/if},
					cycle_days_tuesday: {if $event.cycle_days_tuesday}true{else}false{/if},
					cycle_days_wednesday: {if $event.cycle_days_wednesday}true{else}false{/if},
					cycle_days_thursday: {if $event.cycle_days_thursday}true{else}false{/if},
					cycle_days_friday: {if $event.cycle_days_friday}true{else}false{/if},
					cycle_days_saturday: {if $event.cycle_days_saturday}true{else}false{/if},
					cycle_days_sunday: {if $event.cycle_days_sunday}true{else}false{/if},
					allDay: {if $enable_time}false{else}true{/if}
				{rdelim});
				{/foreach}

				$(document).ready( function() {ldelim}
					var calendar_{$field->seo_name} = $('#fullcalendar_{$field->seo_name}');
					var event_start_{$field->seo_name} = $('#event_start_{$field->seo_name}');
					var event_start_tmstmp_{$field->seo_name} = null;
					var event_end_{$field->seo_name} = $('#event_end_{$field->seo_name}');
					var event_end_tmstmp_{$field->seo_name} = null;
					var event_title_{$field->seo_name} = $('#event_title_{$field->seo_name}');
					var event_description_{$field->seo_name} = $('#event_description_{$field->seo_name}');
					var form_{$field->seo_name} = $('#dialog-form_{$field->seo_name}');

					var selected_event_id;
					var selected_event_key;

					{if $current_language && $current_language != 'en'}
					var date_format = $.datepicker.regional["{$current_language}"].dateFormat;
					{else}
					var date_format = 'mm/dd/yy';
					{/if}

					calendar_{$field->seo_name}.fullCalendar({ldelim}
						height: 400,
						header: {ldelim}
							left: 'prev,next today',
							center: 'title',
							right: 'month,agendaWeek,agendaDay'
						{rdelim},
						dayClick: function(date, allDay, jsEvent, view) {ldelim}
							event_start_tmstmp_{$field->seo_name} = date.getTime();
							event_end_tmstmp_{$field->seo_name} = date.getTime();
							formOpen_{$field->seo_name}('add');
						{rdelim},
						eventClick: function(calEvent, jsEvent, view) {ldelim}
							for (var i=0; i<events_array_{$field->seo_name}.length; i++) {ldelim}
								if (events_array_{$field->seo_name}[i].id == calEvent.id) {ldelim}
									selected_event = events_array_{$field->seo_name}[i];
									selected_event_key = i;
									break;
								{rdelim}
							{rdelim}
							selected_event_id = selected_event.id;

						    event_title_{$field->seo_name}.val(selected_event.title);
						    event_description_{$field->seo_name}.val(selected_event.description);

						    event_start_tmstmp_{$field->seo_name} = selected_event.start_date*1000 + timezone_offset*1000;
						    if (selected_event.end_date === null)
								event_end_tmstmp_{$field->seo_name} = selected_event.start_date*1000 + timezone_offset*1000;
						    else
						    	event_end_tmstmp_{$field->seo_name} = selected_event.end_date*1000 + timezone_offset*1000;

						    if (selected_event.cycle_days_monday)
						    	$("#cycle_days_monday_{$field->seo_name}").attr('checked', true);
						    if (selected_event.cycle_days_tuesday)
						    	$("#cycle_days_tuesday_{$field->seo_name}").attr('checked', true);
						    if (selected_event.cycle_days_wednesday)
						    	$("#cycle_days_wednesday_{$field->seo_name}").attr('checked', true);
						    if (selected_event.cycle_days_thursday)
						    	$("#cycle_days_thursday_{$field->seo_name}").attr('checked', true);
						    if (selected_event.cycle_days_friday)
						    	$("#cycle_days_friday_{$field->seo_name}").attr('checked', true);
						    if (selected_event.cycle_days_saturday)
						    	$("#cycle_days_saturday_{$field->seo_name}").attr('checked', true);
						    if (selected_event.cycle_days_sunday)
						    	$("#cycle_days_sunday_{$field->seo_name}").attr('checked', true);
						    formOpen_{$field->seo_name}('edit');
						{rdelim},
						eventMouseover: function(calEvent, jsEvent, view) {ldelim}
							for (var i=0; i<events_array_{$field->seo_name}.length; i++) {ldelim}
								if (events_array_{$field->seo_name}[i].id == calEvent.id) {ldelim}
									var hover_event = events_array_{$field->seo_name}[i];
									break;
								{rdelim}
							{rdelim}

							var hover_content = '';
							var hover_start_date = new Date(hover_event.start_date*1000 + timezone_offset*1000);
							var hover_start_date_formatted = $.datepicker.formatDate(date_format, hover_start_date);
							var hover_end_date = new Date(hover_event.end_date*1000 + timezone_offset*1000);
							var hover_end_date_formatted = $.datepicker.formatDate(date_format, hover_end_date);

							{if $enable_time}
							var hover_start_time_formatted = $.fullCalendar.formatDate(hover_start_date, "HH:mm");
							var hover_end_time_formatted = $.fullCalendar.formatDate(hover_end_date, "HH:mm");
							
							if (hover_start_date_formatted == hover_end_date_formatted)
								if (hover_start_time_formatted == hover_end_time_formatted)
									hover_content = hover_start_date_formatted+' '+hover_start_time_formatted;
								else
									hover_content = hover_start_date_formatted+' | '+hover_start_time_formatted+' - '+hover_end_time_formatted;
							else
								hover_content = hover_start_date_formatted+' '+hover_start_time_formatted+' - '+hover_end_date_formatted+' '+hover_end_time_formatted;
							{else}
							if (hover_start_date_formatted == hover_end_date_formatted)
								hover_content = hover_start_date_formatted;
							else
								hover_content = hover_start_date_formatted+' - '+hover_end_date_formatted;
							{/if}

							if (hover_event.description)
								hover_content = hover_content+'<br />'+hover_event.description;

							$(this).append('<div id=\"event_{$field->seo_name}_'+hover_event.id+'\" class=\"fc-event-hover\">'+hover_content+'</div>');
							$(this).css('z-index', 100);
						{rdelim},
						eventMouseout: function(calEvent, jsEvent, view) {ldelim}
							$('#event_{$field->seo_name}_'+calEvent.id).remove();
							$(this).css('z-index', 8);
						{rdelim},
						timeFormat: 'H:mm'

						{if $current_language && $current_language != 'en'} ,
						isRTL:  $.datepicker.regional["{$current_language}"].isRTL,
						firstDay: $.datepicker.regional["{$current_language}"].firstDay,
						monthNames: $.datepicker.regional["{$current_language}"].monthNames,
						monthNamesShort: $.datepicker.regional["{$current_language}"].monthNamesShort,
						dayNames: $.datepicker.regional["{$current_language}"].dayNames,
						dayNamesShort: $.datepicker.regional["{$current_language}"].dayNamesShort,
						buttonText: {ldelim}
							today: $.datepicker.regional["{$current_language}"].currentText
						{rdelim}
						{/if}
					{rdelim});

					$("#add_event_button_{$field->seo_name}").click(function() {ldelim}
						formOpen_{$field->seo_name}("add");
						form_{$field->seo_name}.dialog("open");
						return false;
					{rdelim});

					function emptyForm_{$field->seo_name}() {ldelim}
					    event_start_{$field->seo_name}.val("");
					    event_start_tmstmp_{$field->seo_name} = null;
					    event_end_{$field->seo_name}.val("");
					    event_end_tmstmp_{$field->seo_name} = null;
					    event_title_{$field->seo_name}.val("");
					    event_description_{$field->seo_name}.val("");
					    $(".cycle_days_{$field->seo_name}").attr('checked', false);
					    $("#cycle_days_everyday_{$field->seo_name}").attr('checked', false);
					{rdelim}

					function formOpen_{$field->seo_name}(mode) {ldelim}
					    if(mode == 'add') {ldelim}
					        $('#add_{$field->seo_name}').show();
					        $('#edit_{$field->seo_name}').hide();
					        $('#delete_{$field->seo_name}').hide();
					    {rdelim}
					    else if(mode == 'edit') {ldelim}
					        $('#edit_{$field->seo_name}').show();
					        $('#delete_{$field->seo_name}').show();
					        $('#add_{$field->seo_name}').hide();
					    {rdelim}
					    form_{$field->seo_name}.dialog('open');
					{rdelim}

					function manageEvent_{$field->seo_name}(event_id, load_from_form) {ldelim}
						if (!load_from_form) {ldelim}
							for (var i=0; i<events_array_{$field->seo_name}.length; i++) {ldelim}
								if (events_array_{$field->seo_name}[i].id == event_id) {ldelim}
									load_event = events_array_{$field->seo_name}[i];
									break;
								{rdelim}
							{rdelim}
							var title = load_event.title;
							var description = load_event.description;
							var start_date = load_event.start_date;
							var end_date = load_event.end_date;
							var rmonday = load_event.cycle_days_monday;
							var rtuesday = load_event.cycle_days_tuesday;
							var rwednesday = load_event.cycle_days_wednesday;
							var rthursday = load_event.cycle_days_thursday;
							var rfriday = load_event.cycle_days_friday;
							var rsaturday = load_event.cycle_days_saturday;
							var rsunday = load_event.cycle_days_sunday;
						{rdelim} else {ldelim}
							var title = event_title_{$field->seo_name}.val();
							var description = event_description_{$field->seo_name}.val();

							if (event_start_tmstmp_{$field->seo_name} && event_end_tmstmp_{$field->seo_name}) {ldelim}
								var start_date = event_start_tmstmp_{$field->seo_name}/1000 - timezone_offset;
								var end_date = event_end_tmstmp_{$field->seo_name}/1000 - timezone_offset;
							{rdelim} else
								return false;

							var rmonday = $("#cycle_days_monday_{$field->seo_name}").is(":checked");
							var rtuesday = $("#cycle_days_tuesday_{$field->seo_name}").is(":checked");
							var rwednesday = $("#cycle_days_wednesday_{$field->seo_name}").is(":checked");
							var rthursday = $("#cycle_days_thursday_{$field->seo_name}").is(":checked");
							var rfriday = $("#cycle_days_friday_{$field->seo_name}").is(":checked");
							var rsaturday = $("#cycle_days_saturday_{$field->seo_name}").is(":checked");
							var rsunday = $("#cycle_days_sunday_{$field->seo_name}").is(":checked");
						{rdelim}

						var valid_event = false;
						
						var new_event = {ldelim}
							id: event_id,
							title: title,
							description: description,
							start_date: start_date,
							end_date: end_date,
							cycle_days_monday: rmonday,
							cycle_days_tuesday: rtuesday,
							cycle_days_wednesday: rwednesday,
							cycle_days_thursday: rthursday,
							cycle_days_friday: rfriday,
							cycle_days_saturday: rsaturday,
							cycle_days_sunday: rsunday,
							allDay: {if $enable_time}false{else}true{/if}
						{rdelim};

						var start_tmstmp = start_date;
						var end_tmstmp = end_date;
						if (rmonday || rtuesday || rwednesday || rthursday || rfriday || rsaturday || rsunday) {ldelim}
							var r_event = jQuery.extend(true, {ldelim}{rdelim}, new_event);
							while (start_tmstmp <= end_tmstmp) {ldelim}
								var r_date = new Date(start_tmstmp*1000 + timezone_offset*1000).getDay()
								if (rmonday && r_date == 1) {ldelim}
									r_event.start_date = start_tmstmp;
									r_event.end_date = start_tmstmp;
									r_event.start = start_tmstmp;
									r_event.end = start_tmstmp;
									calendar_{$field->seo_name}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
									valid_event = true;
								{rdelim}
								if (rtuesday && r_date == 2) {ldelim}
									r_event.start_date = start_tmstmp;
									r_event.end_date = start_tmstmp;
									r_event.start = start_tmstmp;
									r_event.end = start_tmstmp;
									calendar_{$field->seo_name}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
									valid_event = true;
								{rdelim}
								if (rwednesday && r_date == 3) {ldelim}
									r_event.start_date = start_tmstmp;
									r_event.end_date = start_tmstmp;
									r_event.start = start_tmstmp;
									r_event.end = start_tmstmp;
									calendar_{$field->seo_name}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
									valid_event = true;
								{rdelim}
								if (rthursday && r_date == 4) {ldelim}
									r_event.start_date = start_tmstmp;
									r_event.end_date = start_tmstmp;
									r_event.start = start_tmstmp;
									r_event.end = start_tmstmp;
									calendar_{$field->seo_name}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
									valid_event = true;
								{rdelim}
								if (rfriday && r_date == 5) {ldelim}
									r_event.start_date = start_tmstmp;
									r_event.end_date = start_tmstmp;
									r_event.start = start_tmstmp;
									r_event.end = start_tmstmp;
									calendar_{$field->seo_name}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
									valid_event = true;
								{rdelim}
								if (rsaturday && r_date == 6) {ldelim}
									r_event.start_date = start_tmstmp;
									r_event.end_date = start_tmstmp;
									r_event.start = start_tmstmp;
									r_event.end = start_tmstmp;
									calendar_{$field->seo_name}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
									valid_event = true;
								{rdelim}
								if (rsunday && r_date == 0) {ldelim}
									r_event.start_date = start_tmstmp;
									r_event.end_date = start_tmstmp;
									r_event.start = start_tmstmp;
									r_event.end = start_tmstmp;
									calendar_{$field->seo_name}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
									valid_event = true;
								{rdelim}
								start_tmstmp = start_tmstmp + 86400;
							{rdelim}
						{rdelim} else {ldelim}
							if (start_tmstmp <= end_tmstmp) {ldelim}
								new_event.start = new_event.start_date;
								new_event.end = new_event.end_date;
								calendar_{$field->seo_name}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, new_event), true);
								valid_event = true;
							{rdelim}
						{rdelim}

						if (valid_event) {ldelim}
							var edit = false;
							for (var i=0; i<events_array_{$field->seo_name}.length; i++) {ldelim}
								if (events_array_{$field->seo_name}[i].id == event_id) {ldelim}
									events_array_{$field->seo_name}[i] = new_event;
									edit = true;
									break;
								{rdelim}
							{rdelim}
							if (!edit)
								events_array_{$field->seo_name}.push(new_event);
						{rdelim}
					{rdelim}
					
					function serializeEvents_{$field->seo_name}() {ldelim}
						seen = [];
						$("#serialized_events_{$field->seo_name}").val(JSON.stringify(events_array_{$field->seo_name}, function(key, val) {ldelim}
							if (typeof val == "object") {ldelim}
								if (seen.indexOf(val) >= 0)
									return;
								seen.push(val);
							{rdelim}
							return val;
						{rdelim}));
					{rdelim}

					form_{$field->seo_name}.dialog({ldelim}
						autoOpen: false,
						modal: true,
						width: 420,
						open: function(event, ui) {ldelim}
							$('.ui-widget-overlay').live('click', function() {ldelim}
								form_{$field->seo_name}.dialog('close');
							{rdelim});

							{if $enable_time}
							event_start_{$field->seo_name}.datetimepicker({ldelim}
								hourGrid: 4,
								minuteGrid: 10,
								timeText: 'Время',
								hourText: 'Часы',
								minuteText: 'Минуты',
								{if $current_language && $current_language != 'en'}
								isRTL: $.datepicker.regional["{$current_language}"].isRTL,
								{/if}
							{else}
							event_start_{$field->seo_name}.datepicker({ldelim}
							{/if}
								showOn: "both",
								buttonImage: "{$public_path}images/calendar.png",
								buttonImageOnly: true,
								showButtonPanel: true,
								onSelect: function(dateText) {ldelim}
									{if $enable_time}
	                            	event_start_tmstmp_{$field->seo_name} = $.datepicker.formatDate('@', event_start_{$field->seo_name}.datetimepicker("getDate"));
	                            	event_end_{$field->seo_name}.datetimepicker('option', 'minDate', event_start_{$field->seo_name}.datetimepicker('getDate'));
	                            	{else}
	                            	event_start_tmstmp_{$field->seo_name} = $.datepicker.formatDate('@', event_start_{$field->seo_name}.datepicker("getDate"));
	                            	event_end_{$field->seo_name}.datepicker('option', 'minDate', event_start_{$field->seo_name}.datepicker('getDate'));
	                            	{/if}
	                        	{rdelim}
							{rdelim});
							{if $enable_time}
							event_start_{$field->seo_name}.datetimepicker('setDate', new Date(parseInt(event_start_tmstmp_{$field->seo_name})));
							event_end_{$field->seo_name}.datetimepicker('option', 'minDate', event_start_{$field->seo_name}.datetimepicker('getDate'));
							{else}
							event_start_{$field->seo_name}.datepicker('setDate', new Date(parseInt(event_start_tmstmp_{$field->seo_name})));
							event_end_{$field->seo_name}.datepicker('option', 'minDate', event_start_{$field->seo_name}.datepicker('getDate'));
							{/if}
							{if $current_language && $current_language != 'en'}
							event_start_{$field->seo_name}.datepicker("option", $.datepicker.regional["{$current_language}"]);
							{/if}

							{if $enable_time}
							event_end_{$field->seo_name}.datetimepicker({ldelim}
								hourGrid: 4,
								minuteGrid: 10,
								timeText: 'Время',
								hourText: 'Часы',
								minuteText: 'Минуты',
								{if $current_language && $current_language != 'en'}
								isRTL: $.datepicker.regional["{$current_language}"].isRTL,
								{/if}
							{else}
							event_end_{$field->seo_name}.datepicker({ldelim}
							{/if}
								showOn: "both",
								buttonImage: "{$public_path}images/calendar.png",
								buttonImageOnly: true,
								showButtonPanel: true,
								onSelect: function(dateText) {ldelim}
									{if $enable_time}
	                            	event_end_tmstmp_{$field->seo_name} = $.datepicker.formatDate('@', event_end_{$field->seo_name}.datetimepicker("getDate"));
	                            	event_start_{$field->seo_name}.datetimepicker('option', 'maxDate', event_end_{$field->seo_name}.datetimepicker('getDate'));
	                            	{else}
	                            	event_end_tmstmp_{$field->seo_name} = $.datepicker.formatDate('@', event_end_{$field->seo_name}.datepicker("getDate"));
	                            	event_start_{$field->seo_name}.datepicker('option', 'maxDate', event_end_{$field->seo_name}.datepicker('getDate'));
	                            	{/if}
	                        	{rdelim}
							{rdelim});
							{if $enable_time}
							event_end_{$field->seo_name}.datetimepicker('setDate', new Date(parseInt(event_end_tmstmp_{$field->seo_name})));
							event_start_{$field->seo_name}.datetimepicker('option', 'maxDate', event_end_{$field->seo_name}.datetimepicker('getDate'));
							{else}
							event_end_{$field->seo_name}.datepicker('setDate', new Date(parseInt(event_end_tmstmp_{$field->seo_name})));
							event_start_{$field->seo_name}.datepicker('option', 'maxDate', event_end_{$field->seo_name}.datepicker('getDate'));
							{/if}
							{if $current_language && $current_language != 'en'}
							event_end_{$field->seo_name}.datepicker("option", $.datepicker.regional["{$current_language}"]);
							{/if}
						{rdelim},
						buttons: [
							{ldelim}
								id: 'add_{$field->seo_name}',
								text: 'Добавить',
								class: 'button',
								click: function() {ldelim}
									var new_event_id = new Date().getTime();
									manageEvent_{$field->seo_name}(new_event_id, true);
									serializeEvents_{$field->seo_name}();
									form_{$field->seo_name}.dialog('close');
								{rdelim}
							{rdelim},
							{ldelim}
								id: 'edit_{$field->seo_name}',
								text: 'Изменить',
								class: 'button',
								click: function() {ldelim}
									if (selected_event_id) {ldelim}
										calendar_{$field->seo_name}.fullCalendar('removeEvents', selected_event_id);
										manageEvent_{$field->seo_name}(selected_event_id, true);
										serializeEvents_{$field->seo_name}();
									{rdelim}
									$(this).dialog('close');
								{rdelim}
							{rdelim},
							{ldelim}
								id: 'cancel_{$field->seo_name}',
								text: 'Отмена',
								class: 'button',
								click: function() {ldelim}
									$(this).dialog('close');
								{rdelim}
							{rdelim},
							{ldelim}
								id: 'delete_{$field->seo_name}',
								text: 'Удалить',
								class: 'button',
								click: function() {ldelim}
									if (selected_event_id) {ldelim}
										calendar_{$field->seo_name}.fullCalendar('removeEvents', selected_event_id);
										events_array_{$field->seo_name}.splice(selected_event_key, 1);
										serializeEvents_{$field->seo_name}();
									{rdelim}
									$(this).dialog('close');
								{rdelim}
							{rdelim}
						],
						close: function(event, ui) {ldelim}
							emptyForm_{$field->seo_name}();
						{rdelim}
					{rdelim});
					
					$("#cycle_days_everyday_{$field->seo_name}").change( function() {ldelim}
						if ($("#cycle_days_everyday_{$field->seo_name}").is(":checked")) {ldelim}
							$(".cycle_days_{$field->seo_name}").each( function() {ldelim}
								$(this).attr('checked', true);
							{rdelim});
						{rdelim} else {ldelim}
							$(".cycle_days_{$field->seo_name}").each( function() {ldelim}
								$(this).attr('checked', false);
							{rdelim});
						{rdelim}
					{rdelim});
					$(".cycle_days_{$field->seo_name}").change( function() {ldelim}
						if (!$(this).is(":checked")) {ldelim}
							$("#cycle_days_everyday_{$field->seo_name}").attr('checked', false);
						{rdelim}
					{rdelim});
					
					{foreach from=$field->value item=event}
					manageEvent_{$field->seo_name}({$event.id}, false);
					{/foreach}
					serializeEvents_{$field->seo_name}();
				{rdelim});
			</script>
			<div id="fullcalendar_{$field->seo_name}"></div>
			<input type="hidden" id="serialized_events_{$field->seo_name}" name="serialized_events_{$field->seo_name}" />

			<br />
			<input type="button" id="add_event_button_{$field->seo_name}" class="button" value="Добавить событие" />
			<div id="dialog-form_{$field->seo_name}" title="Событие">
				<div>
					<p>
						<label for="event_title">{$LANG_TITLE}</label>
						<input type="text" id="event_title_{$field->seo_name}" name="event_title" size="55" value="">
					</p>
					<p>
						<label for="event_description">{$LANG_DESCRIPTION_TH}</label>
						<textarea id="event_description_{$field->seo_name}" name="event_description" rows="3" cols="55"></textarea>
					</p>
				</div>
				<div class="admin_option_first_column">
					<b>1. {$LANG_SELECT_DATES_RANGE}:</b><br />
					<p>
						<label for="event_start">{$LANG_FROM_DATERANGE}<span class="red_asterisk">*</span></label>
						<input type="text" name="event_start" id="event_start_{$field->seo_name}"/>
					</p>
					<p>
						<label for="event_end">{$LANG_TO_DATERANGE}<span class="red_asterisk">*</span></label>
						<input type="text" name="event_end" id="event_end_{$field->seo_name}"/>
					</p>
				</div>
				<div class="admin_option_second_column" style="margin-left: 20px">
					<b>2. {$LANG_SELECT_CYCLIC_DATES}:</b><br />
					<label><input type=checkbox id="cycle_days_everyday_{$field->seo_name}" value="1" {if $cycle_days_monday && $cycle_days_tuesday && $cycle_days_wednesday && $cycle_days_friday && $cycle_days_saturday && $cycle_days_sunday}checked{/if} /> {$LANG_EVERY_DAY}</label>
					<label><input type=checkbox id="cycle_days_monday_{$field->seo_name}" name="cycle_days_monday_{$field->seo_name}" value="1" class="cycle_days_{$field->seo_name}" {if $cycle_days_monday}checked{/if} /> {$LANG_EVERY_MONDAY}</label>
					<label><input type=checkbox id="cycle_days_tuesday_{$field->seo_name}" name="cycle_days_tuesday_{$field->seo_name}" value="1" class="cycle_days_{$field->seo_name}" {if $cycle_days_tuesday}checked{/if}/> {$LANG_EVERY_TUESDAY}</label>
					<label><input type=checkbox id="cycle_days_wednesday_{$field->seo_name}" name="cycle_days_wednesday_{$field->seo_name}" value="1" class="cycle_days_{$field->seo_name}" {if $cycle_days_wednesday}checked{/if} /> {$LANG_EVERY_WEDNESDAY}</label>
					<label><input type=checkbox id="cycle_days_thursday_{$field->seo_name}" name="cycle_days_thursday_{$field->seo_name}" value="1" class="cycle_days_{$field->seo_name}" {if $cycle_days_thursday}checked{/if} /> {$LANG_EVERY_THURSDAY}</label>
					<label><input type=checkbox id="cycle_days_friday_{$field->seo_name}" name="cycle_days_friday_{$field->seo_name}" value="1" class="cycle_days_{$field->seo_name}" {if $cycle_days_friday}checked{/if} /> {$LANG_EVERY_FRIDAY}</label>
					<label><input type=checkbox id="cycle_days_saturday_{$field->seo_name}" name="cycle_days_saturday_{$field->seo_name}" value="1" class="cycle_days_{$field->seo_name}" {if $cycle_days_saturday}checked{/if} /> {$LANG_EVERY_SATURDAY}</label>
					<label><input type=checkbox id="cycle_days_sunday_{$field->seo_name}" name="cycle_days_sunday_{$field->seo_name}" value="1" class="cycle_days_{$field->seo_name}" {if $cycle_days_sunday}checked{/if} /> {$LANG_EVERY_SUNDAY}</label>
				</div>
			</div>
			
			<br />
			<br />