{if $current_listing}{assign var="frontend_mode" value="both"}{else}{assign var="frontend_mode" value=$field->frontend_mode}{/if}

					{if $field_value && $field_currency}
					<div class="content_field_output field_type_{$field->type}  field_{$field->seo_name}">
						<nobr>
							{if $frontend_mode == 'name'}
								<strong>{$field->name}</strong>:<br />
							{elseif $frontend_mode == 'frontend_name' && ($field->frontend_name || $field->name)}
								<strong>{$field->frontend_name}</strong>:<br />
							{elseif $frontend_mode == 'icon' && $field->field_icon_image}
								<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
							{elseif $frontend_mode == 'both'}
								{if ($field->frontend_name || $field->name) && $field->field_icon_image}
								<strong><img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> {if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}</strong>:<br />
								{elseif $field->field_icon_image}
								<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" />
								{else}
								<strong>{if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}</strong>:<br />
								{/if}
							{/if}
							{$field_currency} {$VH->number_format($field_value, 2, $decimals_separator, $thousands_separator)}
						</nobr>
					</div>
					{/if}