<script language="JavaScript" type="text/javascript">
	$(document).ready(function() {ldelim}
		$("#page_num").change(function() {ldelim}
			var url = "{$link}page/"+$(this).val();
			$("#select_page_form").attr("action", url);
			$("#select_page_form").submit();
		{rdelim});
	{rdelim});
</script>
<form id="select_page_form" method="get">
	{$LANG_PAGINATION_GOTO_PAGE}:
	<select id="page_num">
	{section name=pagination_pager start=1 loop=$pages_num+1}
		{if $i >= $pages_num}
	    	{php}break;{/php}
	    {else}
	    	{if $smarty.section.pagination_pager.index <= $multiplier}
	    		{assign var=i value=$smarty.section.pagination_pager.index}
	    	{else}
	    		{assign var=i value=$i+$multiplier}
	    	{/if}
	    	{if $i > $pages_num}
				{assign var=i value=$pages_num}
			{/if}
	    {/if}
	    {if $i == $curr_page}
	    	{assign var=selected value='selected'}
	    {else}
	    	{assign var=selected value=''}
	    {/if}
	    <option value="{$i}" {$selected}>{$i}</option>
	{/section}
	</select>
</form>