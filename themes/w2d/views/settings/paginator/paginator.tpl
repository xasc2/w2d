<div id="paginator">
	<span class="paginator_list">
	{if $curr_page_line > 1}
		<a href="{$prev_line_link}" class="prev_line" title="{$LANG_PAGINATOR_PREV_LINE_TITLE}">{$LANG_PAGINATOR_PREV_LINE}</a>
	{/if}
	{if $curr_page > 1}
		<a href="{$prev_link}" class="prev_page" title="{$LANG_PAGINATOR_PREV_PAGE_TITLE}">{$LANG_PAGINATOR_PREV_PAGE}</a>
	{/if}

	{section name=pagination start=$start_page loop=$end_page+1}
		{if $smarty.section.pagination.index != $curr_page}
			{if $smarty.section.pagination.index != 1}
			<a href="{$link}page/{$smarty.section.pagination.index}/">{$smarty.section.pagination.index}</a>
			{else}
			<a href="{$link}">{$smarty.section.pagination.index}</a>
			{/if}
		{else}
			<strong class="current_page">{$smarty.section.pagination.index}</strong>
		{/if}
	{/section}

	{if $curr_page != $pages_num}
		<a href="{$next_link}" class="next_page" title="{$LANG_PAGINATOR_NEXT_PAGE_TITLE}">{$LANG_PAGINATOR_NEXT_PAGE}</a>
	{/if}
	{if $curr_page_line != $pages_lines_num}
		<a href="{$next_line_link}" class="next_line" title="{$LANG_PAGINATOR_NEXT_LINE_TITLE}">{$LANG_PAGINATOR_NEXT_LINE}</a>
	{/if}
	</span>
</div>