{include file="backend/admin_header.tpl"}

                <div class="content">
                     <h3 style="height: 100%;">{$LANG_MANAGE_CATEGORIES_BY_TYPE} "{$type->name}"</h3>
                     <h4 style="height: 100%;">{$LANG_EDIT_CATEGORIES_DESCR}</h4>
                     
                     <a href="{$VH->site_url("admin/categories/by_type/$type_id/create")}" title="{$LANG_NEW_CATEGORY_OPTION}"><img src="{$public_path}/images/buttons/page_add.png" /></a>
                     <a href="{$VH->site_url("admin/categories/by_type/$type_id/create")}">{$LANG_NEW_CATEGORY_OPTION}</a>&nbsp;&nbsp;&nbsp;
                     <br />
                     <br />

                     {render_frontend_block
						block_type='categories'
						block_template='backend/blocks/admin_categories_management.tpl'
						type=$type_id
						is_counter=false
						max_depth='max'
					 }

                     <br/>
                     <form action="" method="post">
                     <select name="copy_categories_from">
                     	<option value="-1">{$LANG_COPY_CATEGORIES_TREE}</option>
                     	<option value="0">{$LANG_GLOBAL_CATEGORIES}</option>
	                    {foreach from=$local_categories_types item=local_type}
	                    	{if $local_type.id != $type->id}
	                    	<option value="{$local_type.id}">{$LANG_LOCAL_CATEGORIES_TYPE} "{$local_type.name}"</option>
	                    	{/if}
	                    {/foreach}
                     </select>
                     <br/>
                     <br/>
                     <input type="submit" class="button copy_button" name="copy" value="{$LANG_COPY_CATEGORIES_BUTTON}">
                     </form>
                </div>

{include file="backend/admin_footer.tpl"}