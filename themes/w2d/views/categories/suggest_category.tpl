<div class="email_form">
	<script type="text/javascript">
		$(document).ready(function() {ldelim}
			$("#submit").click(function() {ldelim}
				$.ajax({ldelim}
					type: "POST",
					url: "{$sender_url}",
					dataType: "html",
					data: $("#email_form").serialize()+"&submit=submit",
					beforeSend: function() { ldelim}
						ajax_loader_show();
					{rdelim},
					success: function(data) {ldelim}
						ajax_loader_hide();
						$(".email_form").html(data);
						close_all_msgs();
						show_msg();
					{rdelim}
				{rdelim});
			{rdelim});
		{rdelim});
	</script>

	{$VH->buildMessagesBlock()}
	{$VH->validation_errors()}
	<form id="email_form" action="{$sender_url}" method="post">
	<b>{$LANG_SUGGEST_CATEGORY}</b><span class="red_asterisk">*</span>
	<div class="px2"></div>
	<i>{$LANG_SUGGEST_CATEGORY_DESCR}</i>
	<div class="px2"></div>
	<input type="text" name="suggested_category" value="{$suggested_category}" style="width:98%">
	<div class="px5"></div>
	<b>{$LANG_FILL_CAPTCHA}</b><span class="red_asterisk">*</span>
	<div class="px2"></div>
	<input type="text" name="captcha" size="4">
	<div class="px3"></div>
	{$captcha->view()}
	<div class="px5"></div>
	<input type="button" id="submit" class="button" name="submit" value="{$LANG_BUTTON_SEND}">&nbsp;&nbsp;&nbsp;<input type="button" class="nyroModalClose button" name="close" value="{$LANG_BUTTON_CLOSE}">
	</form>
</div>