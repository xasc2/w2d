{include file="backend/admin_header.tpl"}

                <div class="content">
                     <h3>{$LANG_TYPES_EXPIRATION_SETTINGS}</h3>
                     {if $types|@count}
                     <form action="" method="post">
                     <table class="presentationTable" border="0" cellpadding="2" cellspacing="2">
                       <tr>
                         <th>{$LANG_TYPE_TH}</th>
                         <th>{$LANG_LEVEL_TH} \ {$LANG_OPTIONS_TH}</th>
                         <td class="td_header">{$LANG_TYPES_EXPIRATION_SETTING_EXPIRED_QUESTION}</td>
                         <td class="td_header">{$LANG_TYPES_EXPIRATION_SETTING_CLAIMED_QUESTION}</td>
                       </tr>
                       {foreach from=$types item=type}
                       <tr>
                         <td class="td_header">
                         	{$type->name}
                         </td>
                         <td class="td_header">
                         	{if $type->levels|@count > 0}
                         	<table width="100%" border="0" cellpadding="0" cellspacing="0">
                         		{foreach from=$type->levels item=level}
                         		<tr>
                         			<td width="100%" style="border: 0;">
                         			{$level->name}
                         			</td>
                         		</tr>
                         		{/foreach}
                         	</table>
                         	{else}
                         	&nbsp;
                         	{/if}
                         </td>
                         <td>
                            {if $type->levels|@count > 1}
                         	<table width="100%" border="0" cellpadding="0" cellspacing="0">
                         		{foreach from=$type->levels item=level}
                         		{assign var="level_id" value=$level->id}
                         		{if $level->active_years || $level->active_months || $level->active_days}
                         		<tr>
                         			<td style="border: 0;">
                         				<select name="after_listings_expiration_{$level_id}">
                         				<option value=0>{$LANG_TYPES_EXPIRATION_SETTING_SUSPEND}</option>
                         				{foreach from=$type->levels item=level_in_select}
                         				{if $level_in_select->id != $level_id}
                         				<option value={$level_in_select->id} {if $level->after_listings_expiration == $level_in_select->id}selected{/if}>{$level_in_select->name}</option>
                         				{/if}
                         				{/foreach}
                         				</select>
                         			</td>
                         		</tr>
                         		{else}
                         		<tr>
                         			<td style="border: 0;">
                         				<span class="green">{$LANG_ETERNAL}</span>
                         			</td>
                         		</tr>
                         		{/if}
                         		{/foreach}
                         	</table>
                         	{else}
                         	&nbsp;
                         	{/if}
                         </td>
                         <td>
                            {if $type->levels|@count > 1}
                         	<table width="100%" border="0" cellpadding="0" cellspacing="0">
                         		{foreach from=$type->levels item=level}
                         		{assign var="level_id" value=$level->id}
                         		<tr>
                         			<td  style="border: 0;">
                         				<select name="after_listings_claim_{$level_id}">
                         				<option value=0>{$LANG_TYPES_EXPIRATION_SETTING_CLAIM}</option>
                         				{foreach from=$type->levels item=level_in_select}
                         				{if $level_in_select->id != $level_id}
                         				<option value={$level_in_select->id} {if $level->after_listings_claim == $level_in_select->id}selected{/if}>{$level_in_select->name}</option>
                         				{/if}
                         				{/foreach}
                         				</select>
                         			</td>
                         		</tr>
                         		{/foreach}
                         	</table>
                         	{else}
                         	&nbsp;
                         	{/if}
                         </td>
                       </tr>
                       {/foreach}
                     </table>
                     <br />
                     <input class="button save_button" type=submit name="submit" value="{$LANG_BUTTON_SAVE_CHANGES}">
                     </form>
                     {/if}
                </div>

{include file="backend/admin_footer.tpl"}