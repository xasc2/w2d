	<span class="orderby">{$LANG_SEARCH_ORDER_BY}:</span>&nbsp;
	{asc_desc_insert base_url=$order_url orderby='l.order_date' orderby_query=$orderby direction=$direction title=$LANG_SEARCH_ORDER_DATE}
	|
	{asc_desc_insert base_url=$order_url orderby='l.title' orderby_query=$orderby direction=$direction title=$LANG_SEARCH_LISTING_TITLE default_direction='asc'}

	{if $orderby_rating}
	|
	{asc_desc_insert base_url=$order_url orderby='rating' orderby_query=$orderby direction=$direction title=$LANG_SEARCH_RATING}
	{/if}
	
	{if $orderby_distance}
	|
	{asc_desc_insert base_url=$order_url orderby='distance' orderby_query=$orderby direction=$direction title=$LANG_SEARCH_DISTANCE default_direction='asc'}
	{/if}

	{foreach from=$content_fields key=field item=seo_name}
		{assign var=field_object value=$VH->unserialize($field)}
		{assign var=field_orderby value=$field_object->field->seo_name}
		{if $field_object->field->frontend_name}{assign var=field_name value=$field_object->field->frontend_name}{else}{assign var=field_name value=$field_object->field->name}{/if}
	
		{if $field_object->field->orderby_enabled && $order_url !== null}
		|
		{asc_desc_insert base_url=$order_url orderby="cf."|cat:$field_orderby orderby_query=$orderby direction=$direction title=$field_name default_direction='desc'}
		{/if}
	{/foreach}