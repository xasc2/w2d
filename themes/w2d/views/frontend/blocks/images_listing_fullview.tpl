{assign var="columns_num" value=$columns_num}

{if $items_array|@count}
						<div class="listing_images_gallery">
							<table>
								<tr>
								{assign var="i" value=0}
								{foreach from=$items_array item=image}
									{assign var="i" value=$i+1}
									<td align="center" valign="middle">
										<a href="{$VH->site_url($listing_url)}#images_tab" title="{$image->title}"><img src="{$users_content}/users_images/thmbs_small/{$image->file}"/></a>
									</td>
									{if $i >= $columns_num}
									</tr><tr>
									{assign var="i" value=0}
									{/if}
								{/foreach}
								</tr>
							</table>
						</div>
{/if}