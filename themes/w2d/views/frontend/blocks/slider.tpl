{if $items_array|@count}
<script language="JavaScript" type="text/javascript">
$(function() {ldelim}
	if ($(window).width() > 1024)
		var slides_number = 2;
	else
		var slides_number = 1;

	$('#slider').anythingSlider({ldelim}
		resizeContents: true,
		theme: 'minimalist-round',
		buildNavigation: false,
		buildStartStop: false,
		autoPlay: true,
		showMultiple: slides_number,
		delay: 6000,
		animationTime: 400,
		expand: true
	{rdelim});
	$('#slider').show();
{rdelim});
</script>

<div style="height: {$slider_height}px; padding-bottom: 20px">
	<ul id="slider" style="display: none;">
		{foreach from=$items_array item=listing}
		<li>
			<div style="padding: 5px 25px">
			{if $listing->level->logo_enabled && $listing->logo_file}
			<a title="{$listing->title()}" href="{$VH->site_url($listing->url())}"><img src="{$users_content}/users_images/thmbs/{$listing->logo_file}" alt="{$listing->title()}" style="float: right; margin: 0 0 2px 10px;"></a>
			{/if}
			<h2><a title="{$listing->title()}" href="{$VH->site_url($listing->url())}">{$listing->title()}</a></h2>
			{if $listing->level->ratings_enabled}
				{assign var=avg_rating value=$listing->getRatings()}
				{$avg_rating->setInactive()}
				{$avg_rating->view()}
			{/if}
			<b>{$listing->order_date|date_format:"%D %H:%M"}</b>
	
			{if $listing->level->description_mode == 'richtext'}
			<div>{$listing->listing_description_teaser()|strip_tags|truncate:340}</div>
			{else}
			<div>{$listing->description()|strip_tags|truncate:340}</div>
			{/if}
			</div>
			<div class="clear_float"></div>
		</li>
		{/foreach}
	</ul>
</div>
{/if}