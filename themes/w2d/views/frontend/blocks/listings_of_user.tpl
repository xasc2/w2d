{if $items_array|@count}
	<div class="px10"></div>
	<div class="px10"></div>
	<div class="px10"></div>
	<div class="listings_of_user">
		<h1>{$LANG_LISTINGS_OF_USER_TITLE} {$LANG_IN} "{$search_type->name}"</h1>
		{$wrapper_object->render()}
	</div>
	{if !$nolinks}
	{assign var=search_type value=$search_type->id}
	{if $search_location}{assign var=search_location value="search_location/"|cat:$search_location->seo_name}{/if}
	<a href='{$VH->site_url("search/search_type/$search_type/search_owner/$search_owner/$search_location")}'>{$LANG_VIEW_USERS_LISTING_OF_TYPE} >>></a>
	{/if}
{/if}