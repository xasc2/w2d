{if $items_array|@count}
					<script language="Javascript" type="text/javascript">
						$(document).ready(function() {ldelim}
							$(".img_small").click(function() {ldelim}
								if ($("#listing_logo").find("img").attr("src") != "{$public_path}images/lightbox-ico-loading.gif") {ldelim}
									var img_small = $(this).clone();
									// retrieve basename of image file
									var file_name = img_small.find("img").attr('src').replace(/^.*\/|\.*$/g, '');

									// Set the size of big image and place ajax loader there
									var big_image_width = $("#listing_logo").find("img").width();
									var big_image_height = $("#listing_logo").find("img").height();
									
									$(".lightbox_image").html("<img src='{$public_path}images/lightbox-ico-loading.gif' />");
									$("#listing_logo").css("width", big_image_width);
									$("#listing_logo").css("height", big_image_height);
									$(".lightbox_image").css("position", "relative");
									$(".lightbox_image").css("top", (big_image_height/2)-20);
									$(".lightbox_image").css("left", (big_image_width/2)-20);

									// Remove thmb of logo from lighbox images
									$(".hidden_divs").each(function() {ldelim}
										if (file_name == $(this).find("a").attr('href').replace(/^.*\/|\.*$/g, ''))
											$(this).find("a").removeClass("hidden_imgs");
										else
											$(this).find("a").addClass("hidden_imgs");
									{rdelim});

									// Load new image into big image container
									var img = new Image();
							        $(img).load(function () {ldelim}
							            $(this).hide();
							            img_small.html(this);
							            img_small.removeClass("img_small").addClass("lightbox_image");
							            $("#listing_logo").html(img_small);
							            $("#listing_logo").css("width", img_small.find("img").width());
										$("#listing_logo").css("height", img_small.find("img").height());
							            $(this).fadeIn();
							        {rdelim}).attr('src', '{$users_content}/users_images/thmbs_big/'+file_name);
								{rdelim}
								return false;
							{rdelim});
						{rdelim});
						</script>
{/if}

							<!-- Carousel js gallery -->
							<script type="text/javascript">
								jQuery(document).ready(function() {ldelim}
								    jQuery('#mycarousel').jcarousel({ldelim}visible:4, scroll:2{rdelim});
								{rdelim});
							</script>
							<div class="px10"></div>
							<ul id="mycarousel" class="jcarousel-skin-tango">
								{foreach from=$items_array item=image}
									<li>
										<a href="{$users_content}/users_images/images/{$image->file}" class="img_small" title="{$image->title}"><img src="{$users_content}/users_images/thmbs_small/{$image->file}"/></a>
										<div class="hidden_divs" style="display:none"><a href="{$users_content}/users_images/images/{$image->file}" {if $image->file != $listing->logo_file}class="hidden_imgs"{/if}></a></div>
									</li>
								{/foreach}
							</ul>