{if $items_array|@count}
<script language="JavaScript" type="text/javascript">
$(function() {ldelim}
	$('#wide_slider').anythingSlider({ldelim}
		resizeContents: true,
		theme: 'minimalist-round',
		theme: 'metallic',
		buildNavigation: false,
		buildStartStop: false,
		showMultiple: 3,
		expand: true
	{rdelim});
	$('#wide_slider').show();
{rdelim});
</script>

<div style="height: {$slider_height}px; padding-bottom: 20px">
	<ul id="wide_slider" style="display: none;">
		{foreach from=$items_array item=listing}
		<li>
			<div style="border: 0; margin: 0 5px 0;">
				<table >
					<tr>
						<td valign="middle" align="center">
							<a title="{$listing->title()}" href="{$VH->site_url($listing->url())}">
							<img src="{$users_content}/users_images/thmbs/{$listing->logo_file}" alt="{$listing->title()}" style="margin: 0 0 3px;">
							<br />
							{$listing->title()}
							</a>
						</td>
					</tr>
				</table>
			</div>
		</li>
		{/foreach}
	</ul>
</div>
{/if}