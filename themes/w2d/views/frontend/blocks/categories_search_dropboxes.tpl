
<!-- CATEGORIES BLOCK -->

				{if $categories_tree|@count}
					{assign var="empty_root_option" value='<option value="" selected="selected">- - - '|cat:$LANG_SELECT_CATEGORY|cat:' - - -</option>'}
					{assign var="empty_sub_option" value='<option value="" selected="selected">- - - '|cat:$LANG_SELECT_SUBCATEGORY|cat:' - - -</option>'}
					<script language="javascript" type="text/javascript">
					$(document).ready(function() {ldelim}
						$(".category_dropdown_list").bind("change", function() {ldelim}
							curr_categorylevel_num = parseFloat($(this).attr('categorylevel'));
							prev_categorylevel_num = parseFloat(curr_categorylevel_num) - 1;
							next_categorylevel_num = parseFloat(curr_categorylevel_num) + 1;
							if (curr_categorylevel_num != {$dropboxes_count} && this.options[this.selectedIndex].value != '') {ldelim}
								if(!$.browser.opera)
									// Adding this line will cause a bug in opera
									$(this).parent().parent().find(".category_dropdown_list").attr('disabled', 'disabled');
								$(this).parent().parent().find(".category_dropdown_list").css('background-image', 'url({$public_path}images/ajax-indicator.gif)');
								$(this).parent().parent().find(".category_dropdown_list").css('background-repeat', 'no-repeat');
								$(this).parent().parent().find(".category_dropdown_list").css('background-position', '50% 50%');
								for (i = next_categorylevel_num; i <= {$dropboxes_count}; i++)
									$(this).parent().parent().find(".categorylevel_"+i).html('{$empty_sub_option}');
								$(this).parent().parent().find(".categorylevel_"+next_categorylevel_num).load(
									"{$VH->site_url('ajax/categories_request/frontend/categories/search_dropbox.tpl')}",
									{ldelim}
										id : this.options[this.selectedIndex].value.split("_")[1],
										type_id : '{$type}',
										is_counter : '{$is_counter}',
										max_depth : 'max',
										selected_categories : '{$VH->json_encode($current_category)}',
										highlight_element : 'selected',
										is_children_label : '',
										is_json: false
									{rdelim},
								function(){ldelim}
									$(this).parent().parent().find(".category_dropdown_list").css('background-image','');
									$(this).parent().parent().find(".category_dropdown_list").css('background-repeat','');
									$(this).parent().parent().find(".category_dropdown_list").css('background-position','');
									$(this).parent().parent().find(".categorylevel_"+next_categorylevel_num).prepend('{$empty_sub_option}');
									$(this).parent().parent().find(".categorylevel_"+next_categorylevel_num).val('');
									$(this).parent().parent().find(".category_dropdown_list").removeAttr('disabled');
								{rdelim});
								$("#search_category").val(this.options[this.selectedIndex].value.split("_")[1]);
							{rdelim} else {ldelim}
								for (var i=next_categorylevel_num; i<={$dropboxes_count}; i++) {ldelim}
									$(".categorylevel_"+i).val('');
									$(".categorylevel_"+i).html('{$empty_sub_option}');
								{rdelim}

								if (curr_categorylevel_num == 1 && !$('.categorylevel_'+curr_categorylevel_num).val())
									$("#search_category").val('');
								else
									if (curr_categorylevel_num == {$dropboxes_count} && $('.categorylevel_'+curr_categorylevel_num).val() != '')
										$("#search_category").val($('.categorylevel_'+curr_categorylevel_num).val().split("_")[1]);
									else if ($('.categorylevel_'+curr_categorylevel_num).val() == '')
										if ($('.categorylevel_'+prev_categorylevel_num).length)
											$("#search_category").val($('.categorylevel_'+prev_categorylevel_num).val().split("_")[1]);
							{rdelim}
						{rdelim});
					{rdelim});
					</script>

					{include file='frontend/categories/search_dropbox.tpl' assign=template}
					{section name=foo loop=$dropboxes_count}
						{assign var=curr_chain_id_num value=$smarty.section.foo.iteration-2}
						{assign var=next_chain_id_num value=$smarty.section.foo.iteration-1}
						{if $current_category && $smarty.section.foo.iteration > 1}
						<script language="javascript" type="text/javascript">
						$(document).ready(function() {ldelim}
							var next_categorylevel_num = {$smarty.section.foo.iteration};
							if(!$.browser.opera)
								// Adding this line will cause a bug in opera
								$(".categorylevel_"+next_categorylevel_num).attr('disabled', 'disabled');
							$(".categorylevel_"+next_categorylevel_num).css('background-image', 'url({$public_path}images/ajax-indicator.gif)');
							$(".categorylevel_"+next_categorylevel_num).css('background-repeat', 'no-repeat');
							$(".categorylevel_"+next_categorylevel_num).css('background-position', '50% 50%');
							$(".categorylevel_"+next_categorylevel_num).load(
									"{$VH->site_url('ajax/categories_request/frontend/categories/search_dropbox.tpl')}",
									{ldelim}
										id : {$current_category->getChainByIds($curr_chain_id_num)},
										type_id : '{$type}',
										is_counter : '{$is_counter}',
										max_depth : 'max',
										{if $current_category->getChainByIds($next_chain_id_num)}
											selected_categories : {$current_category->getChainByIds($next_chain_id_num)},
										{/if}
										highlight_element : 'selected',
										is_children_label : '',
										is_json: false
									{rdelim},
								function(){ldelim}
									$(this).parent().parent().find(".category_dropdown_list").css('background-image','');
									$(this).parent().parent().find(".category_dropdown_list").css('background-repeat','');
									$(this).parent().parent().find(".category_dropdown_list").css('background-position','');
									$(this).parent().parent().find(".category_dropdown_list").removeAttr('disabled');
									$(this).parent().parent().find(".categorylevel_"+next_categorylevel_num).prepend('{$empty_sub_option}');
									$(this).parent().parent().find(".categorylevel_"+next_categorylevel_num).val('');
									$(".categorylevel_"+next_categorylevel_num).val('search_{$current_category->getChainByIds($next_chain_id_num)}');
								{rdelim});
						{rdelim});
						</script>
						{/if}
						<select class="category_dropdown_list categorylevel_{$smarty.section.foo.iteration}" categorylevel="{$smarty.section.foo.iteration}" style="width:100%">
						{if $smarty.section.foo.iteration == 1}
							{$empty_root_option}
						{else}
							{$empty_sub_option}
						{/if}
						{if $smarty.section.foo.iteration==1}
							{foreach from=$categories_tree item=category}
								{if $current_category}
									{$category->render($template, $is_counter, 1, $current_category->getChainByIds(0), 'selected', '')},
								{else}
									{$category->render($template, $is_counter, 1)},
								{/if}
							{/foreach}
						{/if}
						</select>
						<div class="px5"></div>
					{/section}
					<input type="hidden" id="search_category" value="{$current_category->id}">
				{/if}

<!-- /CATEGORIES BLOCK -->
