{if $items_array|@count}
<script language="JavaScript" type="text/javascript">
$(function() {ldelim}
	if ($(window).width() > 1024)
		var slides_number = 4;
	else
		var slides_number = 2;

	$('#slider').anythingSlider({ldelim}
		resizeContents: true,
		theme: 'minimalist-round',
		buildNavigation: false,
		buildStartStop: false,
		autoPlay: false,
		showMultiple: slides_number,
		delay: 6000,
		animationTime: 400,
		expand: true
	{rdelim});
	$('#slider').show();
{rdelim});
</script>

<div style="height: {$slider_height}px; padding-bottom: 20px">
	<ul id="slider" style="display: none;">
		{foreach from=$items_array item=listing}
		<li>
			<div style="border: 0; margin: 0 3px 0;">
				<table >
					<tr>
						<td valign="middle" align="center">
							<a title="{$listing->title()}" href="{$VH->site_url($listing->url())}">
								<div>
									{assign var=price value=$listing->content_fields->getFieldValueAsString('price')}
									{if $price}<div class="price_in_slider">{$price}</div>{/if}

									<img src="{$users_content}/users_images/thmbs/{$listing->logo_file}" alt="{$listing->title()}" style="margin: 0 0 2px;">
								</div>
								{$listing->title()|truncate:60}
							</a>
						</td>
					</tr>
				</table>
			</div>
		</li>
		{/foreach}
	</ul>
</div>
{/if}