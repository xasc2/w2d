{include file="frontend/header.tpl"}

<script language="Javascript" type="text/javascript">
$(document).ready(function() {ldelim}
	$("input:text:visible:first").focus();
{rdelim});
</script>

			<tr>
      			<td id="content_block" style="width: auto;" valign="top">
      				{$VH->validation_errors()}
      				<div id="content_wrapper">
      					<h2>{$LANG_ADVERTISE_STEPS_PROCESS}:</h2>
      					{assign var=step value=1}
      					<div class="adv_step">
      						<div class="adv_circle adv_circle_passed">{$LANG_ADVERTISE_STEP} {$step++}</div>
      						{$LANG_ADVERTISE_STEP_CHOOSE_LEVEL}
      					</div>
      					<div class="adv_line adv_line_passed"></div>
      					<div class="adv_step adv_step_active">
      						<div class="adv_circle adv_circle_active">{$LANG_ADVERTISE_STEP} {$step++}</div>
      						{$LANG_ADVERTISE_STEP_LOGIN}
      					</div>
      					<div class="adv_line adv_line_active"></div>
      					<div class="adv_step">
      						<div class="adv_circle">{$LANG_ADVERTISE_STEP} {$step++}</div>
      						{$LANG_ADVERTISE_STEP_CREATE_LISTING}
      					</div>
      					<div class="adv_line"></div>
      					<div class="adv_step">
      						<div class="adv_circle">{$LANG_ADVERTISE_STEP} {$step++}</div>
      						{$LANG_ADVERTISE_STEP_ADD_IMAGES}
      					</div>
      					{if $pay_invoice_step}
      					<div class="adv_line"></div>
      					<div class="adv_step">
      						<div class="adv_circle">{$LANG_ADVERTISE_STEP} {$step++}</div>
      						{$LANG_ADVERTISE_STEP_INVOICE}
      					</div>
      					{/if}
      					<div class="clear_float"></div>

      					<h1>{$LANG_LOGIN_HEADER}</h1>
						<form action="" method="post">
						<div class="admin_option">
							<div class="admin_option_name">
								{$LANG_LOGIN_EMAIL}<span class="red_asterisk">*</span>
							</div>
							<input type="text" name="login_email" value="{$CI->validation->login_email}" class="login_input" size="45"><br>
						</div>
						<div class="admin_option">
							<div class="admin_option_name">
								{$LANG_LOGIN_PASSWORD}<span class="red_asterisk">*</span>
							</div>
							<input type="password" name="login_password" class="login_input" size="45">
						</div>
						<div class="admin_option">
							<input type="checkbox" name="remember_me"> {$LANG_REMEMBER_ME}
						</div>
						<input type="submit" name="login_button" value="{$LANG_BUTTON_LOGIN}" class="front-btn">
						</form>
						
						<div class="px10"></div>
						<div class="px10"></div>
						<div class="px10"></div>

                        <h1>{$LANG_CREATE_ACCOUNT}</h1>
                         <form action="" method="post">
	                     <div class="admin_option noborder">
	                          <div class="admin_option_name" >
	                          	{$LANG_LOGIN}<span class="red_asterisk">*</span>
	                          </div>
	                          <div class="admin_option_description">
	                          	{$LANG_LOGIN_DESCR}
	                          </div>
	                          <input type=text name="login" value="{$user->login}" size="50" class="admin_option_input">
	                     </div>
	                     <div class="admin_option">
	                          <div class="admin_option_name">
	                          	{$LANG_EMAIL}<span class="red_asterisk">*</span>
	                          </div>
	                          <input type=text name="email" value="{$user->email}" size="50" class="admin_option_input">
	                     </div>
	                     <div class="admin_option">
	                          <div class="admin_option_name">
	                          	{$LANG_PASSWORD}<span class="red_asterisk">*</span>
	                          </div>
	                          <div class="admin_option_description">
	                          	{$LANG_PASSWORD_DESCR}
	                          </div>
	                          <input type=password name="password" size="50" class="admin_option_input">
	                          <div class="admin_option_name">
	                          	{$LANG_PASSWORD_REPEAT}<span class="red_asterisk">*</span>
	                          </div>
	                          <input type=password name="repeat_password" size="50" class="admin_option_input">
	                     </div>

						 <div class="admin_option">
	                          <div class="admin_option_name">
	                          	{$LANG_FILL_CAPTCHA}<span class="red_asterisk">*</span>
	                          </div>
	                          <input type="text" name="captcha" size="4">
	                          <div class="px10"></div>
	                          {if $captcha}
	                          {$captcha->view()}
	                          {else}
	                          {$LANG_USERS_CONTENT_ERROR}
	                          {/if}
						 <div>

						 {if $system_settings.path_to_terms_and_conditions}
						 <div class="admin_option">
	                          <div class="admin_option_name">
	                          	<input type="checkbox" name="terms_agreement" value="1" />&nbsp;&nbsp;{$LANG_TERMS_CONDITIONS_1} <a href="{$VH->site_url($system_settings.path_to_terms_and_conditions)}" target="_blank">{$LANG_TERMS_CONDITIONS_2}</a>
	                          </div>
						 <div>
						 {/if}

						 <div class="px5"></div>
	                     <input class="front-btn" type=submit name="register_button" value="{$LANG_BUTTON_CREATE_ACCOUNT}">
	                     </form>
                 	</div>
				</td>
			</tr>

{include file="frontend/footer.tpl"}