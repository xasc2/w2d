{include file="backend/admin_header.tpl"}
{assign var="node_id" value=$node->id}

                <div class="content">
                	{$VH->validation_errors()}
                     <h3>{if $node_id == 'new'}{$LANG_CREATE_CONTENT_PAGE}{else}{$LANG_EDIT_CONTENT_PAGE} "{$node->title}"{/if}</h3>
                     
                     {if $node_id != 'new'}
                     <a href="{$VH->site_url("admin/pages/create")}" title="{$LANG_CREATE_PAGE}"><img src="{$public_path}/images/buttons/page_add.png" /></a>
                     <a href="{$VH->site_url("admin/pages/create")}">{$LANG_CREATE_PAGE}</a>&nbsp;&nbsp;&nbsp;
                     
                     <a href="{$VH->site_url("admin/pages/preview/$node_id")}" title="{$LANG_VIEW_CONTENT_PAGE}"><img src="{$public_path}/images/buttons/page.png" /></a>
                     <a href="{$VH->site_url("admin/pages/preview/$node_id")}">{$LANG_VIEW_CONTENT_PAGE}</a>&nbsp;&nbsp;&nbsp;
                     
                     <a href="{$VH->site_url("admin/pages/delete/$node_id")}" title="{$LANG_DELETE_CONTENT_PAGE}"><img src="{$public_path}/images/buttons/page_delete.png" /></a>
                     <a href="{$VH->site_url("admin/pages/delete/$node_id")}">{$LANG_DELETE_CONTENT_PAGE}</a>&nbsp;&nbsp;&nbsp;
                     
                     <br />
                     <br />
                     {/if}
                     
                     <form action="" method="post">
                     <div class="admin_option">
                          <div class="admin_option_name" >
                          	{$LANG_PAGE_PUBLIC_URL}<span class="red_asterisk">*</span>
                          </div>
                          <input type=text name="url" value="{$node->url}" size="50" class="admin_option_input">
                     </div>
                     <div class="admin_option">
                          <div class="admin_option_name">
                               {$LANG_PAGE_TITLE}<span class="red_asterisk">*</span>
                               {translate_content table='content_pages' field='title' row_id=$node_id}
                          </div>
                          <input type=text name="title" value="{$node->title}" size="50" class="admin_option_input">
                     </div>
                     <div class="admin_option">
						<div class="admin_option_name">
							{$LANG_ENABLE_LINK_IN_TOP_MENU}
						</div>
						<label><input type="checkbox" name="in_top_menu" value="1" {if $node->in_top_menu}checked{/if} /> {$LANG_ENABLE}</label>
					 </div>
					 <div class="admin_option">
						<div class="admin_option_name">
							{$LANG_ENABLE_LINK_IN_BOTTOM_MENU}
						</div>
						<label><input type="checkbox" name="in_bottom_menu" value="1" {if $node->in_bottom_menu}checked{/if} /> {$LANG_ENABLE}</label>
					 </div>

					 <div class="admin_option">
                     	<div class="admin_option_name">
                          	{$LANG_META_TITLE}
                          	{translate_content table='content_pages' field='meta_title' row_id=$node_id}
                        </div>
                        <div class="admin_option_description">
                        	{$LANG_META_TITLE_DESCR}
                        </div>
                        <input type=text name="meta_title" value="{$node->meta_title}" size="60" class="admin_option_input">
                     </div>
                     <div class="admin_option">
                     	<div class="admin_option_name">
                          	{$LANG_META_DESCRIPTION}
                          	{translate_content table='content_pages' field='meta_description' row_id=$node_id field_type='text'}
                        </div>
                        <div class="admin_option_description">
                        	{$LANG_META_TITLE_DESCR}
                        </div>
                        <textarea name="meta_description" rows="5" cols="50">{$node->meta_description}</textarea>
                     </div>
                     <div class="admin_option">
                     	<div class="admin_option_name">
                          	{$LANG_PAGE_INPUT_PARAMS}
                        </div>
                        <div class="admin_option_description">
                        	{$LANG_PAGE_INPUT_PARAMS_DESCR}
                        </div>
                        <textarea name="input_params" rows="5" cols="40">{$node->input_params}</textarea>
                     </div>
                     <div class="admin_option">
                     	<div class="admin_option_name">
                          	{$LANG_PAGE_PHP_CODE}
                        </div>
                        <div class="admin_option_description">
                        	{$LANG_PAGE_PHP_CODE_DESCR}
                        </div>
                        <textarea name="php_code" rows="14" cols="80">{$node->php_code}</textarea>
                     </div>
                     <div class="admin_option">
                     	<div class="admin_option_name">
                          	{$LANG_PAGE_OUTPUT_PARAMS}
                        </div>
                        <div class="admin_option_description">
                        	{$LANG_PAGE_OUTPUT_PARAMS_DESCR}
                        </div>
                        <textarea name="output_params" rows="5" cols="40">{$node->output_params}</textarea>
                     </div>
                     <div class="admin_option">
                     	<div class="admin_option_name">
                          	{$LANG_PAGE_TEMPLATE_FILE}
                        </div>
                        <div class="admin_option_description">
                        	{$LANG_PAGE_TEMPLATE_FILE_DESCR}
                        </div>
                        <input type=text name="template_file" value="{$node->template_file}" size="60" class="admin_option_input">
                     </div>

                     {$node->inputMode()}
                     <input class="button save_button" type=submit name="submit" value="{if $node_id != 'new'}{$LANG_BUTTON_SAVE_CHANGES}{else}{$LANG_BUTTON_CREATE_NEW_PAGE}{/if}">
                     </form>
                </div>

{include file="backend/admin_footer.tpl"}