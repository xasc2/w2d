{include file="backend/admin_header.tpl"}

                <div class="content">
                     <h3>{$LANG_LISTING_TYPES_1} "{$LANG_CHANGE_LISTING_TYPE_TITLE}"</h3>
                     
                     {include file="listings/admin_listing_options_menu.tpl"}

                     {if $types|@count}
                     <table class="presentationTable" border="0" cellpadding="2" cellspacing="2">
                       <tr>
                         <th>{$LANG_TYPE_TH}</th>
                         <th>{$LANG_LEVEL_TH} \ {$LANG_OPTIONS_TH}</th>
                         <td class="td_header">{$LANG_CHANGE_LISTING_TYPE_OPTION}</td>
                       </tr>
                       {foreach from=$types item=type}
                       <tr>
                         <td class="td_header">
                         	{$type->name}
                         </td>
                         <td class="td_header">
                         	{if $type->levels|@count > 0}
                         	<table width="100%" border="0" cellpadding="0" cellspacing="0">
                         		{foreach from=$type->levels item=level}
                         		<tr>
                         			<td width="100%" style="border: 0;">
                         			{$level->name}
                         			</td>
                         		</tr>
                         		{/foreach}
                         	</table>
                         	{else}
                         	&nbsp;
                         	{/if}
                         </td>
                         <td>
                            {if $type->levels|@count > 0}
                         	<table width="100%" border="0" cellpadding="0" cellspacing="0">
                         		{foreach from=$type->levels item=level}
                         		{assign var="level_id" value=$level->id}
                         		{if $listing->level->id != $level_id}
                         		<tr>
                         			<td  style="border: 0;">
                         				<a href="{$VH->site_url("admin/listings/change_type/$listing_id/level_id/$level_id")}">{$LANG_TYPE_LEVEL_OPTION}</a>
                         			</td>
                         		</tr>
                         		{else}
                         		<tr><td  style="border: 0;">&nbsp;</td></tr>
                         		{/if}
                         		{/foreach}
                         	</table>
                         	{else}
                         	&nbsp;
                         	{/if}
                         </td>
                       </tr>
                       {/foreach}
                     </table>
                     {/if}
                </div>

{include file="backend/admin_footer.tpl"}