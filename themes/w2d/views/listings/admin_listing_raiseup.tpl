{include file="backend/admin_header.tpl"}

                <div class="content">
                	{$VH->validation_errors()}
                     <h3>{$LANG_RAISEUP_LISTINGS_TITLE} "{$listing->title()}"</h3>

                     <form action="" method="post">
                     <div class="admin_option">
                          <div class="admin_option_description">
                          	{$LANG_LISTINGS_RAISEUP_DESCR}
                          </div>
                          <div class="admin_option_name" >
                          	{$LANG_PRICE_TH}: {if !$price || $price.value == 0}<span class="free">{$LANG_FREE}</span>{else}{$price.currency}&nbsp;{$VH->number_format($price.value, 2, $decimals_separator, $thousands_separator)}{/if}
                          </div>
                     </div>
                     <input class="button activate_button" type=submit name="submit" value="{$LANG_BUTTON_RAISE_UP}">
                     &nbsp;&nbsp;
                     <input class="button block_button" type=submit name="cancel" value="{$LANG_BUTTON_CANCEL}">
                     </form>
                </div>

{include file="backend/admin_footer.tpl"}