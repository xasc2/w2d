<?php
class w2dTheme
{
	public $title = "W2D theme";
	public $version = "1.1";
	public $description = "Do not remove. Create your own theme for any customizations";

	public function hooks()
	{
		$hook['w2d_theme_frontend_addJsFiles'] = array(
			'exclusions' => array(
				'install/:any',
				'locations/ajax_autocomplete_request/',
				'refresh_captcha/',
				'locations/get_locations_path_by_id/',
				'login/(.*)',
				'admin/(.*)',
				'install/(.*)',
				'ajax/(.*)'
			),
		);
		
		$hook['w2d_theme_backend_addJsFiles'] = array(
			'inclusions' => array(
				'login/(.*)',
				'admin/(.*)',
				'install/(.*)',
			),
			'exclusions' => array(
				'ajax/(.*)'
			),
		);
		
		return $hook;
	}
}
?>