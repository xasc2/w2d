<div class="pagination pagination-centered">
	<ul>
	{if $curr_page_line > 1}
		<li><a href="{$prev_line_link}" title="{$LANG_PAGINATOR_PREV_LINE_TITLE}">{$LANG_PAGINATOR_PREV_LINE}</a></li>
	{/if}
	{if $curr_page > 1}
		<li><a href="{$prev_link}" title="{$LANG_PAGINATOR_PREV_PAGE_TITLE}">{$LANG_PAGINATOR_PREV_PAGE}</a></li>
	{/if}

	{section name=pagination start=$start_page loop=$end_page+1}
		{if $smarty.section.pagination.index != $curr_page}
			{if $smarty.section.pagination.index != 1}
			<li><a href="{$link}page/{$smarty.section.pagination.index}/">{$smarty.section.pagination.index}</a></li>
			{else}
			<li><a href="{$link}">{$smarty.section.pagination.index}</a></li>
			{/if}
		{else}
			<li class="active disabled"><span>{$smarty.section.pagination.index}</span></li>
		{/if}
	{/section}

	{if $curr_page != $pages_num}
		<li><a href="{$next_link}" title="{$LANG_PAGINATOR_NEXT_PAGE_TITLE}">{$LANG_PAGINATOR_NEXT_PAGE}</a></li>
	{/if}
	{if $curr_page_line != $pages_lines_num}
		<li><a href="{$next_line_link}" title="{$LANG_PAGINATOR_NEXT_LINE_TITLE}">{$LANG_PAGINATOR_NEXT_LINE}</a></li>
	{/if}
	</ul>
</div>