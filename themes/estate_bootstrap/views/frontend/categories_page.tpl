{include file="frontend/header.tpl"}

			<div class="row" id="search_bar">
				{include file="frontend/search_block.tpl"}
			</div>

			<div class="row">
			 	<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>
	
				<div class="span9">
      				<div id="content_wrapper">
      					
      					{if $CI->load->is_module_loaded('rss')}
                        	<div class="rss_icon"">
                        		<a href="{$VH->getRssUrl()}" title="{$VH->getRssTitle()}">
                        			<img src="{$public_path}images/feed.png" />
                        		</a>
                        	</div>
                        {/if}
                        <ul class="breadcrumb">
      						<li><a href="{$VH->index_url()}">{$LANG_HOME_PAGE}</a> <span class="divider">/</span></li>
      						{foreach from=$breadcrumbs item="source_page" key="source_url"}
								<li><a href="{$source_url}">{$source_page}</a> <span class="divider">/</span></li>
							{/foreach}
      						<li class="active">{$current_category->name}</li>
      					</ul>

	      				{if $current_category->children|@count}
						<h4>{$LANG_SUBCATEGORIES}</h4>
                        <div class="subcategories_list">
                        	{foreach from=$current_category->children item=category}
                        	{assign var=subcategory_id value=$category->id}
                        		<span class="subcategory_item">
                        			<a href="{$VH->site_url($category->getUrl())}" class="subcategory">{$category->name}&nbsp;({$category->countListings()})</a>&nbsp;&nbsp;
                        		</span>
                        	{/foreach}
                        	<div class="clear_float"></div>
                        </div>
                        {/if}

                        {render_frontend_block
                        	block_type='listings'
                        	block_template='frontend/blocks/with_paginator.tpl'
                        	items_array=$listings
                        	view_name=$view->view
                        	view_format=$view->format
                        	type=$type
                        	listings_paginator=$listings_paginator
                        	order_url=$order_url
                        	orderby=$orderby
                        	direction=$direction
                        }
					</div>
				</div>
			</div>

{include file="frontend/footer.tpl"}