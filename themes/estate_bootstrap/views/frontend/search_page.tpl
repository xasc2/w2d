{include file="frontend/header.tpl"}

			<div class="row" id="search_bar">
				{include file="frontend/search_block.tpl"}
			</div>

			<div class="row">
			 	<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>
	
				<div class="span9">
      				<div id="content_wrapper">
      				
      					{if $CI->load->is_module_loaded('rss')}
	                        <div class="rss_icon"">
	                        	<a href="{$VH->getRssUrl()}" title="{$VH->getRssTitle()}">
	                        		<img src="{$public_path}images/feed.png" />
	                        	</a>
	                        </div>
						{/if}
      					<ul class="breadcrumb">
      						<li><a href="{$VH->index_url()}">{$LANG_HOME_PAGE}</a> <span class="divider">/</span></li>
      						<li class="active">{$LANG_BREADCRUMBS_SEARCH}</li>
      					</ul>

      					{render_frontend_block
      						block_type='map_and_markers'
      						block_template='frontend/blocks/map_standart.tpl'
      						existed_listings=$listings
      						clasterization=false
      					}

						{if $listings|@count}
							{render_frontend_block
	                        	block_type='listings'
	                        	block_template='frontend/blocks/with_paginator.tpl'
	                        	search_args=$args
	                        	items_array=$listings
	                        	view_name=$view->view
	                        	view_format=$view->format
	                        	type=$type
	                        	listings_paginator=$listings_paginator
	                        	order_url=$order_url
	                        	orderby=$orderby
	                        	direction=$direction
	                        }
                        {else}
							<ul>
								<li>{$LANG_NO_SEARCH}</li>
							</ul>
						{/if}
					</div>
                </div>
			</div>

{include file="frontend/footer.tpl"}