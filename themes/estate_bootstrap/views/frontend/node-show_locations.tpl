<script language="javascript" type="text/javascript">
	$(document).ready(function() {ldelim}
		$("#submit_location").click(function() {ldelim}
			var predefined_location_seo_name;
			$(".location_dropdown_list").each(function() {ldelim}
				if ($(this).find("option:selected").val() != '')
					predefined_location_seo_name = $(this).find("option:selected").attr("seo_name");
			{rdelim});
			if (predefined_location_seo_name) {ldelim}
				window.location = "{$VH->site_url()}location/"+predefined_location_seo_name+"/";
			{rdelim}
		{rdelim});
	{rdelim});
</script>

{if $args.predefined_location_id}
	{$VH->renderLocationDropBoxes($args.predefined_location_id)}
{elseif $current_location}
	{$VH->renderLocationDropBoxes($current_location->id)}
{else}
	{$VH->renderLocationDropBoxes()}
{/if}
<input type="button" id="submit_location" class="btn btn-info" value="{$LANG_LOCATIONS_OTHER}" />