			<script language="javascript" type="text/javascript">
                // Global url string
                {if $base_url}
                	var global_js_url = '{$base_url}';
                {else}
                	var global_js_url = '{$VH->site_url("search/")}';
                {/if}

                // Is advanced search in use? Attach to global_js_url
                var use_advanced = '';
                
                // Was advanced search block loaded?
                var advanced_loaded = false;

                // Scripts code array - will be evaluated when advanced search attached
                var scripts = new Array();
                
                var default_what = '{addslashes string=$LANG_SEARCH_WHAT} ({addslashes string=$LANG_SEARCH_WHAT_DESCR})';
                var default_where = '{addslashes string=$LANG_SEARCH_WHERE} ({addslashes string=$LANG_SEARCH_WHERE_DESCR})';
                
                var use_districts = {$system_settings.geocoded_locations_mode_districts};
                var use_provinces = {$system_settings.geocoded_locations_mode_provinces};

                var ajax_autocomplete_request = '{$VH->site_url("ajax/locations/autocomplete_request/")}';

                $(document).ready(function() {ldelim}
                	{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_where_search) || (!$system_settings.single_type_structure && $current_type && $current_type->where_search && $current_type->locations_enabled)}
		        		$('#where_search').autocomplete({ldelim}
			               	source: function(request, response) {ldelim}
			               		{if $system_settings.predefined_locations_mode != 'only'}
			               			{if $system_settings.predefined_locations_mode != 'disabled'}
			               				// geocode + ajax from DB
			               				geocodeAddress(request.term, response, ajax_autocomplete_request, false);
			               			{else}
			               				// only geocode
			               				geocodeAddress(request.term, response, false, false);
			               			{/if}
			               		{elseif $system_settings.predefined_locations_mode != 'disabled'}
			               			// only ajax from DB
			               			$.post(ajax_autocomplete_request, {ldelim}query: request.term{rdelim}, function(data) {ldelim}
				               			if (data = jQuery.parseJSON(data))
				               				response(data);
				               		{rdelim});
			               		{/if}
							{rdelim},
							focus: function(event, ui) {ldelim}
								$(this).val(ui.item.label);
								return false;
							{rdelim},
							select: function(event, ui) {ldelim}
								$(this).val(ui.item.label);
								if (ui.item.label != ui.item.value)
									$("#predefined_location_id").val(ui.item.value);
								return false;
							{rdelim},
							minLength: 2,
							delay: 600
						{rdelim});
		        		$('#where_search').keyup(function() {ldelim}
		        			$("#predefined_location_id").val('');
		        		{rdelim});
		                $('#where_radius_slider').slider({ldelim}
							min: 0,
							max: 10,
		{assign var=default_radius value=0}
							range: "min",
							value: $("#where_radius_label").val(),
							slide: function(event, ui) {ldelim}
								$("#where_radius_label").val(ui.value);
								$("#where_radius").val(ui.value);
							{rdelim}
						{rdelim});
					{/if}

					// Form submit event
					$("#search_form").submit( function() {ldelim}
						{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_what_search) || (!$system_settings.single_type_structure && $current_type && $current_type->what_search)}
						if ($("#what_search").val() != '' && $("#what_search").val() != default_what) {ldelim}
	            			global_js_url = global_js_url + 'what_search/' + urlencode($("#what_search").val()) + '/';
	            			global_js_url = global_js_url + "what_match" + '/' + $("input[name=what_match]:checked").val() + '/';
	            		{rdelim}
	            		{/if}

						{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_where_search) || (!$system_settings.single_type_structure && $current_type && $current_type->where_search && $current_type->locations_enabled)}
						if ($("#where_search").val() != ''&& $("#where_search").val() != default_where) {ldelim}
							global_js_url = global_js_url + 'where_search/' + urlencode($("#where_search").val()) + '/';
							if (parseInt($("#where_radius").val())>0) {ldelim}
								global_js_url = global_js_url + 'where_radius/' + $("#where_radius").val() + '/';
							{rdelim}
						{rdelim}
						if ($("#predefined_location_id").val() != '') {ldelim}
							global_js_url = global_js_url + 'predefined_location_id/' + urlencode($("#predefined_location_id").val()) + '/';
						{rdelim}
						{/if}

						{if !$system_settings.single_type_structure}
						if ($("#search_type").val() != '0') {ldelim}
							global_js_url = global_js_url + 'search_type/' + $("#search_type").val() + '/';
						{rdelim}
						{/if}

                		global_js_url = global_js_url + use_advanced;
                		window.location.href = global_js_url;
						return false;
					{rdelim});

					{if $advanced_search_fields->fieldsCount() || $content_access_obj->isPermission('Manage all listings')}
					var advanced_opened = false;
					$(".advanced_search").bind('click', function() {ldelim}
						if (advanced_opened) {ldelim}
							$(".advanced_search img").attr('src', '{$public_path}images/icons/accept.png');
							$("#advanced_search_block").hide();
							advanced_opened = false;
							use_advanced = '';
						{rdelim} else {ldelim}
							$(".advanced_search img").attr('src', '{$public_path}images/icons/delete.png');
							$("#advanced_search_block").show();
							advanced_opened = true;

							if (!advanced_loaded) {ldelim}
								ajax_loader_show();
								$.post('{$VH->site_url("ajax/listings/build_advanced_search/")}', {ldelim}type_id: $("#search_type").val(), args: '{addslashes string=$VH->json_encode($args)}'{rdelim},
									function(data) {ldelim}
										$("#advanced_search_block").html(parseScript(data));
										evalScripts(data);
										advanced_loaded = true;
										ajax_loader_hide();
									{rdelim}
								);
							{rdelim}
							use_advanced = 'use_advanced/true/';
						{rdelim}
						return false;
					{rdelim});
					{if $args.use_advanced}
					$('.advanced_search').triggerHandler('click');
					{/if}
					{/if}
				{rdelim});
			</script>

					<form id="search_form" action="" method="post">
						<div class="span6">
							{if $current_type && $current_type->search_type == 'local' && !$system_settings.single_type_structure}
	                     	<input type="hidden" id="search_type" name="search_type" value="{$current_type->id}" />
	                     	{else}
							<input type="hidden" id="search_type" name="search_type" value="0" />
							{/if}
	
							<h4>{$LANG_FRONTEND_SEARCH_LISTINGS}{if $current_type->search_type == 'local'} {$LANG_IN} "{$current_type->name}"{/if}</h4>
							{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_what_search) || (!$system_settings.single_type_structure && $current_type && $current_type->what_search)}
							<div class="input-append">
								<input class="what_where_search_input" type="text" name="what_search" id="what_search" value="{if $args.what_search}{$args.what_search}{/if}" placeholder="{$LANG_SEARCH_WHAT} ({$LANG_SEARCH_WHAT_DESCR})" />
								<div class="btn-group">
									<div class="btn dropdown-toggle" data-toggle="dropdown">
										{$LANG_SEARCH_WHAT_OPION} &nbsp;&nbsp;<span class="caret"></span>
									</div>
									<ul class="dropdown-menu">
										<li>
											<label class="radio"><input type="radio" name="what_match" value="any" {if $args.what_match == 'any' || !$args[$field_mode]}checked{/if} /> {$LANG_ANY_MATCH}</label>
											<label class="radio"><input type="radio" name="what_match" value="exact" {if $args.what_match == 'exact'}checked{/if} /> {$LANG_EXACT_MATCH}</label>
										</li>
									</ul>
								</div>
							</div>
							{/if}
	
							{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_where_search) || (!$system_settings.single_type_structure && $current_type && $current_type->where_search && $current_type->locations_enabled)}
							<div class="input-append">
								<input class="what_where_search_input" id="where_search" type="text" name="where_search" value="{if $args.where_search}{$args.where_search}{elseif $current_location}{$current_location->getChainAsString()}{/if}" placeholder="{$LANG_SEARCH_WHERE} ({$LANG_SEARCH_WHERE_DESCR})" />
								<input type="hidden" name="predefined_location_id" id="predefined_location_id" value="{if $args.predefined_location_id}{$args.predefined_location_id}{/if}" />
								<div class="btn-group">
									<div class="btn dropdown-toggle" data-toggle="dropdown">
										{$LANG_SEARCH_WHERE_OPION} <span class="caret"></span>
									</div>
									<ul class="dropdown-menu">
										<li>
											{$LANG_SEARCH_IN_RADIUS}
											<input type="text" class="span1" id="where_radius_label" value="{if $args.where_radius}{$args.where_radius}{else}{$default_radius}{/if}" size="2" disabled />
											<input type="hidden" name="where_radius" id="where_radius" value="{if $args.where_radius}{$args.where_radius}{else}{$default_radius}{/if}" />
											{if $system_settings.search_in_raduis_measure == 'miles'}
												{$LANG_SEARCH_IN_RADIUS_MILES}
											{else}
												{$LANG_SEARCH_IN_RADIUS_KILOMETRES}
											{/if}
											<div id="where_radius_slider"></div>
										</li>
									</ul>
								</div>
							</div>
							{/if}
						</div>

						<div class="span6">
                     		{$search_fields->inputMode($args)}

							{if $advanced_search_fields->fieldsCount() || $content_access_obj->isPermission('Manage all listings')}
							<div class="clear_float"></div>
							<div id="advanced_search_block" style="display: none;"></div>
							<div class="search_filter_standart">
								<div id="advanced_search_button">
									<a href="javascript: void(0);" class="toggle advanced_search"><img src="{$public_path}images/icons/accept.png"></a> <a href="javascript: void(0);" class="toggle advanced_search">{$LANG_ADVANCED_SEARCH}</a>
								</div>
							</div>
							{/if}
							<div class="clear_float"></div>
		
							<div id="search_button">
								<input type="submit" name="submit" class="btn btn-info" value="{$LANG_BUTTON_SEARCH_LISTINGS}" />
							</div>
						</div>
						<div class="px10"></div>
					</form>