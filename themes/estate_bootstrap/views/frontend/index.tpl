{include file="frontend/header.tpl"}

			<div class="row">
				<div class="span6">
					{include file="frontend/search_block.tpl"}
				</div>
				<div class="span6">
					{render_frontend_block
						block_type='listings'
						block_template="frontend/blocks/slider.tpl"
						only_with_logos=true
						page_name=index
						search_location=$current_location
						limit=5
						search_status=1
						search_users_status=2
						orderby=random
						auto_play=1
						slider_height=340
					}
					
					{$VH->render_banner('header')}
					{$VH->js_advertisement_append('header')}
				</div>
			</div>
			<div class="row">
				<div class="span12">
					{render_frontend_block
						block_type='categories'
						block_template="frontend/blocks/index-categories.tpl"
						is_counter=true
						max_depth=1
					}
						
					{if $CI->load->is_module_loaded('rss')}
					<h1 id="index_header">
						<div class="rss_icon_index"">
							<a href="{$VH->getRssUrl()}" title="{$VH->getRssTitle()}">
								<img src="{$public_path}images/feed.png" />
							</a>
						</div>
					</h1>
					{/if}
				</div>
			</div>

			<div class="row">
				<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>
				<div class="span9" id="center_part">
					{foreach from=$types item=type}
						{assign var=type_id value=$type->id}
						{assign var=view value=$listings_views->getViewByTypeIdAndPage($type_id, 'index')}
						{render_frontend_block
							block_type='listings'
							block_template='frontend/blocks/for_index.tpl'
							items_array=$listings_of_type[$type_id]
							view_name=$view->view
							view_format=$view->format
							type=$type
						}
					{/foreach}
				</div>
			</div>

{include file="frontend/footer.tpl"}