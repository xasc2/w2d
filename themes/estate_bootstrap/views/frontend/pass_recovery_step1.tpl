{include file="frontend/header.tpl"}

			<div class="row" id="search_bar">
				{include file="frontend/search_block.tpl"}
			</div>

			<div class="row">
			 	<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>
	
				<div class="span9">
      				<div id="content_wrapper">
      					{$VH->validation_errors()}
                         <h2>{$LANG_PASSWORD_RECOVERY1}</h2>

                         <form action="" method="post">
	                     <div class="admin_option">
	                          <div class="admin_option_name">
	                          	{$LANG_YOUR_EMAIL}<span class="red_asterisk">*</span>
	                          </div>
	                          <input type=text name="email" value="{$email}" size="45" class="admin_option_input">
	                     </div>
	                     <div class="admin_option">
	                          <div class="admin_option_name">
	                          	{$LANG_FILL_CAPTCHA}<span class="red_asterisk">*</span>
	                          </div>
	                          <input type="text" name="captcha" size="4">
	                          <div class="px10"></div>
	                          {$captcha->view()}
						 </div>
						 <div class="px5"></div>
	                     <input class="btn btn-info" type=submit name="submit" value="{$LANG_BUTTON_PASSWORD_RECOVERY}">
	                     </form>
                 	</div>
                </div>
			</div>

{include file="frontend/footer.tpl"}