{if $items_array|@count}
<script language="JavaScript" type="text/javascript">
$(function() {ldelim}
	/*if ($(window).width() > 1024)
		var slides_number = 1;
	else*/
	var slides_number = 1;

	$('#slider').anythingSlider({ldelim}
		resizeContents: true,
		theme: 'minimalist-round',
		buildNavigation: false,
		buildStartStop: false,
		autoPlay: true,
		showMultiple: slides_number,
		delay: 6000,
		animationTime: 400,
		expand: true
	{rdelim});
	$('#slider').show();
{rdelim});
</script>

<div style="height: {$slider_height}px; padding-bottom: 20px">
	<ul id="slider" style="display: none;">
		{foreach from=$items_array item=listing}
		<li>
			<div class="slider_listing_wrapper">
				{if $listing->level->logo_enabled && $listing->logo_file}
				<div class="slider_listing_logo_wrapper">
					<a title="{$listing->title()}" href="{$VH->site_url($listing->url())}"><img src="{$users_content}/users_images/thmbs/{$listing->logo_file}" alt="{$listing->title()}" class="slider_listing_logo" /></a>
				</div>
				{/if}
				<div>
					<h4><a title="{$listing->title()}" href="{$VH->site_url($listing->url())}">{$listing->title()}</a></h4>
					{if $listing->level->ratings_enabled}
					{assign var=avg_rating value=$listing->getRatings()}
					{$avg_rating->setInactive()}
					{$avg_rating->view()}
					{/if}
					<div class="listing_date">{$listing->order_date|date_format}</div>
				</div>

				<div>
					{if $listing->level->description_mode == 'richtext'}
					{$listing->listing_description_teaser()|strip_tags|truncate:440}
					{else}
					{$listing->description()|strip_tags|truncate:440}
					{/if}
				</div>

				<br />
				{$listing->outputMode(full)}
				
				<br />
				<a href="{$VH->site_url($listing->url())}" class="view_listing_link">{$LANG_VIEW_LISTING} >>></a>

				<div class="clear_float"></div>
			</div>
			
		</li>
		{/foreach}
	</ul>
</div>
{/if}