{if $items_array|@count}
	{if !$system_settings.single_type_structure}
	<h3><a href="{$VH->site_url($type->getUrl())}">{$type->name}</a></h3>
	{/if}

	{if $wrapper_object}
		{$wrapper_object->render()}
	{else}
		{foreach from=$items_array item=item}
			{$item->view($view_name)}
		{/foreach}
	{/if}
{/if}