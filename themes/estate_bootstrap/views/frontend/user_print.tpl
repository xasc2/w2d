<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>{if $title}{$title} - {/if}{$site_settings.website_title}</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
{foreach from=$css_files item=css_media key=css_file}
		<link rel="stylesheet" href="{$css_file}" media="{$css_media}" type="text/css" />
{/foreach}
{foreach from=$ex_css_files item=ex_css_item}
		<link rel="stylesheet" href="{$ex_css_item}" type="text/css" />
{/foreach}
{foreach from=$ex_js_scripts item=ex_js_item}
		<script language="JavaScript" type="text/javascript" src="{$ex_js_item}"></script>
{/foreach}

{if !$CI->config->item('combine_static_files') || $CI->config->item('combine_static_files') === null}
	{foreach from=$js_scripts item=js_item}
			<script language="JavaScript" type="text/javascript" src="{$VH->base_url()}{$js_item}"></script>
	{/foreach}
{else}
	<script language="JavaScript" type="text/javascript" src="{$VH->base_url()},{$VH->implode(',', $js_scripts)}"></script>
{/if}
		<link rel="shortcut icon" href="{$public_path}images/favicon.ico" >
	</head>
<body>
	<script language="JavaScript" type="text/javascript">
		var in_favourites_icon = $('<img />').attr('src', '{$public_path}/images/icons/folder_star.png');
		var not_in_favourites_icon = $('<img />').attr('src', '{$public_path}/images/icons/folder_star_grscl.png');
		var to_favourites_msg = '{addslashes string=$LANG_QUICK_LIST_SUCCESS}';
		var from_favourites_msg = '{addslashes string=$LANG_QUICK_FROM_LIST_SUCCESS}';
		$(document).ready(function() {ldelim}
			$("a").each(function() {ldelim}
				$(this).attr("href", "javascript:void(0);");
			{rdelim});
			$("a").click(function() {ldelim}
				return false;
			{rdelim});
		{rdelim});
	</script>

	<div id="print_main">
		<div id="user_header_block">
			<input type="button" class="button" onclick="window.print();" value="{$LANG_PRINT_PAGE_BTN}">&nbsp;&nbsp;&nbsp;<input type="button" class="button" onclick="window.close();" value="{$LANG_BUTTON_CLOSE}">

			<div class="px10"></div>
			<img src="{$users_content}/users_images/site_logo/{$system_settings.site_logo_file}" >
			<div class="px10"></div>

			<h2 class="user_login">{$user->login}</h2>
			<div class="listing_author">{$LANG_USER_REGISTERED} {$user->registration_date|date_format:"%D %H:%M"}</div>
			<div class="clear_float"></div>
			<div class="px5"></div>

			{if $user->users_group->logo_enabled && ($user->user_logo_image || $user->facebook_logo_file)}
			<div id="user_logo">
				{if $user->use_facebook_logo && $user->facebook_logo_file}
				<img src="{$user->facebook_logo_file}" />
				{elseif $user->users_group->logo_enabled && !$user->use_facebook_logo && $user->user_logo_image}
				<img src="{$users_content}/users_images/users_logos/{$user->user_logo_image}">
				{/if}
			</div>
			{/if}
		</div>
		{if $user->content_fields->fieldsCount() && $user->content_fields->isAnyValue()}
		<div class="px10"></div>
		<h3>{$LANG_LISTING_INFORMATION}</h3>
		{$user->content_fields->outputMode()}
		{/if}

		{foreach from=$types item=type}
			{assign var=type_id value=$type->id}
			{assign var=view value=$listings_views->getViewByTypeIdAndPage($type_id, 'types')}
			{render_frontend_block
				block_type='listings'
				block_template='frontend/blocks/listings_of_user.tpl'
				page_name='types'
				view_name=$view->view
				view_format=$view->format
				search_type=$type
				search_owner=$user->login
				search_location=$current_location
				orderby='l.order_date'
				search_status=1
				search_users_status=2
				limit=3
				nolinks=true
			}
		{/foreach}
		<div class="px10"></div>
		<input type="button" class="button" onclick="window.print();" value="{$LANG_PRINT_PAGE_BTN}">&nbsp;&nbsp;&nbsp;<input type="button" class="button" onclick="window.close();" value="{$LANG_BUTTON_CLOSE}">
	</div>
</body>
</html>