{include file="backend/admin_header.tpl"}

<script language="Javascript" type="text/javascript">
$(document).ready(function() {ldelim}
	$("input:text:visible:first").focus();
{rdelim});
</script>

		<div class="content">
			<div class="row-fluid">
				{$VH->validation_errors()}
				<form id="login_form" class="well" action="" method="post">
					<div>
						{$LANG_LOGIN_EMAIL}<span class="red_asterisk">*</span>:
						<input type=text name="email" value="{$CI->validation->email}" size="30" class="admin_option_input">
					</div>
					<div>
						{$LANG_LOGIN_PASSWORD}<span class="red_asterisk">*</span>:
						<input type=password name="password" value="{$CI->validation->email}" size="30" class="admin_option_input">
					</div>
					<label class="checkbox">
						<input type="checkbox" name="remember_me"> {$LANG_REMEMBER_ME}
					</label>
					<div>
						<input type="submit" name="submit" value="{$LANG_BUTTON_LOGIN}" class="button">
					</div>
					{if $VH->checkIsRegistrationAllowed()}
					<div class="login_block_link">{$VH->anchor('register', $LANG_CREATE_ACCOUNT)}</div>
					{/if}
					<div class="login_block_link">{$VH->anchor('pass_recovery_step1', $LANG_FORGOT_PASS)}</div>
					
					{$VH->callEvent('Login processors block')}
				</form>
			</div>
		</div>
           
{include file="backend/admin_footer.tpl"}