<script language="JavaScript" type="text/javascript">
	$(document).ready(function() {ldelim}
		$(".location_dropdown_list").bind("change", function() {ldelim}
			curr_order_num = parseFloat($(this).attr('order_num'));
			next_order_num = parseFloat(curr_order_num) + 1;
			prev_order_num = parseFloat(curr_order_num) - 1;
			if (curr_order_num != {$location_levels|@count} && this.options[this.selectedIndex].value != '') {ldelim}
				$(this).parent().parent().find(".location_dropdown_list").attr('disabled', 'disabled');
				$(this).parent().parent().find(".location_dropdown_list").css('background-image', 'url({$public_path}images/ajax-indicator.gif)');
				$(this).parent().parent().find(".location_dropdown_list").css('background-repeat', 'no-repeat');
				$(this).parent().parent().find(".location_dropdown_list").css('background-position', '50% 50%');
				for (i = next_order_num; i <= {$location_levels|@count}; i++)
					$(this).parent().parent().find(".loc_level_"+i+"_{$virtual_id}").html('<option value="" selected>- - - {$LANG_LOCATION_SELECT} ' + $(this).parent().parent().find(".loc_level_"+i+"_{$virtual_id}").attr("level_name") + ' - - -</option>');
				$(this).parent().parent().find(".loc_level_"+next_order_num+"_{$virtual_id}").load("{$VH->site_url('ajax/locations/build_drop_box')}", {ldelim}parent_id: this.options[this.selectedIndex].value, for_level: $(this).parent().parent().find(".loc_level_"+next_order_num+"_{$virtual_id}").attr("level_name"){rdelim},
				function(){ldelim}
					$(this).parent().parent().find(".location_dropdown_list").css('background-image','');
					$(this).parent().parent().find(".location_dropdown_list").css('background-repeat','');
					$(this).parent().parent().find(".location_dropdown_list").css('background-position','');
					$(this).parent().parent().find(".location_dropdown_list").removeAttr('disabled');
				{rdelim});
			{rdelim} else {ldelim}
				for (var i=next_order_num; i<={$location_levels|@count}; i++) {ldelim}
					$(".loc_level_"+i+"_{$virtual_id}").val('');
					$(this).parent().parent().find(".loc_level_"+i+"_{$virtual_id}").html('<option value="" selected>- - - {$LANG_LOCATION_SELECT} ' + $(this).parent().parent().find(".loc_level_"+i+"_{$virtual_id}").attr("level_name") + ' - - -</option>');
				{rdelim}
			{rdelim}
		{rdelim});
	{rdelim});
</script>

<div>
{foreach from=$location_levels item=loc_level key=key}
{assign var=order_num value=$loc_level->order_num}
	<div class="location_level">
		<select class="location_dropdown_list loc_level_{$loc_level->order_num}_{$virtual_id}" order_num="{$loc_level->order_num}" level_name="{$loc_level->name}" name="loc_level_{$loc_level->order_num}[]">
			<option value="">- - - {$LANG_LOCATION_SELECT} {$loc_level->name} - - -</option>
			{foreach from=$locations_by_level[$order_num] item=location}
				<option value="{$location->id}" seo_name="{$location->seo_name}" {if $location->selected}selected{/if}>{$location->name}</option>
			{/foreach}
		</select>
	</div>
{/foreach}
</div>