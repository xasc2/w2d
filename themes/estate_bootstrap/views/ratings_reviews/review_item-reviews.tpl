{assign var=user value=$review->getUser()}
{assign var=object value=$review->getObject()}
{assign var=rating value=$review->setRating()}

{assign var=review_id value=$review->id}
{assign var=owner value=$review->object->getOwner()}
<li>
	<div class="review well" itemprop="review" itemscope itemtype="http://schema.org/Review">
		{if $review->status == 2}
			<div class="blocked_review">
				{$LANG_REVIEWS_MODERATED_AND_BLOCKED}
			</div>
		{else}
			<div class="review_header">
				<div class="review_author {if $owner->id == $review->user->id}owner_of_object{/if}" itemprop="author" itemscope itemtype="http://schema.org/Person">
					{if $review->user->users_group->logo_enabled}
					<div class="review_author_img">
						<meta itemprop="image" content="{$review->user->profileImageUrl()}" />
						{$review->user->renderThmbImage()}
					</div>
					{/if}
					<div class="review_author_name">
					{if $review->user_id}
						{if $review->user_id != 1 && $content_access_obj->isPermission('Manage users')}
							<a href="{$review->user->profileUrl()}" itemprop="url">{$review->user->login}</a>
						{else}
							<span itemprop="name">{$review->user->login}</span>
						{/if}
					{else}
						{$LANG_ANONYM}: <span itemprop="name">{$review->anonym_name}</span> ({$review->ip})
					{/if}
					{if $owner->id == $review->user->id} <i>({$LANG_LISTING_OWNER})</i>{/if}
					</div>
				</div>
				{if $review->user_id && $review->rating}
				<div class="review_author_rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
					{$review->rating->view()}
				</div>
				{/if}
				<div class="review_publiched_date">
					<time itemprop="datePublished" datetime="{$review->date_added|date_format:"%Y-%m-%dT%H:%M"}">{$review->date_added|date_format:"%D %H:%M"}</time>
				</div>
				<div class="clear_float"></div>
			</div>
			<div class="review_body" itemprop="reviewBody">
				{$review->review}
			</div>
		{/if}
	</div>
	{if $review->children|@count}
	<ul class="reviews_block">
	{foreach from=$review->children item=child}
		{$child->view()}
	{/foreach}
	</ul>
	{/if}
</li>