<?php

function estate_bootstrap_theme_frontend_addJsFiles($CI)
{
	$system_settings = registry::get('system_settings');
	$view = $CI->load->view();
	
	$view->addExternalJsFile('https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js');
	$view->addExternalJsFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js');

	$view->addCssFile('bootstrap/bootstrap.css');
	$view->addCssFile('common_style.css');
	$view->addCssFile('frontend_style.css');
	if ($CI->router->fetch_module() == 'frontend' && $CI->router->fetch_method() == 'index') {
		$view->addCssFile('anythingslider/css/anythingslider.css');
		$view->addCssFile('anythingslider/css/theme-minimalist-round.css');
		$view->addJsFile('jquery.anythingslider.min.js');
	}

	if ($CI->router->fetch_method() == 'listings') {
		$view->addCssFile('jq_carousel_skin/skin.css');
		$view->addJsFile('jquery.jcarousel.min.js');

		$view->addCssFile('jquery.lightbox-0.5.css');
		$view->addJsFile('jquery.lightbox-0.5.pack.js');
	}
	$view->addJsFile('bootstrap/bootstrap.min.js');

	// There were some problems with full cookie.js name of this file, so now it was renamed to coo_kie.js
	$view->addJsFile('jquery.coo_kie.js');
	$view->addJsFile('jquery.rater.js');

	$view->addCssFile('ui/jquery-ui-1.8.9.custom.css');
	$view->addCssFile('ui/query-ui-customizations.css');

	$view->addJsFile('noty/jquery.noty.js');
	$view->addJsFile('noty/themes/default.js');
	$view->addJsFile('noty/layouts/topRight.js');
	
	$view->addJsFile('js_functions.js');
	$view->addJsFile('jquery.jqURL.js');

	$view->addJsFile('swfobject.js');

	$language_code = registry::get('current_language');
	if ($language_code && $language_code != 'en')
		$view->addJsFile('content_fields/i18n/jquery-ui-i18n.js');

	$view->addExternalJsFile('http://maps.googleapis.com/maps/api/js?v=3.4&sensor=false&language=' . $language_code);
	$view->addJsFile('google_maps_view.js');
}

function estate_bootstrap_theme_backend_addJsFiles($CI)
{
	$system_settings = registry::get('system_settings');
	$view = $CI->load->view();
	
	$view->addExternalJsFile('https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js');
	$view->addExternalJsFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js');
	
	$view->addCssFile('bootstrap/bootstrap.css');
	$view->addCssFile('bootstrap/bootstrap-responsive.css');
	$view->addCssFile('common_style.css');
	$view->addCssFile('admin_style.css');
	$view->addCssFile('ui/query-ui-customizations.css');

	$view->addJsFile('bootstrap/bootstrap.min.js');

	$view->addJsFile('jquery.jqURL.js');
	
	$view->addJsFile('noty/jquery.noty.js');
	$view->addJsFile('noty/themes/default.js');
	$view->addJsFile('noty/layouts/topRight.js');

	$view->addJsFile('js_functions.js');
	// There were some problems with full 'cookie.js' name of this file, so now it was renamed to 'coo_kie.js'
	$view->addJsFile('jquery.coo_kie.js');
	// Backend Main Menu
	$view->addJsFile('jquery.treeview.js');
	$view->addJsFile('swfobject.js');

	$language_code = registry::get('current_language');
	if ($language_code && $language_code != 'en')
		$view->addJsFile('content_fields/i18n/jquery-ui-i18n.js');

	$view->addExternalJsFile('http://maps.google.com/maps/api/js?v=3.4&sensor=false&language=' . $language_code);
	
	$view->addCssFile('ui/jquery-ui-1.8.9.custom.css');
	$view->addCssFile('ui/query-ui-customizations.css');
}
?>