<?php
class wideTheme
{
	public $title = "Full width theme";
	public $version = "1.1";
	public $description = "Slider on index and classifieds pages, categories and locations search dropboxes";
	public $req_version = 3.2;

	public function hooks()
	{
		$hook['wide_theme_frontend_addJsFiles'] = array(
			'exclusions' => array(
				'install/:any',
				'locations/ajax_autocomplete_request/',
				'refresh_captcha/',
				'locations/get_locations_path_by_id/',
				'login/(.*)',
				'admin/(.*)',
				'install/(.*)',
				'ajax/(.*)'
			),
		);
		
		$hook['wide_theme_backend_addJsFiles'] = array(
			'inclusions' => array(
				'login/(.*)',
				'admin/(.*)',
				'install/(.*)',
			),
			'exclusions' => array(
				'ajax/(.*)'
			),
		);
		
		return $hook;
	}
}
?>