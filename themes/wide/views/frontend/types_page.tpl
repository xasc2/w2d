{include file="frontend/header.tpl"}

			<tr>
				<td id="left_sidebar">
				{include file="frontend/left-sidebar.tpl"}
				</td>
      			<td id="content_block" valign="top">
      				<div id="content_wrapper">
      					{if $current_type->seo_name == 'classifieds'}
	      					{render_frontend_block
	                        	block_type='listings'
								block_template="frontend/blocks/slider_classifieds.tpl"
								search_type='classifieds'
								only_with_logos=true
								limit=10
								search_status=1
								search_users_status=2
								orderby=random
								slider_height=190
	                        }
                        {/if}

      					<div class="breadcrumbs">
      						<div class="breadcrumbs_text"">
      							<a href="{$VH->index_url()}">{$LANG_HOME_PAGE}</a> » <span>{$type->name}</span>
      						</div>
	      					{if $CI->load->is_module_loaded('rss')}
	                        	<div class="rss_icon"">
	                        		<a href="{$VH->getRssUrl()}" title="{$VH->getRssTitle()}">
	                        			<nobr><img src="{$public_path}images/feed.png" />&nbsp;<img src="{$public_path}images/rss.png" /></nobr>
	                        		</a>
	                        	</div>
	                        {/if}
	                        <div class="clear_float"></div>
      					</div>

                    	{render_frontend_block
                        	block_type='listings'
                        	block_template='frontend/blocks/with_paginator.tpl'
                        	items_array=$listings
                        	view_name=$view->view
                        	view_format=$view->format
                        	type=$type
                        	listings_paginator=$listings_paginator
                        	order_url=$order_url
                        	orderby=$orderby
                        	direction=$direction
                        }
                 	</div>
                </td>
                <td id="right_sidebar">
                {include file="frontend/right-sidebar.tpl"}
                </td>
			</tr>

{include file="frontend/footer.tpl"}