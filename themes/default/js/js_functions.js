var ajax_fn = '';

$(document).ready(function() 
{
	// Drag table rows and build serialized_order
	if ($("#drag_table").length) {
	    $("#drag_table").tableDnD( {
	    	onDragClass: "drag_class",
	    	onDrop: function(table, row) {
				$("#serialized_order").val($.tableDnD.serialize());
	    	}
	    });
	}
	
	// Show/hide hints
	$(".hint_icon").hover(function() {$(this).next().show();}, function() {$(this).next().hide();});

	show_msg();

	// Add zebra style to tables
	$(".standardTable tr").hover(function() {$(this).addClass("row_highlight");}, function() {$(this).removeClass("row_highlight");});

	// Check/uncheck row of a table
	$(".standardTable tr td").live("click", 
		function(event) {
			if ( this == event.target) {
				if ($(this).parent().find(':first-child:first-child input[type=checkbox]').length) {
					chkbx = $(this).parent().find(':first-child:first-child input[type=checkbox]');
					if ($(chkbx).attr("checked")) {
						$(chkbx).removeAttr("checked");
					} else {
						$(chkbx).attr("checked", "checked");
					}
				}
			}
		}
	);
	// Check/uncheck all checkboxes
	if ($(".standardTable tr th input[type=checkbox]").length) {
		$(".standardTable tr th input[type=checkbox]").click( function() {
			chkbxs = $(this).parent().parent().parent().find("tr td:first-child:first-child input[type=checkbox]");
			if (chkbxs.length) {
				if ($(this).attr("checked")) {
					chkbxs.each( function() {
						$(this).attr("checked", "checked");
					});
				} else {
					chkbxs.each( function() {
						$(this).removeAttr("checked");
					});
				}
			}
		});
	}
	
	// Highlight errors
	if (isset('js_highlights')) {
		$.each(js_highlights , function(index, value) {
			$("input[name="+this+"]").addClass('error_highlight');
			$("textarea[name="+this+"]").addClass('error_highlight');
			$("select[name="+this+"]").addClass('error_highlight');
		});
	}

	// 'Write in Seo Style' button
	$(".seo_rewrite").click( function() {
		if ($("input[name=name]").length)
			name = $("input[name=name]").val().toLowerCase();
		if ($("input[name=login]").length)
			name = $("input[name=login]").val().toLowerCase();
		
		var defaultDiacriticsRemovalMap = [
		                                   {'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
		                                   {'base':'AA','letters':/[\uA732]/g},
		                                   {'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
		                                   {'base':'AO','letters':/[\uA734]/g},
		                                   {'base':'AU','letters':/[\uA736]/g},
		                                   {'base':'AV','letters':/[\uA738\uA73A]/g},
		                                   {'base':'AY','letters':/[\uA73C]/g},
		                                   {'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
		                                   {'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
		                                   {'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
		                                   {'base':'DZ','letters':/[\u01F1\u01C4]/g},
		                                   {'base':'Dz','letters':/[\u01F2\u01C5]/g},
		                                   {'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
		                                   {'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
		                                   {'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
		                                   {'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
		                                   {'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
		                                   {'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
		                                   {'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
		                                   {'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
		                                   {'base':'LJ','letters':/[\u01C7]/g},
		                                   {'base':'Lj','letters':/[\u01C8]/g},
		                                   {'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
		                                   {'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
		                                   {'base':'NJ','letters':/[\u01CA]/g},
		                                   {'base':'Nj','letters':/[\u01CB]/g},
		                                   {'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
		                                   {'base':'OI','letters':/[\u01A2]/g},
		                                   {'base':'OO','letters':/[\uA74E]/g},
		                                   {'base':'OU','letters':/[\u0222]/g},
		                                   {'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
		                                   {'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
		                                   {'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
		                                   {'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
		                                   {'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
		                                   {'base':'TZ','letters':/[\uA728]/g},
		                                   {'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
		                                   {'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
		                                   {'base':'VY','letters':/[\uA760]/g},
		                                   {'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
		                                   {'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
		                                   {'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
		                                   {'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
		                                   {'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
		                                   {'base':'aa','letters':/[\uA733]/g},
		                                   {'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
		                                   {'base':'ao','letters':/[\uA735]/g},
		                                   {'base':'au','letters':/[\uA737]/g},
		                                   {'base':'av','letters':/[\uA739\uA73B]/g},
		                                   {'base':'ay','letters':/[\uA73D]/g},
		                                   {'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
		                                   {'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
		                                   {'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
		                                   {'base':'dz','letters':/[\u01F3\u01C6]/g},
		                                   {'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
		                                   {'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
		                                   {'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
		                                   {'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
		                                   {'base':'hv','letters':/[\u0195]/g},
		                                   {'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
		                                   {'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
		                                   {'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
		                                   {'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
		                                   {'base':'lj','letters':/[\u01C9]/g},
		                                   {'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
		                                   {'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
		                                   {'base':'nj','letters':/[\u01CC]/g},
		                                   {'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
		                                   {'base':'oi','letters':/[\u01A3]/g},
		                                   {'base':'ou','letters':/[\u0223]/g},
		                                   {'base':'oo','letters':/[\uA74F]/g},
		                                   {'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
		                                   {'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
		                                   {'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
		                                   {'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
		                                   {'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
		                                   {'base':'tz','letters':/[\uA729]/g},
		                                   {'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
		                                   {'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
		                                   {'base':'vy','letters':/[\uA761]/g},
		                                   {'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
		                                   {'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
		                                   {'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
		                                   {'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
		                               ];
		for(var i=0; i<defaultDiacriticsRemovalMap.length; i++)
			name = name.replace(defaultDiacriticsRemovalMap[i].letters, defaultDiacriticsRemovalMap[i].base);

		//change spaces and other characters by '_'
		name = name.replace(/\W/gi, "_");
		// remove double '_'
		name = name.replace(/(\_)\1+/gi, "_");

		seo_name_write(name);
	});

	// Auto resize wrong listings' logos
	$(".img_div, #img_div").each( function() {
		div_width = $(this).width();
		div_height = $(this).height();
		
		$(this).find("img").each( function() {
			img_width = $(this).css('width');
			img_height = $(this).css('height');
			//$(this).css("width", "auto").css("height", "auto"); // Remove existing CSS
	        //$(this).removeAttr("width").removeAttr("height");
			
			if (div_width < img_width) {
				$(this).css('width', div_width);
			} else {
				if (div_height < img_height) {
					$(this).css('height', div_height);
				}
			}
		});
	});
	
	// Move slowly through anchors
	$("a.ancLinks").click(function () {
		moveSlowly($(this));
    });
    
    if (window.$.datepicker != undefined) {
    	// Add "Clear" button to all datepickers
    	var dpFunc = $.datepicker._generateHTML; //record the original
    	$.datepicker._generateHTML = function(inst){
    		var thishtml = $( dpFunc.call($.datepicker, inst) ); //call the original
    		thishtml = $('<div />').append(thishtml); //add a wrapper div for jQuery context

    		//locate the button panel and add our button - with a custom css class.
    		$('.ui-datepicker-buttonpane', thishtml).append(
    			$('<button class="ui-datepicker-clear ui-state-default ui-priority-primary ui-corner-all">Clear</button>').click(function(){
    				$.datepicker._clearDate(inst.input);
    			})
    		);

    		thishtml = thishtml.children(); //remove the wrapper div
    		return thishtml; //assume okay to return a jQuery
    	};
    }

    // Modal window
    $('a.popup_dialog, a.nyroModal, a.ajaxDialog').live('click', function() {
    	var url = this.href;
    	var dialog_width = $(this).attr('width');
    	var dialog_title = '';
    	if ($(this).attr('title'))
    		dialog_title = $(this).attr('title');
    	else if ($(this).text())
    		dialog_title = $(this).text();
        ajax_loader_show('Loading...');
        var dialog = $('<div id="dialog" style="display:hidden"></div>').appendTo('body'); 
        // load remote content
        dialog.load(url, {}, function(responseText, textStatus, XMLHttpRequest) {
        	dialog.dialog({
        		modal: true,
        		width: dialog_width,
        		resizable: false,
        		draggable: false,
        		title: dialog_title,
        		open: function(event, ui) { $('.ui-widget-overlay, .nyroModalClose, .ajaxDialogClose').live('click', function(){ dialog.dialog('close'); }); },
        		close: function( event, ui ) { $("#dialog").remove(); }
        	});
        	ajax_loader_hide();
        });
        //prevent the browser to follow the link
        return false;
    });

	// Place listings to/from quick list
	if ($(".add_to_favourites").length) {
		$(".add_to_favourites").each(function() {
			var listing_id = $(this).attr("listingid");

			if ((str = $.cookie("favourites")) != null) {
				var favourites_array = str.split('*');
			} else {
				var favourites_array = new Array();
			}
		
			if (in_array(listing_id, favourites_array) === false) {
				$(this).html(not_in_favourites_icon.clone());
			} else {
				$(this).html(in_favourites_icon.clone());
			}
		});

		$(".add_to_favourites, .add_to_favourites_button").click(function() {
			// there may be 'to/from quick list' link button
			// or just an icon
			if ($(this).hasClass("add_to_favourites_button")) {
				var obj = $(this).prev();
			} else {
				var obj = $(this);
			}
			var listing_id = $(obj).attr("listingid");

			if ((str = $.cookie("favourites")) != null) {
				var favourites_array = str.split('*');
			} else {
				var favourites_array = new Array();
			}
			if (in_array(listing_id, favourites_array) === false) {
				favourites_array.push(listing_id);
				var n = noty({
					layout: 'topRight',
					type: 'success',
					timeout: 32000,
					text: to_favourites_msg
				});
				$(obj).html(in_favourites_icon.clone());
			} else {
				for (var count=0; count<favourites_array.length; count++) {
					if (favourites_array[count] == listing_id) {
						delete favourites_array[count];
					}
				}
				var n = noty({
					layout: 'topRight',
					type: 'success',
					timeout: 32000,
					text: from_favourites_msg
				});
				$(obj).html(not_in_favourites_icon.clone());
			}
			$.cookie("favourites", favourites_array.join('*'), {expires: 365, path: "/"});
			return false;
		});
	}
});

function moveSlowly(obj)
{
	elementClick = $(obj).attr("href");
	destination = $(elementClick).offset().top;
	if ($.browser.safari) {
		bodyElement = $("body");
	} else {
		bodyElement = $("html");
	}
	bodyElement.animate({ scrollTop: destination}, 600 );
	return false;
}

var n = null;
function show_msg(msg, msg_type)
{
	msg = typeof msg !== 'undefined' ? msg : false;
	msg_type = typeof type !== 'undefined' ? msg_type : false;
	
	if (!msg_type) {
		if ($(".success_msgs").length) {
			n = noty({
				layout: 'topRight',
				type: 'success',
				timeout: 32000,
				text: $(".success_msgs").html()
			});
		}
		
		if ($(".error_msgs").length) {
			n = noty({
				layout: 'topRight',
				type: 'error',
				text: $(".error_msgs").html()
			});
		}
	} else {
		if (msg_type == 'success') {
			n = noty({
				layout: 'topRight',
				type: 'success',
				timeout: 32000,
				text: msg
			});
		} else if (msg_type == 'error') {
			n = noty({
				layout: 'topRight',
				type: 'error',
				text: msg
			});
		}
	}
}
function close_all_msgs()
{
	if (typeof $.noty !== 'undefined')
		$.noty.closeAll();
}

function ajax_loader_show(msg)
{
	if (msg == null)
		msg = 'Loading...';
	$("#ajax_loader").dialog({
		title: msg,
		resizable: false,
		draggable: false,
		autoOpen: false,
		modal: true,
		closeOnEscape: false,
		width: 250,
		minHeight: 30,
		open: function(event, ui) {$(".ui-dialog-titlebar-close").hide();}
	});
	$("#ajax_loader").dialog('open');
}

function ajax_loader_hide()
{
	$("#ajax_loader").dialog('close');
}

function parseScript(_source) {
	var source = _source;

	// Strip out tags
	while(source.indexOf("<" + "script") > -1 || source.indexOf("<" + "/script") > -1) {
							var s = source.indexOf("<" + "script");
							var s_e = source.indexOf(">", s);
							var e = source.indexOf("<" + "/script", s);
							var e_e = source.indexOf(">", e);
				 
							// Add to scripts array
							scripts.push(source.substring(s_e+1, e));
							// Strip from source
							source = source.substring(0, s) + source.substring(e_e+1);
	}

						// Return the cleaned source
						return source;
}

function evalScripts() {
	// Loop through every script collected and eval it
	for(var i=0; i<scripts.length; i++) {
		try {
			eval(scripts[i]);
		}
		catch(ex) {
			// do what you want here when a script fails
		}
	}
}

function seo_write(cat_name)
{
	var connect_symbol = "_";
    var str = "";
    var i;
    var exp_reg = new RegExp("[a-zA-Z0-9_]");
    var exp_reg2 = new RegExp("[ ]");
    cat_name.toString();
    for (i=0 ; i < cat_name.length; i++) {
        if (exp_reg.test(cat_name.charAt(i))) {
            str = str+cat_name.charAt(i);
        } else {
            if (exp_reg2.test(cat_name.charAt(i))) {
                if (str.charAt(str.length-1) != connect_symbol) {
                    str = str+connect_symbol;
                }
            }
        }
    }
    if (str.charAt(str.length-1) == connect_symbol)
        str = str.substr(0, str.length-1);
    return str;
}

function seo_name_write(cat_name)
{
	if ($("input[name=name]").length)
		$('#seo_name').val(seo_write(cat_name));
	if ($("input[name=login]").length)
		$('#seo_login').val(seo_write(cat_name));

    //$('#seo_name').val(seo_write(cat_name));
    return false;
}

function in_array(val, arr) 
{
	for (var i = 0; i < arr.length; i++) {
		if (arr[i] == val)
			return i;
	}
	return false;
}

function geocodeAddress(term, response, return_predefined_locations, location_index)
{
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode( { 'address': term}, function(results, status) {
		if (status == "OK") {
			var res = new Array;
			var geocoded_names_array = new Array;
			for (var i=0; i<results.length; i++) {
				var res_string = new Array;
				var town = '';
				var city = '';
				var district = '';
				var province = '';
				var state = '';
				var country = '';
				for (var j=0; j<results[i].address_components.length; j++) {
					if (results[i].address_components[j].types[0] == "sublocality") {
						town =results[i].address_components[j].long_name;
						res_string.push(town);
					}
					if (results[i].address_components[j].types[0] == "locality") {
						city =results[i].address_components[j].long_name;
						res_string.push(city);
					}
					if (use_districts)
						if (results[i].address_components[j].types[0] == "administrative_area_level_3") {
							district = results[i].address_components[j].long_name;
							res_string.push(district);
						}
					if (use_provinces)
						if (results[i].address_components[j].types[0] == "administrative_area_level_2") {
							province = results[i].address_components[j].long_name;
							res_string.push(province);
						}
					if (results[i].address_components[j].types[0] == "administrative_area_level_1") {
						state = results[i].address_components[j].long_name;
						res_string.push(state);
					}
					if (results[i].address_components[j].types[0] == "country") {
						country = results[i].address_components[j].long_name;
						res_string.push(country);
					}
				}
				geocoded_name = res_string.join(', ');
				geocoded_names_array.push(geocoded_name);
				
				// --------------------------------------------------------------------------------------------
				// Here we will play with geocoded suggestions
				// --------------------------------------------------------------------------------------------
				if ((town || city) && (city == district || city == province || city == state)) {
					var res_string = new Array;
					if (district != '') res_string.push(district);
					if (province != '') res_string.push(province);
					if (state != '') res_string.push(state);
					res_string.push(country);
					geocoded_name = res_string.join(', ');
					geocoded_names_array.push(geocoded_name);
					if (district && (district == province || district == state)) {
						var res_string = new Array;
						if (province != '') res_string.push(province);
						if (state != '') res_string.push(state);
						res_string.push(country);
						geocoded_name = res_string.join(', ');
						geocoded_names_array.push(geocoded_name);
					}
					if (province && province == state) {
						var res_string = new Array;
						if (state != '') res_string.push(state);
						res_string.push(country);
						geocoded_name = res_string.join(', ');
						geocoded_names_array.push(geocoded_name);
					}
				}
				// --------------------------------------------------------------------------------------------
			}
			geocoded_names_array = geocoded_names_array.getUnique();
			for (var z=0; z<geocoded_names_array.length; z++) {
				res.push({
					label: geocoded_names_array[z],
					value: geocoded_names_array[z]
				});
			}
			// By the way, we can attach results from our DB (predefined locations)
			if (return_predefined_locations) {
				$.post(return_predefined_locations, {query: term}, function(data) {
					if (data = jQuery.parseJSON(data))
						res = res.concat(data);
					if (res) {
						response(res, location_index);
					} else {
						response(false, location_index);
					}
				});
			} else {
				if (res) {
					response(res, location_index);
				} else {
					response(false, location_index);
				}
			}
		} else {
			response(false, location_index);
		}
	});
}

function urlencode (str) {
    // URL-encodes string  
    // 
    // version: 911.718
    // discuss at: http://phpjs.org/functions/urlencode    // +   original by: Philip Peterson
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: AJ
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Brett Zamir (http://brett-zamir.me)    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: travc
    // +      input by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Lars Fischer    // +      input by: Ratheous
    // +      reimplemented by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Joris
    // +      reimplemented by: Brett Zamir (http://brett-zamir.me)
    // %          note 1: This reflects PHP 5.3/6.0+ behavior    // %        note 2: Please be aware that this function expects to encode into UTF-8 encoded strings, as found on
    // %        note 2: pages served as UTF-8
    // *     example 1: urlencode('Kevin van Zonneveld!');
    // *     returns 1: 'Kevin+van+Zonneveld%21'
    // *     example 2: urlencode('http://kevin.vanzonneveld.net/');    // *     returns 2: 'http%3A%2F%2Fkevin.vanzonneveld.net%2F'
    // *     example 3: urlencode('http://www.google.nl/search?q=php.js&ie=utf-8&oe=utf-8&aq=t&rls=com.ubuntu:en-US:unofficial&client=firefox-a');
    // *     returns 3: 'http%3A%2F%2Fwww.google.nl%2Fsearch%3Fq%3Dphp.js%26ie%3Dutf-8%26oe%3Dutf-8%26aq%3Dt%26rls%3Dcom.ubuntu%3Aen-US%3Aunofficial%26client%3Dfirefox-a'
    str = (str+'').toString();
        // Tilde should be allowed unescaped in future versions of PHP (as reflected below), but if you want to reflect current
    // PHP behavior, you would need to add ".replace(/~/g, '%7E');" to the following.
    return encodeURIComponent(str).replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
    //return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}

function isset(varname)  {
  if(typeof( window[ varname ] ) != "undefined") return true;
  else return false;
}

//Return new array with duplicate values removed
Array.prototype.getUnique = function(){
	var u = {}, a = [];
	for(var i = 0, l = this.length; i < l; ++i){
		if(this[i] in u)
			continue;
		a.push(this[i]);
		u[this[i]] = 1;
	}
	return a;
}

var delay = (function() {
	var timer = 0;
	return function(callback, ms) {
		clearTimeout (timer);
		timer = setTimeout(callback, ms);
		};
})();
