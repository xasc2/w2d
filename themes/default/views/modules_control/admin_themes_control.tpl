{include file="backend/admin_header.tpl"}

           		<div class="content">
                     <h3>{$LANG_THEMES_LIST}</h3>
                     {if $themes|@count > 0}
                     <form action="" method="post">
                     <table class="standardTable" border="0" cellpadding="2" cellspacing="2">
                       <tr>
                         <th>{$LANG_THEME_TITLE_TH}</th>
                         <th>{$LANG_THEME_SETUP_TH}</th>
                         <th>{$LANG_THEME_DESCRIPTION_TH}</th>
                       </tr>
                       {foreach from=$themes item=theme_item}
                       <tr>
                         <td>
                             {$theme_item.name}
                         </td>
                         <td>
                         	{if $theme_item.require_installation}
                         		{if $theme_item.installed}
                         		<input type="hidden" name="installed_{$theme_item.dir}" value="1">
                         		{/if}
                            	<input type="checkbox" name="install_{$theme_item.dir}" value="1" {if $theme_item.installed}checked{/if}>
                            {else}
                            	<img src="{$public_path}images/icons/accept.png" />
                            {/if}
                         </td>
                         <td>
                             {$theme_item.description}&nbsp;
                         </td>
                       </tr>
                       {/foreach}
                     </table>
                     <input class="button save_button" type=submit name="submit" value="{$LANG_BUTTON_SAVE_CHANGES}">
                     </form>
                     {/if}
                </div>
           
{include file="backend/admin_footer.tpl"}