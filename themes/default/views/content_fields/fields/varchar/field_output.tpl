{if $current_listing}{assign var="frontend_mode" value="both"}{else}{assign var="frontend_mode" value=$field->frontend_mode}{/if}

					{if $field->value != ''}
					<div class="content_field_output field_type_{$field->type}  field_{$field->seo_name}">
					{if $frontend_mode == 'name'}
						<strong>{$field->name}</strong>: 
					{elseif $frontend_mode == 'frontend_name' && ($field->frontend_name || $field->name)}
						<strong>{$field->frontend_name}</strong>: 
					{elseif $frontend_mode == 'icon' && $field->field_icon_image}
						<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
					{elseif $frontend_mode == 'both'}
						{if ($field->frontend_name || $field->name) && $field->field_icon_image}
						<strong><img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> {if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}</strong>: 
						{elseif $field->field_icon_image}
						<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
						{else}
						<strong>{if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}</strong>: 
						{/if}
					{/if}
					{if $field->options.0.is_numeric && $VH->is_numeric($field->value)}{$VH->number_format($field->value, 2, $decimals_separator, $thousands_separator)}{else}{$field->value}{/if}
					</div>
					{/if}