{if $current_listing}{assign var="frontend_mode" value="both"}{else}{assign var="frontend_mode" value=$field->frontend_mode}{/if}

					{if $field->value != ''}
					<div class="content_field_output tablerow_odd_even">
						<div class="field_tablerow_left">
						{if $frontend_mode == 'name'}
							<strong>{$field->name}</strong>:<br />
						{elseif $frontend_mode == 'frontend_name' && ($field->frontend_name || $field->name)}
							<strong>{$field->frontend_name}</strong>:<br />
						{elseif $frontend_mode == 'icon' && $field->field_icon_image}
							<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
						{elseif $frontend_mode == 'both'}
							{if ($field->frontend_name || $field->name) && $field->field_icon_image}
							<strong><img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> {if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}</strong>:<br />
							{elseif $field->field_icon_image}
							<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" />
							{else}
							<strong>{if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}</strong>:<br />
							{/if}
						{/if}
						</div>
						<div class="field_tablerow_right">
							{if !$enable_redirect}
								<a href="{$value}" target="_blank" title="{$value}">{$value}</a>
							{else}
								<a href="{$VH->site_url("redirect/$field_value_id")}" rel="nofollow" target="_blank" title="{$value}">{$value}</a>
							{/if}
						</div>
						<div class="clear_float"></div>
					</div>
					{/if}