{if $current_listing}{assign var="frontend_mode" value="both"}{else}{assign var="frontend_mode" value=$field->frontend_mode}{/if}
{assign var="field_id" value=$field->seo_name|cat:"_"|cat:$field->group_custom_name|cat:"_"|cat:$field->object_id|cat:"_"|cat:$VH->rand()}
					
					<div class="content_field_output field_type_{$field->type}  field_{$field->seo_name}">
						{if $frontend_mode == 'name'}
							<strong>{$field->name}</strong>:<br />
						{elseif $frontend_mode == 'frontend_name' && ($field->frontend_name || $field->name)}
							<strong>{$field->frontend_name}</strong>:<br />
						{elseif $frontend_mode == 'icon' && $field->field_icon_image}
							<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
						{elseif $frontend_mode == 'both'}
							{if ($field->frontend_name || $field->name) && $field->field_icon_image}
							<strong><img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> {if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}</strong>:<br />
							{elseif $field->field_icon_image}
							<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" />
							{else}
							<strong>{if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}</strong>:<br />
							{/if}
						{/if}

						<script language="javascript" type="text/javascript">
							var lclDate = new Date();
							var timezone_offset = lclDate.getTimezoneOffset()*60;
		
							var events_array_{$field_id} = [];
							{foreach from=$field->value item=event}
							events_array_{$field_id}.push({ldelim}
								id: {$event.id},
								title: "{$VH->addslashes($event.title)}",
								description: "{$VH->addslashes($event.description)}",
								start_date: {$event.start_date},
								start: {$event.start_date},
								end_date: {$event.end_date},
								end: {$event.end_date},
								cycle_days_monday: {if $event.cycle_days_monday}true{else}false{/if},
								cycle_days_tuesday: {if $event.cycle_days_tuesday}true{else}false{/if},
								cycle_days_wednesday: {if $event.cycle_days_wednesday}true{else}false{/if},
								cycle_days_thursday: {if $event.cycle_days_thursday}true{else}false{/if},
								cycle_days_friday: {if $event.cycle_days_friday}true{else}false{/if},
								cycle_days_saturday: {if $event.cycle_days_saturday}true{else}false{/if},
								cycle_days_sunday: {if $event.cycle_days_sunday}true{else}false{/if},
								allDay: {if $enable_time}false{else}true{/if}
							{rdelim});
							{/foreach}
	
							$(document).ready( function() {ldelim}
								var calendar_{$field_id} = $('#fullcalendar_{$field_id}');
	
								{if $current_language && $current_language != 'en'}
								var date_format = $.datepicker.regional["{$current_language}"].dateFormat;
								{else}
								var date_format = 'mm/dd/yy';
								{/if}
		
								calendar_{$field_id}.fullCalendar({ldelim}
									height: 400,
									header: {ldelim}
										left: 'prev,next today',
										center: 'title',
										right: 'month,agendaWeek,agendaDay'
									{rdelim},
									eventMouseover: function(calEvent, jsEvent, view) {ldelim}
										for (var i=0; i<events_array_{$field_id}.length; i++) {ldelim}
											if (events_array_{$field_id}[i].id == calEvent.id) {ldelim}
												var hover_event = events_array_{$field_id}[i];
												break;
											{rdelim}
										{rdelim}
		
										var hover_start_date = new Date(hover_event.start_date*1000 + timezone_offset*1000);
										var hover_end_date = new Date(hover_event.end_date*1000 + timezone_offset*1000);
										var hover_content = $.datepicker.formatDate(date_format, hover_start_date);
										{if $enable_time}
										hover_content = hover_content+' '+$.fullCalendar.formatDate(hover_start_date, "HH:mm");
										{/if}
										hover_content = hover_content+' - '+$.datepicker.formatDate(date_format, hover_end_date);
										{if $enable_time}
										hover_content = hover_content+' '+$.fullCalendar.formatDate(hover_end_date, "HH:mm");
										{/if}
										if (hover_event.description)
											hover_content = hover_content+'<br />'+hover_event.description;
		
										$(this).append('<div id=\"event_{$field->seo_name}_'+hover_event.id+'\" class=\"fc-event-hover\">'+hover_content+'</div>');
										$(this).css('z-index', 100);
									{rdelim},
									eventMouseout: function(calEvent, jsEvent, view) {ldelim}
										$('#event_{$field->seo_name}_'+calEvent.id).remove();
										$(this).css('z-index', 8);
									{rdelim},
									timeFormat: 'H:mm'
		
									{if $current_language && $current_language != 'en'} ,
									isRTL:  $.datepicker.regional["{$current_language}"].isRTL,
									firstDay: $.datepicker.regional["{$current_language}"].firstDay,
									monthNames: $.datepicker.regional["{$current_language}"].monthNames,
									monthNamesShort: $.datepicker.regional["{$current_language}"].monthNamesShort,
									dayNames: $.datepicker.regional["{$current_language}"].dayNames,
									dayNamesShort: $.datepicker.regional["{$current_language}"].dayNamesShort,
									buttonText: {ldelim}
										today: $.datepicker.regional["{$current_language}"].currentText
									{rdelim}
									{/if}
								{rdelim});
								
								function manageEvent(event_id) {ldelim}
									for (var i=0; i<events_array_{$field_id}.length; i++) {ldelim}
										if (events_array_{$field_id}[i].id == event_id) {ldelim}
											load_event = events_array_{$field_id}[i];
											break;
										{rdelim}
									{rdelim}
									var title = load_event.title;
									var description = load_event.description;
									var start_date = load_event.start_date;
									var end_date = load_event.end_date;
									var rmonday = load_event.cycle_days_monday;
									var rtuesday = load_event.cycle_days_tuesday;
									var rwednesday = load_event.cycle_days_wednesday;
									var rthursday = load_event.cycle_days_thursday;
									var rfriday = load_event.cycle_days_friday;
									var rsaturday = load_event.cycle_days_saturday;
									var rsunday = load_event.cycle_days_sunday;
	
									var new_event = {ldelim}
										id: event_id,
										title: title,
										description: description,
										start_date: start_date,
										end_date: end_date,
										cycle_days_monday: rmonday,
										cycle_days_tuesday: rtuesday,
										cycle_days_wednesday: rwednesday,
										cycle_days_thursday: rthursday,
										cycle_days_friday: rfriday,
										cycle_days_saturday: rsaturday,
										cycle_days_sunday: rsunday,
										allDay: {if $enable_time}false{else}true{/if}
									{rdelim};
		
									var start_tmstmp = start_date;
									var end_tmstmp = end_date;
									if (rmonday || rtuesday || rwednesday || rthursday || rfriday || rsaturday || rsunday) {ldelim}
										var r_event = jQuery.extend(true, {ldelim}{rdelim}, new_event);
										while (start_tmstmp <= end_tmstmp) {ldelim}
											var r_date = new Date(start_tmstmp*1000 + timezone_offset*1000).getDay()
											if (rmonday && r_date == 1) {ldelim}
												r_event.start_date = start_tmstmp;
												r_event.end_date = start_tmstmp;
												r_event.start = start_tmstmp;
												r_event.end = start_tmstmp;
												calendar_{$field_id}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
												valid_event = true;
											{rdelim}
											if (rtuesday && r_date == 2) {ldelim}
												r_event.start_date = start_tmstmp;
												r_event.end_date = start_tmstmp;
												r_event.start = start_tmstmp;
												r_event.end = start_tmstmp;
												calendar_{$field_id}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
												valid_event = true;
											{rdelim}
											if (rwednesday && r_date == 3) {ldelim}
												r_event.start_date = start_tmstmp;
												r_event.end_date = start_tmstmp;
												r_event.start = start_tmstmp;
												r_event.end = start_tmstmp;
												calendar_{$field_id}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
												valid_event = true;
											{rdelim}
											if (rthursday && r_date == 4) {ldelim}
												r_event.start_date = start_tmstmp;
												r_event.end_date = start_tmstmp;
												r_event.start = start_tmstmp;
												r_event.end = start_tmstmp;
												calendar_{$field_id}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
												valid_event = true;
											{rdelim}
											if (rfriday && r_date == 5) {ldelim}
												r_event.start_date = start_tmstmp;
												r_event.end_date = start_tmstmp;
												r_event.start = start_tmstmp;
												r_event.end = start_tmstmp;
												calendar_{$field_id}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
												valid_event = true;
											{rdelim}
											if (rsaturday && r_date == 6) {ldelim}
												r_event.start_date = start_tmstmp;
												r_event.end_date = start_tmstmp;
												r_event.start = start_tmstmp;
												r_event.end = start_tmstmp;
												calendar_{$field_id}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
												valid_event = true;
											{rdelim}
											if (rsunday && r_date == 0) {ldelim}
												r_event.start_date = start_tmstmp;
												r_event.end_date = start_tmstmp;
												r_event.start = start_tmstmp;
												r_event.end = start_tmstmp;
												calendar_{$field_id}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, r_event), true);
												valid_event = true;
											{rdelim}
											start_tmstmp = start_tmstmp + 86400;
										{rdelim}
									{rdelim} else {ldelim}
										if (start_tmstmp <= end_tmstmp) {ldelim}
											new_event.start = new_event.start_date;
											new_event.end = new_event.end_date;
											calendar_{$field_id}.fullCalendar('renderEvent', jQuery.extend(true, {ldelim}{rdelim}, new_event), true);
											valid_event = true;
										{rdelim}
									{rdelim}
								{rdelim}
								
								{foreach from=$field->value item=event}
								manageEvent({$event.id});
								{/foreach}
							{rdelim});
						</script>
						<div id="fullcalendar_{$field_id}" class="fullcalendar"></div>
					</div>