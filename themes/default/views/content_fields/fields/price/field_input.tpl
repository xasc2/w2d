			<div class="admin_option_name">
				{$field->name}{if $field->required}<span class="red_asterisk">*</span>{/if}
			</div>
			<div class="admin_option_description">
				{$field->description|nl2br}
			</div>
			{if $options|@count>1}
			<select name="field_currency_{$field->seo_name}" style="min-width: 30px;">
				<option value="-1">{$LANG_SELECT_CURRENCY}</option>
				{foreach from=$options item=option}
				<option value="{$option.id}" {if $option.id == $field_currency}selected{/if}>{$option.currency_symbol}</option>
				{/foreach}
			</select>
			&nbsp;&nbsp;
			{else}
			{$options.0.currency_symbol}
			<input type="hidden" name="field_currency_{$field->seo_name}" value="{$options.0.id}" />
			{/if}
			<input type="text" name="field_value_{$field->seo_name}" value="{$field_value}" size="5">
			<br />
			<br />