<div class="paginator paginator_prev_next">
	<span class="paginator_list">
		{if $prev_listing}
		<div class="paginator_prev_link"><a href="{$VH->site_url($prev_listing->url())}" id="prev_page" class="current_page" title="{$prev_listing->title()}">{$LANG_PAGINATOR_PREV_LISTING}</a></div>
		{/if}
		{$LANG_PAGINATOR_LISTING_1} {$listing_number} {$LANG_PAGINATOR_LISTING_2} {$listings_count}
		{if $next_listing}
		<div class="paginator_next_link"><a href="{$VH->site_url($next_listing->url())}" id="next_page" class="current_page" title="{$next_listing->title()}">{$LANG_PAGINATOR_NEXT_LISTING}</a></div>
		{/if}
		<div class="clear_float"></div>
	</span>
</div>