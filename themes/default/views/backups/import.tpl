{include file="backend/admin_header.tpl"}

<script language="javascript" type="text/javascript"><!--
	$(document).ready( function() {ldelim}
		var default_behavior = true;
		$("#import_backup_form").submit( function() {ldelim}
			$( "#dialog:ui-dialog" ).dialog( "destroy" );
			$( "#dialog-confirm" ).dialog({ldelim}
				resizable: false,
				height:140,
				modal: true,
				buttons: {ldelim}
					"Yes": function() {ldelim}
						$( this ).dialog( "close" );
						window.location.href = "{$VH->site_url('admin/backups/export/')}";
					{rdelim},
					"No": function() {ldelim}
						default_behavior = false;
						$("#import_backup_form").submit();
						$( this ).dialog( "close" );
					{rdelim},
					"{$LANG_BUTTON_CANCEL}": function() {ldelim}
						$( this ).dialog( "close" );
					{rdelim}
				{rdelim}
			{rdelim});
			if (default_behavior)
				return false;
		{rdelim});
	{rdelim});
</script>

                <div class="content">
                	{$VH->validation_errors()}
                	<h3>{$LANG_IMPORT_BACKUP}</h3>
                	<h4>{$LANG_IMPORT_BACKUP_DESCR} <a href="{$VH->site_url('admin/backups/export/')}">{$LANG_CREATE_NEW_BACKUP_LINK}</a></h4>

					<form action="" method="post" id="import_backup_form" enctype='multipart/form-data'>
					<input type="hidden" name="submit_flag" value=1 />
					<div id="dialog-confirm" style="display: none" title="{$LANG_BACKUP_WARNING_QUESTION}">
						<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>{$LANG_BACKUP_WARNING_MSG}</p>
					</div>
					<div class="admin_option">
                     	<div class="admin_option_name" >
                     		{$LANG_SELECT_BACKUP}
                     	</div>
                     	{if $backups_files|@count}
                     	<select name="backup_file" value="" size="8" class="admin_option_input">
                     		{foreach from=$backups_files item=file}
                     		<option value="{$file}">{$file}</option>
                     		{/foreach}
                     	</select>
                     	{else}
                     	{$LANG_NO_BACKUPS}
                     	{/if}
					</div>
					
					<div class="admin_option">
                     	<div class="admin_option_name" >
                     		{$LANG_UPLOAD_BACKUP}
                     	</div>
                     	<input type=file name="upload_backup_file" />
					</div>
					
					<input class="button save_button" type=submit id="submit_button" name="submit_button" value="{$LANG_IMPORT_BACKUP_BUTTON}">
					</form>
				</div>

{include file="backend/admin_footer.tpl"}