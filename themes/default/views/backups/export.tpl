{include file="backend/admin_header.tpl"}

                <div class="content">
                	{$VH->validation_errors()}
                	<h3>{$LANG_CREATE_NEW_BACKUP}</h3>
                	<h4>{$LANG_CREATE_NEW_BACKUP_DESCR}</h4>

					<form action="" method="post">
					<div class="admin_option">
                     	<div class="admin_option_name" >
                     		{$LANG_BACKUPS_LABEL}<span class="red_asterisk">*</span>
                     	</div>
                     	<div class="admin_option_description" >
                     		{$LANG_BACKUPS_LABEL_DESCR}
                     	</div>
                     	<input type=text name="label" value="" size="50" class="admin_option_input">
                     	<label><input type="checkbox" name="download" value=1 /> {$LANG_DOWNLOAD_BACKUP_CHECKBOX_DESCR}</label>
					</div>
					
					<input class="button save_button" type=submit name="submit" value="{$LANG_CREATE_BACKUP_BUTTON}">
					</form>
				</div>

{include file="backend/admin_footer.tpl"}