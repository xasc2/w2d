{if $listings_paginator->count() != 0}
<span class="paginator_found">
	{$LANG_PAGINATION_FOUND_1} <b>{$listings_paginator->count()}</b> {$LANG_PAGINATION_FOUND_2} | {$LANG_PAGINATION_FOUND_3} <b>{$listings_paginator->getPage()}</b> {$LANG_PAGINATION_FOUND_4} <b>{$listings_paginator->countPages()}</b> {$LANG_PAGINATION_FOUND_5}</h1>
</span>
<div class="clear_float"></div>

<div class="paginator_orderby">
	{if $wrapper_object}
		{$wrapper_object->render_orderby()}
	{/if}
</div>

<div class="paginator_select_page">
	{$listings_paginator->selectPageBlock()}
</div>
<div class="clear_float"></div>

<div class="listings_list">
	{if $wrapper_object && $VH->method_exists($wrapper_object, 'render')}
		{$wrapper_object->render()}
	{else}
		{foreach from=$items_array item=item}
			{$item->view($view_name)}
		{/foreach}
	{/if}
	{$listings_paginator->placeLinksToHtml()}
</div>
{else}
{$LANG_NO_SEARCH}
{/if}