{if $locations_tree}
{assign var=base_url value=$VH->getBaseUrlForLocation()}
		<div><span class="search_in_span">{$LANG_LOCATIONS_SEARCH_IN_1} {$LANG_LOCATIONS_SEARCH_IN_2}:</span>&nbsp;&nbsp;&nbsp;<img src="{$public_path}images/icons/world.png" />&nbsp;<a href="{$VH->site_url("location/any/$base_url")}" rel="nofollow">{$LANG_LOCATIONS_EVERYWHERE}</a></div>
		<div class="clear_float"></div>
		
		<table width="100%" cellspacing="0">
		<tr>
			{include file='frontend/locations/navigate.tpl' assign=template}
			{assign var=td_padding_pixels value=0}
			{assign var=i value=0}
			<td style="padding-right: {$td_padding_pixels}px" valign="top">
				{section name=key loop=$locations_tree start=0 step=3}
					{$locations_tree[key]->render($template, $is_counter, $max_depth, $current_location->seo_name, 'class="labeled_location_selected"', 'class="labeled_location_root"', $is_only_labeled)}
				{/section}
			</td>
			<td style="padding-right: {$td_padding_pixels}px" valign="top">
				{section name=key loop=$locations_tree start=1 step=3}
					{$locations_tree[key]->render($template, $is_counter, $max_depth, $current_location->seo_name, 'class="labeled_location_selected"', 'class="labeled_location_root"', $is_only_labeled)}
				{/section}
			</td>
			<td style="padding-right: {$td_padding_pixels}px" valign="top">
				{section name=key loop=$locations_tree start=2 step=3}
					{$locations_tree[key]->render($template, $is_counter, $max_depth, $current_location->seo_name, 'class="labeled_location_selected"', 'class="labeled_location_root"', $is_only_labeled)}
				{/section}
			</td>
		</tr>
		</table>
{/if}