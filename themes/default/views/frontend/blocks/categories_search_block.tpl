
<!-- CATEGORIES BLOCK -->

				{if $categories_tree|@count}
					<div id="search_by_category_tree"></div>
					
					<script language="javascript" type="text/javascript">
						var selected_categories_array = [{if $search_categories_array}{$VH->implode(', ', $search_categories_array)}{/if}];

						// JsTree v1.0 version
						$("#search_by_category_tree").jstree({ldelim}
							"themes" : {ldelim}
								"theme" : "default",
								"url" : "{$smarty_obj->getFileInTheme('css/jsTree_themes_v10/default/style.css')}",
								"icons" : false
							{rdelim},
							"json_data" : {ldelim}
								"data" : [
								{include file='frontend/categories/search_normal.tpl' assign=template}
								{foreach from=$categories_tree item=category}
									{$category->render($template, $is_counter, $max_depth, $search_categories_array, ', "checked" : "true"', ',"state" : "closed"')}
								{/foreach}
								]
							{rdelim},
							"core" : {ldelim}
									"html_titles" : true
							{rdelim},
							"plugins" : ["themes","json_data","checkbox"]
						{rdelim}).bind("load_node.jstree", function () {ldelim}
							$('#search_by_category_tree li').each(function() {ldelim}
								var check_this_id = parseInt(($(this).attr("id")+"").replace('search_', ''));
								if ($.inArray(check_this_id, selected_categories_array) != -1) {ldelim}
									$.jstree._reference("#search_by_category_tree").check_node($(this));
								{rdelim}
							{rdelim});
						{rdelim});
					</script>
				{/if}

<!-- /CATEGORIES BLOCK -->
