									{php}
										if ($args['predefined_location_id'])
											$predefined_location_id = $args['predefined_location_id'];
										else
											$predefined_location_id = 0;
										include_once(MODULES_PATH . 'listings/classes/listing_location.class.php');
										$location = new listingLocation();
										$location->setLocationFromArray(array('location'=>$predefined_location_id, 'predefined_location_id'=>$predefined_location_id, 'address_line_1'=>'', 'address_line_2'=>''));
										$this->assign('location', $location);
									{/php}
									{$location->renderDropBoxes()}
									<script language="javascript" type="text/javascript">
										$(document).ready(function() {ldelim}
											// Form submit event
											$("#search_form").submit( function() {ldelim}
												var predefined_location_id;
												$(".location_dropdown_list").each(function() {ldelim}
													if ($(this).find("option:selected").val() != '')
														predefined_location_id = $(this).find("option:selected").val();
												{rdelim});
												if (predefined_location_id) {ldelim}
						                			global_js_url = global_js_url + 'predefined_location_id/' + urlencode(predefined_location_id) + '/';
						                		{rdelim}
						                	{rdelim});
										{rdelim});
									</script>