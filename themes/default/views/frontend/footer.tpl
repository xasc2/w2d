					<tr>
						<td class="footer_line" colspan="3">
							{$VH->render_banner('footer')}
							{$VH->js_advertisement_append('footer')}

							<div id="footer">
								{$site_settings.footer_copyrights|nl2br}
								{$VH->buildContentPagesMenu_bottom($CI)}
							</div>
						</td>
					</tr>
				</table>
		</div>
		<!-- Content Ends -->
	</div>

{if $system_settings.google_analytics_profile_id}
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">
	_uacct = "{$system_settings.google_analytics_account_id}";
	urchinTracker();
</script>
{/if}
</body>
</html>