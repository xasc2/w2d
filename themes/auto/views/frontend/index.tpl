{include file="frontend/header.tpl"}

			<tr>
				<td id="left_sidebar">
				{include file="frontend/left-sidebar.tpl"}
				</td>
      			<td id="content_block" valign="top">
      				<div id="content_wrapper">
      					{if $CI->load->is_module_loaded('rss')}
                        	<h1 id="index_header">
                        		<div class="rss_icon_index"">
                        			<a href="{$VH->getRssUrl()}" title="{$VH->getRssTitle()}">
                        				<nobr><img src="{$public_path}images/feed.png" />&nbsp;<img src="{$public_path}images/rss.png" /></nobr>
                        			</a>
                        		</div>
                        	</h1>
                        {/if}
                        
                        {render_frontend_block
                        	block_type='listings'
							block_template="frontend/blocks/slider_classifieds.tpl"
							search_type='cars_classifieds'
							search_location=$current_location
							limit=10
							search_status=1
							search_users_status=2
							orderby=random
							slider_height=180
                        }

                        {render_frontend_block
							block_type='categories'
							block_template="frontend/blocks/index-categories.tpl"
							is_counter=true
							max_depth=1
                        }
                 	</div>
                </td>
                <td id="right_sidebar">
                {include file="frontend/right-sidebar.tpl"}
                </td>
			</tr>

{include file="frontend/footer.tpl"}