
<!-- RIGHT SIDEBAR -->

{include file="frontend/search_block.tpl"}

{if ($listings)}
	{render_frontend_block
		block_type='map_and_markers'
		block_template='frontend/blocks/map_scrolling.tpl'
		existed_listings=$listings
		clasterization=false
		map_height=350
	}
{/if}

{$VH->render_banner('right_sidebar')}
{$VH->js_advertisement_append('right_sidebar')}
             	
<!-- /RIGHT SIDEBAR -->
