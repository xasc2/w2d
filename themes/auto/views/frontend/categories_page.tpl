{include file="frontend/header.tpl"}

			<tr>
				<td id="left_sidebar">
				{include file="frontend/left-sidebar.tpl"}
				</td>
      			<td id="content_block" valign="top">
      				<div id="content_wrapper">
      					<div class="breadcrumbs">
      						<div class="breadcrumbs_text"">
      							<a href="{$VH->index_url()}">{$LANG_HOME_PAGE}</a>{foreach from=$breadcrumbs item="source_page" key="source_url"} » <a href="{$source_url}">{$source_page}</a> » {/foreach}<span>{$current_category->name}</span>
      						</div>
      						{if $CI->load->is_module_loaded('rss')}
                        		<div class="rss_icon"">
                        			<a href="{$VH->getRssUrl()}" title="{$VH->getRssTitle()}">
                        				<nobr><img src="{$public_path}images/feed.png" />&nbsp;<img src="{$public_path}images/rss.png" /></nobr>
                        			</a>
                        		</div>
                        	{/if}
                        	<div class="clear_float"></div>
      					</div>

	      				{if $current_category->children|@count}
						<h1>{$LANG_SUBCATEGORIES}</h1>
                        <div class="subcategories_list">
                        	{foreach from=$current_category->children item=category}
                        	{assign var=subcategory_id value=$category->id}
                        	{assign var=listings_count value=$category->countListings()}
                        		<span class="subcategory_item">
                        			{if $listings_count>0}<b>{/if}<a href="{$VH->site_url($category->getUrl())}" class="subcategory">{$category->name}&nbsp;({$listings_count})</a>{if $listings_count>0}<b>{/if}&nbsp;&nbsp;
                        		</span>
                        	{/foreach}
                        	<div class="clear_float"></div>
                        </div>
                        {/if}

                        {render_frontend_block
                        	block_type='listings'
                        	block_template='frontend/blocks/with_paginator.tpl'
                        	items_array=$listings
                        	view_name=$view->view
                        	view_format=$view->format
                        	type=$type
                        	listings_paginator=$listings_paginator
                        	order_url=$order_url
                        	orderby=$orderby
                        	direction=$direction
                        }
                 	</div>
                </td>
                <td id="right_sidebar">
                {include file="frontend/right-sidebar.tpl"}
                </td>
			</tr>

{include file="frontend/footer.tpl"}