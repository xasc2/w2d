			<div class="admin_option_name">
				{$field->name}{if $field->required}<span class="red_asterisk">*</span>{/if}
			</div>
			<div class="admin_option_description">
				{$field->description|nl2br}
			</div>
			<select name="field_{$field->seo_name}" style="min-width: 200px;">
				<option value="-1">Select year</option>
				<option {if $field->value == "2014"}selected{/if} value="2014">2014</option>
				<option {if $field->value == "2013"}selected{/if} value="2013">2013</option>
				<option {if $field->value == "2012"}selected{/if} value="2012">2012</option>
				<option {if $field->value == "2011"}selected{/if} value="2011">2011</option>
				<option {if $field->value == "2010"}selected{/if} value="2010">2010</option>
				<option {if $field->value == "2009"}selected{/if} value="2009">2009</option>
				<option {if $field->value == "2008"}selected{/if} value="2008">2008</option>
				<option {if $field->value == "2007"}selected{/if} value="2007">2007</option>
				<option {if $field->value == "2006"}selected{/if} value="2006">2006</option>
				<option {if $field->value == "2005"}selected{/if} value="2005">2005</option>
				<option {if $field->value == "2004"}selected{/if} value="2004">2004</option>
				<option {if $field->value == "2003"}selected{/if} value="2003">2003</option>
				<option {if $field->value == "2002"}selected{/if} value="2002">2002</option>
				<option {if $field->value == "2001"}selected{/if} value="2001">2001</option>
				<option {if $field->value == "2000"}selected{/if} value="2000">2000</option>
				<option {if $field->value == "1999"}selected{/if} value="1999">1999</option>
				<option {if $field->value == "1998"}selected{/if} value="1998">1998</option>
				<option {if $field->value == "1997"}selected{/if} value="1997">1997</option>
				<option {if $field->value == "1996"}selected{/if} value="1996">1996</option>
				<option {if $field->value == "1995"}selected{/if} value="1995">1995</option>
				<option {if $field->value == "1994"}selected{/if} value="1994">1994</option>
				<option {if $field->value == "1993"}selected{/if} value="1993">1993</option>
				<option {if $field->value == "1992"}selected{/if} value="1992">1992</option>
				<option {if $field->value == "1991"}selected{/if} value="1991">1991</option>
				<option {if $field->value == "1990"}selected{/if} value="1990">1990</option>
				<option {if $field->value == "1989"}selected{/if} value="1989">1989</option>
				<option {if $field->value == "1988"}selected{/if} value="1988">1988</option>
				<option {if $field->value == "1987"}selected{/if} value="1987">1987</option>
				<option {if $field->value == "1986"}selected{/if} value="1986">1986</option>
				<option {if $field->value == "1985"}selected{/if} value="1985">1985</option>
				<option {if $field->value == "1984"}selected{/if} value="1984">1984</option>
				<option {if $field->value == "1983"}selected{/if} value="1983">1983</option>
				<option {if $field->value == "1982"}selected{/if} value="1982">1982</option>
				<option {if $field->value == "1981"}selected{/if} value="1981">1981</option>
				<option {if $field->value == "1980"}selected{/if} value="1980">1980</option>
				<option {if $field->value == "1979"}selected{/if} value="1979">1979</option>
				<option {if $field->value == "1978"}selected{/if} value="1978">1978</option>
				<option {if $field->value == "1977"}selected{/if} value="1977">1977</option>
				<option {if $field->value == "1976"}selected{/if} value="1976">1976</option>
				<option {if $field->value == "1975"}selected{/if} value="1975">1975</option>
				<option {if $field->value == "1974"}selected{/if} value="1974">1974</option>
				<option {if $field->value == "1973"}selected{/if} value="1973">1973</option>
				<option {if $field->value == "1972"}selected{/if} value="1972">1972</option>
				<option {if $field->value == "1971"}selected{/if} value="1971">1971</option>
				<option {if $field->value == "1970"}selected{/if} value="1970">1970</option>
			</select>
			<br />
			<br />