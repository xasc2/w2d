<?php

$language['LANG_FRONTEND_SEARCH_BY_MAKE_MODEL'] = "Pesquisa por marca/modelo";
$language['LANG_CHOOSE_MAKE_MODEL'] = "Escolha a marca e modelo";
$language['LANG_CHOOSE_MAKE_MODEL_DESCRIPTION'] = "Clique em 'escolher este modelo' link ";
$language['LANG_CHOOSE_MODEL'] = "escolher este modelo";
$language['LANG_LISTING_SELECTED_MAKE_MODEL'] = "marca e modelo selecionado:";
?>