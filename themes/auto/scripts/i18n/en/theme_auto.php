<?php

$language['LANG_FRONTEND_SEARCH_BY_MAKE_MODEL'] = "Search by make/model";
$language['LANG_CHOOSE_MAKE_MODEL'] = "Choose make and model";
$language['LANG_CHOOSE_MAKE_MODEL_DESCRIPTION'] = "Click 'choose this model' link";
$language['LANG_CHOOSE_MODEL'] = "choose this model";
$language['LANG_LISTING_SELECTED_MAKE_MODEL'] = "Selected make and model:";
?>