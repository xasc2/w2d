<?php

function auto_theme_frontend_addJsFiles($CI)
{
	$system_settings = registry::get('system_settings');
	$view = $CI->load->view();
	
	$view->addExternalJsFile('https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js');
	$view->addExternalJsFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js');

	$view->addCssFile('frontend_style.css');
	$view->addCssFile('anythingslider/css/anythingslider.css');
	$view->addCssFile('anythingslider/css/theme-minimalist-round.css');
	$view->addJsFile('jquery.anythingslider.min.js');
	if ($CI->router->fetch_module() == 'frontend' && $CI->router->fetch_method() == 'listings') {
		$view->addCssFile('jquery.lightbox-0.5.css');
		$view->addJsFile('jquery.lightbox-0.5.pack.js');
	}
	
	// We may set listings_fields_visible array artificially,
	// for example for this theme on index page we don't need to render listings by types
	if ($CI->router->fetch_module() == 'frontend' && $CI->router->fetch_method() == 'index') {
		$CI->db->select();
		$CI->db->from('listings_fields_visibility');
		$query = $CI->db->get();
		$db_array = $query->result_array();
		foreach ($db_array As $key=>$row)
			if ($row['page_name'] == 'index')
			$db_array[$key]['format'] = 0;
	
		$CI->load->model('types', 'types_levels');
		$types = $CI->types->getTypesLevels();
		foreach ($types AS $type) {
			$type_exist_in_table = false;
			foreach ($db_array As $key=>$row) {
				if ($row['type_id'] == $type->id && $row['page_name'] == 'index') {
					$type_exist_in_table = true;
				}
			}
			if (!$type_exist_in_table) {
				$db_array[] = array('type_id' => $type->id, 'page_name' => 'index', 'format' => 0);
			}
		}
		registry::set('listings_fields_visible', $db_array);
	}
	
	// There were some problems with full cookie.js name of this file, so now it was renamed to coo_kie.js
	$view->addJsFile('jquery.coo_kie.js');
	$view->addJsFile('jquery.rater.js');

	$view->addCssFile('ui/jquery-ui-1.8.9.custom.css');
	$view->addCssFile('ui/query-ui-customizations.css');

	$view->addJsFile('noty/jquery.noty.js');
	$view->addJsFile('noty/themes/default.js');
	$view->addJsFile('noty/layouts/topRight.js');
	
	$view->addJsFile('js_functions.js');
	$view->addJsFile('jquery.jqURL.js');

	$view->addJsFile('swfobject.js');

	$language_code = registry::get('current_language');
	if ($language_code && $language_code != 'en')
		$view->addJsFile('content_fields/i18n/jquery-ui-i18n.js');

	$view->addExternalJsFile('http://maps.googleapis.com/maps/api/js?v=3.4&sensor=false&language=' . $language_code);
	$view->addJsFile('google_maps_view.js');
}

function auto_theme_backend_addJsFiles($CI)
{
	$system_settings = registry::get('system_settings');
	$view = $CI->load->view();
	
	$view->addExternalJsFile('https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js');
	$view->addExternalJsFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js');
	
	$view->addCssFile('admin_style.css');
	$view->addCssFile('ui/query-ui-customizations.css');

	$view->addJsFile('jquery.jqURL.js');
	
	$view->addJsFile('noty/jquery.noty.js');
	$view->addJsFile('noty/themes/default.js');
	$view->addJsFile('noty/layouts/topRight.js');

	$view->addJsFile('js_functions.js');
	// There were some problems with full 'cookie.js' name of this file, so now it was renamed to 'coo_kie.js'
	$view->addJsFile('jquery.coo_kie.js');
	// Backend Main Menu
	$view->addJsFile('jquery.treeview.js');
	$view->addJsFile('swfobject.js');
	$view->addJsFile('ui/jquery-ui.multidatespicker.js');

	$language_code = registry::get('current_language');
	if ($language_code && $language_code != 'en')
		$view->addJsFile('content_fields/i18n/jquery-ui-i18n.js');

	$view->addExternalJsFile('http://maps.google.com/maps/api/js?v=3.4&sensor=false&language=' . $language_code);
	
	$view->addCssFile('ui/jquery-ui-1.8.9.custom.css');
	$view->addCssFile('ui/query-ui-customizations.css');
}
?>