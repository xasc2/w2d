<?php
class autoTheme
{
	public $title = "Vechicles theme";
	public $version = "1.1";
	public $description = "Ideally for cars, boats, bikes, motorcycles, other classifieds";
	public $req_version = 3.2; 

	public $lang_files = "theme_auto.php";

	public function hooks()
	{
		$hook['auto_theme_frontend_addJsFiles'] = array(
			'exclusions' => array(
				'install/:any',
				'locations/ajax_autocomplete_request/',
				'refresh_captcha/',
				'locations/get_locations_path_by_id/',
				'login/(.*)',
				'admin/(.*)',
				'install/(.*)',
				'ajax/(.*)'
			),
		);
		
		$hook['auto_theme_backend_addJsFiles'] = array(
			'inclusions' => array(
				'login/(.*)',
				'admin/(.*)',
				'install/(.*)',
			),
			'exclusions' => array(
				'ajax/(.*)'
			),
		);
		
		return $hook;
	}
}
?>