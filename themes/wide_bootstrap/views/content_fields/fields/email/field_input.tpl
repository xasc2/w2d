			<div class="admin_option_name">
				{$field->name}{if $field->required}<span class="red_asterisk">*</span>{/if}
			</div>
			<div class="admin_option_description">
				{$field->description|nl2br}
			</div>
			<div class="input-prepend">
				<span class="add-on">@</span>
				<input type="text" name="field_{$field->seo_name}" value="{$value}" size="40" class="admin_option_input">
			</div>
			<br />
			<br />