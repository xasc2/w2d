<script language="JavaScript" type="text/javascript">
jQuery( function($) {ldelim}
	$("#search_form").submit( function() {ldelim}
		if ($("#{$currency_index}").val() != '' && ($("#{$from_index}").val() != '' || $("#{$to_index}").val() != '')) {ldelim}
			global_js_url = global_js_url + $("#{$currency_index}").attr('id') + '/' + $("#{$currency_index}").val() + '/';

			if ($("#{$from_index}").val() != '') 
				global_js_url = global_js_url + $("#{$from_index}").attr('id') + '/' + $("#{$from_index}").val() + '/';
			if ($("#{$to_index}").val() != '') 
				global_js_url = global_js_url + $("#{$to_index}").attr('id') + '/' + $("#{$to_index}").val() + '/';
		{rdelim}

		window.location.href = global_js_url;
		return false;
	{rdelim});
{rdelim});
</script>

							<div class="search_item search_field_type_{$field->type} search_field_{$field->seo_name}">
								<label>
								{if $field->field_icon_image && ($field->frontend_mode == 'icon' || $field->frontend_mode == 'both')}
									<img class="field_icon" src="{$users_content}/users_images/content_fields_icons/{$field->field_icon_image}" /> 
								{/if}
								{if $field->frontend_name}{$field->frontend_name}{else}{$field->name}{/if}
								</label>
								<div class="search_field_currency">
									{if $options|@count>1}
									<select id="{$currency_index}" name="{$currency_index}" style="min-width: 30px;">
										<option value="">{$LANG_SELECT_CURRENCY}</option>
										{foreach from=$options item=option}
										<option value="{$option.currency_code}" {if $option.currency_code == $args[$currency_index]}selected{/if}>{$option.currency_symbol}</option>
										{/foreach}
									</select>
									{else}
									{$options.0.currency_symbol}
									<input type="hidden" id="{$currency_index}" name="{$currency_index}" value="{$options.0.currency_code}" />
									{/if}
								</div>
								{if $options|@count>1}
								<div class="clear_float"></div>
								{/if}
								<div class="search_field_from">
	                     			<span>{$LANG_FROM}</span>
	                     			<input type="text" name="{$from_index}" id="{$from_index}" value="{$args[$from_index]}" size="5">
                     			</div>
                     			<div class="search_field_to">
                     				<span>{$LANG_TO}</span>
	                     			<input type="text" name="{$to_index}" id="{$to_index}" value="{$args[$to_index]}" size="5">
                     			</div>
                     			<div class="clear_float"></div>
                     		</div>