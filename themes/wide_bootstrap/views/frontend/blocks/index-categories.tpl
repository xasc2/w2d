
<!-- CATEGORIES BLOCK -->

				{if $categories_tree|@count}
					{include file='frontend/categories/normal.tpl' assign=template}
					<table width="100%" cellspacing="0">
					<tr>
						{assign var=td_padding_pixels value=15}
						{assign var=i value=0}
	
						<td valign="top" class="categories_columns">
							<div class="left_sidebar_categories">
							{section name=key loop=$categories_tree start=0 step=3}
								{$categories_tree[key]->render($template, $is_counter, $max_depth, $categories_tree[key]->seo_name)}
							{/section}
							</div>
						</td>
						
						<td valign="top" class="categories_columns">
							<div class="left_sidebar_categories">
							{section name=key loop=$categories_tree start=1 step=3}
								{$categories_tree[key]->render($template, $is_counter, $max_depth, $categories_tree[key]->seo_name)}
							{/section}
							</div>
						</td>
						
						<td valign="top" class="categories_columns">
							<div class="left_sidebar_categories">
							{section name=key loop=$categories_tree start=2 step=3}
								{$categories_tree[key]->render($template, $is_counter, $max_depth, $categories_tree[key]->seo_name)}
							{/section}
							</div>
						</td>
					</tr>
					</table>
				{/if}
	                
<!-- /CATEGORIES BLOCK -->
