{if $listings_array|@count}
	{assign var=i value=0}
	{foreach from=$listings_array item=listing}
		{assign var=i value=$i+1}
		{if $i == 1}
			<div class="index_types_listings">
			<table width="100%" cellspacing="0"><tr>
		{else}
			<td width="{$td_space_width}px"></td>
		{/if}
		<td width="{$td_width}px" class="listing_preview {if $listing->level->featured}featured{/if}" itemscope itemtype="http://schema.org/WebPage">{$listing->view('semitable')}</td>
		{if $i == $columns}
			</tr></table>
			</div>
			{assign var=i value=0}
		{/if}
	{/foreach}
	{if $i > 0}
		{section name="myLoop" start=$i loop=$columns-$i}
			<td width="{$td_space_width}px"></td>
			<td width="{$td_width+$td_padding_border}px"></td>
		{/section}
		</tr></table></div>
	{/if}
{/if}