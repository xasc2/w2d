{if (!$current_type) || ($current_type && $current_type->search_type != 'disabled')}
				<script language="javascript" type="text/javascript">
                // Global url string
                {if $base_url}
                	var global_js_url = '{$base_url}';
                {else}
                	var global_js_url = '{$VH->site_url("search/")}';
                {/if}
                
                // Command variable, needs for delete, block listings buttons
                var action_cmd;
                
                // Is advanced search in use? Attach to global_js_url
                var use_advanced = '';
                
                // Was advanced search block loaded?
                var advanced_loaded = false;

                // Scripts code array - will be evaluated when advanced search attached
                var scripts = new Array();
                
                var default_what = '{addslashes string=$LANG_SEARCH_WHAT} ({addslashes string=$LANG_SEARCH_WHAT_DESCR})';
                var default_where = '{addslashes string=$LANG_SEARCH_WHERE} ({addslashes string=$LANG_SEARCH_WHERE_DESCR})';
                
                var use_districts = {$system_settings.geocoded_locations_mode_districts};
                var use_provinces = {$system_settings.geocoded_locations_mode_provinces};

                var ajax_autocomplete_request = '{$VH->site_url("ajax/locations/autocomplete_request/")}';

                $(document).ready(function() {ldelim}
                	{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_where_search) || (!$system_settings.single_type_structure && $current_type && $current_type->where_search && $current_type->locations_enabled)}
                		{if $system_settings.predefined_locations_mode != 'only'}
	                		$('#where_search').autocomplete({ldelim}
				               	source: function(request, response) {ldelim}
				               		{if $system_settings.predefined_locations_mode != 'only'}
				               			{if $system_settings.predefined_locations_mode != 'disabled'}
				               				// geocode + ajax from DB
				               				geocodeAddress(request.term, response, ajax_autocomplete_request, false);
				               			{else}
				               				// only geocode
				               				geocodeAddress(request.term, response, false, false);
				               			{/if}
				               		{elseif $system_settings.predefined_locations_mode != 'disabled'}
				               			// only ajax from DB
				               			$.post(ajax_autocomplete_request, {ldelim}query: request.term{rdelim}, function(data) {ldelim}
					               			if (data = jQuery.parseJSON(data))
					               				response(data);
					               		{rdelim});
				               		{/if}
								{rdelim},
								focus: function(event, ui) {ldelim}
									$(this).val(ui.item.label);
									return false;
								{rdelim},
								select: function(event, ui) {ldelim}
									$(this).val(ui.item.label);
									if (ui.item.label != ui.item.value)
										$("#predefined_location_id").val(ui.item.value);
									return false;
								{rdelim},
								minLength: 2,
								delay: 600
							{rdelim});
						{/if}
					{/if}

					// Form submit event
					$("#search_form").submit( function() {ldelim}
						{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_what_search) || (!$system_settings.single_type_structure && $current_type && $current_type->what_search)}
						if ($("#what_search").val() != undefined && $("#what_search").val() != '' && $("#what_search").val() != default_what) {ldelim}
                			global_js_url = global_js_url + 'what_search/' + urlencode($("#what_search").val()) + '/';
                		{rdelim}
                		{/if}

                		{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_where_search) || (!$system_settings.single_type_structure && $current_type && $current_type->where_search && $current_type->locations_enabled)}
	                		{if $system_settings.predefined_locations_mode != 'only'}
		                		if ($("#where_search").val() != undefined && $("#where_search").val() != '' && $("#where_search").val() != default_where) {ldelim}
		                			global_js_url = global_js_url + 'where_search/' + urlencode($("#where_search").val()) + '/';
		                			if (parseInt($("#where_radius").val())>0) {ldelim}
		                				global_js_url = global_js_url + 'where_radius/' + $("#where_radius").val() + '/';
		                			{rdelim}
		                		{rdelim}
		                		if ($("#predefined_location_id").val() != undefined && $("#predefined_location_id").val() != '') {ldelim}
		                			global_js_url = global_js_url + 'predefined_location_id/' + urlencode($("#predefined_location_id").val()) + '/';
		                		{rdelim}
	                		{else}
		                		var predefined_location_id;
								$(".location_dropdown_list").each(function() {ldelim}
									if ($(this).find("option:selected").val() != '')
										predefined_location_id = $(this).find("option:selected").val();
								{rdelim});
								if (predefined_location_id) {ldelim}
		                			global_js_url = global_js_url + 'predefined_location_id/' + urlencode(predefined_location_id) + '/';
		                			if (parseInt($("#where_radius").val())>0) {ldelim}
		                				global_js_url = global_js_url + 'where_radius/' + $("#where_radius").val() + '/';
		                			{rdelim}
		                		{rdelim}
	                		{/if}
                		{/if}

                		{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_categories_search) || (!$system_settings.single_type_structure && $current_type && $current_type->categories_search)}
                    	if ($("#search_category").val() != undefined && $("#search_category").val() != '') {ldelim}
                    		global_js_url = global_js_url + 'search_category/' + $("#search_category").val() + '/';
                    	{rdelim}
                    	{/if}

                		global_js_url = global_js_url + use_advanced;
                		window.location.href = global_js_url;
						return false;
					{rdelim});

					{if $advanced_search_fields->fieldsCount() || $content_access_obj->isPermission('Manage all listings')}
					var advanced_opened = false;
					$(".advanced_search").bind('click', function() {ldelim}
						if (advanced_opened) {ldelim}
							$(".advanced_search img").attr('src', '{$public_path}images/icons/accept.png');
							$("#advanced_search_block").hide();
							advanced_opened = false;
							use_advanced = '';
						{rdelim} else {ldelim}
							$(".advanced_search img").attr('src', '{$public_path}images/icons/delete.png');
							$("#advanced_search_block").show();
							advanced_opened = true;

							if (!advanced_loaded) {ldelim}
								ajax_loader_show();
								$.post('{$VH->site_url("ajax/listings/build_advanced_search/")}', {ldelim}type_id: $("#search_type").val(), args: '{addslashes string=$VH->json_encode($args)}'{rdelim},
									function(data) {ldelim}
										$("#advanced_search_block").html(parseScript(data));
										evalScripts(data);
										advanced_loaded = true;
										ajax_loader_hide();
									{rdelim}
								);
							{rdelim}
							use_advanced = 'use_advanced/true/';
						{rdelim}
						return false;
					{rdelim});
					{if $args.use_advanced}
					$('.advanced_search').triggerHandler('click');
					{/if}
					{/if}
                {rdelim});
                </script>

                	 <div class="px5"></div>
                     <form id="search_form" action="" method="post">
                     	{if !$system_settings.single_type_structure}
                     	<input type="hidden" id="search_type" name="search_type" value="{$current_type->id}" />
                     	{else}
						<input type="hidden" id="search_type" name="search_type" value="0" />
						{/if}
                     	<div>
                     		<h5>{$LANG_FRONTEND_SEARCH_LISTINGS}{if $current_type->search_type == 'local'} {$LANG_IN} "{$current_type->name}"{/if}</h5>
                     		{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_what_search) || (!$system_settings.single_type_structure && $current_type && $current_type->what_search)}
                     		<div class="search_filter_standart">
                     			<input type="text" class="what_where_search_input what_search_input" name="what_search" id="what_search" value="{if $args.what_search}{$args.what_search}{/if}" placeholder="{$LANG_SEARCH_WHAT} ({$LANG_SEARCH_WHAT_DESCR})" />
                     		</div>
                     		{/if}

                     		{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_categories_search) || (!$system_settings.single_type_structure && $current_type && $current_type->categories_search)}
	                     	<div class="search_filter_standart">
	                     		{$LANG_FRONTEND_SEARCH_IN_CATEGORIES}
			                   	{render_frontend_block
									block_type='categories'
									block_template="frontend/blocks/categories_search_dropboxes.tpl"
									type=$current_type->id
									search_categories_array=$search_categories_array
									is_counter=false
									max_depth=1
									dropboxes_count=2
									no_cache=true
								}
							</div>
			                {/if}

                     		{if (($system_settings.single_type_structure || !$current_type) && $system_settings.global_where_search) || (!$system_settings.single_type_structure && $current_type && $current_type->where_search && $current_type->locations_enabled)}
                     		<div class="search_filter_standart">
                     			<div>
                     				{if $system_settings.predefined_locations_mode != 'only'}
	                     				<input type="text" class="what_where_search_input" name="where_search" id="where_search" value="{if $args.where_search}{$args.where_search}{elseif $current_location}{$current_location->getChainAsString()}{/if}" placeholder="{$LANG_SEARCH_WHERE} ({$LANG_SEARCH_WHERE_DESCR})" />
	                     				<input type="hidden" name="predefined_location_id" id="predefined_location_id" value="{if $args.predefined_location_id}{$args.predefined_location_id}{/if}" />
	                     			{else}
		                     			{$LANG_FRONTEND_SEARCH_IN_LOCATIONS}
										{if $args.predefined_location_id}
											{$VH->renderLocationDropBoxes($args.predefined_location_id)}
										{elseif $current_location}
											{$VH->renderLocationDropBoxes($current_location->id)}
										{else}
											{$VH->renderLocationDropBoxes()}
										{/if}
									{/if}

                     				<div class="px5"></div>
                     				{$LANG_SEARCH_IN_RADIUS}
                     				<select name="where_radius" id="where_radius" style="min-width: 40px;">
                     					<option value=0>0</option>
                     					<option value=1 {if $args.where_radius==1}selected{/if}>1</option>
                     					<option value=2 {if $args.where_radius==2}selected{/if}>2</option>
                     					<option value=3 {if $args.where_radius==3}selected{/if}>3</option>
                     					<option value=4 {if $args.where_radius==4}selected{/if}>4</option>
                     					<option value=5 {if $args.where_radius==5}selected{/if}>5</option>
                     					<option value=6 {if $args.where_radius==6}selected{/if}>6</option>
                     					<option value=7 {if $args.where_radius==7}selected{/if}>7</option>
                     					<option value=8 {if $args.where_radius==8}selected{/if}>8</option>
                     					<option value=9 {if $args.where_radius==9}selected{/if}>9</option>
                     					<option value=10 {if $args.where_radius==10}selected{/if}>10</option>
                     					<option value=15 {if $args.where_radius==15}selected{/if}>15</option>
                     					<option value=20 {if $args.where_radius==20}selected{/if}>20</option>
                     				</select>
									{if $system_settings.search_in_raduis_measure == 'miles'}
										{$LANG_SEARCH_IN_RADIUS_MILES}
									{else}
										{$LANG_SEARCH_IN_RADIUS_KILOMETRES}
									{/if}
	                     		</div>
                     		</div>
                     		{/if}
	                    </div>
	                     	
                     	<div class="search_block">
                     		{$search_fields->inputMode($args)}
                     	</div>
                     	{if $advanced_search_fields->fieldsCount() || $content_access_obj->isPermission('Manage all listings')}
                     	<div class="clear_float"></div>
                     	<div id="advanced_search_block" style="display: none;"></div>
                     	<div class="search_filter_standart">
	                     	<div id="advanced_search_button">
	                     		<a href="javascript: void(0);" class="toggle advanced_search"><img src="{$public_path}images/icons/accept.png"></a> <a href="javascript: void(0);" class="toggle advanced_search">{$LANG_ADVANCED_SEARCH}</a>
	                     	</div>
	                     </div>
	                     {/if}
                     	<div class="clear_float"></div>
                     	
                     	<div id="search_button">
                     		<input type="submit" name="submit" class="btn btn-info" value="{$LANG_BUTTON_SEARCH_LISTINGS}" />
                     	</div>
                     </form>
{/if}
<div class="px5"></div>