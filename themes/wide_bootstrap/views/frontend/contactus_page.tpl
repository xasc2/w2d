{include file="frontend/header.tpl"}

			<div class="row-fluid">
			 	<div class="span3" id="left_sidebar">
					{include file="frontend/left-sidebar.tpl"}
				</div>
	
				<div class="span6" id="center_part">
      				<div id="content_wrapper">
	      				<div class="content">
	      					{$VH->validation_errors()}
	                         <h2>{$LANG_CONTACT_US_TITLE}</h2>
	
	                         <form action="" method="post">
		                     <div class="admin_option noborder">
		                          <div class="admin_option_name" >
		                          	{$LANG_CONTACTUS_NAME}<span class="red_asterisk">*</span>
		                          </div>
		                          <input type=text name="name" value="{$name}" size="50" class="admin_option_input">
		                     </div>
		                     <div class="admin_option">
		                          <div class="admin_option_name">
		                          	{$LANG_CONTACTUS_EMAIL}<span class="red_asterisk">*</span>
		                          </div>
		                          <input type=text name="email" value="{$email}" size="50" class="admin_option_input">
		                     </div>
		                     <div class="admin_option noborder">
		                          <div class="admin_option_name" >
		                          	{$LANG_CONTACTUS_SUBJECT}<span class="red_asterisk">*</span>
		                          </div>
		                          <input type=text name="subject" value="{$subject}" size="50" class="admin_option_input">
		                     </div>
		                     <div class="admin_option">
		                          <div class="admin_option_name">
		                          	{$LANG_CONTACTUS_BODY}<span class="red_asterisk">*</span>
		                          </div>
		                          <textarea name="message" rows="10" cols="47" class="admin_option_input">{$message}</textarea>
		                     </div>
		                     {$content_fields->inputMode()}
		                     <div class="admin_option">
		                          <div class="admin_option_name">
		                          	{$LANG_FILL_CAPTCHA}<span class="red_asterisk">*</span>
		                          </div>
		                          <input type="text" name="captcha" size="4">
		                          <div class="px10"></div>
		                          {$captcha->view()}
							 </div>
							 <div class="px5"></div>
		                     <input class="btn btn-info" type=submit name="submit" value="{$LANG_CONTACTUS_SEND_BUTTON}">
		                     </form>
						</div>
					</div>
				</div>
                <div class="span3" id="right_sidebar">
					{include file="frontend/right-sidebar.tpl"}
				</div>
			</div>

{include file="frontend/footer.tpl"}